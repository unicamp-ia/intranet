<?php

$nivelcomp = -1;
// Alterado por Edson Giordani em Outubro/2019 - Indicacao de Gestor Substituto
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Fevereiro/2020 - Responsividade
include("../base/inicio.php"); /* Faz a verificacao se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
include("../base/email.php"); /* Funcao envia_email($assuntoemail, $mensagem, $remetente_email, $destinatario)*/
?>
<SCRIPT LANGUAGE="JavaScript">

    function openModal(pUrl, pWidth, pHeight,x,y) {

        if (window.showModalDialog) {
            return window.showModalDialog(pUrl, window,
                "dialogWidth:" + pWidth + "px;dialogHeight:" + pHeight + "px;dialogLeft:" + x + "px;dialogTop:" + y + "px;center:on");
        } else {
            try {
                netscape.security.PrivilegeManager.enablePrivilege(
                    "UniversalBrowserWrite");
                window.open(pUrl, "wndModal", "width=" + pWidth
                    + ",height=" + pHeight + ",resizable=no,modal=yes");
                return true;
            }
            catch (e) {
                alert("Script n\u00e3o confi\u00e1vel, n\u00e3o \u00e9 poss\u00edvel abrir janela modal.");
                return false;
            }
        }
    }

</script>
<?
$ArrStatus= array("A"=>"Aguardando Aprova&ccedil;&atilde;o","P"=>"Pendente para RH","T"=>"Aguardando Retorno","Z"=>"Aguardando Portaria","U"=>"Retornada ao Solicitante","F"=>"Finalizada","R"=>"Recusada","C"=>"Cancelada");
$browser=$_SESSION['browser'];
$cod_diretor=$_SESSION["cod_diretor"];
$busca = $_REQUEST['busca']; /*Codigo para nao perder o filtro na busca. */
$pagina=$_REQUEST['pagina'];
$num_linhas=$_REQUEST['num_linhas'];
if($busca) {
    $tipo = $_REQUEST['tipo'];
    $nome = $_REQUEST['nome'];
    $CodArea = $_REQUEST['CodArea'];
    $diainicad = $_REQUEST['diainicad'];
    $mesinicad = $_REQUEST['mesinicad'];
    $anoinicad = $_REQUEST['anoinicad'];
    $diafimcad = $_REQUEST['diafimcad'];
    $mesfimcad = $_REQUEST['mesfimcad'];
    $anofimcad = $_REQUEST['anofimcad'];
    $statusbusca=$_REQUEST['statusbusca'];
    $parametros = "&busca=".$busca."&tipo=".$tipo."&nome=".$nome."&CodArea=".$CodArea."&diainicad=".$diainicad."&mesinicad=".$mesinicad."&anoinicad=".$anoinicad."&diafimcad=".$diafimcad."&mesfimcad=".$mesfimcad."&anofimcad=".$anofimcad."&statusbusca=".$statusbusca;
}
$volta = $_REQUEST['volta'];
$op = $_REQUEST['op'];
$cod = $_REQUEST['cod'];
$statuslista = $_REQUEST['statuslista'];
$codsolicitante = $_REQUEST['codsolicitante'];
$codsolicitante=trim($codsolicitante);
$lotacao=$_REQUEST['departamento'];
$lotacao=trim($lotacao);
$coduser= $_REQUEST['coduser'];
$pecunia = $_REQUEST['pecunia'];
$adiantamento = $_REQUEST['adiantamento'];
$regime = $_REQUEST['regime'];
$tipo = $_REQUEST['tipo'];
$status = $_REQUEST['status'];
$total = $_REQUEST['total'];
$dia = $_REQUEST['dia'];
$mes = $_REQUEST['mes'];
$ano = $_REQUEST['ano'];
$ehchefe = $_REQUEST['ehchefe'];
$MATR_substituto = $_REQUEST['MATR_substituto'];
$nivel = $_SESSION["RH"];
$datainicio = $_REQUEST['datainicio'];
$eM1 = "<html><body>";
$eM2 = "</body></html>";
$assunto = "Sistema de Recursos Humanos";
$ola= "<br><br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
$att= "<br><br> Atenciosamente, Sistema Intranet - RH.";
$nivel = $_SESSION["RH"];
if(!$datainicio) {
    $datainicio =  date("Y-m-d H:i:s");
}
$timehoje=date("Y-m-d H:i:s");
$IP = $_SERVER['REMOTE_ADDR'];
$diainicio = $ano."-".$mes."-".$dia; // Lugar certo da variavel
$dataperiodo = $ano."/".$mes."/".$dia; // Lugar certo da variavel
$DataSistema=date("Y-m-d H:i") . ":00";
$DataHoje = date("Y-m-d");
if ($op=='1' || $op=='6') { // Se inclusao ou alteracao, verifica se estah dentro do prazo determinado pelo RH do IA
    $SQL="SELECT * from RHfruicao where fruicao_de <='$diainicio' and fruicao_ate >='$diainicio'";
    $res = mysql_query($SQL);
    if ($linha=mysql_fetch_array($res)) {
        $data_prazo=$linha['fruicao_prazo']. " 17:00:00";
        if (strtotime($DataSistema) > strtotime($data_prazo)) {
            ?>
            <SCRIPT LANGUAGE="JavaScript">
                var URL='./RHinforma.php?msg=O prazo para marca&ccedil;&atilde;o de f&eacute;rias para o per&iacute;odo requisitado[<b>in&iacute;cio em <? echo strftime("%d-%m-%Y", strtotime($dataperiodo)) . " por " . $total . " dias"; ?></b>] se encerrou em <b> <? echo  strftime("%d-%m-%Y &agrave;s %H:%M:%S", strtotime($data_prazo));?></b>.  Qualquer d&uacute;vida procure a RH.';
                var win = window.open(URL, 'Advertencia', 'left=200,top=200,height=250,width=500,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no');
                if(win == null || typeof(win) == 'undefined') {
                    alert('O prazo para marca\u00e7\u00e3o de f\u00e9rias para o per\u00edodo requisitado[in\u00edcio em <? echo strftime("%d-%m-%Y", strtotime($dataperiodo)) . " por " . $total . " dias"; ?>] se encerrou em <b> <? echo  strftime("%d-%m-%Y \u00e0s %H:%M:%S", strtotime($data_prazo));?></b>.  Qualquer d\u00favida procure a RH.');
                    alert("Aten\u00e7\u00e3o! O sistema Intranet utiliza janelas popups para notifica\u00e7\u00e3o e apresenta\u00e7\u00e3o de resultados. Verificamos que seu browser <?echo $browser;?> n\u00e3o est\u00e1 habilitado para apresenta\u00e7\u00e3o de janelas popups. Sugerimos que essa fun\u00e7\u00e3o seja habilitada em seu navegador.");
                }
            </script>
            <?
            die();
        }
    }
}
if($op=="1") {    //Inserir um nova solicitacao de ferias

    // Verifica se jah existe uma solicitacao aberta para o usuario no periodo, mas que nao esteja recusada ou cancelada
    $SQL="select s.cod,s.codsolicitante,s.tipo,s.status, s.datacadastro,date_format(s.datacadastro, '%d/%m/%Y' ) as dtc, f.datainicio, date_format(f.datainicio, '%d/%m/%Y' ) as dti, f.total, f.MATR_substituto from RHsolicitacoes s left join RHferias f on s.cod=f.cod where s.codsolicitante='$codsolicitante' and f.datainicio ='$diainicio' and f.total='$total' and s.status <> 'R' and s.status <> 'C'";
    $res=mysql_query($SQL);
    if (mysql_num_rows($res) > 0) {
        $linha=mysql_fetch_array($res);
        $dtcadastro=$linha['dtc'];
        $sta=$linha['status'];
        $stadescr=$ArrStatus[$sta];
        $dtinicio= $linha['dti'];
        $totaldias=$linha['total'];
        ?>
        <SCRIPT LANGUAGE="JavaScript">
            var URL='./RHinforma.php?msg=Encontrado no sistema uma solicita&ccedil;&atilde;o de f&eacute;rias com o status <font color=red><b><? echo  $stadescr; ?></b></font>, cadastrada em <? echo $dtcadastro; ?> para in&iacute;cio em <? echo $dtinicio; ?>, por <? echo $totaldias; ?> dias. Por favor, utilize a op&ccedil;&atilde;o <b>Minhas Solicita&ccedil;&otilde;es</b> do menu para visualiz&aacute;-la. Qualquer d&uacute;vida procure a RH.';
            var win = window.open(URL, 'Advertencia', 'left=200,top=200,height=250,width=500,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no');
            if(win == null || typeof(win) == "undefined" || win.location.href == 'about:blank') {
                alert('Encontrado no sistema uma solicita\u00e7\u00e3o de f\u00e9rias com o status <? echo  $stadescr; ?>, cadastrada em <? echo $dtcadastro; ?> para in\u00edcio em <? echo $dtinicio; ?>, por <? echo $totaldias; ?> dias. Por favor, utilize a op\u00e7\u00e3o Minhas Solicita\u00e7\u00f5es do menu para visualiz\u00e1-la. Qualquer d\u00favida procure a RH.');
                alert("Aten\u00e7\u00e3o! O sistema Intranet utiliza janelas popups para notifica\u00e7\u00e3o e apresenta\u00e7\u00e3o de resultados. Verificamos que seu browser <?echo $browser;?> n\u00e3o est\u00e1 habilitado para apresenta\u00e7\u00e3o de janelas popups. Sugerimos que essa fun\u00e7\u00e3o seja habilitada em seu navegador.");
            }
        </script>
        <?
        die();
    } else {
        // Verifica se o solicitante eh o diretor do IA. Se for, o status passa automaticamente a Pendente para RH
        if ($codarea_conected == $cod_diretoria && $coduser_conected==$_SESSION['coduser_chefe']) {
            $STA="P";
        } else {
            include("busca_aprovador.php");
            $STA="A";
        }
        $SQL = "Insert into RHsolicitacoes (codsolicitante, coduser, codaprovador, lotacao, status, tipo, dataaltera, datacadastro,ip_solicitacao) values ('$codsolicitante', '$coduser', '$codaprovador', '$lotacao', '$STA', '$tipo', '$datainicio','$DataHoje','$IP')";
        $res = mysql_query("$SQL");
        if (!$res) {
            envia_email("[exibeferias] Erro ao cadastrar o pedido. Entre em contato com o administrador. 1", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
            die("[exibeferias] Erro ao cadastrar o pedido. Entre em contato com o administrador. 1");
        }
        $S= addslashes($SQL);
        $SQL = "Select cod from RHsolicitacoes where dataaltera='$datainicio' and tipo='$tipo' and codsolicitante='$codsolicitante'";
        $res = mysql_query("$SQL");
        $emailsolicitante=$_SESSION["emailuser_conected"];
        if($linha=mysql_fetch_array($res)) {
            $cod = $linha['cod'];
            if ($ehchefe=="sim") {
                $SQL = "Insert into RHferias (cod, total, datainicio, pecunia, adiantamento,MATR_substituto) values ('$cod', '$total','$diainicio', '$pecunia', '$adiantamento',$MATR_substituto)";
            } else {
                $SQL = "Insert into RHferias (cod, total, datainicio, pecunia, adiantamento) values ('$cod', '$total','$diainicio', '$pecunia', '$adiantamento')";
            }
            $res = mysql_query("$SQL");
            if (!$res) {
                envia_email("[exibeferias] Erro ao cadastrar o pedido. Entre em contato com o administrador. 2", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
                die("[exibeferias] Erro ao cadastrar o pedido. Entre em contato com o administrador. 2");
            }

            $SQL = "Select * from RHsolicitacoes where cod='$cod'";
            $res = mysql_query("$SQL");
            if($linha=mysql_fetch_array($res)) {
                $tipo = $linha['tipo'];
            }
            $desctipo="f&eacute;rias";
            $dt=strftime("%d/%m/%Y", strtotime($dataperiodo));
            // Envia mensagem ao aprovador (chefe do departamento/secao ou chefe imediato, caso o solicitante nao seja o diretor
            if ($STA=="A") {
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                    $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                    $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                    $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .= "<br>Foi cadastrada pelo colaborador(a) " . $_SESSION['nomeuser_conected'] . ", uma solicita&ccedil;&atilde;o de ".$desctipo." para in&iacute;cio em $dt, por $total dias.";
                $mensagem .= "<br>Por favor, aprovar ou recusar o pedido acessando link abaixo.<br>";
                $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibeferias.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
                envia_email($assunto, $eM1.$mensagem. $att . $ola . $eM2, $email_notif);

                $emailros="intra.ia@iar.unicamp.br";
                $mensagem .="<br> Aprovador(es):" . $email_notif;
                // envia_email("Notificacao RH - Ferias - Aprovador", $eM1 . $mensagem. $att . $ola . $eM2, $emailros);

                $mensagem = "<br>Caro(a) " . $_SESSION['nomeuser_conected'] . ", <br>Sua solicita&ccedil;&atilde;o de f&eacute;rias foi encaminhada para aprova&ccedil;&atilde;o de sua chefia.";
                $mensagem .="<br>O(a) respons&aacute;vel pela aprova&ccedil;&atilde;o de sua solicita&ccedil;&atilde;o &eacute; " . $nomeaprovador;
                if ($codsubstituto <> "") {
                    $mensagem .=" ou o(a) substituto(a) indicado(a), sr(a) " . $nomesubstituto;
                }
                $mensagem .=  " . Por favor, aguarde. ";
                envia_email($assunto, $eM1 . $mensagem. $att . $ola . $eM2, $emailsolicitante);
            } else {
                // O solicitante eh o diretor do IA.
                // Busca os e-mails dos pessoal do RH
                $SQL="select u.nome, u.email, u.coduser,s.idsistema, s.permissao from tblusuarios u left join tblususist s on u.coduser=s.coduser where s.idsistema=19 and permissao=3";
                $res = mysql_query("$SQL");
                $ll=0;
                $emailRH="";
                while($linha3=mysql_fetch_array($res)) {
                    $ll=$ll+1;
                    if ($ll>1) {
                        $emailRH .= ",";
                    }
                    $emailRH .= $linha3['email'];
                }
                $mensagem = "O(a) diretor(a) do Instituto de Artes, " . $_SESSION['nomeuser_conected'] . " solicita f&acute;rias para in&iacute;cio em $dt, por $total dias.<br>";
                $mensagem .= "O n&uacute;mero da solicita&ccedil;&atilde;o &eacute; $cod e o status foi alterado para <b>Pendente para RH<b>. Por favor, providenciar of&iacute;cio comunicando o Reitor da Unicamp.";
                envia_email($assunto, $eM1. $mensagem . $stt . $ola . $eM2, $emailRH);
            }
            $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Inser&ccedil;&atilde;o de registro - Solicita&ccedil;&atilde;o de f&eacute;rias $cod', '$S','$timehoje')";
            $res=mysql_query($SQLlog) or die("R[exibeferias] H: erro ao inserir LOG 1");

        } else {
            ?>
            <SCRIPT LANGUAGE="JavaScript">
                alert('Erro na rotina de inser\u00e7\u00e3o do registro de solicita\u00e7\u00e3o de f\u00e9rias. Por favor, comunique este erro ao RH.');
            </script>
            <?
            die();
        }
    }
}
if($op=="3") { // Usuario RH alterou o status da solicitacao, mantendo o Status de Pendente ou Finalizada ou o solicitante cancelou seu proprio pedido.
    $SQL = "Select s.tipo, s.dataaltera, s.codsolicitante, u.usuario, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod'";
    $res = mysql_query("$SQL");
    if(mysql_num_rows($res) > 0) {
        $linha = mysql_fetch_array($res);
        $codsolicitante=$linha['codsolicitante'];
        $emailsolicitante=$linha['email'];
        $nomesolicitante=$linha['nome'];
        $SQL = "Update RHsolicitacoes set dataaltera = '$datainicio', codatendente='$coduser_conected', status='$status' where cod='$cod'";
        $S= addslashes($SQL);
        $res = mysql_query("$SQL") or die("[exibeferias] Erro ao cadastrar o pedido. Entre em contato com o administrador. 8");
        if ($status == "F") {
            $mensagem = "Caro(a) $nomesolicitante .<br>Sua solicita&ccedil;&atilde;o de f&eacute;rias n&uacute;mero $cod foi finalizada pelo RH do IA.";
            $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o";
            $mensagem .= "<br><br>Atenciosamente,<br> $nomeuser_conected <br> RH";
            envia_email($assunto, $eM1.$mensagem . $ola . $eM2, $emailsolicitante);
            $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Finalizando - Solicita&ccedil;&atilde;o de f&eacute;rias $cod', '$S','$timehoje')";

            $res=mysql_query($SQLlog) or die("[exibeferias] RH: erro ao inserir LOG - F&eacute;rias 4");
        }
        if ($status == "C") {
            if ($codsolicitante <> $coduser_conected) {
                $mensagem = "Caro(a) $nomesolicitante .<br>Sua solicita&ccedil;&atilde;o de f&eacute;rias n&uacute;mero $cod foi cancelada pelo RH do IA.";
                $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o.";
                $mensagem .= "<br><br>Atenciosamente,<br> $nomeuser_conected <br> RH";
            } else {
                $mensagem = "Caro(a) $nomesolicitante .<br>Sua solicita&ccedil;&atilde;o de f&eacute;rias n&uacute;mero $cod foi por voc&ecirc; cancelada.";
                $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta RH.";
                $mensagem .= "<br><br>Intranet/IA";
            }
            envia_email($assunto, $eM1 . $mensagem  . $ola . $eM2, $emailsolicitante);
            $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Cancelando - Solicita&ccedil;&atilde;o de f&eacute;rias $cod', '$S','$timehoje')";

            $res=mysql_query($SQLlog) or die("[exibeferias] RH: erro ao inserir LOG - F&eacute;rias 5");
        }
    } else {
        die("Algu&eacute;m estava alterando o pedido. N&atilde;o foi poss&iacute;vel realizar sua altera&ccedil;&atilde;o, tente novamente.");
    }
}
// Efetiva um alteracao feita pelo Solicitante ou pelo RH
if($op=="4" || $op=="6") {
    // A lotacao serah alterada somente se o usuario que estiver manipulando a solicitacao for o RH
    $SQL = "Update RHsolicitacoes set lotacao='$lotacao', codsolicitante='$coduser', coduser='$coduser', dataaltera = '$datainicio', status='$status' where cod='$cod'";
    $res = mysql_query("$SQL") or die("[exibeferias] Erro ao cadastrar o pedido. Entre em contato com o administrador. 7");
    if ($ehchefe=="sim") {
        $SQL = "Update RHferias set total='$total', datainicio='$diainicio', adiantamento='$adiantamento', pecunia='$pecunia', MATR_substituto=$MATR_substituto where cod='$cod'";
    } else {
        $SQL = "Update RHferias set total='$total', datainicio='$diainicio', adiantamento='$adiantamento', pecunia='$pecunia' where cod='$cod'";
    }
    $res = mysql_query("$SQL") or die("[exibeferias] Erro ao cadastrar o pedido. Entre em contato com o administrador. 2 ");

    if($status=="U") {
        echo "[exibeferias] Problema na execu&ccedil;&atilde;o do fluxo de dados. Comunique este erro &agrave; DTI 8";
        die();
        $SQL = "Select u.usuario,u.email, s.permissao from tblusuarios u left join RHsolicitacoes r on u.coduser=r.codsolicitante left join tblususist s on u.coduser=s.coduser and s.idsistema='19' where r.cod='$cod'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $email = $linha['email'];
            $permissao = $linha['permissao'];
            $assunto = "Sistema de Recursos Humanos";
            $mensagem = "<html>";
            $mensagem .= "<body>";
            $mensagem .= "Ol&aacute; .<br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
            $SQL = "Select * from RHsolicitacoes where cod='$cod'";
            $res = mysql_query("$SQL");
            if($linha=mysql_fetch_array($res)) {
                $tipo = $linha['tipo'];
            }
            if($tipo=="F") { $desctipo="f&eacute;rias"; } else if($tipo=="L") { $desctipo="licen&ccedil;a pr&ecirc;mio"; } else if($tipo=="A") { $desctipo="afastamento"; }
            $mensagem .= "Foi solicitada a altera&ccedil;&atilde;o de um pedido de ".$desctipo." cadastrado por voc&ecirc;.";
            $mensagem .= "<br>Voc&ecirc; deve alterar o pedido no link abaixo.<br>";
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitaoutroferias.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";

            envia_email($assunto, $eM1 . $mensagem . $att . $ola . $eM2, $email);
        }
    } else if($status=="A") {
        /*  Verifica quem eh o aprovador para a area em que o usuario estah lotado. Se o usuario eh chefe da area, entao o aprovador eh o chefe imediato
            Retorna a variavel $codaprovador e $nomeaprovador */
        include("busca_aprovador.php");
        $SQL = "Select s.tipo, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod'";
        $res = mysql_query("$SQL");
        $S= addslashes($SQL);
        if($linha=mysql_fetch_array($res)) {
            $tipo = $linha['tipo'];
            $emailsolicitante=$linha['email'];
            $nomesolicitante=$linha['nome'];
        }
        if($tipo=="F") { $desctipo="f&eacute;rias"; } else if($tipo=="L") { $desctipo="licen&ccedil;a pr&ecirc;mio"; } else if($tipo=="A") { $desctipo="afastamento"; }
        $email_notif=$emailaprovador;
        if ($codsubstituto <> "") {
            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
            $email_notif=$email_notif . "," . $emailsubstituto;
        } else {
            $mensagem = "Caro(a) $nomeaprovador,";
        }
        $mensagem .= "<br>Foi alterada uma solicita&ccedil;&atilde;o de ".$desctipo." que necessita de sua aprova&ccedil;&atilde;o.";
        $mensagem .= "<br>Por favor, acesse o link abaixo para aprovar ou recusar o pedido.<br>";
        $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibeferias.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
        envia_email($assunto, $eM1.$mensagem  . $att . $ola . $eM2, $email_notif);
        // Notifica o usuario
        $mensagem = "Caro(a) $nomesolicitante ";
        $mensagem .= "<br>Sua solicita&ccedil;&atilde;o de " . $desctipo . " foi alterada e ser&aacute; submetida &agrave; aprova&ccedil;&atilde;o do Chefe de Departamento/Se&ccedil;&atilde;o,";
        $mensagem .= " Sr(a) " . $nomeaprovador;
        if ($codsubstituto <> "") {
            $mensagem .=" ou ao(&agrave;) substituto(a) indicado(a), " . $nomesubstituto;
        }
        $mensagem .=  ". Por favor, aguarde. ";
        $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o.";
        envia_email($assunto, $eM1.$mensagem  . $att . $ola . $eM2, $emailsolicitante);

        $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Alterando - Solicita&ccedil;&atilde;o de f&eacute;rias $cod', '$S','$timehoje')";
        $res=mysql_query($SQLlog) or die("[exibeferias] RH: erro ao inserir LOG - F&eacute;rias 6");
    }
}
$EoChefe=false;
$EoSubstituto=false;
$Echefe=false;
$EaprovadorIndicado=false;
//Verifica a posicao do usuario $coduser_conected
include ("VerifiquePosicao.php");
if($cod) {
    $SQL="select s.*, u.nome,ap.nome as nomeaprovador from RHsolicitacoes s left join tblusuarios u on s.coduser=u.coduser left join tblusuarios ap on s.codaprovador=ap.coduser  where s.cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $status = $linha['status'];
    $obs = $linha['obs'];
    $coduser = $linha['coduser'];
    $codaprovador= $linha['codaprovador'];
    $nomeaprovador= $linha['nomeaprovador'];
    $codsolicitante= $linha['codsolicitante'];
    $codatendente= $linha['codatendente'];
    $nome = $linha['nome'];
    $lotacao = $linha['lotacao'];

    $SQL = "select *, date_format(datainicio, '%d/%m/%Y') AS datainicio1 from RHferias where cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $total = $linha['total'];
    $pecunia = $linha['pecunia'];
    $adiantamento = $linha['adiantamento'];
    $datainicio1 = $linha['datainicio1'];
    $MATR_substituto = $linha['MATR_substituto'];
    //Se o usuario nao for chefe nem substituto, entao verifica se o usuario eh o aprovador desta solicitacao
    if (!$EoChefe && !$Echefe && !$EoSubstituto && $codaprovador && $codaprovador==$coduser_conected) {
        $EaprovadorIndicado=true;
    }
}
//Quem pode aprovar, caso a solicitacao ainda esteja com status "Aguardando aprovacao"?
$PodeAprovar=false;
if ($status=="A") {
    if ($coduser_conected==$codaprovador) {
        $PodeAprovar=true;
    }
    if ($EoSubstituto && is_array($aprovadores_substituto) && count($aprovadores_substituto)>0 ) {
        if (in_array($codaprovador,$aprovadores_substituto)) {
            $PodeAprovar=true;
        }
    }
}
// Busca o aprovador substituto para a area de lotacao do solicitante, caso ele exista
// Monta a hierarquia para a area base ($lotacao) e acima da area base
$nomesubstituto="";
MontaHierarquia($lotacao,"S","S","N");
// Se aprovador
if ($hierarquia['chefia']['matr'] == $codaprovador || $hierarquia['acima']['matr']==$codaprovador || $hierarquia['ATU']['matr']==$codaprovador || $hierarquia['DIR']['matr']==$codaprovador) {
    if ($hierarquia['chefia']['matr'] !=  $coduser) {
        if ($hierarquia['substituto_chefia']['matr']==$coduser) { // solicitante eh substituto no departamento dele
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        } else {
            if ($hierarquia['chefia']['matr'] == $codaprovador) {
                if ($hierarquia['substituto_chefia']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_chefia']['nome']; }
            } elseif ($hierarquia['acima']['matr']==$codaprovador) {
                if ($hierarquia['substituto_acima']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_acima']['nome']; }
            } elseif ($hierarquia['ATU']['matr']==$codaprovador) {
                if ($hierarquia['substituto_atu']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_atu']['nome']; }
            } else {
                if ($hierarquia['substituto_dir']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_dir']['nome']; }
            }
        }
    } else {
        if ($hierarquia['substituto_acima']['matr'] && $hierarquia['substituto_acima']['matr'] !=  $coduser) {
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        }
    }
}
?>
<SCRIPT LANGUAGE="JavaScript">

    function salvar () {
        if(document.formulario.status.value=='U') {
            document.location='retornasolicitacoes.php?cod=<? print $cod; ?>&datainicio=<? print $datainicio; ?>&volta=<? print $volta; ?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';
        } else {
            document.location='exibeferias.php?cod=<? print $cod; ?>&op=3&datainicio=<? print $datainicio; ?>&volta=<? print $volta;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status='+document.formulario.status.value;
        }
    }
    function aceita () {
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=P&op=1';
    }
    function recusa () {
        if(document.formulario.obs.value=="") {
            alert('Preencha uma justificativa para a recusa no campo observa\u00e7\u00e3o!');
            document.formulario.obs.focus();
            return false;
        }
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=R&op=1&obs='+document.formulario.obs.value;
    }
    function altproc() {
        document.location='altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1';
    }
</SCRIPT>
<?
include("menu.php");
?>

<form name="formulario">

    <? if($PodeAprovar) { ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 bg-light">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Observa&ccedil;&atilde;o (pertinente à aprovação/recusa do pedido)</label>
                        <textarea class="form-control" name="obs" id="obs" rows="3" readonly><? print $obs; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 bg-light">
                <div class="form-group">
                    <label for="exampleFormControlInput1"><b>Solicita&ccedil;&atilde;o de f&eacute;rias</b></label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Matr&iacute;cula:&nbsp;<? print $coduser; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Nome:&nbsp;<? print $nome; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Lota&ccedil;&atilde;o:&nbsp;
                        <?
                        $SQL ="SELECT * FROM areas where CodArea='$lotacao' and origem='I'";
                        $resultado = mysql_query($SQL);
                        if ($row1=mysql_fetch_array($resultado)) { ?>
                            <? print $row1['Descricao']; ?>
                        <? } ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Inicio das f&eacute;rias:&nbsp;<? print $datainicio1; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Total de dias:&nbsp;<? print $total; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Pec&uacute;nia:&nbsp;<? if($pecunia=="S") { ?>Sim<? } else { ?>N&atilde;o<? } ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Adiantamento 13&ordm; sal&aacute;rio:&nbsp;<? if($adiantamento=="S") { ?>Sim<? } else { ?>N&atilde;o<? } ?>
                    </label>
                </div>

                <? if (($ehchefe=="sim")||($MATR_substituto!=0)) { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">
                            Substituto(a):&nbsp;
                            <?
                            $SQL = "Select Nome from tblusuarios where coduser=$MATR_substituto";
                            $res = mysql_query("$SQL");
                            if($linha=mysql_fetch_array($res)) {
                                $nomechefesubstituto=$linha['Nome'];
                            }
                            print $nomechefesubstituto; ?>
                        </label>
                    </div>
                <? } ?>

                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Aprovador(a):&nbsp;<? print $nomeaprovador;
                        if ($nomesubstituto) echo " <b><font color=green>ou o(a) substituto(a) indicado(a) pelo RH, " .  $nomesubstituto . "</font></b>";
                        ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Status:&nbsp;</label>
                    <? if($nivel ==3 && ($status=="P" || $status=="T")) { ?>
                        <select class="form-control" name="status">
                            <option value="P" <? if($status=="P") { print "selected"; }?>>Pendente para o RH</option>
                            <option value="T" <? if($status=="T") { print "selected"; }?>>Aguardando retorno</option>
                            <option value="U" <? if($status=="U") { print "selected"; }?>>Retornar ao solicitante</option>
                            <option value="F" <? if($status=="F") { print "selected"; }?>>Finalizada</option>
                        </select>
                    <? } else { ?>
                        <label for="exampleFormControlInput1">
                            <? if($status=="F") { ?>
                                <font class="fonte1">Finalizada</font>
                            <? } else if($status=="A") { ?>
                                <font class="fonte1">Aguardando aprova&ccedil;&atilde;o do chefe</font>
                            <? } else if($status=="P") { ?>
                                <font class="fonte1">Pendente para o RH</font>
                            <? } else if($status=="T") { ?>
                                <font class="fonte1">Aguardando retorno</font>
                            <? } else if($status=="U") { ?>
                                <font class="fonte1">Retornado ao solicitante</font>
                            <? } else if($status=="R") { ?>
                                <font class="fonte1">Recusada pelo chefe</font>
                            <?} else if($status=="C") { ?>
                                <font class="fonte1">Cancelada</font>
                            <? } ?>
                        </label>
                    <? } ?>
                </div>
                <? 	if ($op!="1") { ?>
                    <? if($nivel ==3 && ($status=="P" || $status=="T")) { ?>
                        <input type="button" class="btn btn-primary" name="Salvar" value="Salvar" onclick="salvar();">
                    <? }
                    //  Quem pode aprovar
                    if($PodeAprovar) { ?>
                        <input type="button" class="btn btn-primary" name="Aceitar" value="Aceitar" onclick="aceita();">
                        <input type="button" class="btn btn-primary" name="Recusar" value="Recusar" onclick="recusa();">
                    <? }
                    // quem pode alterar o aprovador eh somente o gestor de RH
                    if($status=="A" && $nivel == 3 && $codsolicitante != $cod_diretor) {?>
                        <!--input type="button" name="Aprovacao" value="Alterar aprovador" onclick="altproc();"!-->
                        <input type="button" class="btn btn-primary" name="Aprovacao" value="Alterar aprovador" onclick="window.open('altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1',null,'left=120,top=300,height=220,width=780,status=yes,toolbar=no,menubar=no,location=no');">
                    <?}
                    //Se o usuario eh o solicitante entao ele pode cancelar a sua propria solicitacao
                    $btn_cancelar=false;
                    $btn_alterar=false;
                    // Se o usuario eh o gestor de RH e a sol nao tenha sido finaliza (F), nao cancelada (C) ou
                    // nao recusada pelo chefe (R), entao o gestor pode altera-la ou cancela-la
                    if(($nivel == 3) && $status <> "F" && $status <> "C" && $status <> "R") {
                        $btn_cancelar=true;
                        $btn_alterar=true;
                    }

                    if(($codsolicitante==$coduser_conected || $coduser==$coduser_conected) && ($status == "A" || $status == "U" || $status=="P" || $status == "T") ) {
                        $btn_cancelar=true;
                        if ($status == "A" || $status == "U") {
                            $btn_alterar=true;
                        }
                    }
                    if ($btn_alterar) { ?>
                        <input type="button" class="btn btn-primary" name="Alterar" value="Alterar Solicita&ccedil;&atilde;o" onclick="document.location='solicitaoutroferias.php?cod=<? print $cod; ?>&op=6&volta=<? print $volta;?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';">
                    <?}
                    if ($btn_cancelar) { ?>
                        <input type="button" class="btn btn-primary" name="Cancelar" value="Cancelar Solicita&ccedil;&atilde;o" onclick="if(confirm('Tem certeza que deseja cancelar esta solicita&ccedil;&atilde;o?')) { document.location='exibeferias.php?cod=<? print $cod;?>&op=3&datainicio=<? print $datainicio; ?>&status=C&volta=<? print $volta;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';}">
                    <?}?>
                    <input type="button" class="btn btn-primary" name="Voltar" value="Voltar" onclick="document.location='<? print $volta; ?>?pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?><?echo $parametros;?>';">
                <? } else {?>
                    <font size="3" color="green">Solicita&ccedil;&atilde;o criada. Aguarde aprova&ccedil;&atilde;o pela chefia indicada.</font>
                <? } ?>
            </div>
        </div>
    </div>
</form>
</body>
</html>
