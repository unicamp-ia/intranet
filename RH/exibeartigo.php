<?php
/*
    Significados do campo STATUS
    F = Finalizada
    A = Aguardando aprovacao do chefe
    P = Pendente para o RH
    U = Retornado ao solicitante
    R = Recusada pelo chefe
*/
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Fevereiro/2020 - Responsividade

function dtSubtrair($data,$dias) {
    $data = str_replace("-","",$data);
    $ano = substr ( $data, 0, 4 );
    $mes = substr ( $data, 4, 2 );
    $dia = substr ( $data, 6, 2 );
    $novaData = mktime ( 0, 0, 0, $mes, $dia - $dias, $ano );
    return strftime("%Y-%m-%d", $novaData);
}
function ChecaPrecedencia($dtartigo,$dias_precedencia) {
    global $TblResAnt;
    $TblResAnt="";
    $dias_precedencia +=0;
    $d_aux=date('Y-m-d');
    $na=0;
    //Loop ateh achar a quantidade de dias uteis chegar a no maximo 4 ou ateh a data de saida: o que ocorrer primeiro.

    while($na < 4 && $d_aux > $dtartigo) {
        $vectdata = explode("-",$d_aux);
        $dia_a=$vectdata[2]+0;
        $mes_a=$vectdata[1]+0;
        $ano_a=$vectdata[0]+0;
        $diasemana = date("w", mktime(0,0,0,$mes_a,$dia_a,$ano_a)) + 0;
        $ht="";
        $dia_ok=true;
        $img="s_success.png";

        if ($diasemana == 0 || $diasemana == 6) {
            $img="s_error.png";
            if ($diasemana == 6) {
                $ht =" S&aacute;bado : n&atilde;o h&aacute; expediente";
            } else {
                $ht =" Domingo : n&atilde;o h&aacute; expediente";
            }
            $dia_ok=false;
        }
        $SQL="select cal_descricao from calendario_adm where cal_dia=$dia_a and cal_mes=$mes_a and cal_ano=$ano_a";
        $res=mysql_query($SQL);
        if ($ln=mysql_fetch_array($res)) {
            $ht .= "[" . $ln['cal_descricao'] . "]";
            $dia_ok=false;
            $img="s_error.png";
        }
        if ($dia_ok) {
            $ht="dia &uacute;til";
            $na++;
        }

        $TblResAnt .="<tr><td><b><font color=\"blue\">$dia_a/$mes_a/$ano_a</font></b></td><td>$ht</td><td><img src=\"../imagens/$img\"</tr>";
        $dd=dtSubtrair($d_aux,1);
        $d_aux=$dd;
    }
    return $na;
}


$nivelcomp = -1;
include("../base/inicio.php"); /* verifica se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
include("../base/email.php"); /* Funcao envia_email($assuntoemail, $mensagem, $remetente_email, $destinatario)*/

$busca = $_REQUEST['busca'];/* Codigo para nao perder o filtro na busca. */
$volta = $_REQUEST['volta'];
$pagina=$_REQUEST['pagina'];
$num_linhas=$_REQUEST['num_linhas'];
if($busca) {
    $tipo = $_REQUEST['tipo'];
    $nome = $_REQUEST['nome'];
    $CodArea = $_REQUEST['CodArea'];
    $diainicad = $_REQUEST['diainicad'];
    $mesinicad = $_REQUEST['mesinicad'];
    $anoinicad = $_REQUEST['anoinicad'];
    $diafimcad = $_REQUEST['diafimcad'];
    $mesfimcad = $_REQUEST['mesfimcad'];
    $anofimcad = $_REQUEST['anofimcad'];
    $statusbusca=$_REQUEST['statusbusca'];
    $parametros = "&busca=".$busca."&tipo=".$tipo."&nome=".$nome."&CodArea=".$CodArea."&diainicad=".$diainicad."&mesinicad=".$mesinicad."&anoinicad=".$anoinicad."&diafimcad=".$diafimcad."&mesfimcad=".$mesfimcad."&anofimcad=".$anofimcad."&statusbusca=".$statusbusca;
}
$datadehoje=date('Y-m-d H:i:s');
$op = $_REQUEST['op'];
$cod = $_REQUEST['cod'];
$codsolicitante = $_REQUEST['codsolicitante'];
$lotacao = $_REQUEST['lotacao'];
$coduser = $_REQUEST['coduser'];
$tipo = $_REQUEST['tipo'];
$status = $_REQUEST['status'];
$dia = $_REQUEST['dia'];
$mes = $_REQUEST['mes'];
$ano = $_REQUEST['ano'];
$codigofalta=$_REQUEST['codigofalta'];
$justificativa=$_REQUEST['justificativa'];
$obs=$_REQUEST['obs'];
$msgE1 = "<html><body>";
$msgE2 = "</body></html>";
$assunto = "Sistema de Recursos Humanos";
$NaoResponda = "<br>Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
$timehoje=date("Y-m-d H:i:s");
$IP = $_SERVER['REMOTE_ADDR'];
$cod_diretor=$_SESSION["cod_diretor"];
if($op=="1") { // Inserindo uma Solicitacao de artigo 31
    $datafalta = $ano."-".$mes."-".$dia;
    $dt=strftime("%d/%m/%Y", strtotime($datafalta));

    // Numero de dias de precedencia(atraso) informado pelo RH do IA.
    $ndp=1;

    $SQL="select * from parametros";
    $res=mysql_query($SQL);
    if ($ln=mysql_fetch_array($res)) {
        $ndp=$ln['atraso_abonada'];
    }
    $ndb=ChecaPrecedencia($datafalta,1);

    if ($ndb > $ndp){
        // Nao aceita pedido de afastamento pois nao atende a precedencia(atraso) de $ndp dias uteis
        ?>
        <table width="30%" align="center"><tr><td colspan="3">
                    <b>Caro(a) <?echo $_SESSION["nomeuser_conected"];?></b>,<br>
                    Sua Solicita&ccedil;&atilde;o de falta abonada n&atilde;o pode ser prosseguida por n&atilde;o atender ao prazo de preced&ecirc;ncia(atraso) de <?echo $ndp;?>
                    dia &uacute;til estabelecido pela DGRH/Unicamp. Por favor, volte ao formul&aacute;rio e corrija a data da falta solicitada
                    ou entre em contado com a se&ccedil;&atilde;o de Recursos Humanos do IA</td>
                <? echo $TblResAnt;?>
            <tr><td align="center" colspan="3"> <br><input type="button" name="Voltar" value="Voltar" class="botao" onClick="javascript:history.go(-1);"></td></tr>
        </table>
        <?
        exit(0);
    }

    $hora=date('H');
    $minuto=date('i');
    $hora = $hora + 0;
    $minuto = $minuto + 0;
    if ($ndb==$ndp && $hora >=17 && $minuto > 1) {
        $mensagem = "Solicita&ccedil;&atilde;o de falta abonada com preced&ecirc;cia de 1 dia, somente pode ser aceita antes das 17h. Qualquer d&uacute;vida, procure a se&ccedil;&atilde;o de RH do Instituto de Artes.";
        ?><br><br><table width="30%" align="center"><tr><td colspan="3">
                <b>Caro(a) <?echo $_SESSION["nomeuser_conected"];?></b>,<br>
                <?echo $mensagem;?></td></tr><tr><td colspan="3" align="left"><h2><b><font color="green">Dia e Hora: <?echo date("d/m/Y H:i");?></font></h2></td></tr></table><?
        $mensagem = $_SESSION["nomeuser_conected"]. "<br> " . $mensagem . "<br><br>Data e Hora:" . date("d/m/Y H:i");
        envia_email("RH: Sol. de falta abonada c/ atraso", $msgE1.$mensagem . $NaoResponda . $msgE2, "saa@iar.unicamp.br");
        exit(0);
    }

    $data = date("Y-m-d");
    // Verifica se o solicitante eh o diretor do IA. Se for, o status passa automaticamente a Pendente para RH
    if ($codarea_conected == $cod_diretoria && $coduser_conected==$_SESSION['coduser_chefe']) {
        $STA="P";
    } else {
        include("busca_aprovador.php");
        $STA="A";
    }

    $SQL = "Insert into RHsolicitacoes (codsolicitante, coduser, codaprovador, lotacao, status, tipo, dataaltera, datacadastro,ip_solicitacao) values ('$codsolicitante', '$coduser', '$codaprovador', '$lotacao', '$STA', '$tipo', '$data','$data','$IP')";
    $S= addslashes($SQL);
    $res = mysql_query("$SQL");
    if (!$res) {
        envia_email("[exibeartigo] Erro ao cadastrar o pedido. Entre em contato com o administrador. 1", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
        die("[exibeartigo] Erro ao cadastrar o pedido. Entre em contato com o administrador. 1");
    }
    $cod=mysql_insert_id();
    $SQL = "Select cod,tipo from RHsolicitacoes where cod=$cod and codsolicitante='$codsolicitante'";
    $res = mysql_query("$SQL");
    if($linha=mysql_fetch_array($res)) {

        $tipo = $linha['tipo'];
        $SQL = "Insert into RHartigo (cod, datafalta,codigofalta,justificativa) values ('$cod','$datafalta','$codigofalta','$justificativa')";
        $res = mysql_query("$SQL");
        if (!$res) {
            envia_email("[exibeartigo] Erro ao cadastrar o pedido. Entre em contato com o administrador. 2", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
            die("[exibeartigo] Erro ao cadastrar o pedido. Entre em contato com o administrador. 2");
        }

        $SQL = "Select u.usuario,u.email  from tblusuarios u where u.coduser='$coduser'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $email = $linha['email'];
        }
        $dt=strftime("%d-%m-%Y", strtotime($datafalta));
        $emailsolicitante=$_SESSION["emailuser_conected"];
        $desctipo="F1 - Falta abonada - Artigo 31";
        if ($codigofalta=='F2') {
            $desctipo="F2 - Falta integral abonada";
        }
        if ($STA=='A') {
            $email_notif=$emailaprovador;
            if ($codsubstituto <> "") {
                $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                $email_notif=$email_notif . "," . $emailsubstituto;
            } else {
                $mensagem = "Caro(a) $nomeaprovador,";
            }
            $mensagem .= "<br> Foi cadastrada pelo(a) colaborador(a) " . $_SESSION['nomeuser_conected']. ", uma solicita&ccedil;&atilde;o de ".$desctipo." para o dia $dt.";
            $mensagem .= "<br>Por favor, acesse o link abaixo para aprovar ou recusar o pedido.<br>";
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibeartigo.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";

            envia_email($assunto, $msgE1 . $mensagem . $ola. $msgE2, $email_notif);

            $mensagem .= "<br> Aprovador:" . $email_notif;
            $emailros="intra.ia@iar.unicamp.br";
            // envia_email("Notificacao RH - $desctipo - Aprovador", $msgE1 .  $mensagem . $msgE2 . $NaoResponda, $emailros);

            $mensagem = "<br>Caro(a)" . $_SESSION['nomeuser_conected'] . ",<br>Sua Solicitação de $desctipo numero $cod foi encaminhada para aprovação de sua chefia.";
            $mensagem .="<br>O(a) responsavel pela aprovação de sua Solicitação ï¿½ " . $nomeaprovador;
            if ($codsubstituto <> "") {
                $mensagem .=" ou o(a) substituto(a) indicado(a), sr(a)" . $nomesubstituto;
            }
            $mensagem .=  ". Por favor, aguarde.";
            $mensagem .= "<br><br> Atenciosamente, RH.";
            envia_email($assunto, $msgE1. $mensagem. $NaoResponda . $msgE2, $emailsolicitante);


        } else {
            // O solicitante eh o diretor do IA.
            // Busca os e-mails do pessoal do RH
            $SQL="select u.nome, u.email, u.coduser,s.idsistema, s.permissao from tblusuarios u left join tblususist s on u.coduser=s.coduser where s.idsistema=19 and permissao=3";
            $res = mysql_query("$SQL");
            $ll=0;
            $emailRH="";
            while($linha3=mysql_fetch_array($res)) {
                $ll=$ll+1;
                if ($ll>1) {
                    $emailRH .= ",";
                }
                $emailRH .= $linha3['email'];
            }

            $mensagem = "O(a) diretor(a) do Instituto de Artes, " . $_SESSION['nomeuser_conected'] . " solicita $desctipo para o  dia $dt.<br>";
            $mensagem .= "O numero da Solicitação é $cod e o status foi alterado para <b>Pendente para RH<b>. Por favor, providenciar tramite junto a DGRH.";
            $mensagem .= $att;
            envia_email($assunto, $msgE1. $mensagem . $NaoResponda . $msgE2, $emailRH);
        }
        $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Inserção de registro - Solicitação de abonada $cod', '$S','$timehoje')";
        $res=mysql_query($SQLlog) or die("[exibeartirgo] RH: erro ao inserir LOG");

    }
}
if($op=="3") { // Usuario RH alterou o status da Solicitacao, mantendo o Status de Pendente, Aguardando retorno ou Finalizada.
    $SQL = "Select s.tipo, s.dataaltera, u.usuario, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod'";
    $S= addslashes($SQL);
    $res = mysql_query("$SQL");
    if(mysql_num_rows($res) > 0) {
        $linha = mysql_fetch_array($res);
        $emailsolicitante=$linha['email'];
        $nomesolicitante=$linha['nome'];
        $SQL = "Update RHsolicitacoes set dataaltera = '$datadehoje', status='$status',codatendente='$coduser_conected' where cod='$cod'";

        $res = mysql_query("$SQL") or die("[exibeartigo] Erro ao cadastrar o pedido. Entre em contato com o administrador. 3");
        if ($status == "F") {
            $mensagem = "Caro(a) $nomesolicitante .<br>Sua Solicitação de falta abonada numero $cod foi finalizada pelo RH do IA.";
            $mensagem .= "<br>Qualquer duvida, entrar em contato com esta seção";
            $mensagem .= "<br><br>Atenciosamente,<br> $nomeuser_conected <br> RH";
            envia_email($assunto, $msgE1 . $mensagem . $NaoResponda . $msgE2 , $emailsolicitante);
            $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'RH-Finalização de Solicitação $cod', '$S','$timehoje')";
            $res=mysql_query($SQLlog)  or die("[exibeartigo] RH-Finalizacao: erro ao inserir LOG 2");
        }
        if ($status == "C") {
            if ($coduser <> $coduser_conected) {
                $mensagem = "Caro(a) $nomesolicitante .<br>Sua Solicitação de falta abonada numero $cod foi cancelada pelo RH do IA.";
                $mensagem .= "<br>Qualquer duvida, entrar em contato com esta seção";
                $mensagem .= "<br><br>Atenciosamente,<br> $nomeuser_conected <br> RH";
                envia_email($assunto, $msgE1.$mensagem . $NaoResponda . $msgE2, $emailsolicitante);
                $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'RH-Cancelamento de registro - Solicitação $cod', '$S','$timehoje')";
                $res=mysql_query($SQLlog) or die("[exibeartigo] RH-Cancelamento: erro ao inserir LOG 3");
            }
        }

    } else {
        die("Algu&eacute;m estava alterando o pedido. N&atilde;o foi poss&iacute;vel realizar sua altera&ccedil;&atilde;o, tente novamente.");
    }
}
// Operacao de atualizacao de uma Solicitacao requisitada pelo solicitante ou pelo RH
if($op=="4" || $op=="6") {
    $SQL = "Update RHsolicitacoes set lotacao='$lotacao', coduser='$coduser', codsolicitante='$coduser', codatendente='$coduser_conected', dataaltera = '$datadehoje', status='$status' where cod='$cod'";
    $S= addslashes($SQL);
    $res = mysql_query("$SQL") or die("[exibeartigo] Erro ao cadastrar o pedido. Entre em contato com o administrador. 5");
    $datafalta = $ano."/".$mes."/".$dia;
    $SQL = "Update RHartigo set datafalta='$datafalta', codigofalta='$codigofalta', justificativa='$justificativa' where cod='$cod'";

    $res = mysql_query("$SQL") or die("[exibeartigo] Erro ao cadastrar o pedido. Entre em contato com o administrador. 6 ");

    if($status=="U") {
        echo "[exibeartigo]  Problema na execucao do fluxo de dados. Comunique este erro a DTI";
        die();
        $SQL = "Select u.usuario, u.email, s.permissao from tblusuarios u left join RHsolicitacoes r on u.coduser=r.codsolicitante left join tblususist s on u.coduser=s.coduser and s.idsistema='19' where r.cod='$cod'";

        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $email = $linha['email'];
            $permissao = $linha['permissao'];
            $mensagem .= "Ol&aacute; .<br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
            $SQL = "Select * from RHsolicitacoes where cod='$cod'";
            $res = mysql_query("$SQL");
            if($linha=mysql_fetch_array($res)) {
                $tipo = $linha['tipo'];
            }
            if($tipo=="L") { $desctipo="licen&ccedil;a pr&ecirc;mio"; } else if($tipo=="A") { $desctipo="afastamento"; }
            $mensagem .= "Foi solicitada a altera&ccedil;&atilde;o de um pedido de ".$desctipo." cadastrado por voc&ecirc;.";
            $mensagem .= "<br>Voc&ecirc; deve alterar o pedido no link abaixo.<br>";

            if($tipo=="A") {
                $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitaoutrofastamento.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";
            } else if($tipo=="L") {
                $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitaoutrolicenca.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";
            }

            $mensagem .= "<br><br> Atenciosamente, RH.";
            envia_email($assunto, $msgE1 . $mensagem . $NaoResponda . $msgE2, $email);
        }
    } else if($status=="A") {
        /*  Verifica quem eh o aprovador para a area em que o usuario estah lotado. Se o usuario eh chefe da
        area, entao o aprovador eh o chefe imediato. Retorna a variavel $codaprovador e $nomeaprovador */
        include("busca_aprovador.php");
        $SQL = "Select s.tipo, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod'";

        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $tipo = $linha['tipo'];
            $emailsolicitante=$linha['email'];
            $nomesolicitante=$linha['nome'];
        }
        if ($codigofalta=='F1') $desctipo = "F1 - falta abonada - Artigo 31";
        if ($codigofalta=='F2') $desctipo = "F2 - Falta integral abonada";
        // Notifica o solicitante
        $email_notif=$emailaprovador;

        if ($codsubstituto <> "") {
            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
            $email_notif=$email_notif . "," . $emailsubstituto;
        } else {
            $mensagem = "Caro(a) $nomeaprovador,";
        }
        $mensagem = "<br>Foi alterada uma solicita&ccedil;&atilde;o de ".$desctipo." que necessita de sua aprova&ccedil;&atilde;o.";
        $mensagem .= "<br>Por favor, acesse o link abaixo para aprovar ou recusar o pedido.<br>";

        if($tipo=="G") {
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibeartigo.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
        } else if($tipo=="L") {
            echo "[exibeartigo]  Problema na execucao do fluxo de dados. Comunique este erro a DTI 7";
            die();
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibelicenca.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
        }
        $mensagem .= "<br><br> Atenciosamente, RH.";

        // Notifica o solicitante
        $mensagem = "Caro(a) $nomesolicitante ";
        $mensagem .= "<br>Sua Solicitação de " . $desctipo . " foi alterada e sera submetida a aprovação do Chefe de Departamento/Seção,";
        $mensagem .= " Sr(a) " . $nomeaprovador;
        if ($codsubstituto <> "") {
            $mensagem .=" ou ao(a) substituto(a) indicado(a), sr(a) " . $nomesubstituto;
        }
        $mensagem .=  ". Por favor, aguarde. ";
        $mensagem .= "<br>Qualquer duvida, entrar em contato com esta secao.";
        $mensagem .= "<br>Atenciosamente,<br> RH";

        $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Alteraï¿½ï¿½o de registro - Solicitação $cod', '$S','$timehoje')";
        $res=mysql_query($SQLlog) or die("[exibeartigo] RH-Alteracao: erro ao inserir LOG 4");

    }
}
$EoChefe=false;
$EoSubstituto=false;
$Echefe=false;
$EaprovadorIndicado=false;
include ("VerifiquePosicao.php");

if($cod) {
    $SQL="select s.*, u.nome,ap.nome as nomeaprovador from RHsolicitacoes s left join tblusuarios u on s.coduser=u.coduser left join tblusuarios ap on s.codaprovador=ap.coduser  where s.cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $status = $linha['status'];
    $obs = $linha['obs'];
    $coduser = $linha['coduser'];
    $codaprovador= $linha['codaprovador'];
    $nomeaprovador= $linha['nomeaprovador'];
    $codatendente= $linha['codatendente'];
    $codsolicitante= $linha['codsolicitante'];
    $nome = $linha['nome'];
    $lotacao = $linha['lotacao'];

    $SQL = "select *, date_format(datafalta, '%d/%m/%Y') AS datafalta from RHartigo where cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $datafalta = $linha['datafalta'];
    $codigofalta=$linha['codigofalta'];
    $justificativa=$linha['justificativa'];
}
//Quem pode aprovar, caso a Solicitação ainda esteja com status "Aguardando aprovacao"?
$PodeAprovar=false;
if ($status=="A") {
    if ($coduser_conected==$codaprovador) {
        $PodeAprovar=true;
    }
    if ($EoSubstituto && is_array($aprovadores_substituto) && count($aprovadores_substituto)>0 ) {
        if (in_array($codaprovador,$aprovadores_substituto)) {
            $PodeAprovar=true;
        }
    }
}
// Busca o aprovador substituto, para a area de lotacao do solicitante, caso ele exista
// Monta a hierarquia para a area base ($lotacao) e acima da area base
$nomesubstituto="";
MontaHierarquia($lotacao,"S","S","N");
if ($hierarquia['chefia']['matr'] == $codaprovador || $hierarquia['acima']['matr']==$codaprovador || $hierarquia['ATU']['matr']==$codaprovador || $hierarquia['DIR']['matr']==$codaprovador) {
    if ($hierarquia['chefia']['matr'] !=  $coduser) {
        if ($hierarquia['substituto_chefia']['matr']==$coduser) { // solicitante eh substituto no departamento dele
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        } else {
            if ($hierarquia['chefia']['matr'] == $codaprovador) {
                if ($hierarquia['substituto_chefia']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_chefia']['nome']; }
            } elseif ($hierarquia['acima']['matr']==$codaprovador) {
                if ($hierarquia['substituto_acima']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_acima']['nome']; }
            } elseif ($hierarquia['ATU']['matr']==$codaprovador) {
                if ($hierarquia['substituto_atu']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_atu']['nome']; }
            } else {
                if ($hierarquia['substituto_dir']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_dir']['nome']; }
            }
        }
    } else {
        if ($hierarquia['substituto_acima']['matr'] && $hierarquia['substituto_acima']['matr'] !=  $coduser) {
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        }
    }
}
?>
<SCRIPT LANGUAGE="JavaScript">
    function salvar () {
        if(document.formulario.status.value=='U') {
            document.location='retornasolicitacoes.php?cod=<? print $cod; ?>&volta=<? print $volta;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';
        } else {
            document.location='exibeartigo.php?cod=<? print $cod; ?>&op=3&volta=<? print $volta;?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status='+document.formulario.status.value;
        }
    }
    function aceita () {
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=P&op=1'
    }
    function recusa () {
        if(document.formulario.obs.value=="") {
            alert('Preencha uma justificativa para a recusa no campo observa&ccedil;&atilde;o!');
            document.formulario.obs.focus();
            return false;
        }
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=R&op=1&obs='+document.formulario.obs.value;
    }
    function altproc() {
        document.location='altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1'
    }
</SCRIPT>
<?
include("menu.php");
?>
<form name="formulario">

    <? if($PodeAprovar) { ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 bg-light">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Observa&ccedil;&atilde;o (pertinente à aprovação/recusa do pedido)</label>
                        <textarea class="form-control" name="obs" id="obs" rows="3" readonly><? print $obs; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 bg-light">
                <div class="form-group">
                    <label for="exampleFormControlInput1"><b>Solicita&ccedil;&atilde;o de falta abonada</b></label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Matr&iacute;cula:&nbsp;<? print $coduser; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Nome:&nbsp;<? print $nome; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Lota&ccedil;&atilde;o:&nbsp;
                        <?
                        $SQL ="SELECT * FROM areas where CodArea='$lotacao' and origem='I'";
                        $resultado = mysql_query($SQL);
                        if ($row1=mysql_fetch_array($resultado)) { ?>
                            <? print $row1['Descricao']; ?>
                        <? } ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Tipo de falta:&nbsp;
                        <? if ($codigofalta=="F1") print "F1 - Abonada - Artigo 31";
                        if ($codigofalta=="F2") print "F2 - Abonada integral"; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Falta abonada para o dia:&nbsp;<? print $datafalta; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Justificativa</label>
                    <textarea class="form-control" type="text" name="justificativa" id="justificativa" cols="50" rows="3" disabled><? print $justificativa; ?></textarea>
                </div>
                <? if ($status <> "A") { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Observa&ccedil;&otilde;es do aprovador</label><br/>
                        <textarea class="form-control" type="text" name="obs" id="obs" cols="50" rows="3" disabled><? print $obs; ?></textarea>
                    </div>
                <? }
                $SQL = "Select u.nome, u.tipo from tblusuarios u where u.coduser='$codaprovador'";
                $res = mysql_query("$SQL");
                if($linha2=mysql_fetch_array($res)) { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Aprova&ccedil;&atilde;o:&nbsp;
                            <?
                            echo $linha2['nome'];
                            if ($nomesubstituto) echo " <b><font color=green> ou o(a) substituto(a) indicado(a) pelo RH, " . $nomesubstituto . "</font></b>";
                            ?>
                        </label>
                        <textarea class="form-control" type="text" name="obs" id="obs" cols="50" rows="3" disabled><? print $obs; ?></textarea>
                    </div>
                <? } ?>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Status:&nbsp;</label>
                    <? if($nivel>1 && ($status=="P" || $status=="T")) { ?>
                        <select class="form-control" name="status">
                            <option value="P" <? if($status=="P") { print "selected"; }?>>Pendente para o RH</option>
                            <option value="T" <? if($status=="T") { print "selected"; }?>>Aguardando retorno</option>
                            <option value="U" <? if($status=="U") { print "selected"; }?>>Retornar ao solicitante</option>
                            <option value="F" <? if($status=="F") { print "selected"; }?>>Finalizada</option>
                        </select>
                    <? } else { ?>
                        <label for="exampleFormControlInput1">
                            <? if($status=="F") { ?>
                                <font class="fonte1">Finalizada</font>
                            <? } else if($status=="A") { ?>
                                <font class="fonte1">Aguardando aprova&ccedil;&atilde;o do chefe</font>
                            <? } else if($status=="P") { ?>
                                <font class="fonte1">Pendente para o RH</font>
                            <? } else if($status=="T") { ?>
                                <font class="fonte1">Aguardando retorno</font>
                            <? } else if($status=="U") { ?>
                                <font class="fonte1">Retornado ao solicitante</font>
                            <? } else if($status=="R") { ?>
                                <font class="fonte1">Recusada pelo chefe</font>
                            <?} else if($status=="C") { ?>
                                <font class="fonte1">Cancelada</font>
                            <? } ?>
                        </label>
                    <? } ?>
                </div>
                <? if ($op!="1") { ?>
                    <? if($nivel>1 && ($status=="P" || $status=="T")) { ?>
                        <input type="button" class="btn btn-primary" name="Salvar" value="Salvar" onclick="salvar();">
                    <? }

                    if($PodeAprovar) { ?>
                        <input type="button" class="btn btn-primary" name="Aceitar" value="Aceitar" onclick="aceita();">
                        <input type="button" class="btn btn-primary" name="Recusar" value="Recusar" onclick="recusa();">
                    <? }
                    if($status=="A" && $nivel ==3 && $codsolicitante != $cod_diretor) {?>
                        <!--input type="button" name="Aprovacao" value="Alterar aprovador" onclick="altproc();"!-->
                        <input type="button" class="btn btn-primary" name="Aprovacao" value="Alterar aprovador" onclick="window.open('altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1',null,'left=120,top=300,height=220,width=780,status=yes,toolbar=no,menubar=no,location=no');">
                    <? }
                    if(($nivel == 3 || $codsolicitante==$coduser_conected || $coduser==$coduser_conected) && $status <> "F" && $status <> "C" && $status <> "R") { ?>
                        <input type="button" class="btn btn-primary" name="Cancelar" value="Cancelar Solicita&ccedil;&atilde;o" onclick="if(confirm('Tem certeza que deseja cancelar esta Solicitação?')) { document.location='exibeartigo.php?cod=<? print $cod;?>&op=3&status=C&volta=<? print $volta;?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';}">
                        <input type="button" class="btn btn-primary" name="Alterar" value="Alterar Solicita&ccedil;&atilde;o" onclick="document.location='solicitaoutroartigo.php?cod=<? print $cod; ?>&op=6&volta=<? print $volta;?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';">
                    <?}?>
                    <input type="button" class="btn btn-primary" name="Voltar" value="Voltar" onclick="document.location='<? print $volta;?>?pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?><? print $parametros;?>';">
                <? } else {?>
                    <font size="3" color="green">Solicita&ccedil;&atilde;o criada. Aguarde aprova&ccedil;&atilde;o pela chefia indicada.</font>
                <?}?>
            </div>
        </div>
    </div>
</form>
</body>
</html>
