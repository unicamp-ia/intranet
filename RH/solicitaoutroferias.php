<?php
// Esse procedimento serve ao propoosito de apresentar o formulario para o usuario solicitar a criacao de
// um pedido de ferias ou para alteracao de um pedido existente por parte do solicitante ou do RH.
// OP = 1 -> Prepara o formulario para a criacao de um pedido de ferias
// OP = 4 ou 6 -> Prepara o formulario para alteracao de um pedido ja existente
// Alterado por Edson Giordani em Outubro/2019 - Indicacao de Gestor Substituto
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Maio/2020 - Novo organograma ATU para CTUIA

header("Content-type: text/html; charset=utf-8");
$nivelcomp = 0;

include("../base/inicio.php"); /*Faz a verificacao se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
include("../base/FuncoesOrganograma.php");
$coduser = $_SESSION["coduser_conected"];
$browser=$_SESSION['browser'];
$op = $_REQUEST['op'];
if (!$op || ($op <> 1 && $op <> 4 && $op <> 6)) {
    echo "Fluxo de execu&ccedil;&atilde;o desconhecido ou acesso indevido ao m&oacute;dulo RH - F&eacute;rias. Operador n&atilde;o indicado.";
    die();
}
$volta = $_REQUEST['volta'];
$datainicio =  date("Y-m-d H:i:s");

if($op==4 || $op==6) {
    // OP= 4 ou 6 indicam que o usuario ou o RH estah querendo alterar a solicitacao ou o aprovador .
    // Nesse caso, seleciona a solicitacao para ser alterada pelo solicitante ou pelo RH e disponibiliza os campos para alteracao na parte final deste codigo
    $cod = $_REQUEST['cod'];
    if (!$cod) {
        echo "Fluxo de execu&ccedil;&atilde;o desconhecido ou acesso indevido. C&oacute;digo da solicita&ccedil;&atilde;o de F&eacute;rias n&atilde;o encontrado!";
        die();
    }
    $SQL = "select * from RHsolicitacoes where cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $status = $linha['status'];
    $coduser= $linha['coduser'];
    $codsolicitante=$coduser;
    $obs = $linha['obs'];
    $codatendente= $linha['codatendente'];
    $lotacao=$linha['lotacao'];
    $departamento=$lotacao;
    $SQL = "Select * from tblusuarios where coduser='$coduser'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $nome = $linha['nome'];
    $regime=$linha['regime'];
    $areabaseRequisitante = $linha['codarea'];
    $SQL = "select *, date_format(datainicio, '%d/%m/%Y') AS datainicio1 from RHferias where cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $total = $linha['total'];
    $pecunia = $linha['pecunia'];
    $adiantamento = $linha['adiantamento'];
    $data = $linha['datainicio1'];
    $ehchefe = 'nao';
    $MATR_substituto = 0;
    if (($linha['MATR_substituto']!=0)&&($linha['MATR_substituto']!=NULL)) {
        $ehchefe = 'sim';
        $MATR_substituto = $linha['MATR_substituto'];
    }
    $vect = explode("/",$data);
    $dia = $vect[0];
    $mes = $vect[1];
    $ano = $vect[2];
    $dias = $linha['dias'];
    $objetivo = $linha['objetivo'];
} else {
    // OP = 1 Pega os dados pricipais do usuario para o caso de ele estar fazendo um novo pedido
    $SQL = "Select * from tblusuarios where coduser='$coduser'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $nome = $linha['nome'];
    $regime=$linha['regime'];
    $areabaseRequisitante = $linha['codarea'];
    $departamento=$linha['codarea'];
    $departamento=trim($departamento);
    $lotacao = $linha['codarea'];
    $codsolicitante=$coduser;
}
/* Data para não permitir o cadastro de pedido para data posterior a hoje. */
$anopedido = date("Y");
$mespedido = date("m");
$diapedido = date("d");
?>
<SCRIPT LANGUAGE="JavaScript">
    function openModal(pUrl, pWidth, pHeight,x,y) {
        if (window.showModalDialog) {
            return window.showModalDialog(pUrl, window,
                "dialogWidth:" + pWidth + "px;dialogHeight:" + pHeight + "px;dialogLeft:" + x + "px;dialogTop:" + y + "px;center:on");
        } else {
            try {
                netscape.security.PrivilegeManager.enablePrivilege(
                    "UniversalBrowserWrite");
                window.open(pUrl, "wndModal", "width=" + pWidth
                    + ",height=" + pHeight + ",resizable=no,modal=yes");
                return true;
            }
            catch (e) {
                alert("Script n\u00e3o confi\u00e1vel, n\u00e3o \u00e9 poss\u00edvel abrir janela modal.");
                return false;
            }
        }
    }
    function SelectServidor(form) {

        var ob=document.formulario.coduser;
        var regime = ob[ob.selectedIndex].getAttribute('regime');
        var departamento = ob[ob.selectedIndex].getAttribute('departamento');
        formulario.regime.value=regime;
        formulario.clt_ou_cle.value=regime;
        formulario.departamento.value=departamento;
    }
    function checa_form(form){
        form.dia.value = form.datacompleta.value.substr(8,10);
        form.mes.value = form.datacompleta.value.substr(5,2);
        form.ano.value = form.datacompleta.value.substr(0,4);
        var regime=formulario.regime.value;
        if (form.dia.value == ""){
            alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
            form.dia.focus();
            return (false);
        }
        if(isNaN(form.dia.value)) {
            alert("A data deve conter apenas n\u00fameros!");
            form.dia.focus();
            return (false);
        }
        if(form.dia.value > 31) {
            alert("Preencha corretamente a data! O dia deve ser menor ou igual do que 31.");
            form.dia.focus();
            return (false);
        }
        if (form.mes.value == ""){
            alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
            form.mes.focus();
            return (false);
        }

        if(isNaN(form.mes.value)) {
            alert("A data deve conter apenas n\u00fameros!");
            form.mes.focus();
            return (false);
        }
        if(form.mes.value > 12) {
            alert("Preencha corretamente a data! O m\u00eas deve ser menor ou igual do que 12.");
            form.mes.focus();
            return (false);
        }
        if (form.ano.value == ""){
            alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
            form.ano.focus();
            return (false);
        }
        if(isNaN(form.ano.value)) {
            alert("A data deve conter apenas n\u00fameros!");
            form.ano.focus();
            return (false);
        }

        if(form.ano.value < <? print $anopedido; ?> ||
            (form.ano.value == <? print $anopedido; ?> && form.mes.value < <? print $mespedido; ?>) ||
            (form.ano.value == <? print $anopedido; ?> && form.mes.value == <? print $mespedido; ?> && form.dia.value < <? print $diapedido; ?>)){
            alert('O in\u00edcio das f\u00e9rias n\u00e3o pode ser menor do que a data atual.');
            form.dia.focus();
            return (false);
        }
        valor13 = "N";
        for(var i=0; i<form.adiantamento.length; i++){
            if(form.adiantamento[i].checked) {
                valor13=form.adiantamento[i].value;
                break;
            }
        }

        if ((form.mes.value == 11 || form.mes.value == 12 || form.mes.value == 1) && (valor13=="S")){
            alert("N\u00e3o \u00e9 poss\u00edvel solicitar adiantamento para os meses de novembro, dezembro e janeiro!!!");
            form.adiantamento.focus();
            return (false);
        }
        if (form.total.value=="") {
            alert("Informe o total de dias.");
            form.total.focus();
            return(false);
        }
        if(isNaN(form.total.value)) {
            alert("O total de dias deve ser num\u00e9rico.");
            form.total.focus();
            return (false);
        }
        if ((form.ehchefe.value=="sim") && (form.MATR_substituto.value=="000000")) {
            alert("O substituto para o cargo deve ser informado.");
            form.MATR_substituto.focus();
            return (false);
        }

        return(true);

    }
    function enviar(mtr) {
        if(checa_form(document.formulario) == true)  {
            document.formulario.submit();
        }
        else {
            return(false);
        }
        return true;
    }

    function cancelar() {
        if(confirm('Tem certeza de que deseja cancelar esta solicita\u00e7\u00e3o'))  {
            document.formulario.status.value = 'C';
            document.formulario.submit();
        }
        else {
            return(false);
        }
        return true;
    }

    function PulaCampo(ev, oOrg, pDest, vLen) {
        var key;

        if (window.event)
            key = window.event.keyCode;
        else if (ev)
            key = ev.which;
        else
            return true;
        if ((key==null) || (key==0) || (key==8) || (key==9) ||
            (key==13) || (key==16) || (key==17) || (key==18) ||
            (key==20) || (key==27) )
            return true;

        var oDest =document.getElementById(pDest);
        if(String(oOrg.value).length >= vLen) {
            if (pDest=="localsaida") {
                if (ChecaAntecedencia()==false) {
                    return false;
                }
            }
            oDest.focus();
        }
    }
</script>
<?
include("menu.php");
?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 offset-lg-3 offset-sm-3 offset-md-3">
            <? if($cod && $obs) { ?>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Observa&ccedil;&atilde;o</label>
                    <textarea class="form-control" name="obs" id="obs" rows="3" readonly><? print $obs; ?></textarea>
                </div>
            <? }
            $width = "<script>sw=screen.width;</script>";
            $height = "<script>sh=screen.height;</script>";
            ?>
            <form action="exibeferias.php" method="post" name="formulario" >
                <input type="hidden" name="codsolicitante" id="codsolicitante" value="<? print $coduser; ?>">
                <input type="hidden" name="op" id="op" value="<? print $op; ?>">
                <input type="hidden" name="datainicio" id="datainicio" value="<? print $datainicio; ?>">
                <input type="hidden" name="tipo" id="tipo" value="F">
                <input type="hidden" name="status" id="status" value="A">
                <input type="hidden" name="cod" id="cod" value="<? print $cod; ?>">
                <input type="hidden" name="volta" id="volta" value="<? print $volta; ?>">
                <input type="hidden" name="regime" id="regime" value="<? print $regime; ?>">
                <div class="form-group">
                    <label for="exampleFormControlInput1"><b>Solicita&ccedil;&atilde;o de f&eacute;rias</b></label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        <font color="red">
                            Fique atento aos prazos para solicita&ccedil;&atilde;o de f&eacute;rias
                        </font>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        <a href="#" onclick="
                        <?if ($browser=="Chrome" || $browser=="Safari" ) {?>
                                window.open('http://www.dgrh.unicamp.br/documentos/cronograma-siarh', null, 'left=100,top=50,height=700,width=1000,status=yes,toolbar=no,menubar=no,location=no');">
                            <?} else {?>
                                openModal('http://www.dgrh.unicamp.br/documentos/cronograma-siarh',1000,700,100,50);">
                            <?}?>
                            Veja aqui os prazos
                        </a>
                    </label>
                </div>
                <div class="form-group">
                    <? if (($op==1 || $op==6) && $nivel==3) { ?>
                        <label for="exampleFormControlSelect1">Servidor</label>
                        <? if ($nivel <> 3) { ?>
                            <input type="hidden" name="coduser" value = "<?= $coduser ?>">
                            <label for="exampleFormControlSelect1"><font color="red"><?= $nome ?></font></label>
                        <? } else { ?>
                            <select class="form-control" name="coduser" id="coduser" onchange="javascript:SelectServidor(this);">
                                <option value="0">Escolha</option>
                                <?
                                $consulta1 = mysql_query("SELECT * FROM tblusuarios where status='A' and (tipo='FUNCIONARIO' || tipo='DOCENTE') order by nome");
                                while ($row1=mysql_fetch_array($consulta1)) {   ?>
                                    <option regime="<?print $row1['regime'];?>" departamento = "<?print $row1['codarea'];?>" value="<? print $row1['coduser']; ?>" <? if($coduser==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?>
                                    </option>
                                <? } ?>
                            </select>
                        <? } ?>
                        <input type="hidden" name="departamento" value="<?= $departamento ?>">
                        <label for="exampleFormControlSelect1">Regime: <input type="text" name="clt_ou_cle" value="<?= $regime ?>" size="3" disabled></label>
                    <? } else { ?>
                        <label for="exampleFormControlSelect1">Matr&iacute;cula: </label>
                        <label for="exampleFormControlSelect1"><? print $coduser; ?></label>
                        <input name="coduser" id="coduser" type="hidden" value="<? print $coduser; ?>">
                        <input type="hidden" name="departamento" value="<? echo $departamento; ?>">
                    <? }
                    if($op!=1 && $op!=4) { ?>
                        <label for="exampleFormControlSelect1">Nome</label>
                        <label for="exampleFormControlSelect1"><? print $nome; ?></label>
                    <? } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">In&iacute;cio das f&eacute;rias</label>
                    <input class="form-control" style="max-width:200px;" type="date" name="datacompleta" id="datacompleta" value="<? print $ano; ?>-<? print $mes; ?>-<? print $dia; ?>">
                    <input class="form-control" name="dia" id="dia" type="hidden" size="1" maxlength="2" value="<? print $dia; ?>" onkeydown="javascript:PulaCampo(event, this,'mes',2);" onkeyup="javascript:PulaCampo(event, this,'mes',2);">
                    <input class="form-control" name="mes" id="mes" type="hidden" size="1" maxlength="2" value="<? print $mes; ?>" onkeydown="javascript:PulaCampo(event, this,'ano',2);" onkeyup="javascript:PulaCampo(event, this,'ano',2);">
                    <input class="form-control" name="ano" id="ano" type="hidden" size="2" maxlength="4" value="<? print $ano; ?>" onkeydown="javascript:PulaCampo(event, this,'total',4);" onkeyup="javascript:PulaCampo(event, this,'total',4);">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Total de dias</label>
                    <input class="form-control" style="max-width:200px;" name="total" id="total" type="text" size="2" maxlength="3" value="<? print $total; ?>">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1"><b>Op&ccedil;&otilde;es para funcion&aacute;rio regido pela CLT:</b></label><p></p>
                    <label for="exampleFormControlTextarea1">Pec&uacute;nia:&nbsp;&nbsp;</label>
                    Sim<input type="radio" name="pecunia" id="pecunia" value="S" <? if($pecunia=="S") { ?>checked<? } ?>>&nbsp;&nbsp;&nbsp;&nbsp;
                    N&atilde;o<input type="radio" name="pecunia" id="pecunia" value="N" <? if($pecunia=="N" || !$pecunia) { ?>checked<? } ?>><p></p>
                    <label for="exampleFormControlTextarea1">Adiantamento 13&ordm; sal&aacute;rio:&nbsp;&nbsp;</label>
                    Sim<input type="radio" name="adiantamento" id="adiantamento" value="S" <? if($adiantamento=="S") { ?>checked<? } ?>>&nbsp;&nbsp;&nbsp;&nbsp;
                    N&atilde;o<input type="radio" name="adiantamento" id="adiantamento" value="N" <? if($adiantamento=="N" || !$adiantamento) { ?>checked<? } ?>><p></p>
                    <label for="exampleFormControlTextarea1">O adiantamento salarial &eacute; autom&aacute;tico, n&atilde;o cabe op&ccedil;&atilde;o!</label>
                </div>
                <?
                // recupera matrÃ­cula do chefe da Ã¡rea do solicitante
                $SQL="Select MATR_chefe from areas where CodArea='$departamento' and status='A'";
                $func = mysql_query($SQL);
                if (mysql_num_rows($func) > 0) {
                    $res = mysql_fetch_array($func);
                    $coduser_chefe_requisitante=$res['MATR_chefe'];
                }
                // se o solicitante eh chefe, deve indicar um substituto
                if($coduser_chefe_requisitante==$coduser) { ?>
                    <div class="form-group">
                        <input type="hidden" name="ehchefe" id="ehchefe" value="sim">
                        <label for="exampleFormControlInput1"><b>Indicar substituto (obrigat&oacute;rio, somente para gestores)</b></label>
                        <select class="form-control" name="MATR_substituto" id="MATR_substituto">
                            <option value="000000" selected>Selecionar substituto</option>
                            <?
                            if (($coduser==$_SESSION["cod_diretor"]) || ($coduser==$_SESSION["cod_associado"])) {
                                $AREA_imediato="DIR";
                                if ($coduser==$_SESSION["cod_diretor"]) {
                                    $MATR_imediato=$_SESSION['cod_associado'];
                                } else {
                                    $MATR_imediato=$_SESSION['cod_diretor'];
                                }
                                $SQL = "SELECT coduser, nome FROM tblusuarios where coduser='".$MATR_imediato."'";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_imediato==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } elseif ($areabaseRequisitante=="CTUIA") {
                                // $areabaseRequisitante = $linha['codarea'];
                                $SQL = "SELECT coduser, nome FROM tblusuarios where coduser='".$_SESSION['cod_diretor']."' OR coduser='".$_SESSION['cod_associado']."'";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_imediato==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } else {
                                MontaHierarquia($_SESSION["codarea_conected"],'N','S','S');
                                $MATR_imediato=$hierarquia['acima']['matr'];
                                $AREA_imediato=$hierarquia['acima']['area'];
                                $NOME_imediato=$hierarquia['acima']['nome']; ?>
                                <option value="<?echo $MATR_imediato;?>"<?if ($MATR_substituto==$MATR_imediato) echo " selected";?>><?echo $NOME_imediato;?></option>
                                <?
                                if ($hierarquia['substituto_acima']['matr']) {?>
                                    <option value="<?echo $hierarquia['substituto_acima']['matr'];?>"<?if ($MATR_substituto==$hierarquia['substituto_acima']['matr']) echo " selected";?>><?echo $hierarquia['substituto_acima']['nome'];?></option>
                                <?}
                                $SQL = "SELECT coduser, nome FROM tblusuarios where status='A' and tipo!='INSTITUCIONAL' and codarea='".$departamento."' and coduser <> '".$coduser."' order by nome";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_substituto==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } ?>
                        </select>
                    </div>
                <? } else { ?>
                    <input type="hidden" name="ehchefe" id="ehchefe" value="nao">
                <? }
                if($op==1 || $op==6 || $status=="U") { ?>
                    <input class="btn btn-primary" type="button" name="Salvar" value="Salvar" onclick="enviar('<?echo $coduser; ?>');">
                <? }
                if($volta) { ?>
                    <input class="btn btn-primary" type="button" name="Voltar" value="Voltar" onclick="document.location='<? print $volta; ?>'">
                <? } ?>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>

            </form>
        </div>
    </div>
</div>
</body>
</html>
