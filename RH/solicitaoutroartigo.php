<?php
        // Incluida obrigatoriedade de justificativa de F1 e F2 em 18/09/2019
        // Procedimento para inclusao de um solicitacao de falta abonada ou para alteracao de uma ja existente
        // Este procedimento esta disponivel ao solicitante ou ao pessoal de RH
        // Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
 
        $nivelcomp = -1;
 	include("../base/inicio.php"); /* Faz a verificacao se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
 	include("../base/FuncoesUteis.php");
	$coduser = $_SESSION["coduser_conected"];
	$volta = $_REQUEST['volta'];
        $busca = $_REQUEST['busca']; /* Codigo para nao perder o filtro na busca. */
        $pagina=$_REQUEST['pagina'];
        $num_linhas=$_REQUEST['num_linhas'];
	if($busca) {
		$tipo = $_REQUEST['tipo'];
		$nome = $_REQUEST['nome'];
		$CodArea = $_REQUEST['CodArea'];
		$diainicad = $_REQUEST['diainicad'];
		$mesinicad = $_REQUEST['mesinicad'];
		$anoinicad = $_REQUEST['anoinicad'];
		$diafimcad = $_REQUEST['diafimcad'];
		$mesfimcad = $_REQUEST['mesfimcad'];
		$anofimcad = $_REQUEST['anofimcad'];
                $statusbusca=$_REQUEST['statusbusca'];
		$parametros = "&busca=".$busca."&tipo=".$tipo."&nome=".$nome."&CodArea=".$CodArea."&diainicad=".$diainicad."&mesinicad=".$mesinicad."&anoinicad=".$anoinicad."&diafimcad=".$diafimcad."&mesfimcad=".$mesfimcad."&anofimcad=".$anofimcad."&statusbusca=".$statusbusca;
	}
	$obs = $_REQUEST['obs'];
	$op = $_REQUEST['op'];
	$cod = $_REQUEST['cod'];
	$datainicio =  date("Y-m-d G:i:s");
        $regime=$_SESSION["regime_trabalho"];
        
        if (!$op || ($op <> 1 && $op <> 4 && $op <> 6)) {
            echo "Fluxo de execucao desconhecido ou acesso indevido ao modulo RH - Abonadas. Operador nao indicado.";
            die();
        }
	if($op==4 || $op==6) {  // Prepara os dados para alteracao de uma solicitacao botao "alterar solicitacao OP=6" acionado pelo RH ou pelo solicitante  ou para responder a um retorno OP=4 
                
                 if (!$cod) {
                    echo "Fluxo de execucao desconhecido ou acesso indevido ao Modulo RH - abonadas. Codigo da solicitacao de abonada nao encontrado!";
                    die();
                }
		$SQL = "select r.*, u.nome from RHsolicitacoes r left join tblusuarios u on r.coduser=u.coduser where r.cod='$cod'";
		$res = mysql_query($SQL);
		$linha = mysql_fetch_array($res);
		$status = $linha['status'];
		$coduser= $linha['coduser'];
		$codsolicitante= $linha['codsolicitante'];
		$obs = $linha['obs'];
		$codatendente= $linha['codatendente'];
                $dataaltera= $linha['dataaltera'];
                $datacadastro= $linha['datacadastro'];
		$SQL = "select *, date_format(datafalta, '%d/%m/%Y') AS datafalta from RHartigo where cod='$cod'";
		$res = mysql_query($SQL);
		$linha = mysql_fetch_array($res);
		$datafalta = $linha['datafalta'];
                $codigofalta=$linha['codigofalta'];
                $justificativa=$linha['justificativa'];
		$vect=explode("/",$datafalta);
		$dia = $vect[0];
		$mes = $vect[1];
		$ano = $vect[2];
	}
        
        $SQL = "Select nome,codarea,regime from tblusuarios where coduser='$coduser'";
	$res = mysql_query($SQL);
	$linha = mysql_fetch_array($res);
	$lotacao = $linha['codarea'];
        $nome = $linha['nome'];
        $regime= $linha['regime'];
?>
<SCRIPT LANGUAGE="JavaScript">
        function ChecaAntecedencia() {
            var diasemana=document.formulario.diasemana.value;
            dia = document.formulario.dia.value;
            mes = document.formulario.mes.value;
            ano = document.formulario.ano.value;
            hora=0;
            minutos=0;
            DataAbonada=new Date(ano,mes-1,dia,hora,minutos);
            var dtS = document.formulario.DataMais90.value;
            diaS = dtS.substring(8,10);
            mesS = dtS.substring(5,7);
            anoS  = dtS.substring(0,4);
            horaS  = 0;
            minutoS = 0;
            mesS=mesS-1;
            DataHojeMais90=new Date(anoS,mesS,diaS,horaS, minutoS);
            if (DataAbonada > DataHojeMais90) {
               alert("Data da falta abonada muito distante da data atual. O m\u00faximo de anteced\u00eancia que o sistema pode aceitar \u00ea de 90 dias. Verifique se a data est\u00e1 correta!");
               return false;
            }
            return true;
        }
	function checa_form(form){

                form.dia.value = form.datacompleta.value.substr(8,10);
                form.mes.value = form.datacompleta.value.substr(5,2);
                form.ano.value = form.datacompleta.value.substr(0,4);

                var objFalta = form.codigofalta;
                 var v=objFalta.selectedIndex;
                 if (v==0) {
                     alert("Por favor, selecione o tipo de falta abonada.");
                     return(false);
                 }
                
		if (form.coduser.value == "0"){
			alert("Selecione o funcion\u00e1rio!!!");
			form.coduser.focus();
			return (false);
		}
		if (form.dia.value == ""){
			alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
			form.dia.focus();
			return (false);
		}
		if(isNaN(form.dia.value)) {
		  	alert("A data deve conter apenas n\u00fameros!");
		  	form.dia.focus();
			return (false);
		}
		if(form.dia.value > 31) {
			alert("Preencha corretamente a data! O dia deve ser menor ou igual do que 31.");
			form.diasaida.focus();
			return (false);
		}
		if (form.mes.value == ""){
			alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
			form.mes.focus();
			return (false);
		}
		if(isNaN(form.mes.value)) {
		  	alert("A data deve conter apenas n\u00fameros!");
		  	form.mes.focus();
			return (false);
		}
		if(form.mes.value > 12) {
			alert("Preencha corretamente a data! O m\u00eas deve ser menor ou igual do que 12.");
			form.messaida.focus();
			return (false);
		}
		if (form.ano.value == ""){
			alert("O preenchimento do campo \u00e9 obrigat\u00f3rio !!!");
			form.ano.focus();
			return (false);
		}
		if(isNaN(form.ano.value)) {
		  	alert("A data deve conter apenas n\u00fameros!");
		  	form.ano.focus();
			return (false);
		}
		if (form.justificativa.value == ""){
			alert("O preenchimento do campo justificativa \u00e9 obrigat\u00f3rio !!!");
			form.justificativa.focus();
			return (false);
		}
           
                if (ChecaAntecedencia()==false) {
                          return false;
                }
               
		return (true);
	}

 	function enviar() {
          
		if(checa_form(document.formulario) == true)  {
			document.formulario.submit();
		}
		else {
			return false;
		}
		return true;
	}
        function PulaCampo(ev, oOrg, pDest, vLen) {
            var key;
            
            if (window.event)
                key = window.event.keyCode;
            else if (ev)
                key = ev.which;
            else
                return true;
            if ((key==null) || (key==0) || (key==8) || (key==9) || 
                 (key==13) || (key==16) || (key==17) || (key==18) ||
                 (key==20) || (key==27) ) 
                return true;
            
            var oDest =document.getElementById(pDest);
            if(String(oOrg.value).length >= vLen) {
                oDest.focus();
            }
        }
</script>
<?
include("menu.php");
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3">
            <?
            if($cod && $obs) { ?>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Observa&ccedil;&atilde;o</label>
                    <textarea class="form-control" name="obs" id="obs" rows="3" readonly><? print $obs; ?></textarea>
                </div>
            <? } ?>
            <form action="exibeartigo.php" method="post" name="formulario" >
                <input type="hidden" name="codsolicitante" id="codsolicitante" value="<? print $coduser; ?>">
                <input type="hidden" name="op" id="op" value="<? print $op; ?>">
                <input type="hidden" name="datainicio" id="datainicio" value="<? print $datainicio; ?>">
                <input type="hidden" name="tipo" id="tipo" value="G">
                <input type="hidden" name="status" id="status" value="A">
                <input type="hidden" name="cod" id="cod" value="<? print $cod; ?>">
                <input type="hidden" name="volta" id="volta" value="<? print $volta; ?>">
                <input type="hidden" name="statuslista" id="statuslista" value="<? print $statuslista; ?>">
                <input name="DataMenos1" type="hidden" value="<? echo  DtSomar(date("Y-m-d"),$som); ?>">
                <input name="DataMais90" type="hidden" value="<? echo  DtSomar(date("Y-m-d"),90); ?>">
                <input name="diasemana" type="hidden" value="<? echo $diasemana; ?>">

                <?
                $todayh=getdate();
                $som=-1 * 1;
                $wd=$todayh['wday']*1;
                if ($wd==1) $som=-3;
                if ($wd==0) $som=-2;
                $dmenos1=DtSomar(date("Y-m-d"),$som);
                $t=explode("-",$dmenos1);
                $d=$t[2];
                $m=$t[1];
                $y=$t[0];
                $WeekMon  = mktime(0, 0, 0, $m  , $d, $y);    //monday week begin calculation
                $todayh = getdate($WeekMon); //monday week begin reconvert
                $diasemana=$todayh['wday'];
                ?>

                <div class="form-group">
                    <label for="exampleFormControlInput1"><b>Solicita&ccedil;&atilde;o de falta abonada</b></label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        <font color="red">
                            Esta solicita&ccedil;&atilde;o pode ser preenchida com at&eacute; 1 (um) dia &uacute;til de posterioridade da data a ser solicitada, desde que efetuada at&eacute; 17h.
                        </font>
                    </label>
                </div>
                <div class="form-group">
                    <? if (($op==1 || $op==6) && $nivel==3) { ?>
                        <label for="exampleFormControlSelect1">Servidor</label>
                        <select class="form-control" name="coduser" id="coduser" style="color:blue;text-align:left;font-size:16px" <? if (!$nivel || $nivel < 1) {  echo "disabled=\"disabled\"";} ?> >
                            <option value="0">Escolha</option>
                            <?
                            $consulta1 = mysql_query("SELECT * FROM tblusuarios where status='A' and (tipo='FUNCIONARIO' || tipo='DOCENTE') order by nome");
                            while ($row1=mysql_fetch_array($consulta1)) {   ?>
                                <option value="<? print $row1['coduser']; ?>" <? if($coduser==$row1['coduser']){ ?> selected <? } ?>>
                                    <? print $row1['nome']; ?> </option>
                            <? } ?>
                        </select>
                    <? } else { ?>
                        <label for="exampleFormControlSelect1">Nome</label>
                        <label for="exampleFormControlSelect1"><? print $nome; ?></label>
                        <label for="exampleFormControlSelect1">Matr&iacute;cula</label>
                        <label for="exampleFormControlSelect1"><? print $coduser; ?></label>
                        <input name="coduser" id="coduser" type="hidden" value="<? print $coduser; ?>">
                    <? } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Lota&ccedil;&atilde;o</label>
                    <? if (($op==1 || $op==6) && $nivel==3) {
                        $SQL ="SELECT * FROM areas where status='A' and origem='I' and celula_instancia='I' and situacao_organograma='A' order by Descricao";
                        $resultado = mysql_query($SQL);
                        ?>
                        <select class="form-control" name="lotacao" style="color:blue;text-align:left;font-size:16px" <? if (!$nivel || $nivel < 1) {  echo "disabled=\"disabled\"";} ?>>
                            <option value="0">Escolha o Departamento</option>
                            <? while ($row1=mysql_fetch_array($resultado)) { ?>
                                <option value="<? print $row1['CodArea']; ?>" <? if($lotacao==$row1['CodArea']){ ?> selected <? } ?>>
                                    <? print $row1['Descricao']; ?>
                                </option>
                            <? } ?>
                        </select>
                    <? } else {
                        $SQL ="SELECT * FROM areas where CodArea='$lotacao' and origem='I'";
                        $resultado = mysql_query($SQL);
                        if ($row1=mysql_fetch_array($resultado)) {
                            ?>
                            <label for="exampleFormControlInput1"><? print $row1['Descricao']; ?></label>
                            <input type ="hidden" name="lotacao" value="<?echo $lotacao; ?>">
                        <? }
                    } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Tipo de Falta</label>
                    <select class="form-control" style="max-width:300px;" name="codigofalta" id="codigofalta" style="color:blue;text-align:left;font-size:16px">
                        <option value="0" >Escolha</option>
                        <?
                        $SQL="SELECT * FROM RHcodigos where codRH='F1' or codRH='F2' order by codRH";
                        if ($regime=="CLT") {
                            $SQL="SELECT * FROM RHcodigos where codRH='F2' order by codRH";
                        }
                        $consulta1 = mysql_query($SQL);
                        while ($row1=mysql_fetch_array($consulta1)) { ?>
                            <option value="<? print $row1['codRH']; ?>" <? if($codigofalta==$row1['codRH'] || $regime=='CLT'){ ?> selected <? } ?>>
                                <? print $row1['descricaoRH']; ?>
                            </option>
                        <? } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Falta abonada para o dia</label>
                    <input class="form-control" style="max-width:200px;" type="date" name="datacompleta" id="datacompleta" value="<? print $ano; ?>-<? print $mes; ?>-<? print $dia; ?>">
                    <input class="form-control" name="dia" id="dia" type="hidden"  size="1" maxlength="2" value="<? print $dia; ?>" onkeydown="javascript:PulaCampo(event, this,'mes',2);" onkeyup="javascript:PulaCampo(event, this,'mes',2);">
                    <input class="form-control" name="mes" id="mes" type="hidden"  size="1" maxlength="2" value="<? print $mes; ?>" onkeydown="javascript:PulaCampo(event, this,'ano',2);" onkeyup="javascript:PulaCampo(event, this,'ano',2);">
                    <input class="form-control" name="ano" id="ano" type="hidden"  size="2" maxlength="4" value="<? print $ano; ?>" onkeydown="javascript:PulaCampo(event, this,'justificativa',4);" onkeyup="javascript:PulaCampo(event, this,'justificativa',4);">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Justificativa (obrigat&oacute;rio)</label>
                    <?
                    // Abre o campo "justificativa" para edicao somente se operacao de insercao (op=1) ou (alteracao por retorno op=4) ou se alteracao espontanea por parte do solicitante (op=6)
                    // Se a alteracao por parte do RH, a justificativa nao pode ser alterada.
                    if (($op==1 || $op==4) || ($op==6 && $codsolicitante==$coduser_conected)) { ?>
                        <textarea class="form-control" type="text" name="justificativa" id="justificativa" rows="3" maxlength="250"><? print $justificativa; ?></textarea>
                        <small id="emailHelp" class="form-text text-muted">
                            <strong>Importante: </strong>
                            &Eacute; necess&aacute;rio detalhar o motivo e n&atilde;o ser&aacute; aceito somente 'particular' ou 'pessoal'.
                            Digite no m&aacute;ximo 250 caracteres
                        </small>
                    <? } else {?>
                        <input type="hidden" name="justificativa" id="justificativa" value="<?echo $justificativa;?>">
                        <label for="exampleFormControlInput1"><? echo $justificativa; ?> </label>
                    <? } ?>
                </div>
                <? if ($nivel==3 && $obs && $op !=1) {?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Observa&ccedil;&ccedil;o do aprovador</label>
                        <label for="exampleFormControlInput1"><? echo $obs;?></label>
                    </div>
                <? }
                if($op==1 || $op==6 || $op=4 || ($status=="P" && $nivel == 3)) { ?>
                    <input class="btn btn-primary" type="button" name="Salvar" value="Salvar" onclick="enviar();">
                <? } ?>
                <? if($volta) { ?>
                    <input class="btn btn-primary" type="button" name="Voltar" value="Voltar" onclick="document.location='<? print $volta; ?>?pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?><? print $parametros;?>';">
                <? } ?>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </form>
        </div>
    </div>
</div>
</body>
</html>
