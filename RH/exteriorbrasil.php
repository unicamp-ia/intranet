<?php
$nivelcomp = -1;
include("../base/inicio.php"); /*Faz a verificacao se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/

?>

<script type="text/javascript" language="JavaScript">
	function enviar() {
            document.formulario.submit();
            return(true);
	}
 </script>

<?
include("menu.php");
?>
<div class="container">
    <h1>Solicita&ccedil;&atilde;o de afastamento - m&aacute;ximo 90 dias</h1>
</div>
<p></p>
<div class="container">
    <form action="solicitaoutrofastamento.php" method="post" name="formulario" enctype="multipart/form-data">
        <input type="hidden" name="op" id="op" value="1">
        <input type="hidden" name="exterior" id="exterior" value="E">
        <div class="form-group">
            <label for="exampleFormControlInput1">
                <div class="alert alert-info">
                    Esta solicita&ccedil;&atilde;o deve ser preenchida com 3 dias &uacute;teis de anteced&ecirc;ncia para viagens no Brasil e 7 dias &uacute;teis para viagens ao exterior.
                    O prazo de afastamento &eacute; de no m&aacute;ximo 90 dias.
                </div>
            </label>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <h2>
                        Viagem dentro do pa&iacute;s
                    </h2>
                    <div class="row">
                        <?php include "blocofalp.php"; ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <h2>Viagem ao exterior</h2>
                    <button type="submit" class="btn btn-primary">Continuar para viagem ao exterior</button>
                </div>
            </div>
        </div>
    </form>
</div>
<p></p>
<p></p>
<p></p>


</body>

