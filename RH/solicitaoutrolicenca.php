<?php
// Alterado por Edson Giordani em Outubro/2019 - Indicacao de Gestor Substituto
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Maio/2020 - Novo organograma ATU para CTUIA

$nivelcomp = -1;
include("../base/inicio.php"); /*Faz a verificacao se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
include("../base/FuncoesOrganograma.php");

$coduser = $_SESSION["coduser_conected"];
$volta = $_REQUEST['volta'];
$statuslista = $_REQUEST['statuslista'];
$obs = $_REQUEST['obs'];
$op = $_REQUEST['op'];
$cod = $_REQUEST['cod'];
$datainicio =  date("Y-m-d G:i:s");

$SQL = "Select * from tblusuarios where coduser='$coduser'";
$res = mysql_query($SQL);
$linha = mysql_fetch_array($res);
$nome = $linha['nome'];
$areabaseRequisitante = $linha['codarea'];
if (!$op || ($op <> 1 && $op <> 4 && $op <> 6)) {
    echo "Fluxo de execu&ccedil;&atilde;o desconhecido ou acesso indevido ao m&oacute;dulo RH - Licenca-premio. Operador n&atlde;o indicado.";
    die();
}
if($op==4 || $op==6) {
    if (!$cod) {
        echo "Fluxo de execu&ccedil;&atilde;o desconhecido ou acesso indevido ao m&oacute;dulo RH - licenca-premio. C&oacute;digo da solicita&ccedil;&atilde;o de licen&ccedil;a n&atilde;o encontrado!";
        die();
    }
    $SQL = "select r.*, u.nome from RHsolicitacoes r left join tblusuarios u on r.coduser=u.coduser where r.cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $status = $linha['status'];
    $coduser= $linha['coduser'];
    $codsolicitante= $linha['codsolicitante'];
    $nome= $linha['nome'];
    $obs = $linha['obs'];
    $codatendente= $linha['codatendente'];

    $SQL = "select *, date_format(datainicio, '%d/%m/%Y') AS datainicio1 from RHlicenca where cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $rg = $linha['rg'];
    $cargo = $linha['cargo'];
    $periodo = $linha['periodo'];
    $data = $linha['datainicio1'];
    $vect=explode("/",$data);
    $dia = $vect[0];
    $mes = $vect[1];
    $ano = $vect[2];
    $dias = $linha['dias'];
    $objetivo = $linha['objetivo'];
    $ehchefe = 'nao';
    $MATR_substituto = 0;
    if (($linha['MATR_substituto']!=0)&&($linha['MATR_substituto']!=NULL)) {
        $ehchefe = 'sim';
        $MATR_substituto = $linha['MATR_substituto'];
    }
}

$SQL = "Select codarea from tblusuarios where coduser='$coduser'";
$res = mysql_query($SQL);
$linha = mysql_fetch_array($res);
$lotacao = $linha['codarea'];
$departamento=$lotacao;
?>
<SCRIPT LANGUAGE="JavaScript">
    function checa_form(form){
        form.dia.value = form.datacompleta.value.substr(8,10);
        form.mes.value = form.datacompleta.value.substr(5,2);
        form.ano.value = form.datacompleta.value.substr(0,4);
        if (form.coduser.value == "0"){
            alert("Selecione o funcion\u00e1rio!!!");
            form.coduser.focus();
            return (false);
        }
        if (form.dia.value == ""){
            alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
            form.dia.focus();
            return (false);
        }
        if(isNaN(form.dia.value)) {
            alert("A data deve conter apenas n\u00fameros!");
            form.dia.focus();
            return (false);
        }
        if(form.dia.value > 31) {
            alert("Preencha corretamente a data! O dia deve ser menor ou igual do que 31.");
            form.diasaida.focus();
            return (false);
        }
        if (form.mes.value == ""){
            alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
            form.mes.focus();
            return (false);
        }
        if(isNaN(form.mes.value)) {
            alert("A data deve conter apenas n\u00fameros!");
            form.mes.focus();
            return (false);
        }
        if(form.mes.value > 12) {
            alert("Preencha corretamente a data! O m\u00eas deve ser menor ou igual do que 12.");
            form.messaida.focus();
            return (false);
        }
        if (form.ano.value == ""){
            alert("O preenchimento do campo data \u00e9 obrigat\u00f3rio !!!");
            form.ano.focus();
            return (false);
        }
        if(isNaN(form.ano.value)) {
            alert("A data deve conter apenas n\u00fameros!");
            form.ano.focus();
            return (false);
        }

        if ((form.ehchefe.value=="sim") && (form.MATR_substituto.value=="000000")) {
            alert("O substituto para o cargo deve ser informado.");
            form.MATR_substituto.focus();
            return (false);
        }

        var dtS = form.DataHoje.value;

        diaS = dtS.substring(8,10);
        mesS = dtS.substring(5,7);
        anoS  = dtS.substring(0,4);
        mesS=mesS-1;
        DataHoje=new Date(anoS,mesS,diaS);

        dia = form.dia.value;
        mes = form.mes.value;
        ano = form.ano.value;
        hora=0;
        minutos=0;
        DataAbonada=new Date(ano,mes-1,dia,hora,minutos);
        if (DataAbonada <=DataHoje) {
            alert("A solicita\u00e7\u00e3o de licen\u00e7a pr\u00eamio deve ser feita com no m\u00ednimo 1 (um) dia \u00fatil de anteced\u00eancia.");
            return(false);
        }
        //alert(DataHoje + " " + DataAbonada);
        return (true);
    }

    function enviar() {
        if(checa_form(document.formulario) == true)  {
            document.formulario.submit();
        }
        else {
            return(false);
        }
        return true;
    }
    function PulaCampo(ev, oOrg, pDest, vLen) {
        var key;
        if (window.event)
            key = window.event.keyCode;
        else if (ev)
            key = ev.which;
        else
            return true;
        if ((key==null) || (key==0) || (key==8) || (key==9) ||
            (key==13) || (key==16) || (key==17) || (key==18) ||
            (key==20) || (key==27) )
            return true;

        var oDest =document.getElementById(pDest);
        if(String(oOrg.value).length >= vLen) {
            oDest.focus();
        }
    }
</script>
<?
include("menu.php");
?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 offset-lg-3 offset-sm-3 offset-md-3">
            <?
            if($cod && $obs) { ?>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Observa&ccedil;&atilde;o</label>
                    <textarea class="form-control" name="obs" id="obs" rows="3" readonly><? print $obs; ?></textarea>
                </div>
            <? } ?>

            <form action="exibelicenca.php" method="post" name="formulario" >
                <input type="hidden" name="codsolicitante" id="codsolicitante" value="<? print $coduser; ?>">
                <input type="hidden" name="op" id="op" value="<? print $op; ?>">
                <input type="hidden" name="datainicio" id="datainicio" value="<? print $datainicio; ?>">
                <input type="hidden" name="tipo" id="tipo" value="L">
                <input type="hidden" name="status" id="status" value="A">
                <input type="hidden" name="cod" id="cod" value="<? print $cod; ?>">
                <input type="hidden" name="volta" id="volta" value="<? print $volta; ?>">
                <input type="hidden" name="statuslista" id="statuslista" value="<? print $statuslista; ?>">
                <input name="DataHoje" type="hidden" value="<? echo date("Y-m-d"); ?>">

                <div class="form-group">
                    <label for="exampleFormControlInput1"><b>Solicita&ccedil;&atilde;o de licen&ccedil;a pr&ecirc;mio</b></label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        <font color="red">
                            Esta solicita&ccedil;&atilde;o pode ser preenchida com at&eacute; 1 dia de anteced&ecirc;ncia, desde que efetuada at&eacute; 17h.
                        </font>
                    </label>
                </div>
                <div class="form-group">
                    <? if (($op==1 || $op==6) && $nivel==3) { ?>
                        <label for="exampleFormControlSelect1">Servidor</label>
                        <select class="form-control" name="coduser" id="coduser" style="color:blue;text-align:left;font-size:16px" <? if (!$nivel || $nivel < 1) {  echo "disabled=\"disabled\"";} ?> >
                            <option value="0">Escolha</option>
                            <?
                            $consulta1 = mysql_query("SELECT * FROM tblusuarios where status='A' and (tipo='FUNCIONARIO' || tipo='DOCENTE') order by nome");
                            while ($row1=mysql_fetch_array($consulta1)) {   ?>
                                <option value="<? print $row1['coduser']; ?>" <? if($coduser==$row1['coduser']){ ?> selected <? } ?>>
                                    <? print $row1['nome']; ?> </option>
                            <? } ?>
                        </select>
                    <? } else { ?>
                        <label for="exampleFormControlSelect1">Nome</label>
                        <label for="exampleFormControlSelect1"><? print $nome; ?></label>
                        <label for="exampleFormControlSelect1">Matr&iacute;cula</label>
                        <label for="exampleFormControlSelect1"><? print $coduser; ?></label>
                        <input name="coduser" id="coduser" type="hidden" value="<? print $coduser; ?>">
                    <? } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Lota&ccedil;&atilde;o</label>
                    <? if (($op==1 || $op==6) && $nivel==3) {
                        $SQL ="SELECT * FROM areas where status='A' and origem='I' and celula_instancia='I' and situacao_organograma='A' order by Descricao";
                        $resultado = mysql_query($SQL);
                        ?>
                        <select class="form-control" name="lotacao" style="color:blue;text-align:left;font-size:16px" <? if (!$nivel || $nivel < 1) {  echo "disabled=\"disabled\"";} ?>>
                            <option value="0">Escolha o Departamento</option>
                            <? while ($row1=mysql_fetch_array($resultado)) { ?>
                                <option value="<? print $row1['CodArea']; ?>" <? if($lotacao==$row1['CodArea']){ ?> selected <? } ?>>
                                    <? print $row1['Descricao']; ?>
                                </option>
                            <? } ?>
                        </select>
                    <? } else {
                        $SQL ="SELECT * FROM areas where CodArea='$lotacao' and origem='I'";
                        $resultado = mysql_query($SQL);
                        if ($row1=mysql_fetch_array($resultado)) {
                            ?>
                            <label for="exampleFormControlInput1"><? print $row1['Descricao']; ?></label>
                            <input type ="hidden" name="lotacao" value="<?echo $lotacao; ?>">
                        <? }
                    } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Data de in&iacute;cio da licen&ccedil;a</label>
                    <input class="form-control" style="max-width:200px;" type="date" name="datacompleta" id="datacompleta" value="<? print $ano; ?>-<? print $mes; ?>-<? print $dia; ?>">
                    <input class="form-control" name="dia" id="dia" type="hidden"  size="1" maxlength="2" value="<? print $dia; ?>" onkeydown="javascript:PulaCampo(event, this,'mes',2);" onkeyup="javascript:PulaCampo(event, this,'mes',2);">
                    <input class="form-control" name="mes" id="mes" type="hidden"  size="1" maxlength="2" value="<? print $mes; ?>" onkeydown="javascript:PulaCampo(event, this,'ano',2);" onkeyup="javascript:PulaCampo(event, this,'ano',2);">
                    <input class="form-control" name="ano" id="ano" type="hidden"  size="2" maxlength="4" value="<? print $ano; ?>" onkeydown="javascript:PulaCampo(event, this,'dias',4);" onkeyup="javascript:PulaCampo(event, this,'dias',4);">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Dias</label><BR>
                    <input aria-label="90" type="radio" name="dias" id="dias" value="90" <? if($dias=="90" || !$dias) { ?>checked<? } ?>>90&nbsp;&nbsp;&nbsp;&nbsp;
                    <input aria-label="75" type="radio" name="dias" id="dias" value="75" <? if($dias=="75") { ?>checked<? } ?>>75&nbsp;&nbsp;&nbsp;&nbsp;
                    <input aria-label="60" type="radio" name="dias" id="dias" value="60" <? if($dias=="60") { ?>checked<? } ?>>60&nbsp;&nbsp;&nbsp;&nbsp;
                    <input aria-label="45" type="radio" name="dias" id="dias" value="45" <? if($dias=="45") { ?>checked<? } ?>>45&nbsp;&nbsp;&nbsp;&nbsp;
                    <input aria-label="30" type="radio" name="dias" id="dias" value="30" <? if($dias=="30") { ?>checked<? } ?>>30&nbsp;&nbsp;&nbsp;&nbsp;
                    <input aria-label="15" type="radio" name="dias" id="dias" value="15" <? if($dias=="15") { ?>checked<? } ?>>15
                </div>
                <?
                // recupera matrÃ­cula do chefe da Ã¡rea do solicitante
                $SQL="Select MATR_chefe from areas where CodArea='$departamento' and status='A'";
                $func = mysql_query($SQL);
                if (mysql_num_rows($func) > 0) {
                    $res = mysql_fetch_array($func);
                    $coduser_chefe_requisitante=$res['MATR_chefe'];
                }
                // se o solicitante eh chefe, deve indicar um substituto
                if($coduser_chefe_requisitante==$coduser) { ?>
                    <div class="form-group">
                        <input type="hidden" name="ehchefe" id="ehchefe" value="sim">
                        <label for="exampleFormControlInput1"><b>Indicar substituto (obrigat&oacute;rio, somente para gestores)</b></label>
                        <select class="form-control" name="MATR_substituto" id="MATR_substituto">
                            <option value="000000" selected>Selecionar substituto</option>
                            <?
                            if (($coduser==$_SESSION["cod_diretor"]) || ($coduser==$_SESSION["cod_associado"])) {
                                $AREA_imediato="DIR";
                                if ($coduser==$_SESSION["cod_diretor"]) {
                                    $MATR_imediato=$_SESSION['cod_associado'];
                                } else {
                                    $MATR_imediato=$_SESSION['cod_diretor'];
                                }
                                $SQL = "SELECT coduser, nome FROM tblusuarios where coduser='".$MATR_imediato."'";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_imediato==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } elseif ($areabaseRequisitante=="CTUIA") {
                                // $areabaseRequisitante = $linha['codarea'];
                                $SQL = "SELECT coduser, nome FROM tblusuarios where coduser='".$_SESSION['cod_diretor']."' OR coduser='".$_SESSION['cod_associado']."'";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_imediato==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } else {
                                MontaHierarquia($_SESSION["codarea_conected"],'N','S','S');
                                $MATR_imediato=$hierarquia['acima']['matr'];
                                $AREA_imediato=$hierarquia['acima']['area'];
                                $NOME_imediato=$hierarquia['acima']['nome']; ?>
                                <option value="<?echo $MATR_imediato;?>"<?if ($MATR_substituto==$MATR_imediato) echo " selected";?>><?echo $NOME_imediato;?></option>
                                <?
                                if ($hierarquia['substituto_acima']['matr']) {?>
                                    <option value="<?echo $hierarquia['substituto_acima']['matr'];?>"<?if ($MATR_substituto==$hierarquia['substituto_acima']['matr']) echo " selected";?>><?echo $hierarquia['substituto_acima']['nome'];?></option>
                                <?}
                                $SQL = "SELECT coduser, nome FROM tblusuarios where status='A' and tipo!='INSTITUCIONAL' and codarea='".$departamento."' and coduser <> '".$coduser."' order by nome";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_substituto==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } ?>
                        </select>
                    </div>
                <? } else { ?>
                    <input type="hidden" name="ehchefe" id="ehchefe" value="nao">
                <? }
                if($op==1 || $op==6 || $op=4 || ($status=="P" && $nivel == 3)) { ?>
                    <input class="btn btn-primary" type="button" name="Salvar" value="Salvar" onclick="enviar();">
                <? } ?>
                <? if($volta) { ?>
                    <input class="btn btn-primary" type="button" name="Voltar" value="Voltar" onclick="document.location='<? print $volta; ?>?statuslista=<? print $statuslista; ?>'">
                <? } ?>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </form>
        </div>
    </div>
</div>

</body>
</html>
