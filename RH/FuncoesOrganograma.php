<?php
function MontaHierarquia($areabase,$area_base,$nivel_acima,$nivel_abaixo,$nivelDet='C') {
    
    // nivel_abaixo N: nï¿½o monta hierarquia das instancias abaixo
    //              T: monta a hierarquia de todas as instancias abaixo, em todos os nï¿½veis
    //              P: monta a hierarquia somente do primeiro nivel abaixo
    // nivelDet = C ->completo
    //            S ->Simples
    global $hierarquia;
    $hierarquia=array();
    $SQL="select a.CodArea,a.MATR_chefe,a.MATR_substituto,a.prerrogativas, a.codigo_orgao, u.nome,u.email from areas a left join tblusuarios u on a.MATR_chefe=u.coduser where a.origem='I' and a.CodArea='$areabase'";
    $res=mysql_query($SQL);
    
    if (mysql_num_rows($res) > 0) {
         // echo $SQL;
        $ln=mysql_fetch_array($res);
        // Se obter os dados do chefe do depto $areabase...
        if ($area_base=='S') {
            $hierarquia['chefia']['area_chefe']=$areabase;
            $hierarquia['chefia']['matr_chefe']=$ln['MATR_chefe'];
            $hierarquia['chefia']['nome_chefe']=$ln['nome'];
            $hierarquia['chefia']['email_chefe']=$ln['email'];
            // Se o RH indicou o nome de algum substituto temporï¿½rio para a area $areabase...
            
            if ($ln['MATR_substituto'] <> "000000" && $ln['prerrogativas']==1) {
                $SQL="select nome,email from tblusuarios where coduser= '" . $ln['MATR_substituto'] . "'";
                $rst=mysql_query($SQL);
               
                if ($ln1=mysql_fetch_array($rst)) {
                    $hierarquia['substituto_chefia']['matr_substituto']=$ln['MATR_substituto'];
                    $hierarquia['substituto_chefia']['nome_substituto']=$ln1['nome'];
                    $hierarquia['substituto_chefia']['email_substituto']=$ln1['email'];
                }
            }
        }
        $cd_orgaobase=$ln['codigo_orgao'];
        $parts_cd=explode(".",$cd_orgaobase);
        $np=count($parts_cd);
        // Se obter os dados do chefe hierarquico acima
        if ($nivel_acima=='S') {
            $cdh="";
            for ($ii=0;$ii<$np-1;$ii++) {
                if ($ii>0) $cdh .=".";
                $cdh .=$parts_cd[$ii];
            } 
           
            if ($cdh <> "") {
              $SQL="select u.nome,u.email,a.MATR_chefe,a.CodArea,a.MATR_substituto,a.prerrogativas from areas a left join tblusuarios u on a.MATR_chefe=u.coduser where a.codigo_orgao='$cdh'";
              $res=mysql_query($SQL);
             // echo $SQL;
              if ($ln=mysql_fetch_array($res)) {
                $hierarquia['acima']['area_chefe']=$ln['CodArea'];
                $hierarquia['acima']['matr_chefe']=$ln['MATR_chefe'];
                $hierarquia['acima']['nome_chefe']=$ln['nome'];
                $hierarquia['acima']['email_chefe']=$ln['email'];
                // Se o RH indicou o nome de algum substituto temporï¿½rio para a area de hierarquia acima...
                if ($ln['MATR_substituto'] <> "000000" && $ln['prerrogativas']==1  && $nivelDet=='C') {
                    $SQL="select nome,email from tblusuarios where coduser= '" . $ln['MATR_substituto'] . "'";
                    $rst=mysql_query($SQL);
                    if ($ln1=mysql_fetch_array($rst)) {
                        $hierarquia['substituto_acima']['matr_substituto']=$ln['MATR_substituto'];
                        $hierarquia['substituto_acima']['nome_substituto']=$ln1['nome'];
                        $hierarquia['substituto_acima']['email_substituto']=$ln1['email'];
                    }
                }
              } else {
                  return("NE");
              }
            }
        }
        if ($nivel_abaixo=='P' || $nivel_abaixo=='T') {
            $ll=strlen($cd_orgaobase);
            if ($nivel_abaixo=='T') {
                $SQL="SELECT aa.*,uu.nome,uu.email from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser where  SUBSTRING(aa.codigo_orgao,1," . $ll . ")='" . $cd_orgaobase . "' and aa.codigo_orgao <> '$cd_orgaobase' and aa.status='A' and aa.situacao_organograma='A' and aa.origem='I'";
            }else {
                $t=strlen($cd_orgaobase) + 3;
                $SQL="SELECT aa.*,uu.nome,uu.email from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser where LENGTH(aa.codigo_orgao)=$t and aa.codigo_orgao like '$cd_orgaobase%' and aa.status='A' and aa.situacao_organograma='A' and aa.origem='I'";   
            }
            $rst=mysql_query($SQL);
            //SELECT aa.*,uu.nome,uu.email from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser where LENGTH(aa.codigo_orgao)=5 and aa.codigo_orgao like '17%' and aa.status='A' and aa.situacao_organograma='A' and aa.origem='I'
            //echo $SQL;
           // echo mysql_num_rows($rst);
            while ($ln1=mysql_fetch_array($rst)) {
               // echo "<br>" . $ln1['CodArea'];
                $hierarquia['abaixo'][]=array('area_chefe'=>$ln1['CodArea'],'matr_chefe'=>$ln1['MATR_chefe'],'nome_chefe'=>$ln1['nome'],'email_chefe'=>$ln1['email']);
                // Se o RH indicou o nome de algum substituto temporï¿½rio para a area de hierarquia acima...
                if ($ln1['MATR_substituto'] <> "000000" && $ln1['prerrogativas']==1  && $nivelDet=='C') {
                    $SQL="select nome,email from tblusuarios where coduser= '" . $ln1['MATR_substituto'] . "'";
                    $rst1=mysql_query($SQL);
                    if ($ln2=mysql_fetch_array($rst1)) {
                        $hierarquia['substituto_abaixo'][]=array('area_chefe'=>$ln2['CodArea'],'matr_chefe'=>$ln2['MATR_chefe'],'nome_chefe'=>$ln2['nome'],'email_chefe'=>$ln2['email']);
                    }
                }
            }            
            //print_r($hierarquia);//echo $SQL;
        }
        return("OK");
    } else {
        return ("NE");
    }
}
//SELECT aa.*, uu.nome,uu.email  from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser where SUBSTRING(aa.codigo_orgao,1,5)='17.30' and aa.codigo_orgao <> '17.30' and aa.status='A' and aa.situacao_organograma='A'
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
// Monta a hierarquia da area para baixo
function MontaHierarquiaSimples($areabase) {
    global $hierarquia;
    $hierarquia=array();
    $SQL="select a.CodArea,a.MATR_chefe,a.MATR_substituto,a.prerrogativas, a.codigo_orgao, u.nome,u.email from areas a left join tblusuarios u on a.MATR_chefe=u.coduser where a.origem='I' and a.CodArea='$areabase'";
    $res=mysql_query($SQL);
    if (mysql_num_rows($res) > 0) {
        $ln=mysql_fetch_array($res);
        // Se obter os dados do chefe do depto $areabase...
        $hierarquia['chefia']['area_chefe']=$areabase;
        $hierarquia['chefia']['matr_chefe']=$ln['MATR_chefe'];
        $hierarquia['chefia']['nome_chefe']=$ln['nome'];
        $hierarquia['chefia']['email_chefe']=$ln['email'];
        $cd_orgaobase=$ln['codigo_orgao'];
        
        // Se obter os dados do chefe hierarquico acima
        $ll=strlen($cd_orgaobase);
        $SQL="SELECT aa.*,uu.nome,uu.email from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser where  SUBSTRING(aa.codigo_orgao,1," . $ll . ")='" . $cd_orgaobase . "' and aa.codigo_orgao <> '$cd_orgaobase' and aa.status='A' and aa.origem='I' and aa.situacao_organograma='A'";
        $rst=mysql_query($SQL);
        //echo $SQL;
        while ($ln1=mysql_fetch_array($rst)) {
           $hierarquia['abaixo'][]=array('area_chefe'=>$ln1['CodArea'],'matr_chefe'=>$ln1['MATR_chefe'],'nome_chefe'=>$ln1['nome'],'email_chefe'=>$ln1['email']);
        }
    }
      
}
function ExtraiaAreas() {
    global $ArrAreas;
    global $hierarquia;
   
    if (count($hierarquia) > 0) {
         $ArrAreas=array();
        if  ($hierarquia['chefia']['matr_chefe']) {
            $ArrAreas[]= $hierarquia['chefia']['area_chefe'];
           
        }
        if (is_array($hierarquia['abaixo'])) {
            $nh=count($hierarquia['abaixo']);
            for ($lk=0;$lk<$nh;$lk++) {
              $ArrAreas[]=$hierarquia['abaixo'][$lk]['area_chefe'];
            }
        }
       //  echo "cc=" . count($ArrAreas);
    }
     
}
?>
