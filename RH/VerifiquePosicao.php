<?php
//
//      Intranet VerifquePosicao
//      ---------------------------
// Verifica a posicao do usuario no organograma para a instancia em que ele eh lotado.
// EoChefe = true indica que ele eh chefe do departamento ao qual ele eh lotado 
// Echefe = true indica que ele eh chefe de outro departamento que nao aquele onde ele estah lotado.
// EoSusbtituto = true indica que ele eh substituto de algum departamento. Neste caso as matriculas para as quais ele deve
//                aprovar sao armazenadas no array aprovadores_substituto
// $hierarquia_abaixo: A mont somente as areas de aprovacao  
//                     C monta somente o array com os chefes
//                     T monta ambos 
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador

include_once("../base/FuncoesOrganograma.php");
if (!$instancias_abaixo) $instancias_abaixo="N";
MontaHierarquia($codarea_conected,'S','N',$instancias_abaixo);
$areas_substituto=array();
$aprovadores_substituto=array();
$areas_aprovacao=array();
$Echefe=false;
$EoChefe=false;
if (!$hierarquia_abaixo) $hierarquia_abaixo='A';
if ($hierarquia['chefia']['matr']==$coduser_conected) {
    $EoChefe=true;
    $areas_aprovacao[]=$codarea_conected;
    if (is_array($hierarquia['abaixo'])) {
        $chefes_abaixo=array();
        $nh=count($hierarquia['abaixo']);
        for ($lk=0;$lk<$nh;$lk++) {
          if ($hierarquia_abaixo=='T' || $hierarquia_abaixo=='A') {
              $areas_aprovacao[]=$hierarquia['abaixo'][$lk]['area'];
          }
          if ($hierarquia_abaixo=='T' || $hierarquia_abaixo=='C') {
              $chefes_abaixo[]=$hierarquia['abaixo'][$lk]['matr'];
          }
        }
    }
}

//Verifica se o usuario eh chefe de algum outro departamento, que nao o departamento ao qual ele pertence
$SQL="select * from areas where MATR_chefe='$coduser_conected' and CodArea <> '$codarea_conected' and status='A' and origem='I'";
$res=mysql_query($SQL);
if (mysql_num_rows($res) > 0) {
    $Echefe=true;
    while ($linha=mysql_fetch_array($res)) {
        $areas_aprovacao[]=$linha['CodArea'];
    }
}

if(isset($_SESSION["cod_secretario"]) and $_SESSION["cod_secretario"]==$coduser_conected) {
    $EoSecretario=true;
}
// Verifique em quais departamentos o usuario eh substituto, se for o caso;
$SQL="select * from areas where MATR_substituto = '$coduser_conected' and prerrogativas=1 and status='A' and origem='I'";
$res1=mysql_query($SQL);
$ns=0;
if (mysql_num_rows($res1) > 0) {
    $EoSubstituto=true;
    while ($linha=mysql_fetch_array($res1)) {
        $aprovadores_substituto[]=$linha['MATR_chefe'];
        $areas_substituto[]=$linha['CodArea'];
    }
}
