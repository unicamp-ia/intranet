<?php
// Alterado por Edson Giordani em Outubro/2019 - Indicacao de Gestor Substituto
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Maio/2020 - Novo organograma ATU para CTUIA

$nivelcomp = -1;
include("../base/inicio.php"); /* Faz a verificação se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html */
include("../base/FuncoesOrganograma.php");
// echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"/intranet/css/estilo_dan_01.css\">";
?>
<script type="text/javascript" language="JavaScript">
    function openModal(pUrl, pWidth, pHeight) {
        if (window.showModalDialog) {
            return window.showModalDialog(pUrl, window,
                "dialogWidth:" + pWidth + "px;dialogHeight:" + pHeight + "px");
        } else {
            try {
                netscape.security.PrivilegeManager.enablePrivilege(
                    "UniversalBrowserWrite");
                window.open(pUrl, "wndModal", "width=" + pWidth + ",height=" + pHeight + ",resizable=yes,modal=yes,top=150,left=300");
                return true;
            }
            catch (e) {
                alert("Script n&atilde;o confi&aacute;vel, n&atilde;o &eacute; poss&iacute;vel abrir janela modal.");
                return false;
            }
        }
    }
</script>
<?
$browser=$_SESSION['browser'];
$volta = $_REQUEST['volta'];
$busca = $_REQUEST['busca'];/*Codigo para nao perder o filtro na busca. */
$pagina=$_REQUEST['pagina'];
$num_linhas=$_REQUEST['num_linhas'];
if($busca) {
    $tipo = $_REQUEST['tipo'];
    $nome = $_REQUEST['nome'];
    $CodArea = $_REQUEST['CodArea'];
    $diainicad = $_REQUEST['diainicad'];
    $mesinicad = $_REQUEST['mesinicad'];
    $anoinicad = $_REQUEST['anoinicad'];
    $diafimcad = $_REQUEST['diafimcad'];
    $mesfimcad = $_REQUEST['mesfimcad'];
    $anofimcad = $_REQUEST['anofimcad'];
    $statusbusca=$_REQUEST['statusbusca'];
    $parametros = "&busca=".$busca."&tipo=".$tipo."&nome=".$nome."&CodArea=".$CodArea."&diainicad=".$diainicad."&mesinicad=".$mesinicad."&anoinicad=".$anoinicad."&diafimcad=".$diafimcad."&mesfimcad=".$mesfimcad."&anofimcad=".$anofimcad."&statusbusca=".$statusbusca;
}
$op = $_REQUEST['op'];
if (!$op || ($op <> 1 && $op <> 4 && $op <> 6)) {
    echo "Fluxo de execu&ccedil;&atilde;o desconhecido ou acesso indevido ao m&oacute;dulo RH - Afastamento. Operador n&atilde;o indicado.";
    die();
}
$cod = $_REQUEST['cod'];

$datainicio = $_REQUEST['datainicio'];
if(!$datainicio) {
    $datainicio =  date("Y-m-d G:i:s");
}
$rg = $_REQUEST['rg'];
$tipo = $_REQUEST['tipo'];
$cargo = $_REQUEST['cargo'];
$lotacao = $_REQUEST['lotacao'];

$datasaida = $_REQUEST['datasaida'];
$dataretorno = $_REQUEST['dataretorno'];
$obs = $_REQUEST['obs'];
$objetivo = $_REQUEST['objetivo'];
$cidade = $_REQUEST['cidade'];
$financiadora = $_REQUEST['financiadora'];
$outro = $_REQUEST['outro'];
if($objetivo=="-1") { /* Se selecionou outro objetivo não concatena. */
    $objetivo = $outro;
} else if($outro) { /* Se não selecionou outro objetivo concatena. E há algun texto na variável outro. */
    $objetivo = $objetivo.". ".$outro;
}
$coduser = $_REQUEST['coduser'];
if(!$coduser) {
    $coduser = $_SESSION["coduser_conected"];
}
$exterior = $_REQUEST['exterior'];

$SQL = "Select * from tblusuarios where coduser='$coduser'";
$res = mysql_query($SQL);
$linha = mysql_fetch_array($res);
$nome = $linha['nome'];
$areabaseRequisitante = $linha['codarea'];
if (!$rg) {
    $rg=$linha['rg'];
}
if($op==4 || $op==6) { //Mostra o formulario para alteracao
    if (!$cod) {
        echo "Fluxo de execu&ccedil;&atilde;o desconhecido ou acesso indevido ao m&oacute;dulo RH - Afastamento. Coacute;digo da solicita&ccedil;&atilde;o de ve&iacute;culo n&atilde;o encontrado!";
        die();
    }
    $SQL = "select r.*, u.nome from RHsolicitacoes r left join tblusuarios u on r.coduser=u.coduser where r.cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $status = $linha['status'];
    $coduser= $linha['coduser'];
    $codsolicitante= $linha['codsolicitante'];
    $obs = $linha['obs'];
    $tipo = $linha['tipo'];
    $codatendente= $linha['codatendente'];
    $lotacao = $linha['lotacao'];
    $departamento=$lotacao;
    $nome = $linha['nome'];

    $SQL = "select *, date_format(dataretorno, '%d/%m/%Y') as dataretorno, date_format(datasaida, '%d/%m/%Y') as datasaida from RHafastamento where cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $rg = $linha['rg'];
    $cargo = $linha['cargo'];
    $datasaida = $linha['datasaida'];

    $dataretorno = $linha['dataretorno'];
    $objetivo = $linha['objetivo'];
    $cidade = $linha['cidade'];
    $exterior = $linha['exterior'];
    $email_voucher=$linha['email_voucher'];
    $cc_email_voucher=$linha['cc_email_voucher'];
    $celular_voucher=$linha['celular_voucher'];
    $nome_contato_sinistro=$linha['nome_contato_sinistro'];
    $celular_contato_sinistro=$linha['celular_contato_sinistro'];
    $financiadora = $linha['financiadora'];
    $arquivo_convite = $linha['arquivo_convite'];
    $arquivo_convite2 = $linha['arquivo_convite2'];
    $ehchefe = 'nao';
    $MATR_substituto = 0;
    if (($linha['MATR_substituto']!=0)&&($linha['MATR_substituto']!=NULL)) {
        $ehchefe = 'sim';
        $MATR_substituto = $linha['MATR_substituto'];
    }
}
if($op==5) {
    if($cod) {
        $op=4;
        $status="U";
    } else {
        $op=1;
    }
}

if(!$lotacao) {
    $SQL = "Select * from tblusuarios where coduser='$coduser'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $lotacao = $linha['codarea'];
    $departamento=$lotacao;
}
?>
<SCRIPT LANGUAGE="JavaScript">
    function ChecaAntecedencia() {
        var ARext = document.formulario.exterior;

        var DataAtual=new Date();
        var DataAntes=new Date();
        var DataAgendamento=new Date();

        var dtS = document.formulario.DataSistema.value;
        diaS = dtS.substring(0,2);
        mesS = dtS.substring(3,5);
        anoS  = dtS.substring(6,10);
        horaS  = dtS.substring(11,13);
        minutoS = dtS.substring(14);

        mesS=mesS-1;
        DataAtual=new Date(anoS,mesS,diaS,horaS, minutoS);
        DataAntes=new Date(anoS,mesS,diaS,horaS, minutoS);
        var dia = document.formulario.datasaida.value.substring(0,2);
        var mes = document.formulario.datasaida.value.substring(3,5);
        var ano = document.formulario.datasaida.value.substring(6,10);
        var hora=0;
        var minutos=0;
        var sd=0;
        var sh=0;
        DataAgendamento=new Date(ano,mes-1,dia,hora,minutos);

        if (DataAgendamento <= DataAtual) {
            alert("Data do afastamento n\u00e3o pode ser menor ou igual a data atual.");
            return false;
        }
        /* O controle de antececencia agora eh feito no modulo exibeafastamento.php */
        return true;


    }
    function validarData(campo){
        var expReg = /^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$/;
        var msgErro = 'Formato inv\u00e1lido de data.';
        var ARext = document.formulario.exterior;
        if (ARext[0].checked==false &&  ARext[1]==false ) {
            alert("Informe o tipo de viagem.");
            return false;
        }
        if ((campo.value.match(expReg)) && (campo.value!='')){
            var dia = campo.value.substring(0,2);
            var mes = campo.value.substring(3,5);
            var ano = campo.value.substring(6,10);
            if((mes==4 || mes==6 || mes==9 || mes==11) && dia > 30){
                alert("Dia incorreto !!! O m\u00eas especificado cont\u00e9m no m\u00e1ximo 30 dias.");
                return false;
            } else{
                if(ano%4!=0 && mes==2 && dia>28){
                    alert("Data incorreta!! O m\u00eas especificado cont\u00e9m no m\u00e1ximo 28 dias.");
                    return false;
                } else{
                    if(ano%4==0 && mes==2 && dia>29){
                        alert("Data incorreta!! O m\u00eas especificado cont\u00e9m no m\u00e1ximo 29 dias.");
                        return false;
                    } else{
                        if (campo.name=='datasaida') {
                            if (ChecaAntecedencia()==false) {
                                return(false);
                            }
                        }
                        return true;
                    }
                }
            }
        } else {
            alert(msgErro);
            campo.focus();
            return false;
        }

    }

    function checa_form(form){
        if (form.coduser.value == "0"){
            alert("Selecione o servidor!!!");
            form.coduser.focus();
            return (false);
        }
        if (form.rg.value == ""){
            alert("O preenchimento do campo RG \u00e9 obrigat\u00f3rio !!!");
            form.rg.focus();
            return (false);
        }
        if (form.cargo.value == ""){
            alert("O preenchimento do campo cargo/fun\u00e7\u00e3o \u00e9 obrigat\u00f3rio !!!");
            form.cargo.focus();
            return (false);
        }
        if (form.lotacao.value == ""){
            alert("O preenchimento do campo lota\u00e7\u00e3o \u00e9 obrigat\u00f3rio !!!");
            form.lotacao.focus();
            return (false);
        }
        if (form.datasaida.value == ""){
            alert("O preenchimento do campo data de sa\u00edda \u00e9 obrigat\u00f3rio !!!");
            form.datasaida.focus();
            return (false);
        }
        if(!validarData(form.datasaida)) {
            form.datasaida.focus();
            return (false);
        }
        if (form.dataretorno.value == ""){
            alert("O preenchimento do campo data de retorno \u00e9 obrigat\u00f3rio !!!");
            form.dataretorno.focus();
            return (false);
        }
        if(!validarData(form.dataretorno)) {
            form.dataretorno.focus();
            return (false);
        }
        if (form.objetivo.value == "0"){
            alert("Selecione um objetivo");
            form.objetivo.focus();
            return (false);
        }
        if(form.objetivo.value=="-1" && form.outro.value=="") {
            alert("Digite um objetivo");
            form.outro.focus();
            return (false);
        }
        if (form.cidade.value == ""){
            alert("O preenchimento do campo cidade/pa\u00eds \u00e9 obrigat\u00f3rio !!!");
            form.cidade.focus();
            return (false);
        }
        ARobj = document.formulario.exterior;
        if (ARobj.value=='E'){
            var vv=0;
            ev=form.email_voucher.value;
            form.email_voucher.value=ev.trim();
            ev=form.cc_email_voucher.value;
            form.cc_email_voucher.value=ev.trim();
            ev=form.celular_voucher.value;
            form.celular_voucher.value=ev.trim();
            if (form.email_voucher.value=="") {
                vv=1;
            }
            if (form.cc_email_voucher.value=="") {
                vv=1;
            }
            if (form.celular_voucher.value=="") {
                vv=1;
            }
            if (vv==1) {
                alert("Por favor, preencha todos os campos referentes ao Voucher do Seguro!!");
                return (false);
            }
            ev=form.nome_contato_sinistro.value;
            form.nome_contato_sinistro.value=ev.trim();
            ev=form.celular_contato_sinistro.value;
            form.celular_contato_sinistro.value=ev.trim();
            if (form.nome_contato_sinistro.value=="") {
                vv=1;
            }
            if (form.celular_contato_sinistro.value=="") {
                vv=1;
            }
            if (vv==1) {
                alert("Por favor, preencha todos os campos referentes ao Contato para Sinistro!!");
                return (false);
            }
        }

        if ((form.ehchefe.value=="sim") && (form.MATR_substituto.value=="000000")) {
            alert("O substituto para o cargo deve ser informado.");
            form.MATR_substituto.focus();
            return (false);
        }

        return (true);
    }

    function enviar() {
        // Procedimento para verificar se o prazo de afastamento esta dentro do limite de 90 dias
        var dtS = document.formulario.datasaida.value;

        diaS = dtS.substring(0,2);
        mesS = dtS.substring(3,5);
        anoS  = dtS.substring(6,10);
        mesS=mesS-1;
        dini=new Date(anoS,mesS,diaS);
        dini90=new Date(anoS,mesS,diaS);
        dini90.setDate(dini90.getDate()+90);

        var dtS = document.formulario.dataretorno.value;
        diaS = dtS.substring(0,2);
        mesS = dtS.substring(3,5);
        anoS  = dtS.substring(6,10);
        mesS=mesS-1;
        dfim=new Date(anoS,mesS,diaS);
        //alert(dini90 +  " df= " + dfim);
        if (dfim>dini90) {
            alert('O prazo m\u00e1ximo para afastamento n\u00e3o pode ultrapassar os 90 dias.');
            return(false);
        }
        // Verifica se a data de solicitacao antecede os 3 dias para interno ou 7 dias para exterior
        // Faz uma verificacao apenas com relacao ao numero de dias, nao considera os dias uteis. Isso
        // sera feito em xibe afastamento
        var oper=document.formulario.op.value;
        if (oper=="6") {
            var da_a=document.formulario.datasaida_anterior.value;
            var dr_a=document.formulario.dataretorno_anterior.value;
        }
        var da=document.formulario.datasaida.value;
        var dr=document.formulario.dataretorno.value;

        //alert(da_a + " " + dr_a);

        if ((oper == "6" && (da != da_a || dr != dr_a)) || oper !="6") {
            // alert('A');
            if (ChecaAntecedencia()==false) {
                return(false);
            }
        }
        //alert(document.formulario.datasaida_anterior.value);
        //return(false);
        if(checa_form(document.formulario) == true)  {
            document.formulario.submit();
        } else {
            return(false);
        }
        ARobj = document.formulario.exterior;
        if (ARobj.value=='E' && document.formulario.aviso_traducao.value != "1"){
            alert('ATEN\u00c7\u00c3O!! A carta convite deve, necessariamente, ser traduzida para o portugu\u00eas!');
        }
        document.formulario.submit();
        return(true);
    }
    function remover_upload(NF) {
        document.formulario.op.value="2";
        document.formulario.nUP.value=NF;
        // alert(document.formulario.op.value + " " + document.formulario.nUP.value );
        document.formulario.submit();
        return(true);
    }

    function recarrega() {
        form = document.formulario;
        form.op.value="5";
        form.action='solicitaoutrofastamento.php';
        form.submit();
    }

    function TrataExterior(opt)
    {
        //var divExterior = document.getElementById("exterior");
        var divVoucherSeguro = document.getElementById("voucher_seguro");
        var divSinistro = document.getElementById("sinistro_contato");

        //if (ARobj[1].checked) {
        //    alert(ARobj.value);
        //}
        if(opt.value=='E') {
            //divExterior.style.display="";
            divVoucherSeguro.style.display="";
            divSinistro.style.display="";
        } else  {
            //divExterior.style.display="none";
            divVoucherSeguro.style.display="none";
            divSinistro.style.display="none";
        }
    }
    function clearFileInputField(tagId) {
        document.getElementById(tagId).innerHTML =
            document.getElementById(tagId).innerHTML;
    }
</script>

<?
include("menu.php");
?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 offset-lg-3 offset-sm-3 offset-md-3">
            <b>Solicita&ccedil;&atilde;o de afastamento - m&aacute;ximo 90 dias</b>
        </div>
    </div>
</div>
<p></p>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 offset-lg-3 offset-sm-3 offset-md-3">
            <form action="exibeafastamento.php" method="post" name="formulario" enctype="multipart/form-data">
                <input type="hidden" name="codsolicitante" id="codsolicitante" value="<? print $coduser; ?>">
                <input type="hidden" name="op" id="op" value="<? print $op; ?>">
                <input type="hidden" name="datainicio" id="datainicio" value="<? print $datainicio; ?>">
                <input type="hidden" name="tipo" id="tipo" value="A">
                <input type="hidden" name="status" id="status" value="A">
                <input type="hidden" name="cod" id="cod" value="<? print $cod; ?>">
                <input type="hidden" name="volta" id="volta" value="<? print $volta; ?>">
                <input type="hidden" name="parametros" id="parametros" value="<? print $parametros; ?>">
                <input type="hidden" name="pagina" id="pagina" value="<? print $pagina; ?>">
                <input type="hidden" name="num_linhas" id="num_linhas" value="<? print $num_linhas; ?>">
                <input type="hidden" name="busca" id="busca" value="<? print $busca; ?>">
                <input type="hidden" name="aviso_traducao" id="aviso_traducao" value="">
                <input name="DataSistema" type="hidden" value="<? echo  date("d-m-Y H:i"); ?>">
                <?if ($op==4 || $op==6) {?>
                    <input type="hidden" name="datasaida_anterior" id="datasaida_anterior" value="<? print $datasaida; ?>">
                    <input type="hidden" name="dataretorno_anterior" id="dataretorno_anterior" value="<? print $dataretorno; ?>">
                    <input type="hidden" name="exterior_anterior" id="dataretorno_anterior" value="<? print $exterior; ?>">
                <?}?>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        <font color="red">Esta solicita&ccedil;&atilde;o deve ser preenchida com 3 dias &uacute;teis de anteced&ecirc;ncia para viagens no Brasil e 7 dias &uacute;teis para viagens ao exterior.
                            O prazo de afastamento &eacute; de no m&aacute;ximo 90 dias.</a></b>
                        </font>
                    </label>
                </div>
                <div class="form-group">
                    <input type="radio" name="exterior" value="N" disabled>
                    <input type="radio" name="exterior" value="E" checked><font class="fontetitulo">Viagem ao exterior</font>
                </div>
                <div class="form-group">
                    <? if(($op==1 || $op==6) && $nivel==3) { ?>
                        <label for="exampleFormControlSelect1">Colaborador</label>
                        <? if ($nivel==3) { ?>
                            <select class="form-control" name="coduser" id="coduser">
                                <option value="0">Escolha</option>
                                <? $consulta1 = mysql_query("SELECT * FROM tblusuarios where status='A' and (tipo='FUNCIONARIO' || tipo='DOCENTE') order by nome");
                                while ($row1=mysql_fetch_array($consulta1)) {   ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($coduser==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? } ?>
                            </select>
                        <? }
                    } else { ?>
                        <label for="exampleFormControlSelect1">Matr&iacute;cula</label>
                        <label for="exampleFormControlSelect1"><? print $coduser; ?></label>
                        <input name="coduser" id="coduser" type="hidden" value="<? print $coduser; ?>">
                    <? } ?>
                    <? if($op!=1) { ?>
                        <label for="exampleFormControlSelect1">Nome</label>
                        <label for="exampleFormControlSelect1"><? print $nome; ?></label>
                    <? } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">RG</label>
                    <? if($op==1 || $status=="U" || ($status=="P" && $nivel>2)) { ?>
                        <input class="form-control" name="rg" id="rg" type="text" size="10" maxlength="20" value="<? print $rg; ?>">
                    <? } else { ?>
                        <label for="exampleFormControlSelect1"><? print $rg; ?></label>
                        <input name="rg" id="rg" type="hidden" size="10" maxlength="20" value="<? print $rg; ?>">
                    <? } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Cargo/Fun&ccedil;&atilde;o</label>
                    <? if($op==1 || $status=="U" || ($status=="P" && $nivel>2)) { ?>
                        <input class="form-control" name="cargo" id="cargo" type="text" size="30" maxlength="50" value="<? print $cargo; ?>">
                    <? } else { ?>
                        <label for="exampleFormControlSelect1"><? print $cargo; ?></label>
                        <input name="cargo" id="cargo" type="hidden" size="30" maxlength="50" value="<? print $cargo; ?>">
                    <? } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Lota&ccedil;&atilde;o</label>
                    <? if(($op==1 || $op==6) && $nivel==3) {
                        $SQL ="SELECT * FROM areas where status='A' and origem='I' and celula_instancia='I' order by Descricao";
                        $resultado = mysql_query($SQL);
                        ?>
                        <select class="form-control" name="lotacao">
                            <option value="0">Escolha o Departamento</option>
                            <? while ($row1=mysql_fetch_array($resultado)) { ?>
                                <option value="<? print $row1['CodArea']; ?>" <? if($lotacao==$row1['CodArea']){ ?> selected <? } ?>>
                                    <? print $row1['Descricao']; ?>
                                </option>
                            <? } ?>
                        </select>
                    <? } else {
                        $SQL ="SELECT * FROM areas where CodArea='$lotacao' and origem='I'";
                        $resultado = mysql_query($SQL);
                        if ($row1=mysql_fetch_array($resultado)) { ?>
                            <label for="exampleFormControlSelect1"><? print $row1['Descricao']; ?></label>
                            <input type ="hidden" name="lotacao" value="<?echo $lotacao; ?>">
                        <? }
                    } ?>
                </div>
                <div class="form-group border rounded">
                    <label for="exampleFormControlSelect1">Per&iacute;odo do afastamento</label><br/>
                    <label for="exampleFormControlSelect1">&nbsp;&nbsp;Data Sa&iacute;da:</label>
                    <input class="form-control" style="max-width:200px;" placeholder="dd/mm/aaaa" name="datasaida" id="datasaida" type="text" size="10" maxlength="10" onChange="validarData(this)" value="<? print $datasaida; ?>">
                    <label for="exampleFormControlSelect1">&nbsp;&nbsp;Data Retorno:</label>
                    <input class="form-control" style="max-width:200px;" placeholder="dd/mm/aaaa" name="dataretorno" id="dataretorno" type="text" size="10" maxlength="10" onChange="validarData(this);" value="<? print $dataretorno; ?>">
                    <!--input class="form-control" placeholder="dd/mm/aaaa" name="dataretorno" id="dataretorno" type="text" size="10" maxlength="10" onChange="validarData(this);" onfocus="ChecaAntecedencia();" value="< ? print $dataretorno; ?>"!-->
                </div>
                <?if ($exterior=='E') { ?>
                    <div class="form-group border rounded">
                        <label for="exampleFormControlSelect1">Voucher de seguro</label><br/>
                        <label for="exampleFormControlSelect1">&nbsp;&nbsp;e-mail p/ envio</label>
                        <input class="form-control" style="max-width:260px;" name="email_voucher" id="email_voucher" type="text" size="40" maxlength="60" value="<?echo $email_voucher;?>">
                        <label for="exampleFormControlSelect1">&nbsp;&nbsp;CC (Com c&oacute;pia)</label>
                        <input class="form-control" style="max-width:260px;" name="cc_email_voucher" id="cc_email_voucher" type="text" size="40" maxlength="60" value="<?echo $cc_email_voucher;?>">
                        <label for="exampleFormControlSelect1">&nbsp;&nbsp;Celular p/contato</label>
                        <input class="form-control" style="max-width:260px;" name="celular_voucher" id="celular_voucher" type="text" size="15" maxlength="15" value="<?echo $celular_voucher;?>">
                    </div>
                    <div class="form-group border rounded">
                        <label for="exampleFormControlSelect1">Contato p/ sinistro</label><br/>
                        <label for="exampleFormControlSelect1">&nbsp;&nbsp;Nome</label>
                        <input class="form-control" style="max-width:260px;" name="nome_contato_sinistro" id="nome_contato_sinistro" type="text" size="50" maxlength="60" value="<?echo $nome_contato_sinistro;?>">
                        <label for="exampleFormControlSelect1">&nbsp;&nbsp;Celular</label>
                        <input class="form-control" style="max-width:260px;" name="celular_contato_sinistro" id="celular_contato_sinistro" type="text" size="15" maxlength="15" value="<?echo $celular_contato_sinistro;?>">
                    </div>
                <? } ?>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">
                        Finalidade<br/>
                        (caso participe de mais de um evento, dever&aacute; ser indicado per&iacute;odo e local - cidade e pa&iacute;s - de cada um deles.)
                    </label>
                    <? if($op==1 || $op==6 || $status=="U" || ($status=="P" && $nivel==3)) {
                        $SQL ="SELECT * FROM RHobjetivos where tipo_afastamento='A-' order by texto";
                        $resultado = mysql_query($SQL);
                        $outro=true;
                        ?>
                        <select class="form-control" name="objetivo" id="objetivo">
                            <!--select class="form-control" name="objetivo" id="objetivo" onfocus="ChecaAntecedencia();"!-->
                            <option value="0" selected>Escolha o objetivo do afastamento</option>
                            <? $outro == true;
                            while ($row1=mysql_fetch_array($resultado)) {
                                $pos = strpos($objetivo, $row1['texto']);
                                ?>
                                <option value="<? print $row1['texto']; ?>" <?
                                if($pos==0 && !($pos==FALSE)){
                                    $outro = false;
                                    if(strlen($objetivo)>strlen($row1['texto'])) {
                                        $objetivo= substr($objetivo,strlen($row1['texto'].". "));
                                    } else {
                                        $objetivo="";
                                    }
                                } ?>>
                                    <? print $row1['texto']; ?>
                                </option>
                            <? } ?>
                            <option value="-1"<? if($outro){ ?> selected <? } ?>>Outro</option>
                        </select>
                        <textarea class="form-control" name="outro" id="outro" rows="3"><? print $objetivo; ?></textarea>
                        <!--textarea class="form-control" name="outro" id="outro" rows="3" onfocus="ChecaAntecedencia();">< ? print $objetivo; ?></textarea!-->
                    <? } else { ?>
                        <label for="exampleFormControlSelect1"><? print $objetivo; ?></label>
                    <? } ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">
                        Cidade-Pa&iacute;s<br/>
                        (caso participe de mais de um evento, dever&aacute; ser indicado o local - cidade e pa&iacute;s - de cada um deles.)
                    </label><br/>
                    <? if($op==1 || $op==6 || $status=="U" || ($status=="P" && $nivel>2)) { ?>
                        <textarea class="form-control" name="cidade" id="cidade" rows="3"><? print $cidade; ?></textarea>
                        <!--textarea class="form-control" name="cidade" id="cidade" rows="3" onfocus="ChecaAntecedencia();">< ? print $cidade; ?></textarea!-->
                    <? } else { ?>
                        <label for="exampleFormControlSelect1"><? print $cidade; ?></label>
                    <? } ?>
                </div>
                <div class="form-group border rounded">
                    <label for="exampleFormControlSelect1">&nbsp;&nbsp;Ag&eacute;ncia financiadora</label><br/>
                    <input type="radio" name="temfinanciadora" id="temfinanciadora" value="S" <? if($financiadora) { ?> checked <? } ?> onchange="document.formulario.financiadora.disabled=false;">Sim &nbsp;&nbsp;&nbsp;&nbsp;
                    <label for="exampleFormControlSelect1">&nbsp;&nbsp;</label>
                    <input type="radio" name="temfinanciadora" id="temfinanciadora" value="N" <? if(!$financiadora) { ?> checked <? } ?> onchange="document.formulario.financiadora.disabled=true;document.formulario.financiadora.value='';">N&atilde;o<br/>
                    <label for="exampleFormControlSelect1">&nbsp;&nbsp;Qual?</label>
                    <input class="form-control" style="max-width:260px;" type="text" name="financiadora" id="financiadora" value="<? print $financiadora; ?>" maxlength="90" <? if(!$financiadora) { ?> disabled <? } ?>>
                </div>
                <? if ($op==2 && $codatendente) {
                    $SQL = "Select ramal, nome from tblusuarios where u.coduser='$codatendente'";
                    $res = mysql_query("$SQL");
                    if($linha=mysql_fetch_array($res)) { ?>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Respons&aacute;vel</label>
                            <label for="exampleFormControlSelect1"><? print $linha['nome']; ?></label>
                            <label for="exampleFormControlSelect1">Ramal</label>
                            <label for="exampleFormControlSelect1"><? print $linha['ramal']; ?></label>
                        </div>
                    <? }
                } ?>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Observa&ccedil;&atilde;o</label>
                    <textarea class="form-control" name="obs" id="obs" rows="2"><? print $obs; ?></textarea>
                    <!--textarea class="form-control" name="obs" id="obs" rows="2" onfocus="ChecaAntecedencia();">< ? print $obs; ?></textarea!-->
                </div>
                <div class="form-group">
                    <? if ($arquivo_convite) { ?>
                        <label for="exampleFormControlSelect1">Arquivo convite</label>
                        <? if ($browser=="Chrome" || $browser=="Safari" ) { ?>
                            <a href="#" onClick="window.open('/intranet/base/upload/<? echo $arquivo_convite;?>');"> <img src="../imagens/attachment.png" width="24" heigth="24"></a>
                        <? } else { ?>
                            <a href="#" onClick="openModal('/intranet/base/upload/<? echo $arquivo_convite;?>', 900, 700)"> <img src="../imagens/attachment.png " width="24" heigth="24"></a>
                        <? } ?>
                        <input class="form-control" type="button" name="Remover1" value="excluir" onclick="remover_upload('1');">
                    <? }
                    if ($arquivo_convite2) { ?>
                        <? if ($browser=="Chrome" || $browser=="Safari" ) { ?>
                            <a href="#" onClick="window.open('/intranet/base/upload/<? echo $arquivo_convite2;?>');"> <img src="../imagens/attachment.png" width="24" heigth="24"></a>
                        <? } else { ?>
                            <a href="#" onClick="openModal('/intranet/base/upload/<? echo $arquivo_convite2;?>', 900, 700)"> <img src="../imagens/attachment.png " width="24" heigth="24"></a>
                        <? } ?>
                        <input class="form-control" type="button" name="Remover2" value="excluir" onclick="remover_upload('2');">
                    <? } ?>
                    <input type="hidden" name="nUP" value="">
                    <label for="arquivo_convite">
                        <? if ($arquivo_convite || $arquivo_convite2) {
                            echo " Substituir/anexar arquivo convite";
                        } else {
                            echo "Anexar at&eacute; 2 documentos digitais para justificativa/convite:";
                        } ?>
                    </label>
                    <div id="uploadFile_div">1 -
                        <input type="file" name="arquivo_convite" id="arquivo_convite" size="40"
                               onclick="var ARext = document.formulario.exterior;
                                    if (ARext[1].checked==true) {
                                    alert('ATEN\u00c7\u00c3O!! A carta convite deve, necessariamente, ser traduzida para o portugu\u00eas!');
                                    document.formulario.aviso_traducao.value='1';
                                    }"/>
                        <a onclick="clearFileInputField('uploadFile_div')" href="javascript:noAction();">Limpar anexo</a>
                    </div>
                    <div id="uploadFile_div2"><br/>2 -
                        <input type="file" name="arquivo_convite2" id="arquivo_convite2" size="40"
                               onclick="var ARext = document.formulario.exterior;
                                    if (ARext[1].checked==true) {
                                    alert('ATEN\u00c7\u00c3O!! A carta convite deve, necessariamente, ser traduzida para o portugu\u00eas!');
                                    document.formulario.aviso_traducao.value='1';
                                    }"/>
                        <a onclick="clearFileInputField('uploadFile_div2')" href="javascript:noAction();">Limpar anexo</a>
                    </div>
                    <label for="exampleFormControlSelect1">
                        Formatos aceitos:<br/>
                        jpg, pdf, doc, docx e odt (tamanho m&aacute;ximo do arquivo de 2,1 Mb)</label>
                </div>
                <? // recupera matricula do chefe da area do solicitante
                $SQL="Select MATR_chefe from areas where CodArea='$departamento' and status='A'";
                $func = mysql_query($SQL);
                if (mysql_num_rows($func) > 0) {
                    $res = mysql_fetch_array($func);
                    $coduser_chefe_requisitante=$res['MATR_chefe'];
                }
                // se o solicitante eh chefe, deve indicar um substituto
                if($coduser_chefe_requisitante==$coduser) { ?>
                    <div class="form-group">
                        <input type="hidden" name="ehchefe" id="ehchefe" value="sim">
                        <label for="exampleFormControlSelect1"><b>Indicar substituto (obrigat&oacute;rio, somente para gestores)</b></label>
                        <select class="form-control" name="MATR_substituto" id="MATR_substituto">
                            <option value="000000" selected>Selecionar substituto</option>
                            <?
                            if (($coduser==$_SESSION["cod_diretor"]) || ($coduser==$_SESSION["cod_associado"])) {
                                $AREA_imediato="DIR";
                                if ($coduser==$_SESSION["cod_diretor"]) {
                                    $MATR_imediato=$_SESSION['cod_associado'];
                                } else {
                                    $MATR_imediato=$_SESSION['cod_diretor'];
                                }
                                $SQL = "SELECT coduser, nome FROM tblusuarios where coduser='".$MATR_imediato."'";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_imediato==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } elseif ($areabaseRequisitante=="CTUIA") {
                                // $areabaseRequisitante = $linha['codarea'];
                                $SQL = "SELECT coduser, nome FROM tblusuarios where coduser='".$_SESSION['cod_diretor']."' OR coduser='".$_SESSION['cod_associado']."'";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_imediato==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } else {
                                MontaHierarquia($_SESSION["codarea_conected"],'N','S','S');
                                $MATR_imediato=$hierarquia['acima']['matr'];
                                $AREA_imediato=$hierarquia['acima']['area'];
                                $NOME_imediato=$hierarquia['acima']['nome']; ?>
                                <option value="<?echo $MATR_imediato;?>"<?if ($MATR_substituto==$MATR_imediato) echo " selected";?>><?echo $NOME_imediato;?></option>
                                <?
                                if ($hierarquia['substituto_acima']['matr']) {?>
                                    <option value="<?echo $hierarquia['substituto_acima']['matr'];?>"<?if ($MATR_substituto==$hierarquia['substituto_acima']['matr']) echo " selected";?>><?echo $hierarquia['substituto_acima']['nome'];?></option>
                                <?}
                                $SQL = "SELECT coduser, nome FROM tblusuarios where status='A' and tipo!='INSTITUCIONAL' and codarea='".$departamento."' and coduser <> '".$coduser."' order by nome";
                                $consulta1 = mysql_query($SQL);
                                while ($row1=mysql_fetch_array($consulta1)) { ?>
                                    <option value="<? print $row1['coduser']; ?>" <? if($MATR_substituto==$row1['coduser']){ ?> selected <? } ?>>
                                        <? print $row1['nome']; ?> </option>
                                <? }
                            } ?>
                        </select>
                    </div>
                <? } else { ?>
                    <input type="hidden" name="ehchefe" id="ehchefe" value="nao">
                <? }
                // botoes salvar e voltar
                if($op==1 || $op==6 || $status=="U" || ($status=="P" && $status=="Z" && $nivel==3)) { ?>
                    <input class="btn btn-primary" type="button" name="Salvar" value="Salvar<? if ($op==6) echo " altera&ccedil;&otilde;es";?>" onclick="enviar();">
                <? }
                if($volta) { ?>
                    <input class="btn btn-primary" type="button" name="Voltar" value="Voltar" onclick="document.location='<? print $volta; ?>?pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?><? print $parametros;?>';">
                <? } ?>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <? if ($exterior=='E') { ?>
                    <SCRIPT LANGUAGE="JavaScript">
                        TrataExterior(document.formulario.exterior);
                    </script>
                <? } ?>
            </form>
        </div>
    </div>
</div>

</body>

