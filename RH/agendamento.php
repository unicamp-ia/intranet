<?
        function IdentifiqueOaprovador($Oaprovador) {
             global $nomeaprovador,$emailaprovador,$codsubstituto,$nomesubstituto,$emailsubstituto;
             $nomeaprovador="";
             $emailaprovador="";
             $nomesubstituto="";
             $emailsubstituto="";
             $codsubstituto="";
             // Busca o depto do chefe imediato, para verificar se hï¿½ algum substituto indicado naquele depto.
             // select u.*,a.* from tblusuarios u left join areas a on u.codarea=a.CodArea where u.coduser='284854'

             $SQL="select u.nome,u.coduser,u.email,a.* from tblusuarios u left join areas a on u.codarea=a.CodArea where u.coduser='$Oaprovador'";
             $res1=mysql_query($SQL);
             if ($linha=mysql_fetch_array($res1)) {
                   $nomeaprovador=$linha['nome'];
                   $emailaprovador=$linha['email'];
                   if ($linha['MATR_substituto'] <> "000000" and $linha['prerrogativas']==1) {
                        $emailsubstituto =  $linha['email_substituto'];
                        $SQL_A="select * from tblusuarios where coduser='$codsubstituto'";
                        $res = mysql_query("$SQL_A");
                        if($linha = mysql_fetch_array($res)) {
                              $nomesubstituto = $linha['nome'];
                              $codsubstituto = $linha['coduser'];
                        }    
                    }
              }     
        }
        envia_email("[cron]Sistema de notificaï¿½ï¿½o periodica", "Iniciando as notificaï¿½ï¿½es", "intra.ia@iar.unicamp.br");
       //goto pl;
	$timestamp = time();
        $atenciosamenteRH="<br> Atenciosamente, RH/IA (Recursos Humanos)";
        //
        // Notificaï¿½ï¿½es aos aprovadores - Afastamento
        //
        //Quantidade em seguntos de hoje a 3 dias apï¿½s
	$timestampdepois = $timestamp + 259200;
	$data_mais_3d = date('Y-m-d', $timestampdepois);
	$SQL = "select u.nome,u.email,s.codaprovador, s.codsolicitante,date_format(a.datasaida, '%d/%m/%Y' ) as dtsaida from RHafastamento a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A' and a.datasaida <='$data_mais_3d'";
	$res = mysql_query($SQL);
        $assunto = "Sistema de Recursos Humanos";
       
	while ($linha=mysql_fetch_array($res)) {
                $cod=$linha['cod'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datasaida = $linha['dtsaida'];
		$mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de afastamento para inicio em $datasaida at&eacute; hoje n&atilde;o foi aprovada pelo(a) chefe. Consulte-o para saber os motivos da demora na an&aacute;lise, pois o atraso pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                IdentifiqueOaprovador($codaprovador);
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .=  "<br> Verificamos a existï¿½ncia de uma Solicitação de afastamento do colaborador $nomesolicitante para início";
                $mensagem .= " em $datasaida atï¿½ o momento nï¿½o aprovada. Pedimos-lhe a gentileza de analisï¿½-la pois a demora pode comprometer os prazos exigidos pelo"; 
                $mensagem .= " RH da Unicamp e, como consequï¿½ncia, prejudicar o solicitante. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                envia_email("[cron]Sistema RH - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
	}
        //
        // Notificaï¿½ï¿½es aos aprovadores - Licenï¿½a-premio
        //
        //Quantidade em seguntos de hoje a 1 dia
        $timestampdepois = $timestamp + 86400;
	$data_mais_1d = date('Y-m-d', $timestampdepois);
	$SQL = "select u.nome, u.email, s.codaprovador, s.codsolicitante, date_format(a.datainicio, '%d/%m/%Y' ) as dtinicio from RHlicenca a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A' and a.datainicio<='$data_mais_1d'";
	$res = mysql_query($SQL);
	while ($linha=mysql_fetch_array($res)) {
            
            $cod=$linha['cod'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datainicio = $linha['dtinicio'];
                
		// Notifica o solicitante
                $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de licenï¿½a prï¿½mio para inicio em $datainicio at&eacute; o momento n&atilde;o foi aprovada pelo(a) chefe. Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                // Notifica o apovador
                IdentifiqueOaprovador($codaprovador);
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .= "<br> Verificamos a existï¿½ncia de uma Solicitação de licï¿½nca prï¿½mio do colaborador $nomesolicitante para início";
                $mensagem .= " em $datainicio atï¿½ o momento nï¿½o aprovada. Pedimos-lhe a gentileza de analisï¿½-la pois a demora pode comprometer os prazos"; 
                $mensagem .= " exigidos pelo  RH da Unicamp. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                envia_email("[cron]Sistema RH - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
	}
        
        //
        // Notificaï¿½ï¿½es aos aprovadores - Faltas abonadas
	//
        
        $timestampdepois = $timestamp + 86400;
	$data_mais_1d = date('Y-m-d', $timestampdepois);
	$SQL = "select u.nome, u.email, s.codaprovador, s.codsolicitante, date_format(a.datafalta, '%d/%m/%Y' ) as dtfalta,a.codigofalta  from RHartigo a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A' and a.datafalta<='$data_mais_1d'";
	$res = mysql_query($SQL);
	while ($linha=mysql_fetch_array($res)) {
            
            $cod=$linha['cod'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datafalta = $linha['dtfalta'];
               
		 if ($linha['codigofalta']=="F2") {
                    $desctipo="F2 - Falta integral abonada";
                }
                // Notifica o solicitante
                $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o $desctipo, para o dia $datafalta at&eacute; o momento n&atilde;o foi aprovada pelo(a) chefe. Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois isso pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
                $desctipo="F1 - Falta abonada (artigo 31)";
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                // Notifica o apovador
                IdentifiqueOaprovador($codaprovador);
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .= "<br> Verificamos a existï¿½ncia de uma Solicitação $desctipo, do(a) colaborador(a) $nomesolicitante, para o dia";
                $mensagem .= " $datafalta atï¿½ o momento nï¿½o aprovada. Pedimos-lhe a gentileza de analisï¿½-la pois a demora pode comprometer os prazos exigidos pelo"; 
                $mensagem .= " RH da Unicamp. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                envia_email("[cron]Sistema RH - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
	}
        
        //
        // Notificaï¿½ï¿½es aos aprovadores - Fï¿½rias
        //
        
        $SQL = "select u.nome, u.email, s.codaprovador, s.codsolicitante, s.datacadastro, s.lotacao, date_format(a.datainicio, '%d/%m/%Y' ) as dtinicio,a.total as totaldias  from RHferias a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A'";
	$res = mysql_query($SQL);
        $datadehoje=date('Y-m-d');
	while ($linha=mysql_fetch_array($res)) {
            $datacadastro=$linha['datacadastro'];
            $data_mais_7d= dtSomar($datacadastro,7);
            if ($data_mais_7d <= $datadehoje) {
                $cod=$linha['cod'];
                $totaldias=$linha['totaldias'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datainicio = $linha['dtinicio'];
                $lotacao=$linha['lotacao'];
                IdentifiqueOaprovador($codaprovador);
                // Notifica o solicitante
                $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de fï¿½rias para o início em $datainicio, por $totaldias dias at&eacute; o momento n&atilde;o foi aprovada pelo(a) chefe, $nomeaprovador. <br>Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois isso pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
                
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                // Notifica o apovador
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .= "<br> Verificamos a existï¿½ncia de uma Solicitação de fï¿½rias, do(a) colaborador(a) $nomesolicitante, para início em";
                $mensagem .= " $datainicio, por $totaldias dias atï¿½ o momento nï¿½o aprovada. Pedimos-lhe a gentileza de analisï¿½-la pois a demora pode comprometer os prazos exigidos pelo"; 
                $mensagem .= " RH da Unicamp. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                envia_email("[cron]Sistema RH - Fï¿½rias - Lembrete ao aprovador [$lotacao]", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
          }
	}
//pl:
        // Notificaï¿½ï¿½es aos aprovadores - Transportes
        $SQL = "select u.nome,u.email,t.cod,t.ano,t.coduser,t.codarea,t.codaprovador,t.codrecurso,t.veiculo,t.datasaida,t.finalidade,t.destino,t.status,";
        $SQL .= "date_format(t.datasaida, '%d/%m/%Y ï¿½s %H:%i') as dtsaida,v.descricao as modeloveiculo,v.antes as antes, v.antes_dh as antes_dh, r.descricao as nomerecurso from transpedidos t";
        $SQL .=" left join tblusuarios u on t.coduser=u.coduser left join transpveiculos v on t.veiculo=v.cod  left join transprecurso";
        $SQL .=" r on t.codrecurso=r.cod where t.status='P' or t.status='R'";
	$res = mysql_query($SQL);
        //echo "sddsds" . mysql_num_rows($res);
        //echo $SQL;
        $datadehoje=date('Y-m-d');
        //echo mysql_num_rows($res);
        $assunto = "Sistema Intranet - Transportes";
	while ($linha=mysql_fetch_array($res)) {
            $antes_horas=$linha['antes'];
            if ($linha['antes_dh'] =='d') {
                $antes_horas=$antes_horas * 24;
            }
            if ($antes_horas <=24) {
                $antes_horas = $antes_horas+24;
            }
            $data_antecipacao=strftime("%Y-%m-%d:%H:%M:%S", strtotime("+$antes_horas hours"));
            $datasaida=$linha['datasaida'];
            $status=$linha['status'];
            //echo "\n Data Saida: " . $datasaida . " Antes: $antes_horas Data Antecipacao: " . $data_antecipacao . " >" . $linha['cod'] . " " . $linha['ano'];
            //echo "\n---------------------------------------------------------------------------------";
            if ($data_antecipacao >= $datasaida || $status=='R') {
                $cod=$linha['cod'];
                $ano=$linha['ano'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $modeloveiculo=$linha['modeloveiculo'];
                $nomerecurso=$linha['nomerecurso'];
                $destino=$linha['destino'];
                $finalidade=$linha['finalidade'];
                $dtsaida=$linha['dtsaida'];
               
                // Notifica o solicitante
                if ($status=='P') {
                    $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de veï¿½culo nï¿½mero $cod / $ano com data de saï¿½da em $dtsaida at&eacute; o momento n&atilde;o foi analisada pelo aprovador(a), $nomeaprovador. Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois isso pode, caso seja um veï¿½culo externo, comprometer os prazos exigidos pela ï¿½rea de transportes da DGA.<br><br>";
                    $mensagem .= "<br> Atenciosamente, APOPER/IA.";
                    //echo "\n\n" . $mensagem . "\n" . $emailsolicitante; 
                    envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                    // Notifica o apovador
                    IdentifiqueOaprovador($codaprovador);
                    $email_notif=$emailaprovador;
                    if ($codsubstituto <> "") {
                                $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                                $email_notif=$email_notif . "," . $emailsubstituto;
                    } else {
                                $mensagem = "Caro(a) $nomeaprovador,";
                    }
                    $mensagem .= "<br> Verificamos a existï¿½ncia de uma Solicitação de veiculo nï¿½mero $cod / $ano criada pelo(a) colaborador(a) $nomesolicitante, com data de saï¿½da em $dtsaida";
                    $mensagem .= " atï¿½ o momento nï¿½o aprovada. Pedimos-lhe a gentileza de analisï¿½-la pois a demora pode comprometer, no caso de veï¿½culo externo, os prazos exigidos pela ï¿½rea de transporte da DGA. <br><br>Outros dados da Solicitação:<br>";
                    $mensagem .= "Veï¿½culo: $modeloveiculo<br>Recurso:$nomerecurso<br>Destino:$destino<br>Finalidade:$finalidade";
                    $mensagem .= "<br>Para acessar a referida Solicitação, selecione o mï¿½dulo <b>Transportes</b> da Intranet(www.iar.unicamp.br/intranet) e clique na opï¿½ï¿½o <b>APROVAR SOLICITAï¿½ï¿½ES</b><br><br>  Atenciosamente, APOPER/IA.";

                    envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                    envia_email("[cron]Modulo de transportes - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
                } else {
                    $mensagem =  "Caro(a) $nomesolicitante,<br>  Verificamos que a solicita&ccedil;&atilde;o de veï¿½culo nï¿½mero $cod / $ano com data de saï¿½da em $dtsaida foi retornada a vocï¿½ pelo Gestor do sistema de Transporte do IA, cobrando-lhe alguma aï¿½ï¿½o para prosseguimento."; 
                    $mensagem .= "Por favor, acesse a referida Solicitação, faï¿½a as correï¿½ï¿½es necessï¿½rias solicitadas pelo Gestor ou cancele-a se for o caso.<br><br>";
		    $mensagem .= "<br> Atenciosamente, <br>Sistema Intranet - Mï¿½dulo de Transportes.";
                    envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                    envia_email("[cron]Modulo de transportes - Notificaï¿½ï¿½o ao solicitante", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
                }
                //echo "\n\n" . $mensagem . "\n" . $emailaprovador;
          }
	}
         
?>
