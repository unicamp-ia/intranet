<?php
/*
    Significados do campo STATUS
    F = Finalizada
    A = Aguardando aprovacao do chefe
    P = Pendente para o RH
    U = Retornado ao solicitante
    R = Recusada pelo chefe
*/
// Alterado por Edson Giordani em Outubro/2019 - Indicacao de Gestor Substituto
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Fevereiro/2020 - Responsividade

$nivelcomp = -1;
include("../base/inicio.php"); /* Faz a verificacao se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
include("../base/email.php"); /* Funcao envia_email($assuntoemail, $mensagem, $remetente_email, $destinatario)*/
$busca = $_REQUEST['busca'];/* Codigo para nao perder o filtro na busca. */
$pagina=$_REQUEST['pagina'];
$num_linhas=$_REQUEST['num_linhas'];
if($busca) {
    $tipo = $_REQUEST['tipo'];
    $nome = $_REQUEST['nome'];
    $CodArea = $_REQUEST['CodArea'];
    $diainicad = $_REQUEST['diainicad'];
    $mesinicad = $_REQUEST['mesinicad'];
    $anoinicad = $_REQUEST['anoinicad'];
    $diafimcad = $_REQUEST['diafimcad'];
    $mesfimcad = $_REQUEST['mesfimcad'];
    $anofimcad = $_REQUEST['anofimcad'];
    $statusbusca=$_REQUEST['statusbusca'];
    $parametros = "&busca=".$busca."&tipo=".$tipo."&nome=".$nome."&CodArea=".$CodArea."&diainicad=".$diainicad."&mesinicad=".$mesinicad."&anoinicad=".$anoinicad."&diafimcad=".$diafimcad."&mesfimcad=".$mesfimcad."&anofimcad=".$anofimcad."&statusbusca=".$statusbusca;
}
$volta = $_REQUEST['volta'];
$op = $_REQUEST['op'];
$cod = $_REQUEST['cod'];
$statuslista = $_REQUEST['statuslista'];
$codsolicitante = $_REQUEST['codsolicitante'];
$lotacao = $_REQUEST['lotacao'];
$coduser = $_REQUEST['coduser'];
$tipo = $_REQUEST['tipo'];
$status = $_REQUEST['status'];
$periodo = $_REQUEST['periodo'];
$dia = $_REQUEST['dia'];
$mes = $_REQUEST['mes'];
$ano = $_REQUEST['ano'];
$dias = $_REQUEST['dias'];
$datainicio = $_REQUEST['datainicio'];
$ehchefe = $_REQUEST['ehchefe'];
$MATR_substituto = $_REQUEST['MATR_substituto'];
$msgE1 = "<html><body>";
$msgE2 = "</body></html>";
$att= "<br> Atenciosamente, Sistema Intranet - RH.";
$assunto = "Sistema de Recursos Humanos";
$NaoResponda = "<br><br>Esta mensagem foi gerada automaticamente, n&atilde;o responda.";
$timehoje=date("Y-m-d H:i:s");
$IP = $_SERVER['REMOTE_ADDR'];
if(!$datainicio) {
    $datainicio =  date("Y-m-d G:i:s");
}
$cod_diretor=$_SESSION["cod_diretor"];
if ($op=="1" || $op=="6") {
    include_once("../base/VerificaAntecedencia.php");
    $nda = 1; // Prazo eh, no minimo, de 1 dia util de antecedencia. Limitado as 17h
    $datalicenca = $ano."-".$mes."-".$dia;
    $ndb=ChecaAntecedencia($datalicenca,$nda); // $nbd serah zero se nao cumprir prazo!

    if ($ndb < $nda){
        // Nao aceita pedido de afastamento pois nao atende a antecedencia de $nda dias uteis
        ?>
        <table width="30%" align="center"><tr><td colspan="3">
                    <b>Caro(a) <?echo $_SESSION["nomeuser_conected"];?></b>,<br>
                    Sua solicita&ccedil;&atilde;o de licen&ccedil;a-pr&ecir;mio n&atilde;o pode ser continuada por n&atilde;o atender ao prazo de anteced&ecir;ncia de <?echo $nda;?>
                    dia(s) &uacute;til(eis) establecido(s) pela DGRH/Unicamp. Por favor, volte ao formul&aacute;rio e corrija a data de in&iacute;cio da licen&ccedil;a
                    ou entre em contado com a se&ccedil;&atilde;o de Recursos Humanos do IA</td>
                <? echo $TblResAnt;?>
            <tr><td align="center" colspan="3"> <br><input type="button" name="Voltar" value="Voltar" class="botao" onClick="javascript:history.go(-1);"></td></tr>
        </table>
        <?
        die();
    }

    $hora=date('H');
    $minuto=date('i');
    $hora = $hora + 0;
    $minuto = $minuto + 0;
    if ($hora>=17) {
        $ndb=$ndb-1;
    }
    if ($ndb<$nda) {
        $mensagem = "Solicita&ccedil;&atilde;o de licen&ccedil;a-pr&ecirc;mio com anteced&ecirc;ncia de 1 dia, somente pode ser aceita antes das 17h. Qualquer d&uacute;vida, procure a se&ccedil;&atilde;o de RH do Instituto de Artes.";
        ?><br><br><table width="30%" align="center"><tr><td colspan="3">
                <b>Caro(a) <?echo $_SESSION["nomeuser_conected"];?></b>,<br>
                <?echo $mensagem;?></td></tr><tr><td colspan="3" align="left"><h2><b><font color="green">Dia e Hora: <?echo date("d/m/Y H:i");?></font></h2></td></tr></table><?
        $mensagem = $_SESSION["nomeuser_conected"]. "<br> " . $mensagem . "<br><br>Data e Hora:" . date("d/m/Y H:i");
        envia_email("RH: Sol. de falta abonada c/ atraso", $msgE1.$mensagem . $NaoResponda . $msgE2, "saa@iar.unicamp.br");
        die();
    }
}
if($op=="1") {
    $data = date("Y-m-d");
    // Verifica se o solicitante eh o diretor do IA. Se for, o status passa automativamente a Pendente para RH
    if ($codarea_conected == $cod_diretoria && $coduser_conected==$_SESSION['coduser_chefe']) {
        $STA="P";
    } else {
        $STA="A";
        include("busca_aprovador.php");
    }
    $SQL = "Insert into RHsolicitacoes (codsolicitante, coduser, codaprovador, lotacao, status, tipo, dataaltera, datacadastro,ip_solicitacao) values ('$codsolicitante', '$coduser', '$codaprovador', '$lotacao', '$STA', '$tipo', '$datainicio','$data','$IP')";
    $S= addslashes($SQL);
    $res = mysql_query("$SQL");
    if (!$res) {
        envia_email("[exibelicenca] Erro 1 ao cadastrar o pedido. Entre em contato com o administrador. 1 ", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
        die("[exibelicenca] Erro 1 ao cadastrar o pedido. Entre em contato com o administrador. 1 ");
    }

    $SQL = "Select cod from RHsolicitacoes where dataaltera='$datainicio' and tipo='$tipo' and codsolicitante='$codsolicitante'";
    $res = mysql_query("$SQL");
    $emailsolicitante=$_SESSION["emailuser_conected"];
    if($linha=mysql_fetch_array($res)) {
        $cod = $linha['cod'];
        $dataperiodo = $ano."/".$mes."/".$dia;
        if ($ehchefe=="sim") {
            $SQL = "Insert into RHlicenca (cod, periodo, datainicio, dias, MATR_substituto) values ('$cod', '$periodo','$dataperiodo', '$dias', $MATR_substituto)";
        } else {
            $SQL = "Insert into RHlicenca (cod, periodo, datainicio, dias) values ('$cod', '$periodo','$dataperiodo', '$dias')";
        }
        $res = mysql_query("$SQL");
        if (!$res) {
            envia_email("[exibelicenca] Erro 2 ao cadastrar o pedido. Entre em contato com o administrador. 2 ", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
            die("[exibelicenca] Erro 2 ao cadastrar o pedido. Entre em contato com o administrador. 2 ");
        }

        $SQL = "Select u.usuario,u.email  from tblusuarios u where u.coduser='$coduser'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $emailsolicitante = $linha['email'];
        }

        $SQL = "Select * from RHsolicitacoes where cod='$cod'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $tipo = $linha['tipo'];
        }
        $dt=strftime("%d/%m/%Y", strtotime($dataperiodo));
        if ($STA=='A') {
            $desctipo="licenca premio";
            $email_notif=$emailaprovador;
            if ($codsubstituto <> "") {
                $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                $email_notif=$email_notif . "," . $emailsubstituto;
            } else {
                $mensagem = "Caro(a) $nomeaprovador,";
            }
            $mensagem .= "<br> Foi cadastrada pelo(a) colaborador(a) " . $_SESSION['nomeuser_conected'] . ", uma solicita&ccedil;&atilde;o de $desctipo  para in&iacute;cio em $dt, por $dias dias.";

            $mensagem .= "<br>Por favor, acesse o link abaixo para aprovar ou recusar o pedido.<br>";
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibelicenca.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";

            envia_email($assunto, $msgE1 . $mensagem . $att . $NaoResponda. $msgE2, $email_notif);

            $mensagem .= "<br> Aprovador:" . $email_notif;
            $emailros="intra.ia@iar.unicamp.br";
            // envia_email("Notificacao RH - $desctipo - Aprovador", $msgE1 .  $mensagem  . $NaoResponda . $msgE2, $emailros);

            $mensagem = "Caro(a) " . $_SESSION['nomeuser_conected'] . ",<br>Sua solicita&ccedil;&atilde;o de li&ccedil;enca pr&ecirc;mio foi encaminhada para aprova&ccedil;&atilde;o de sua chefia.";
            $mensagem .="<br>O(a) respons&aacute;vel pela aprova&ccedil;&atilde;o de sua solicita&ccedil;&atilde;o &eacute; " . $nomeaprovador;
            if ($codsubstituto <> "") {
                $mensagem .=" ou o substituto indicado, sr(a)" . $nomesubstituto . ".";
            }
            envia_email($assunto, $msgE1. $mensagem . $att . $NaoResponda. $msgE2, $emailsolicitante);
        } else {
            // O solicitante eh o diretor do IA.
            // Busca os e-mails dos pessoal do RH
            $SQL="select u.nome, u.email, u.coduser,s.idsistema, s.permissao from tblusuarios u left join tblususist s on u.coduser=s.coduser where s.idsistema=19 and permissao=3";
            $res = mysql_query("$SQL");
            $ll=0;
            $emailRH="";
            while($linha3=mysql_fetch_array($res)) {
                $ll=$ll+1;
                if ($ll>1) {
                    $emailRH .= ",";
                }
                $emailRH .= $linha3['email'];
            }

            $mensagem = "O(a) diretor(a) do Instituto de Artes, " . $_SESSION['nomeuser_conected'] . " solicita licen&ccedil;a-pr&ecirc;mio para in&iacute;cio em $dt, por $dias dias.<br>";
            $mensagem .= "O n&uacute;mero da solicita&ccedil;&atilde;o &eacute; $cod e o status foi alterado para <b>Pendente para RH<b>. Por favor, providenciar tr&acirc;mite junto &agrave; DGRH.";
            envia_email($assunto, $eM1. $mensagem. $att . $NaoResponda . $eM2, $emailRH);

        }
        $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Inser&ccedil;&atilde;o de registro - Solicita&ccedil;&atilde;o de licen&ccedil;a-pr&ecirc;mio $cod', '$S','$timehoje')";
        $res=mysql_query($SQLlog) or die("[exibelicenca] RH: erro ao inserir LOG");
    }
}
if($op=="3") { // Usuario RH alterou o status da solicitacao, mantendo o Status de Pendente ou Finalizada.
    $SQL = "Select s.tipo, s.dataaltera, u.usuario, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod' and s.dataaltera < '$datainicio'";
    $res = mysql_query("$SQL");
    if(mysql_num_rows($res) > 0) {
        $linha = mysql_fetch_array($res);
        $emailsolicitante=$linha['email'];
        $nomesolicitante=$linha['nome'];
        $SQL = "Update RHsolicitacoes set dataaltera = '$datainicio', codatendente='$coduser_conected', status='$status' where cod='$cod'";
        $res = mysql_query("$SQL") or die("[exibelicenca] Erro 3 ao cadastrar o pedido. Entre em contato com o administrador.");
        // Busca o periodo da licenca-premio na tabela propria
        $SQL = "Select dias, date_format(datainicio, '%d/%m/%Y') as inicio from RHlicenca where cod='$cod'";
        $res1 = mysql_query("$SQL");
        if($linha2=mysql_fetch_array($res1)) {
            $dtinicio = $linha2['inicio'];
            $dias = $linha2['dias'];
        }
        if ($status == "F") {
            $mensagem = "Caro(a) $nomesolicitante .<br>Sua solicita&ccedil;&atilde;o de licen&ccedil;a-pr&ecirc;mio de $dias dias, iniciando em $dtinicio, foi finalizada pelo RH do IA.";
            $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o";
            $mensagem .= "<br>Atenciosamente,<br> $nomeuser_conected <br> RH";
            envia_email($assunto, $msgE1.$mensagem  . $NaoResponda . $msgE2, $emailsolicitante);
        }
        if ($status == "C") {
            if ($coduser <> $coduser_conected) {
                $mensagem = "Caro(a) $nomesolicitante .<br>Sua solicita&ccedil;&atilde;o de licen&ccedil;a-pr&ecirc;mio para inicio em $dtinicio, por $dias dias, foi cancelada pelo RH do IA.";
                $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o";
                $mensagem .= "<br><br>Atenciosamente,<br> $nomeuser_conected <br> RH";
                envia_email($assunto, $msgE1.$mensagem . $NaoResponda . $msgE2, $emailsolicitante);
            }
        }

    } else {
        die("Algu&eacute;m estava alterando o pedido. N&atilde;o foi poss&iacute;vel realizar sua altera&ccedil;&atilde;o, tente novamente.");
    }
}
if($op=="4" || $op=="6") {

    $SQL = "Update RHsolicitacoes set lotacao='$lotacao', coduser='$coduser', codsolicitante='$coduser', dataaltera = '$datainicio', status='$status' where cod='$cod'";
    $res = mysql_query("$SQL") or die("[exibelicenca] Erro 4 ao cadastrar o pedido de licen&ccedil;a. Entre em contato com o administrador.");
    $dataperiodo = $ano."/".$mes."/".$dia;
    if ($ehchefe=="sim") {
        $SQL = "Update RHlicenca set periodo='$periodo', datainicio='$dataperiodo', dias='$dias' where cod='$cod'";
    } else {
        $SQL = "Update RHlicenca set periodo='$periodo', datainicio='$dataperiodo', dias='$dias', MATR_substituto=$MATR_substituto where cod='$cod'";
    }
    $res = mysql_query("$SQL") or die("[exibelicenca] Erro 5 ao cadastrar o pedido. Entre em contato com o administrador. 2 ");

    if($status=="U") {
        echo "[Rastreamento de erro]. Problema na execu&ccedil;&atilde;o do fluxo de dados. Comunique este erro &agrtave; DTI";
        die();
        $SQL = "Select u.usuario, u.email, s.permissao from tblusuarios u left join RHsolicitacoes r on u.coduser=r.codsolicitante left join tblususist s on u.coduser=s.coduser and s.idsistema='19' where r.cod='$cod'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $email = $linha['email'];
            $permissao = $linha['permissao'];
            $mensagem .= "Ol&aacute; .<br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
            $SQL = "Select * from RHsolicitacoes where cod='$cod'";
            $res = mysql_query("$SQL");
            if($linha=mysql_fetch_array($res)) {
                $tipo = $linha['tipo'];
            }
            $desctipo="licen&ccedil;a-pr&ecirc;mio";
            $mensagem .= "Foi solicitada a altera&ccedil;&atilde;o de um pedido de ".$desctipo." cadastrado por voc&ecirc;.";
            $mensagem .= "<br>Voc&ecirc; deve alterar o pedido no link abaixo.<br>";
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitaoutrolicenca.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";
            envia_email($assunto, $msqE1 . $mensagem . $att . $NaoResponda . $msgE2, $email);
        }
    } else if($status=="A") {
        /*  Verifica que eh o aprovador para a area em que o usuario estah lotado. Se o usuario eh chefe da
            area, entao o aprovador eh o chefe imediato. Retorna a variavel $codaprovador e $nomeaprovador */
        include("busca_aprovador.php");
        $SQL = "Select s.tipo, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $tipo = $linha['tipo'];
            $emailsolicitante=$linha['email'];
            $nomesolicitante=$linha['nome'];
        }
        if($tipo=="L") { $desctipo="licen&ccedil;a-pr&ecirc;mio"; } else if($tipo=="A") { $desctipo="afastamento"; }
        $email_notif=$emailaprovador;
        if ($codsubstituto <> "") {
            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
            $email_notif=$email_notif . "," . $emailsubstituto;
        } else {
            $mensagem = "Caro(a) $nomeaprovador,";
        }
        $mensagem .= "<br>Foi alterada uma solicita&ccedil;&atilde;o de ".$desctipo." que necessita de sua aprova&ccedil;&atilde;o.";
        $mensagem .= "<br>Por favor, acesse o link abaixo para aprovar ou recusar o pedido.<br>";

        if($tipo=="A") {
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibeafastamento.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
        } else if($tipo=="L") {
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibelicenca.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
        }
        envia_email($assunto, $msgE1 . $mensagem  . $att . $NaoResponda . $msgE2, $email_notif);
        // Notifica o usuario
        $mensagem = "Caro(a) $nomesolicitante ";
        $mensagem .= "<br>Sua solicita&ccedil;&atilde;o de " . $desctipo . " foi alterada e ser&aacute; submetida &eacute; aprova&ccedil;&atilde;o do Chefe de Departamento/Se&ccedil;&atilde;o,";
        $mensagem .= " Sr(a) " . $nomeaprovador;
        if ($codsubstituto <> "") {
            $mensagem .=" ou ao(&agrave;) substituto(a) indicado(a), sr(a) " . $nomesubstituto;
        }
        $mensagem .= ". Por favor, aguarde. ";
        $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o.";
        envia_email($assunto, $msgE1.$mensagem  . $att . $NaoResponda . $msgE2, $emailsolicitante);
    }
}
$EoChefe=false;
$EoSubstituto=false;
$Echefe=false;
$EaprovadorIndicado=false;
include ("VerifiquePosicao.php");
//Depois do include, a variavel $cod estah perdendo seu conteudo echo "cd1=" . $cod;
if($cod) {
    $SQL = "select s.*, u.nome,ap.nome as nomeaprovador from RHsolicitacoes s left join tblusuarios u on s.coduser=u.coduser left join tblusuarios ap on s.codaprovador=ap.coduser  where s.cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $status = $linha['status'];
    $obs = $linha['obs'];
    $coduser = $linha['coduser'];
    $codaprovador= $linha['codaprovador'];
    $codatendente= $linha['codatendente'];
    $codsolicitante=$linha['codsolicitante'];
    $nome = $linha['nome'];
    $lotacao = $linha['lotacao'];

    $SQL = "select *, date_format(datainicio, '%d/%m/%Y') AS datainicio1 from RHlicenca where cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $periodo = $linha['periodo'];
    $dias = $linha['dias'];
    $datainicio1 = $linha['datainicio1'];
    $MATR_substituto = $linha['MATR_substituto'];
    //Se o usuario nao for chefe nem substituto, entao verifica se o usuario eh o aprovador desta solicitacao
    if (!$EoChefe && !$Echefe && !$EoSubstituto && $codaprovador && $codaprovador==$coduser_conected) {
        $EaprovadorIndicado=true;
    }
}
//Quem pode aprovar, caso a solicitacao ainda esteja com status "Aguardando aprovacao"?
$PodeAprovar=false;
if ($status=="A") {
    if ($coduser_conected==$codaprovador) {
        $PodeAprovar=true;
    }
    if ($EoSubstituto && is_array($aprovadores_substituto) && count($aprovadores_substituto)>0 ) {
        if (in_array($codaprovador,$aprovadores_substituto)) {
            $PodeAprovar=true;
        }
    }
}
// Busca o aprovador substituto para a area de lotacao do solicitante, caso ele exista
// Monta a hierarquia para a area base ($lotacao) e acima da area base
$nomesubstituto="";
MontaHierarquia($lotacao,"S","S","N");
if ($hierarquia['chefia']['matr'] == $codaprovador || $hierarquia['acima']['matr']==$codaprovador || $hierarquia['ATU']['matr']==$codaprovador || $hierarquia['DIR']['matr']==$codaprovador) {
    if ($hierarquia['chefia']['matr'] !=  $coduser) {
        if ($hierarquia['substituto_chefia']['matr']==$coduser) { // solicitante eh substituto no departamento dele
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        } else {
            if ($hierarquia['chefia']['matr'] == $codaprovador) {
                if ($hierarquia['substituto_chefia']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_chefia']['nome']; }
            } elseif ($hierarquia['acima']['matr']==$codaprovador) {
                if ($hierarquia['substituto_acima']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_acima']['nome']; }
            } elseif ($hierarquia['ATU']['matr']==$codaprovador) {
                if ($hierarquia['substituto_atu']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_atu']['nome']; }
            } else {
                if ($hierarquia['substituto_dir']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_dir']['nome']; }
            }
        }
    } else {
        if ($hierarquia['substituto_acima']['matr'] && $hierarquia['substituto_acima']['matr'] !=  $coduser) {
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        }
    }
}
?>
<SCRIPT LANGUAGE="JavaScript">
    function salvar () {
        if(document.formulario.status.value=='U') {
            document.location='retornasolicitacoes.php?cod=<? print $cod; ?>&datainicio=<? print $datainicio; ?>&volta=<? print $volta; ?>&statuslista=<? print $statuslista; ?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>'
        } else {
            document.location='exibelicenca.php?cod=<? print $cod; ?>&op=3&datainicio=<? print $datainicio; ?>&volta=<? print $volta; ?>&statuslista=<? print $statuslista;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status='+document.formulario.status.value;
        }
    }
    function aceita () {
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=P&op=1'
    }
    function recusa () {
        if(document.formulario.obs.value=="") {
            alert('Preencha uma justificativa para a recusa no campo observa\u00e7\u00e3o!');
            document.formulario.obs.focus();
            return false;
        }
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=R&op=1&obs='+document.formulario.obs.value;
    }
    function altproc() {
        document.location='altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1'
    }
</SCRIPT>
<?
include("menu.php");
?>
<form name="formulario">

    <? if($PodeAprovar) { ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 bg-light">
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Observa&ccedil;&atilde;o (pertinente à aprovação/recusa do pedido)</label>
                        <textarea class="form-control" name="obs" id="obs" rows="3" readonly><? print $obs; ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 bg-light">
                <div class="form-group">
                    <label for="exampleFormControlInput1"><b>Solicita&ccedil;&atilde;o de licen&ccedil;a pr&ecirc;mio</b></label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Matr&iacute;cula:&nbsp;<? print $coduser; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Nome:&nbsp;<? print $nome; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Lota&ccedil;&atilde;o:&nbsp;
                        <?
                        $SQL ="SELECT * FROM areas where CodArea='$lotacao' and origem='I'";
                        $resultado = mysql_query($SQL);
                        if ($row1=mysql_fetch_array($resultado)) { ?>
                            <? print $row1['Descricao']; ?>
                        <? } ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Inicio da licen&ccedil;a:&nbsp;<? print $datainicio1; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Dias:&nbsp;<? print $dias; ?>
                    </label>
                </div>

                <? if (($ehchefe=="sim")||($MATR_substituto!=0)) { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">
                            Substituto(a):&nbsp;
                            <?
                            $SQL = "Select Nome from tblusuarios where coduser=$MATR_substituto";
                            $res = mysql_query("$SQL");
                            if($linha=mysql_fetch_array($res)) {
                                $nomechefesubstituto=$linha['Nome'];
                            }
                            print $nomechefesubstituto; ?>
                        </label>
                    </div>
                <? }

                $SQL = "Select nome, tipo from tblusuarios where coduser='$codaprovador'";
                $res = mysql_query("$SQL");
                if($linha2=mysql_fetch_array($res)) { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">
                            Aprovador(a):&nbsp;<? print $nomeaprovador;
                            if ($nomesubstituto) echo " <b><font color=green> ou o(a) substituto(a) indicado(a) pelo RH, " .  $nomesubstituto . "</font></b>";
                            ?>
                        </label>
                    </div>
                <? } ?>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Status:&nbsp;</label>
                    <? if($nivel>1 && ($status=="P" || $status=="T")) { ?>
                        <select class="form-control" name="status">
                            <option value="P" <? if($status=="P") { print "selected"; }?>>Pendente para o RH</option>
                            <option value="T" <? if($status=="T") { print "selected"; }?>>Aguardando retorno</option>
                            <option value="U" <? if($status=="U") { print "selected"; }?>>Retornar ao solicitante</option>
                            <option value="F" <? if($status=="F") { print "selected"; }?>>Finalizada</option>
                        </select>
                    <? } else { ?>
                        <label for="exampleFormControlInput1">
                            <? if($status=="F") { ?>
                                <font class="fonte1">Finalizada</font>
                            <? } else if($status=="A") { ?>
                                <font class="fonte1">Aguardando aprova&ccedil;&atilde;o do chefe</font>
                            <? } else if($status=="P") { ?>
                                <font class="fonte1">Pendente para o RH</font>
                            <? } else if($status=="T") { ?>
                                <font class="fonte1">Aguardando retorno</font>
                            <? } else if($status=="U") { ?>
                                <font class="fonte1">Retornado ao solicitante</font>
                            <? } else if($status=="R") { ?>
                                <font class="fonte1">Recusada pelo chefe</font>
                            <?} else if($status=="C") { ?>
                                <font class="fonte1">Cancelada</font>
                            <? } ?>
                        </label>
                    <? } ?>
                </div>
                <? if ($op!="1") { ?>
                    <? if($nivel>1 && ($status=="P" || $status=="T")) { ?>
                        <input type="button" class="btn btn-primary" name="Salvar" value="Salvar" onclick="salvar();">
                    <? }

                    if($PodeAprovar) { ?>
                        <input type="button" class="btn btn-primary" name="Aceitar" value="Aceitar" onclick="aceita();">
                        <input type="button" class="btn btn-primary" name="Recusar" value="Recusar" onclick="recusa();">
                    <? }
                    if($status=="A" && $nivel ==3 && $codsolicitante != $cod_diretor) {?>
                        <!--input type="button" name="Aprovacao" value="Alterar aprovador" onclick="altproc();"!-->
                        <input type="button" class="btn btn-primary" name="Aprovacao" value="Alterar aprovador" onclick="window.open('altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1',null,'left=120,top=300,height=220,width=780,status=yes,toolbar=no,menubar=no,location=no');">
                    <? }
                    if(($nivel == 3 || $codsolicitante==$coduser_conected || $coduser==$coduser_conected) && $status <> "F" && $status <> "C" && $status <> "R") { ?>
                        <input type="button" class="btn btn-primary" name="Cancelar" value="Cancelar Solicita&ccedil;&atilde;o" onclick="if(confirm('Tem certeza que deseja cancelar esta solicitacao?')) { document.location='exibelicenca.php?cod=<? print $cod; ?>&op=3&datainicio=<? print $datainicio; ?>&status=C&volta=<? print $volta; ?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>'; }">
                        <input type="button" class="btn btn-primary" name="Alterar" value="Alterar Solicita&ccedil;&atilde;o" onclick="document.location='solicitaoutrolicenca.php?cod=<? print $cod; ?>&op=6&volta=<? print $volta;?><?print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';">
                    <?}?>
                    <input type="button" class="btn btn-primary" name="Voltar" value="Voltar" onclick="document.location='<? print $volta; ?>?volta=<? print $volta?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';">
                <? } else { ?>
                    <font size="3" color="green">Solicita&ccedil;&atilde;o criada. Aguarde aprova&ccedil;&atilde;o pela chefia indicada.</font>
                <?}?>
            </div>
        </div>
    </div>
</form>
</body>
</html>
