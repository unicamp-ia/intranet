<?php
mysqli_set_charset('utf8');

$nivelcomp = -1;
// Alterado por Edson Giordani em Outubro/2019 - Indicacao de Gestor Substituto
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Fevereiro/2020 - Responsividade

include("../base/inicio.php"); /* Faz a verificacao se nivel[sist] > nivelcomp e conecta ao banco. Coloca o head do html.*/
include("../base/email.php"); /* Funcao envia_email($assuntoemail, $mensagem, $remetente_email, $destinatario)*/
//echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"/intranet/css/estilo_dan_01.css\">";
function TrataUpload($UPl,$ArqCampo,$NA) {
    global $ArrMimes;
    global $ArrExt;
    global $coduser;
    $TamanhoDoArquivo=$_FILES[$ArqCampo]["size"];
    $TipoDeArquivo=$_FILES[$ArqCampo]["type"];
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mimetype = finfo_file($finfo, $_FILES [$ArqCampo] ['tmp_name']);
    $TMA=$TamanhoDoArquivo/1048576;

    finfo_close($finfo);
    if (!in_array($mimetype, $ArrMimes) && !in_array($TipoDeArquivo,$ArrMimes)) {
        ?>
        <SCRIPT LANGUAGE="JavaScript">
            alert("Formato de arquivo n\u00e3o permitido para upload[mime].");
        </script>
        <?
        return('ErroUpload');
    }
    $z=strlen($UPl);
    $extensao=strrchr($UPl,".");
    $extensao = strtolower($extensao);
    if (!in_array($extensao, $ArrExt)) {
        ?>
        <SCRIPT LANGUAGE="JavaScript">
            alert("Formato de arquivo n\u00e3o permitido para upload[extensao].");
        </script>
        <?
        return('ErroUpload');
    }
    if ($TamanhoDoArquivo > 2300000) {
        ?>
        <SCRIPT LANGUAGE="JavaScript">
            alert("Tamanho do arquivo n\u00e3o pode exceder a 2,1 MB. O tamanho do arquivo indicado \u00e9 de <? echo number_format(round($TMA,2),2,',','');?> MB");
        </script>
        <?
        return('ErroUpload');
    }
    $NU="AF" . $NA . "_" . $coduser . '_' . $_REQUEST['cod'] . $extensao;
    return $NU;
}
include("../base/VerificaAntecedencia.php");
?>
<script type="text/javascript" language="JavaScript">

    function openModal(pUrl, pWidth, pHeight,x,y) {
        var H=document.body.clientHeight;
        var W=document.body.clientWidth;
        var S="/intranet/base/abramodal.php?Lx=" + pWidth + "&Ly=" + pHeight + "&pUrl=" + pUrl;
        if (window.showModalDialog) {
            return window.showModalDialog(S, window,
                "dialogWidth:" + pWidth + "px;dialogHeight:" + pHeight + "px;dialogLeft:" + x + "px;dialogTop:" + y + "px;center:on");
        } else {
            try {
                netscape.security.PrivilegeManager.enablePrivilege(
                    "UniversalBrowserWrite");
                window.open(S, "wndModal", "width=" + pWidth
                    + ",height=" + pHeight + ",resizable=no,modal=yes");
                return true;
            }
            catch (e) {
                alert("Script n\u00e3o confi\u00e3vel, n\u00e3o \u00e9 poss\u00edvel abrir janela modal.");
                return false;
            }
        }
    }
</script>
<?


$cod_diretor=$_SESSION["cod_diretor"];

$busca = $_REQUEST['busca'];/*Codigo para não perder o filtro na busca. */
$pagina=$_REQUEST['pagina'];
$num_linhas=$_REQUEST['num_linhas'];
if($busca) {
    $tipo = $_REQUEST['tipo'];
    $nome = $_REQUEST['nome'];
    $CodArea = $_REQUEST['CodArea'];
    $diainicad = $_REQUEST['diainicad'];
    $mesinicad = $_REQUEST['mesinicad'];
    $anoinicad = $_REQUEST['anoinicad'];
    $diafimcad = $_REQUEST['diafimcad'];
    $mesfimcad = $_REQUEST['mesfimcad'];
    $anofimcad = $_REQUEST['anofimcad'];
    $statusbusca=$_REQUEST['statusbusca'];
    $parametros = "&busca=".$busca."&tipo=".$tipo."&nome=".$nome."&CodArea=".$CodArea."&diainicad=".$diainicad."&mesinicad=".$mesinicad."&anoinicad=".$anoinicad."&diafimcad=".$diafimcad."&mesfimcad=".$mesfimcad."&anofimcad=".$anofimcad."&statusbusca=".$statusbusca;
}

$op = $_REQUEST['op'];
if ($op==12) {
    echo "OK 12";
    exit;
}

$cod = $_REQUEST['cod'];
$nUP = $_REQUEST['nUP'];
$obs = $_REQUEST['obs'];
$volta = $_REQUEST['volta'];
$datainicio = $_REQUEST['datainicio'];
$exterior = $_REQUEST['exterior'];
$exterior_anterior = $_REQUEST['exterior_anterior'];
$status = $_REQUEST['status'];
$timehoje=date("Y-m-d H:i:s");
if(!$datainicio) {
    $datainicio =  date("Y-m-d G:i:s");
}
$rg = $_REQUEST['rg'];
$codsolicitante = $_REQUEST['codsolicitante'];
$coduser = $_REQUEST['coduser'];
$tipo = $_REQUEST['tipo'];
$cargo = $_REQUEST['cargo'];
$lotacao = $_REQUEST['lotacao'];
$datasaida = $_REQUEST['datasaida'];
$dataretorno = $_REQUEST['dataretorno'];
$datasaida_anterior = $_REQUEST['datasaida_anterior'];
$dataretorno_anterior = $_REQUEST['dataretorno_anterior'];
$ehchefe = $_REQUEST['ehchefe'];
$MATR_substituto = $_REQUEST['MATR_substituto'];

$eM1 = "<html><body>";
$eM2 = "</body></html>";
$NaoResponda .= "<br><br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
if ($op=="1" || $op=="4" || $op=="6") {
    $saida_aux=$datasaida;
    $retorno_aux=$dataretorno;
    $vectdata = explode("/",$datasaida);
    $datasaida = $vectdata[2]."-".$vectdata[1]."-".$vectdata[0];
    $vectdata = explode("/",$dataretorno);
    $dataretorno = $vectdata[2]."-".$vectdata[1]."-".$vectdata[0];
}
if ($op=="1" || $op=="4" || ($op=="6" && $saida_aux != $datasaida_anterior)) {
    // Busca o prazo de antecedencia de afastamento
    $antecedencia_afastamento_br=3;
    $antecedencia_afastamento_ex=7;

    $SQL="select * from parametros";
    $res=mysql_query($SQL);
    if ($ln=mysql_fetch_array($res)) {
        $antecedencia_afastamento_br=$ln['antecedencia_afastamento_br'];
        $antecedencia_afastamento_ex=$ln['antecedencia_afastamento_ex'];
    }
    $nda= $antecedencia_afastamento_br + 0;
    if ($exterior=="E") {
        $nda=$antecedencia_afastamento_ex + 0;
    }
    $ndb=0;
    $ndb=ChecaAntecedencia($datasaida,$nda);
    $ndb = $ndb + 0;
    if ($ndb < $nda){
        // Nao aceita pedido de afastamento pois nao atende a antecedencia de $nda dias uteis
        ?>
        <table width="30%" align="center"><tr><td colspan="3">
                    <b>Caro(a) <?echo $_SESSION["nomeuser_conected"];?></b>,<br>
                    Sua solicita&ccedil;&atilde;o de afastamento n&atilde;o pode ser continuada por n&atilde;o atender ao prazo de anteced&ecirc;ncia de <?echo $nda;?>
                    dias &uacute;teis establecidos pela DGRH/Unicamp. Por favor, volte ao formul&aacute;rio e corrija a data de in&iacute;cio
                    ou entre em contado com a se&ccedil;&atilde;o de Recursos Humanos do IA</td>
                <? echo $TblResAnt;?>
            <tr><td align="center" colspan="3"> <br><input type="button" name="Voltar" value="Voltar" class="botao" onClick="javascript:history.go(-1);"></td></tr>
        </table>
        <?
        exit(0);
    }

    $hora=date('H');
    $minuto=date('i');
    $hora = $hora + 0;
    $minuto = $minuto + 0;
    $h1=$hora * 60 + $minuto;
    $h17=1022; // (17h + 2 min)
    if ($ndb==$nda && $h1 >= $h17) {
        $ndb = $ndb - 1;
    }
    if ($ndb<$nda) {
        $mensagem = "Solicita&ccedil;&atilde;o recusada!! Sua solicita&ccedil;&atilde;o para estar no prazo de $nda dia(s) &uacute;til(eis) deveria ter sido feita at&eacute; 17h00. Qualquer d&uacutevida, procure a se&ccedil;&atilde;o de RH do Instituto de Artes.";
        ?><br><br><table width="30%" align="center"><tr><td colspan="3">
                <b>Caro(a) <?echo $_SESSION["nomeuser_conected"];?></b>,<br>
                <?echo $mensagem;?></td></tr><tr><td colspan="3" align="left"><h2><b><font color="green">Dia e Hora da solicita&ccedil;&atilde;o: <?echo date("d/m/Y H:i") . " <br>Inicio do afastamento:". $saida_aux . "<br> Retorno" . $retorno_aux;?></font></h2></td></tr></table><?
        $mensagem = $_SESSION["nomeuser_conected"]. "<br> " . $mensagem . "<br><br>Data e Hora:" . date("d/m/Y H:i")  . " <br>Inicio do afastamento:". $saida_aux . "<br> Retorno" . $retorno_aux;
        envia_email("RH: Sol. de afastamento < 90 dias", $eM1.$mensagem . $NaoResponda . $eM2, "saa@iar.unicamp.br");
        exit(0);
    }
}
$objetivo = $_REQUEST['objetivo'];
$cidade = $_REQUEST['cidade'];
$financiadora = $_REQUEST['financiadora'];
$email_voucher=$_REQUEST['email_voucher'];
$cc_email_voucher=$_REQUEST['cc_email_voucher'];
$celular_voucher=$_REQUEST['celular_voucher'];
$nome_contato_sinistro=$_REQUEST['nome_contato_sinistro'];
$celular_contato_sinistro=$_REQUEST['celular_contato_sinistro'];
$outro = $_REQUEST['outro'];
$ArrMimes = array("image/jpeg","text/plain","application/pdf", "application/msword","application/vnd.ms-office","application/vnd.oasis.opendocument.text","application/vnd.openxmlformats-officedocument.wordprocessingml.document");
$ArrExt= array(".jpg",".JPG",".pdf","PDF",".doc",".DOC",".docx",".DOCX",".odt",".ODT",".txt",".TXT");
$NomeDoArquivo=$_FILES["arquivo_convite"]["name"];
$NomeDoArquivo2=$_FILES["arquivo_convite2"]["name"];

$UPload1="";
$UPload2="";

if ($NomeDoArquivo <> "" ) {
    $UPload1=TrataUpload($NomeDoArquivo,"arquivo_convite","1");
    $NomeDoArquivo = $UPload1;
}

if ($NomeDoArquivo2 <> "" ) {
    $UPload2=TrataUpload($NomeDoArquivo2,"arquivo_convite2","2");
    $NomeDoArquivo2 = $UPload2;
}
if ($UPload1 == "ErroUpload" || $UPload2=="ErroUpload") {
    die("[exibeafastamento] Erro upload.");
}

if ($UPload1 <> "" ) {
    move_uploaded_file($_FILES["arquivo_convite"]["tmp_name"],"../base/upload/" . $UPload1);
}
if ($UPload2 <> "" ) {
    move_uploaded_file($_FILES["arquivo_convite2"]["tmp_name"],"../base/upload/" . $UPload2);
}

if($objetivo=="-1") { /* Se selecionou outro objetivo nao concatena. */
    $objetivo = $outro;
} else if($outro) { /* Se nao selecionou outro objetivo concatena. E hah algum texto na variável outro. */
    $objetivo = $objetivo.". ".$outro;
}
$assunto = "Sistema de Recursos Humanos";

$att= "<br><br>Atenciosamente, Sistema Intranet - RH.";
$IP = $_SERVER['REMOTE_ADDR'];
if($op=="1") { // Insere uma solicitacao de afastamento
    /*  Verifica quem eh o aprovador para a area em que o usuario estah lotado. Se o usuario eh chefe da
        area, entao o aprovador eh o chefe imediato. Retorna a variavel $codaprovador e $nomeaprovador */

    $data = date("Y-m-d");
    // Verifica se o solicitante eh o diretor do IA. Se for, o status passa automativamente a Pendente para RH
    if ($codarea_conected == $cod_diretoria && $coduser_conected==$_SESSION['coduser_chefe']) {
        $STA="P";
    } else {
        include("busca_aprovador.php");
        $STA="A";
    }

    // verifica se existe um pedido na mesma data
    $SQL = "Select count(*) as total
            from RHafastamento T1, RHsolicitacoes T2 
            WHERE T1.cod=T2.cod and rg='$rg' and datasaida='$datasaida' and dataretorno='$dataretorno' and status='A' AND T2.codsolicitante='$codsolicitante'";
    $res = mysql_query("$SQL");
    $linha=mysql_fetch_array($res);
    if ($linha['total']>0) { ?>
        <script type="text/javascript" language="JavaScript">
            alert("Foi encontrada no sistema, aguardando aprovação do chefe, uma solicitação de afastamento para a data informada. Verifique o pedido. ");
        </script>
        <? include("menu.php");
        die();
    }

    $SQL = "Insert into RHsolicitacoes (codsolicitante, coduser, codaprovador, lotacao, status, tipo, dataaltera, datacadastro, obs,ip_solicitacao) values ('$codsolicitante', '$coduser', '$codaprovador', '$lotacao', '$STA', '$tipo', '$datainicio','$data','".addslashes($obs)."','$IP')";
    $res = mysql_query("$SQL");
    if (!$res) {
        envia_email("[exibeafastamento] Erro de cadastro 1", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
        include("menu.php");
        die("[exibeafastamento] Erro de cadastro 1. Entre em contato com o responsavel pelo desenvolvimento da Intranet na DTI/IA ");
    }
    $SQL = "Select cod from RHsolicitacoes where dataaltera='$datainicio' and tipo='$tipo' and codsolicitante='$codsolicitante'";
    $res = mysql_query("$SQL");
    if($linha=mysql_fetch_array($res)) {
        $cod = $linha['cod'];
        if ($ehchefe=="sim") {
            $SQL = "Insert into RHafastamento (MATR_substituto, cod, rg, cargo, datasaida, dataretorno, objetivo, cidade, exterior, financiadora,email_voucher,cc_email_voucher,celular_voucher,nome_contato_sinistro,celular_contato_sinistro";
        } else {
            $SQL = "Insert into RHafastamento (cod, rg, cargo, datasaida, dataretorno, objetivo, cidade, exterior, financiadora,email_voucher,cc_email_voucher,celular_voucher,nome_contato_sinistro,celular_contato_sinistro";
        }

        if ($UPload1 <> "") {
            $SQL .=", arquivo_convite";
        }
        if ($UPload2 <> "") {
            $SQL .=", arquivo_convite2";
        }
        if ($ehchefe=="sim") {
            $SQL .=") values ($MATR_substituto,'$cod','$rg','$cargo', '$datasaida', '$dataretorno', '".addslashes($objetivo)."', '$cidade','$exterior','$financiadora','$email_voucher','$cc_email_voucher','$celular_voucher','$nome_contato_sinistro','$celular_contato_sinistro'";
        } else {
            $SQL .=") values ('$cod','$rg','$cargo', '$datasaida', '$dataretorno', '".addslashes($objetivo)."', '$cidade','$exterior','$financiadora','$email_voucher','$cc_email_voucher','$celular_voucher','$nome_contato_sinistro','$celular_contato_sinistro'";
        }
        if ($UPload1 <> "") {
            $SQL .=",'$UPload1'";
        }
        if ($UPload2 <> "") {
            $SQL .=",'$UPload2'";
        }
        $SQL .=")";
        $res = mysql_query("$SQL");
        if (!$res) {
            envia_email("[exibeafastamento] Erro de cadastro 2", "Invalid query: ".mysql_error()."<BR>SQL: ".$SQL, "giordani@unicamp.br");
            include("menu.php");
            die("[exibeafastamento] Erro de cadastro 2. Entre em contato com o responsavel pelo desenvolvimento da Intranet na DTI/IA ");
        }

        $S=addslashes($SQL);

        $SQL = "Select * from RHsolicitacoes where cod='$cod'";
        $res = mysql_query("$SQL");
        if($linha1=mysql_fetch_array($res)) {
            $tipo = $linha1['tipo'];
        }
        $desctipo="afastamento";
        $dt1=strftime("%d/%m/%Y", strtotime($datasaida));
        $dt2=strftime("%d/%m/%Y", strtotime($dataretorno));
        $emailsolicitante=$_SESSION["emailuser_conected"];
        if ($STA=="A") {
            $email_notif=$emailaprovador;
            if ($codsubstituto <> "") {
                $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                $email_notif=$email_notif . "," . $emailsubstituto;
            } else {
                $mensagem = "Caro(a) $nomeaprovador,";
            }
            $mensagem .= "<br><br>Foi cadastrada pelo(a) colaborador(a) " . $_SESSION['nomeuser_conected'] . ", uma solicita&ccedil;&atilde;o de $desctipo para o periodo de $dt1 a $dt2.";
            $mensagem .= "<br>Por favor, acesse o link abaixo para aprovar ou recusar o pedido.<br>";
            $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibeafastamento.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
            envia_email($assunto, $eM1 . $mensagem . $att . $NaoResponda . $eM2, $email_notif);

            $mensagem .= "<br> Aprovador(es):" . $email_notif;
            $emailros="intra.ia@iar.unicamp.br";
            //envia_email("Notificacao RH - $desctipo - Aprovador", $eM1 .  $mensagem .  $att . $NaoResponda . $eM2, $emailros);

            $mensagem = "<br>Caro(a) " . $_SESSION['nomeuser_conected'] . ", <br>Sua solicita&ccedil;&atilde;o de afastamento foi encaminhada para aprova&ccedil;&atilde;o de sua chefia.";
            $mensagem .="<br>O(a) respons&aacute;vel pela aprova&ccedil;&atilde;o de sua solicita&ccedil;&atilde;o &eacute; " . $nomeaprovador;
            if ($codsubstituto <> "") {
                $mensagem .=" ou o substituto indicado, sr(a) " . $nomesubstituto;
            }
            $mensagem .= " . Por favor, aguarde.";
            envia_email($assunto, $eM1. $mensagem. $att . $NaoResponda . $eM2, $emailsolicitante);
        }  else {
            // O solicitante eh o diretor do IA.
            // Busca os e-mails dos pessoal do RH
            $SQL="select u.nome, u.email, u.coduser,s.idsistema, s.permissao from tblusuarios u left join tblususist s on u.coduser=s.coduser where s.idsistema=19 and permissao=3";
            $res = mysql_query("$SQL");
            $ll=0;
            $emailRH="";
            while($linha3=mysql_fetch_array($res)) {
                $ll=$ll+1;
                if ($ll>1) {
                    $emailRH .= ",";
                }
                $emailRH .= $linha3['email'];
            }
            $mensagem = "O(a) diretor(a) do Instituto de Artes, " . $_SESSION['nomeuser_conected'] . " solicita afastamento para periodo de $dt1 a $dt2.<br>";
            $mensagem .= "O n&uacute;mero da solicita&ccedil;&atilde;o &eacute; $cod e o status foi alterado para <b>Pendente para RH<b>. Por favor, providenciar portaria de afastamento junto &agrave; DGRH.";
            $mensagem .= $att;
            envia_email($assunto, $eM1. $mensagem. $att . $NaoResponda . $eM2, $emailRH);
        }
        $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Inser&ccedil;&atilde;o de registro - Solicita&ccedil;&atilde;o Afastamento $cod', '$S','$timehoje')";
        $res=mysql_query($SQLlog) or die("[exibeafastamento] RH: erro ao inserir LOG 0");
    } else {?>
        <script type="text/javascript" language="JavaScript">
            alert("N&atilde;o foi poss&iacute;vel criar a solicita&ccedil;&atilde;o de afastamento. Provavelmente houve algum erro no servidor Mysql. Informe este problema ao respons&aacute;vel pelo desenvolvimento da Intranet na DTI/IA.");
        </script>
        <?
        die();
    }
}
if($op=="3") {  // Usuario RH alterou o status da solicitacao, mantendo o Status de Pendente ou Finalizada.
    $SQL = "Select s.tipo, s.dataaltera, u.usuario, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod'";
    $res = mysql_query("$SQL");
    if(mysql_num_rows($res) > 0) {
        $linha = mysql_fetch_array($res);
        $emailsolicitante=$linha['email'];
        $nomesolicitante=$linha['nome'];
        $SQL = "Update RHsolicitacoes set dataaltera = '$datainicio', codatendente='$coduser_conected', status='$status' where cod='$cod'";
        $S=addslashes($SQL);
        $res = mysql_query("$SQL") or die("[exibeafastamento] Erro ao cadastrar o pedido. Entre em contato com o administrador.");
        if ($status == "F") {
            $mensagem = "Caro(a) $nomesolicitante .<br>Sua solicita&ccedil;&atilde;o de afastamento n&uacute;mero $cod foi finalizada pelo RH do IA.";
            $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o";
            $mensagem .= "<br><br>Atenciosamente,<br> " . $_SESSION["nomeuser_conected"] . " <br> RH";
            envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
            $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Finaliza&ccedil;&atilde;o - Solicita&ccedil;&atilde;o Afastamento $cod', '$S','$timehoje')" ;
            $res=mysql_query($SQLlog) or die("[exibeafastamento] RH: erro ao inserir LOG 1");

        }
        if ($status == "C") {
            if ($coduser <> $coduser_conected) {
                $mensagem = "Caro(a) $nomesolicitante .<br>Sua solicita&ccedil;&atilde;o de afastamento n&uacute;mero $cod foi cancelada pelo RH do IA.";
                $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o";
                $mensagem .= "<br><br>Atenciosamente,<br> " . $_SESSION["nomeuser_conected"] . " <br> RH";
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Cancelamento - Solicita&ccedil;&atilde;o Afastamento $cod', '$S','$timehoje')";
                $res=mysql_query($SQLlog) or die("[exibeafastamento] RH: erro ao inserir LOG 2");
            }
        }

    } else {
        die("Algu&eacute;m estava alterando o pedido. N&atilde;o foi poss&iacute;vel realizar sua altera&ccedil;&atilde;o, tente novamente.");
    }
}
if($op=="4" || $op=="6") {  // Alteracao de uma solicitacao pelo proprio a partir da relacao de solitacoes ou atendendo ao retorno do RH  usuario ou pelo usuario RH
    $SQL = "Update RHsolicitacoes set lotacao='$lotacao', coduser='$coduser', codsolicitante='$coduser', dataaltera = '$datainicio', lotacao='$lotacao',  status='$status' where cod='$cod'";
    $S=addslashes($SQL);
    $res = mysql_query("$SQL") or die("Erro ao cadastrar o pedido. Entre em contato com o administrador.");
    if ($ehchefe=="sim") {
        $SQL = "Update RHafastamento set MATR_substituto=$MATR_substituto, financiadora='$financiadora', rg='$rg', cargo='$cargo', datasaida='$datasaida', dataretorno= '$dataretorno', objetivo='$objetivo', cidade='$cidade', exterior='$exterior', email_voucher='$email_voucher',celular_voucher='$celular_voucher',cc_email_voucher='$cc_email_voucher',nome_contato_sinistro='$nome_contato_sinistro',celular_contato_sinistro='$celular_contato_sinistro'";
    } else {
        $SQL = "Update RHafastamento set financiadora='$financiadora', rg='$rg', cargo='$cargo', datasaida='$datasaida', dataretorno= '$dataretorno', objetivo='$objetivo', cidade='$cidade', exterior='$exterior', email_voucher='$email_voucher',celular_voucher='$celular_voucher',cc_email_voucher='$cc_email_voucher',nome_contato_sinistro='$nome_contato_sinistro',celular_contato_sinistro='$celular_contato_sinistro'";
    }
    if ($NomeDoArquivo) {
        $SQL .= ",arquivo_convite='$NomeDoArquivo'";
    }
    $SQL .= " where cod='$cod'";
    $res = mysql_query("$SQL") or die("[exibeafastamento] Erro ao cadastrar o pedido. Entre em contato com o administrador. 3 ");
    if($status=="U") {
        echo "[exibeafastamento] Problema na execu&ccedil;&atilde;o do fluxo de dados. 4 Comunique este erro &agrave; DTI";
        die();

        $SQL = "Select u.usuario, u.email, s.permissao from tblusuarios u left join RHsolicitacoes r on u.coduser=r.codsolicitante left join tblususist s on u.coduser=s.coduser and s.idsistema='19' where r.cod='$cod'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $email = $linha['email'];
            $permissao = $linha['permissao'];
            $assunto = "Sistema de Recursos Humanos";

            $SQL = "Select * from RHsolicitacoes where cod='$cod'";
            $res = mysql_query("$SQL");
            if($linha=mysql_fetch_array($res)) {
                $tipo = $linha['tipo'];
            }
            $desctipo="afastamento";
            $mensagem = "Foi solicitada a altera&ccedil;&atilde;o de um pedido de ".$desctipo." cadastrado por voc&ecirc;.";
            $mensagem .= "<br>Voc&ecirc; deve alterar o pedido no link abaixo.<br>";

            if($tipo=="A") {
                if(!$permissao || $permissao<1) {
                    $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitafastamento.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";
                } else {
                    $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitaoutrofastamento.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";
                }
            } else if($tipo=="L") {
                if(!$permissao || $permissao<1) {
                    $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitalicenca.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";
                } else {
                    $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/solicitaoutrolicenca.php&parametros=cod=".$cod."andvolta=histsolicitacoes.phpandop=4\">Acesse este link</a>";
                }
            }

            $mensagem .= "<br><br> Atenciosamente, RH.";

            envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email);
        }
    } else if($status=="A" && ($saida_aux != $datasaida_anterior || $retorno_aux != $dataretorno_anterior || $exterior != $exterior_anterior)) {
        /*  Verifica quem eh o aprovador para a area em que o usuario esta lotado. Se o usuario eh chefe da area, entao o aprovador eh o chefe imediato
            Retorna a variavel $codaprovador e $nomeaprovador
       */
        include("busca_aprovador.php");
        $SQL = "Select s.tipo, u.email, u.nome from RHsolicitacoes s left join tblusuarios u on s.codsolicitante=u.coduser where s.cod='$cod'";
        $res = mysql_query("$SQL");
        if($linha=mysql_fetch_array($res)) {
            $tipo = $linha['tipo'];
            $emailsolicitante=$linha['email'];
            $nomesolicitante=$linha['nome'];
        }
        if($tipo=="L") { $desctipo="licen&ccedil;a pr&ecirc;mio"; } else if($tipo=="A") { $desctipo="afastamento"; }
        $email_notif=$emailaprovador;
        if ($codsubstituto <> "") {
            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
            $email_notif=$email_notif . "," . $emailsubstituto;
        } else {
            $mensagem = "Caro(a) $nomeaprovador,";
        }
        $mensagem .= "<br>Foi alterada uma solicita&ccedil;&atilde;o de $desctipo que necessita de sua aprova&ccedil;&atilde;o.";
        $mensagem .= "<br>Por favor, aprovar ou recusar o pedido no link abaixo.<br>";
        $mensagem .= "<a href=\"".$URLemail."?sist=RH&pagina=RH/exibeafastamento.php&parametros=cod=".$cod."andvolta=autorizar.php\">Acesse este link</a>";
        $mensagem .= "<br><br> Atenciosamente, RH.";
        envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
        // Notifica o usuario
        $mensagem = "Caro(a) $nomesolicitante ";
        $mensagem .= "<br>Sua solicita&ccedil;&atilde;o de " . $desctipo . " foi alterada e ser&aacute; submetida &agrave; aprova&ccedil;&atilde;o do Chefe de Departamento/Se&ccedil;&atilde;o,";
        $mensagem .= " Sr(a) " . $nomeaprovador;
        if ($codsubstituto <> "") {
            $mensagem .=" ou ao(&agrave;) substituto(a) indicado(a), " . $nomesubstituto;
        }
        $mensagem .=  " . Por favor, aguarde. ";
        $mensagem .= "<br>Qualquer d&uacute;vida, entrar em contato com esta se&ccedil;&atilde;o.";
        $mensagem .= "<br>Atenciosamente,<br> RH";
        envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
        $SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','RH', 'Altera&ccedil;&atilde;o - Solicita&ccedil;&atilde;o Afastamento $cod', '$S','$timehoje')";
        $res=mysql_query($SQLlog) or die("RH: erro ao inserir LOG");
    }
}
if($op=="7" && $cod && $status) {  // Reabertura de uma solicitacao de afastamento pelo RH, nesnte mesmo script
    $SQL = "Update RHsolicitacoes set status='$status' where cod='$cod'";
    $S=addslashes($SQL);
    $res = mysql_query("$SQL") or die("[exibeafastamento] Erro 5 ao cadastrar o pedido. Entre em contato com o administrador.");
    $status="";
}
$EoChefe=false;
$EoSubstituto=false;
$Echefe=false;
include ("VerifiquePosicao.php");
// Se o codigo da solicitacao, entao o chamadodo pode ter vindo:
// De uma mensagem de e-mail solicitando aprovacao ou da tela minhas solicitacoes

if($cod) {
    if ($op=="2") { // Usuario pode estar querendo remover um arquivo idicado como carta-convite
        $SQL = "select cod,arquivo_convite,arquivo_convite2 from RHafastamento where cod=$cod";
        $res = mysql_query($SQL);
        $linha = mysql_fetch_array($res);
        $OC="arquivo_convite";
        $OF=$linha['arquivo_convite'];
        if ($nUP=='2')  {
            $OC="arquivo_convite2";
            $OF=$linha['arquivo_convite2'];
        }
        $SQL="update RHafastamento set " . $OC . "='' where cod=$cod";
        $res = mysql_query($SQL);
        $linhacomando=sprintf("/bin/rm -f /usr/local/www/data/intranet/base/upload/%s",$OF);
        system($linhacomando);
    }
    $SQL="select s.*, u.nome,ap.nome as nomeaprovador from RHsolicitacoes s left join tblusuarios u on s.coduser=u.coduser left join tblusuarios ap on s.codaprovador=ap.coduser  where s.cod='$cod'";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $status = $linha['status'];
    $codsolicitante = $linha['codsolicitante'];
    $coduser = $linha['coduser'];
    $codaprovador= $linha['codaprovador'];
    $codatendente= $linha['codatendente'];
    $nome = $linha['nome'];
    $obs = $linha['obs'];
    $lotacao = $linha['lotacao'];

    $SQL = "select *, date_format(dataretorno, '%d/%m/%Y') as dataretorno, date_format(datasaida, '%d/%m/%Y') as datasaida from RHafastamento where cod=$cod";
    $res = mysql_query($SQL);
    $linha = mysql_fetch_array($res);
    $rg = $linha['rg'];
    $cargo = $linha['cargo'];
    $datasaida = $linha['datasaida'];
    $dataretorno = $linha['dataretorno'];
    $exterior = $linha['exterior'];
    $email_voucher=$linha['email_voucher'];
    $cc_email_voucher=$linha['cc_email_voucher'];
    $celular_voucher=$linha['celular_voucher'];
    $nome_contato_sinistro=$linha['nome_contato_sinistro'];
    $celular_contato_sinistro=$linha['celular_contato_sinistro'];
    $objetivo = $linha['objetivo'];
    $cidade = $linha['cidade'];
    $financiadora = $linha['financiadora'];
    $arquivo_convite=$linha['arquivo_convite'];
    $arquivo_convite2=$linha['arquivo_convite2'];
    $MATR_substituto = $linha['MATR_substituto'];
}
//Quem pode aprovar, caso a solicitacao ainda esteja com status "Aguardando aprovacao"?
$PodeAprovar=false;
if ($status=="A") {
    if ($coduser_conected==$codaprovador) {
        $PodeAprovar=true;
    }
    if ($EoSubstituto && is_array($aprovadores_substituto) && count($aprovadores_substituto)>0 ) {
        if (in_array($codaprovador,$aprovadores_substituto)) {
            $PodeAprovar=true;
            $InformeComoAprovador=true;
        }
    }
}

// Busca o aprovador substituto, para a area de lotacao do solicitante, caso ele exista
// Monta a hierarquia para a area base ($lotacao) e acima da area base
$nomesubstituto="";
MontaHierarquia($lotacao,"S","S","N");
if ($hierarquia['chefia']['matr'] == $codaprovador || $hierarquia['acima']['matr']==$codaprovador || $hierarquia['ATU']['matr']==$codaprovador || $hierarquia['DIR']['matr']==$codaprovador) {
    if ($hierarquia['chefia']['matr'] !=  $coduser) {
        if ($hierarquia['substituto_chefia']['matr']==$coduser) { // solicitante eh substituto no departamento dele
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        } else {
            if ($hierarquia['chefia']['matr'] == $codaprovador) {
                if ($hierarquia['substituto_chefia']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_chefia']['nome']; }
            } elseif ($hierarquia['acima']['matr']==$codaprovador) {
                if ($hierarquia['substituto_acima']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_acima']['nome']; }
            } elseif ($hierarquia['ATU']['matr']==$codaprovador) {
                if ($hierarquia['substituto_atu']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_atu']['nome']; }
            } else {
                if ($hierarquia['substituto_dir']['matr']) {
                    $nomesubstituto=$hierarquia['substituto_dir']['nome']; }
            }
        }
    } else {
        if ($hierarquia['substituto_acima']['matr'] && $hierarquia['substituto_acima']['matr'] !=  $coduser) {
            $nomesubstituto=$hierarquia['substituto_acima']['nome'];
        }
    }
}
?>
<SCRIPT LANGUAGE="JavaScript">
    function salvar () {
        if(document.formulario.status.value=='U') {
            document.location='retornasolicitacoes.php?cod=<? print $cod; ?>&datainicio=<? print $datainicio; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>'
        } else {
            document.location='exibeafastamento.php?cod=<? print $cod; ?>&op=3&datainicio=<? print $datainicio; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status='+document.formulario.status.value;
        }
    }
    function aceita () {
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=P&op=1&obs='+document.formulario.obs.value;
    }
    function reabrir () {
        sta=document.formulario.status.value;
        document.location='exibeafastamento.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=7&status='+sta;
    }
    function recusa () {
        if(document.formulario.obs.value=="") {
            alert('Preencha uma justificativa para a recusa no campo observa\u00e7\u00e3o!');
            document.formulario.obs.focus();
            return false;
        }
        document.location='autorizar.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&status=R&op=1&obs='+document.formulario.obs.value;
    }
    function altproc() {
        document.location='altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1'
    }
</SCRIPT>
<?
include("menu.php");
?>
<form name="formulario">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 bg-light">
                <div class="form-group">
                    <label for="exampleFormControlInput1"><b>Solicita&ccedil;&atilde;o afastamento at&eacute; 90 dias -
                            <? if($exterior=="N") { ?> Viagem dentro do pa&iacute;s<? } ?>
                            <? if($exterior=="E") { ?> Viagem ao exterior<? } ?></b>
                    </label>
                </div>
                <div class="form-group bg-light">
                    <label for="exampleFormControlInput1">
                        Matr&iacute;cula:&nbsp;<? print $coduser; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Nome:&nbsp;<? print $nome; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        RG:&nbsp;<? print $rg; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Cargo/Fun&ccedil;&atilde;o:&nbsp;<? print $cargo; ?>
                    </label>
                </div>
                <div class="form-group bg-light">
                    <label for="exampleFormControlInput1">
                        Lota&ccedil;&atilde;o:&nbsp;
                        <?
                        $SQL ="SELECT * FROM areas where CodArea='$lotacao' and origem='I'";
                        $resultado = mysql_query($SQL);
                        if ($row1=mysql_fetch_array($resultado)) { ?>
                            <? print $row1['Descricao']; ?>
                        <? } ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1"><font color="green">Per&iacute;odo do afastamento</font></label><br/>
                    <label for="exampleFormControlInput1">&nbsp;&nbsp;&nbsp;
                        Data: sa&iacute;da em <?print $datasaida;?> e retorno em <?print $dataretorno;?>
                    </label>
                </div>

                <? if ($exterior=='E') { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><font color="green">Voucher de seguro</font></label><br/>
                        <label for="exampleFormControlInput1">&nbsp;&nbsp;&nbsp;
                            e-mail p/ envio:&nbsp;<?echo $email_voucher;?>
                        </label><br/>
                        <label for="exampleFormControlInput1">&nbsp;&nbsp;&nbsp;
                            CC (Com c&oacute;pia): <?echo $cc_email_voucher;?>
                        </label><br/>
                        <label for="exampleFormControlInput1">&nbsp;&nbsp;&nbsp;
                            Celular p/contato: <?echo $celular_voucher;?>
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><font color="green">Contato p/ sinistro</font></label><br/>
                        <label for="exampleFormControlInput1">&nbsp;&nbsp;&nbsp;
                            Nome: <?echo $nome_contato_sinistro;?>
                        </label><br/>
                        <label for="exampleFormControlInput1">&nbsp;&nbsp;&nbsp;
                            Celular: <?echo $celular_contato_sinistro;?>
                        </label>
                    </div>
                <?}?>

                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Finalidade: <? print $objetivo; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Cidade-Pa&iacute;s: <? print $cidade; ?>
                    </label>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        Ag&ecirc;ncia financiadora -
                        <? if($financiadora) { print $financiadora;
                        } else { ?> Não<? } ?>
                    </label>
                </div>

                <? if ($arquivo_convite || $arquivo_convite2) {      ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1"><font color="green">Convite anexado</font></label><br/>
                        <label for="exampleFormControlInput1">&nbsp;&nbsp;&nbsp;
                            Clique no &iacute;cone para visualiz&aacute;-lo
                        </label>
                        <?
                        $browser=$_SESSION['browser'];
                        if ($arquivo_convite) {
                            if ($browser=="Chrome" || $browser=="Safari" ) {?>
                                <a href="#" onClick="window.open('/intranet/base/upload/<? echo $arquivo_convite;?>');"> <img src="../imagens/list.ico" height="35" width="35"></a>
                            <?} else {?>
                                <a href="#" onClick="openModal('/intranet/base/upload/<? echo $arquivo_convite;?>', 1200, 700)"> <img src="../imagens/list.ico" height="35" width="35"></a>
                            <?}
                        }
                        if ($arquivo_convite2) {
                            if ($browser=="Chrome"  || $browser=="Safari") {?>
                                <a href="#" onClick="window.open('/intranet/base/upload/<? echo $arquivo_convite2;?>');"> <img src="../imagens/list.ico" height="35" width="35"></a>
                            <?} else {?>
                                <a href="#" onClick="openModal('/intranet/base/upload/<? echo $arquivo_convite2;?>', 1200, 700)"> <img src="../imagens/list.ico" height="35" width="35"></a>
                            <?}
                        }?>
                    </div>
                    <div class="form-group">
                        <?php //lista arquivos
                        $i = 0;
                        $files = glob("../../intranet/base/upload/AF1_".$coduser."*");
                        foreach ($files as $file){
                            if($i <= 5) {
                                if ($browser == "Chrome" || $browser == "Safari") { ?>
                                    &nbsp;&nbsp;&nbsp;<a href="#" onClick="window.open('<? echo $file; ?>');"><?php echo $file ?></a>
                                <? } else { ?>
                                    &nbsp;&nbsp;&nbsp;<a href="#" onClick="openModal('<? echo $arquivo_convite; ?>', 1200, 700)"><?php echo $file ?></a>
                                <? }
                                $i++;
                            }
                        } ?>
                    </div>
                <? }

                if (($ehchefe=="sim")||($MATR_substituto!=0)) { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">
                            Substituto(a):&nbsp;
                            <?
                            $SQL = "Select Nome from tblusuarios where coduser=$MATR_substituto";
                            $res = mysql_query("$SQL");
                            if($linha=mysql_fetch_array($res)) {
                                $nomechefesubstituto=$linha['Nome'];
                            }
                            print $nomechefesubstituto; ?>
                        </label>
                    </div>
                <? }

                if ($codatendente) {
                    $SQL = "Select ramal, nome from tblusuarios where coduser='$codatendente'";
                    $res = mysql_query("$SQL");
                    if($linha=mysql_fetch_array($res)) { ?>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">
                                Respons&aacute;vel:&nbsp;<? print $linha['nome']; ?>
                            </label>
                            <label for="exampleFormControlInput1">
                                , ramal:&nbsp;<? print $linha['ramal']; ?>
                            </label>
                        </div>
                    <? }
                }

                // Informa quem eh o aprovador
                $SQL = "Select u.nome, u.tipo from tblusuarios u where u.coduser='$codaprovador'";
                $res = mysql_query("$SQL");
                if($linha2=mysql_fetch_array($res)) {
                    $aprovs=$linha2['nome'];
                    if ($nomesubstituto) $aprovs .= " <b><font color=green>ou o(a) substituto(a) indicado(a) pelo RH, " . $nomesubstituto . "</font></b>";
                    ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">
                            Aprovador(a):&nbsp;<? print $aprovs; ?>
                        </label>
                    </div>
                <? }

                if(($nivel == 3 && $status=="P") || ($PodeAprovar)) { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Observa&ccedil;&atilde;o:</label>
                        <textarea class="form-control" type="text" name="obs" id="obs" cols="50" rows="4"><? print $obs; ?></textarea>
                    </div>
                <? } else { ?>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Observa&ccedil;&atilde;o: <? print $obs; ?></label>
                        <input type="hidden" name="obs" id="obs" value="<? print $obs; ?>">
                    </div>
                <? } ?>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Status:&nbsp;</label>
                    <? // Se usuario eh RH e o status = Pendente para RH, entao disponibiliza a ele as opcoes de Finalizar e Retornar ao solicitante
                    if($nivel == 3 && ($status=="P" || $status=="T" || $status=="Z")) { ?>
                        <select class="form-control" name="status">
                            <option value="P" <? if($status=="P") { print "selected"; }?>>Pendente para o RH</option>
                            <option value="U" <? if($status=="U") { print "selected"; }?>>Retornar ao solicitante</option>
                            <option value="Z" <? if($status=="Z") { print "selected"; }?>>Aguardando portaria</option>
                            <option value="T" <? if($status=="T") { print "selected"; }?>>Aguardando retorno</option>
                            <option value="F" <? if($status=="F") { print "selected"; }?>>Finalizada</option>
                        </select>
                    <? } else { ?>
                        <label for="exampleFormControlInput1">
                            <? if($status=="F") { ?>
                                <font class="fonte1">Finalizada</font>
                            <? } else if($status=="A") { ?>
                                <font class="fonte1">Aguardando aprova&ccedil;&atilde;o do chefe</font>
                            <? } else if($status=="P") { ?>
                                <font class="fonte1">Pendente para o RH</font>
                            <? } else if($status=="T") { ?>
                                <font class="fonte1">Aguardando retorno</font>
                            <? } else if($status=="Z") { ?>
                                <font class="fonte1">Aguardando portaria</font>
                            <? } else if($status=="U") { ?>
                                <font class="fonte1">Retornado ao solicitante</font>
                            <? } else if($status=="R") { ?>
                                <font class="fonte1">Recusada pelo chefe</font>
                            <?} else if($status=="C") { ?>
                                <font class="fonte1">Cancelada</font>
                            <? } ?>
                        </label>
                    <? } ?>
                </div>

                <? if($nivel == 3 && ($status=="F" || $status=='R' || $status=='C')) {?>
                    <input type="button" name="Reabra" value="Reabrir solicita&ccedil;&atilde;o com o status ->" onclick="reabrir();">&nbsp;&nbsp;
                    <select name="status" class="form-control">
                        <option value="A">Aguardando aprova&ccedil;&atilde;o da chefia</option>
                        <option value="P">Pendente para o RH</option>
                        <option value="Z">Aguardando portaria</option>
                        <option value="T">Aguardando retorno</option>
                    </select>
                    <br/>
                <? }

                if ($op!="1") { ?>
                    <? if($nivel == 3 && $status !="F" && $status !="C") { // Disponibiliza o botao salvar pois o campo OBS estah ativo e o RH poderah retornar ao solicitante ou finalizar ?>
                        <input type="button" class="btn btn-primary" name="Salvar" value="Salvar" onclick="salvar();">
                    <? }
                    if($PodeAprovar) { ?>
                        <input type="button" class="btn btn-primary" name="Aceitar" value="Aceitar" onclick="aceita();">
                        <input type="button" class="btn btn-primary" name="Recusar" value="Recusar" onclick="recusa();">
                    <? }
                    if($status=="A" && $nivel == 3 && $codsolicitante <> $cod_diretor) {?>
                        <!-- input type="button" name="Aprovacao" value="Alterar aprovador" onclick="altproc();"!-->
                        <input type="button" class="btn btn-primary" name="Aprovacao" value="Alterar aprovador" onclick="window.open('altaprovador.php?cod=<? print $cod; ?>&volta=<? print $volta; ?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>&op=1',null,'left=120,top=300,height=220,width=780,status=yes,toolbar=no,menubar=no,location=no');">
                    <? }
                    if(($nivel == 3 || $codsolicitante==$coduser_conected || $coduser==$coduser_conected) && $status <> "F" && $status <> "C" && $status <> "R") { ?>
                        <input type="button" class="btn btn-primary" name="Cancelar" value="Cancelar Solicita&ccedil;&atilde;o" onclick="if(confirm('Tem certeza que deseja cancelar esta solicita&ccedil;&atilde;o?')) { document.location='exibeafastamento.php?cod=<? print $cod; ?>&op=3&datainicio=<? print $datainicio; ?>&status=C&volta=<? print $volta;?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>';}">
                        <input type="button" class="btn btn-primary" name="Alterar" value="Alterar Solicita&ccedil;&atilde;o" onclick="document.location='solicitaoutrofastamento.php?cod=<? print $cod; ?>&op=6&volta=<? print $volta;?><? print $parametros;?>&pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?>'">
                    <?}?>
                    <input type="button" class="btn btn-primary" name="Voltar" value="Voltar" onclick="document.location='<? print $volta; ?>?pagina=<?echo $pagina;?>&num_linhas=<?echo $num_linhas;?><? print $parametros;?>'">
                <?} else {?>
                    <font size="3" color="green">Solicita&ccedil;&atilde;o criada. Aguarde aprova&ccedil;&atilde;o pela chefia indicada.</font>
                <?}?>
            </div>
        </div>
    </div>
</form>

<?if ($op==7){?>
    <SCRIPT LANGUAGE="JavaScript">
        alert('Solicita\u00e7\u00e3o reaberta com o status selecionado!');
    </script>
<?}?>
</body>
</html>











