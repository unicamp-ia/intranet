<?
include("../base/inicio.php"); /* Faz a verificacao se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
include("../base/FuncoesUteis.php");
include("menu.php");
?>
<div class="container">
    <p class="alert alert-primary">Os pedidos de afastamentos maiores de 90 dias
        dever&atilde;o ser solicitados junto à <strong>Seção de Apoio aos Departamentos</strong>
        do Instituto de Artes.
    </p>

    <strong>Contato</strong>
    <p>
        <a href="mailto:apdepia (arroba) unicamp (ponto) br">Edimilson do Carmo</a><br />
    </p>
    <p>
        (19) 3521-7081
    </p>

</div>