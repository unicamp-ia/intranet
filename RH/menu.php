<?
// Permissoes de acesso ao modulo RH
// ------------------------------------------
//   $nivel representa a permissao
//   1 = Relatorios produtividade
//   2 = Cadastro (legado: nao utilizado)
//   3 = Gerenciamento

include("../base/VerifiqueLogin.php");

$tipo_conected = $_SESSION["tipo_conected"];
$codarea_conected = $_SESSION["codarea_conected"];
$coduser_conected = $_SESSION["coduser_conected"];
// Para efeito de teste em producao
// if ($coduser_conected=='119873') $_SESSION["RH"]=3; // Remover assim que deixar de desenvolver a intranet
// Se usuario eh o gestor de informatica, entao ele deve ter permissao de gerenciamento de RH, isso para acompanhar o funcionamento
// do sistema
if ($_SESSION['GestorInformatica']==$coduser_conected) {
    $_SESSION["RH"]=3;
}
$regime_trabalho = $_SESSION["regime_trabalho"];
$nivel = $_SESSION["RH"];
$EoChefe = false;
$EoSecretario = false;
$EoSubstituto = false;
$Echefe=false;
$Eapoiador=false;
//echo $nivel;
include("VerifiquePosicao.php");

// Verifica se o usuario pode ser aprovador indicado de alguma solicitacao.
$EaprovadorIndicado=false;
if (!$EoChefe && !$Echefe && !$EoSubstituto) {
    $SQL="SELECT count(cod) as qta from RHsolicitacoes where codaprovador='$coduser_conected' and status='A'";
    // echo $SQL;
    $RsI=mysql_query($SQL);
    if ($LNC=mysql_fetch_array($RsI)) {
        if ($LNC['qta'] > 0) $EaprovadorIndicado=true;
    }
}
//Verifica se o usuario eh Apoiador Departamental
if($codarea_conected=="APDEPT") {
    $Eapoiador=true;
}
/* function ShowOPC($OPC,$LNK) {
    global $td;
    if ($td==6) {
        echo "</tr>";
        $td=0; }
    if ($td==0) echo "<tr>";
    $td ++;
    ?>
    <td align="center">
        <font color="#990000" size="1" face=" sans-serif"><strong>
                <a href="<?echo $LNK; ?>" target="mainFrameIn" class="LMS"> <? echo $OPC; ?></a>
        </strong></font>
    </td>
<? } */

$rec_fin_="08.03.05";
?>
    <!-- Responsividade menu inicio !-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">        <title>INTRANET IA</title>
    </head>
    <body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>    <!-- Responsividade menu fim !-->

    <table border="0" width="100%" cellspacing="0">
        <tr>
            <td colspan="5" height="1%"><hr></td>
        </tr>
        <tr>
            <td height="1%" colspan="5">

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <font color="#990000" size="1" face="Verdana, Arial, Helvetica, sans-serif">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a white-space="nowrap" class="nav-link" href="./exteriorbrasil.php?op=1" target="mainFrameIn">AFASTAMENTO <=90 DIAS</a>
                                </li>
                                <li class="nav-item">
                                    <a white-space="nowrap" class="nav-link" href="./apdeptos.php" target="mainFrameIn">AFASTAMENTO >90 DIAS</a>
                                    <!--a class="nav-link" href="#" onClick="parent.location='./../pg_inicial.php?sist=apdeptos&pagina=apdeptos/solicitaafastamentoADP.php&transicao=S'">AFASTAMENTO >90 DIAS</a!-->
                                </li>
                                <? if ($regime_trabalho=="CLE") { ?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="./avisovidafuncional.php" target="mainFrameIn">LICEN&Ccedil;A PR&Ecirc;MIO</a>
                                        <!--a class="nav-link" href="./solicitaoutrolicenca.php?op=1" target="mainFrameIn">LICEN&Ccedil;A PR&Ecirc;MIO</a!-->
                                    </li>
                                <? } ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="./solicitaoutroartigo.php?op=1" target="mainFrameIn">FALTA ABONADA</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="./avisovidafuncional.php" target="mainFrameIn">F&Eacute;RIAS</a>
                                    <!--a class="nav-link" href="./solicitaoutroferias.php?op=1" target="mainFrameIn">F&Eacute;RIAS</a!-->
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="./histsolicitacoes.php?op=1" target="mainFrameIn">MINHAS SOLICITA&Ccedil;&Otilde;ES</a>
                                </li>
                                <? if($nivel == 3) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="./consultatodos.php" target="mainFrameIn">CONSULTAR SOLICITA&Ccedil;&Otilde;ES</a>
                                    </li>
                                <? } else {
                                    if ($EoChefe || $EoSecretario || $Echefe || $Eapoiador) {
                                        $m_opc="CONSULTAR SOLICITA&Ccedil;&Otilde;ES"; {
                                            if ($Eapoiador) $m_opc=$m_opc." DEPARTAMENTAIS"; ?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="./histdepartamento.php" target="mainFrameIn"><?=$m_opc?></a>
                                            </li>
                                        <? }
                                    }
                                } ?>
                                <? if($nivel==3) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="./gerenciamento.php" target="mainFrameIn">GERENCIAMENTO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="./EnviaComunicado.php" target="mainFrameIn">COMUNICADOS RH</a>
                                    </li>
                                <? } ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="./ocorrenciasRH.php" target="mainFrameIn">OCORR&Ecirc;NCIAS RH</a>
                                </li>
                                <? if($EoChefe || $EoSubstituto || $Echefe || $EaprovadorIndicado) { ?>
                                    <li class="nav-item">
                                        <a class="nav-link" href="./autorizar.php" target="mainFrameIn">AUTORIZAR</a>
                                    </li>
                                <? }
                                if($nivel==3 || $nivel==1) {
                                    if ($nivel==1) { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" href="./listaafastamento.php" target="mainFrameIn">RELAT&Oacute;RIO PRODU&Ccedil;&Atilde;O</a>
                                        </li>
                                    <? } else { ?>
                                        <li class="nav-item">
                                            <a class="nav-link" href="./relatorios.php" target="mainFrameIn">RELAT&Oacute;RIOS</a>
                                        </li>
                                    <? }
                                } ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="./links.php" target="mainFrameIn">LINKS &Uacute;TEIS</a>
                                </li>
                            </ul>
                        </div>
                    </font>
                </nav>
            </td>
        </tr>
        <tr>
            <td colspan="5" height="1%"><hr></td>
        </tr>
    </table>
<?
$res = mysql_query("SELECT * from RHrecesso where RecessoSituacao='1'");

if ($linha = mysql_fetch_array($res)) {
    $RecessoTexto=  $linha['RecessoTexto'];
    $RecessoInicio=$linha['RecessoInicio'] . " " . $linha['RecessoInicioHora'] . ":00:00" ;
    $RecessoInicioHora=$linha['RecessoInicioHora'];
    $RecessoFim=$linha['RecessoFim'] . " 23:59:00";
    $Dt=date("Y-m-d H:i:s");
    //echo $Dt . " I=" . $RecessoInicio . " F=" . $RecessoFim;
    //$RecessoTexto="Caro usuario, no periodo de 20/12/2013 a 01/01/2014 , o modulo RH ficara inoperante. Retornaremos nossas atividades em 02 de Janeiro de 2014. RH ";
    //if ($nivel <> "3" && ($Dt >=$RecessoInicio && $Dt<=$RecessoFim)) echo $RecessoTexto;{
    if ($nivel !=3 && ($Dt >=$RecessoInicio  && $Dt<=$RecessoFim)) { ?>
        <SCRIPT LANGUAGE="JavaScript">
            alert('<?echo $RecessoTexto;?>');
            parent.location='./../menu_inicial.php';
        </script>
    <? }
}
if( $EoChefe && $nivel !=3) { ?>
    <script language="javascript">
        location.href.target="MainFrameIn";
        //location.href="./histdepartamento.php";
        //document.location="./histdepartamento.php";
        //document.target="MainFrameIn";
        //window.open("./histdepartamento.php","MainFrameIn");
    </script>
    <!--<a href="./solicitaoutrofastamento.php?op=1" target="mainFrameIn" class="LMS"> <strong><=90</a></strong></font> &nbsp|&nbsp;-->
<? } ?>