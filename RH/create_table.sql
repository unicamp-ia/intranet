set names 'UTF8';

CREATE TABLE RHemail (
    `email` VARCHAR(50) default NULL,
    PRIMARY KEY  (`email`)
);

CREATE TABLE RHsolicitacoes (
    `cod` int(10) unsigned auto_increment,
    `codsolicitante` VARCHAR(14) default NULL,
    `codatendente` VARCHAR(14) default NULL,
    `coduser` VARCHAR(14) default NULL,
    `codaprovador` VARCHAR(14) default NULL,
    `lotacao` VARCHAR(51) default NULL,
    `status` Char(1) default NULL,
    `tipo` Char(1) default NULL,
    `obs` text default NULL,
    `dataaltera` datetime default NULL,
    `datacadastro` date default NULL,
    `dtaprova` datetime NULL DEFAULT NULL,
    PRIMARY KEY  (`cod`)
);

CREATE TABLE RHafastamento (
    `cod` int(10) unsigned NOT NULL,
    `rg` VARCHAR(14) default NULL,
    `cargo` VARCHAR(51) default NULL,
    `datasaida` date default NULL,
    `dataretorno` date default NULL,
    `objetivo` text default NULL,
    `cidade` text default NULL,
    `exterior` CHAR(1) default NULL,
    `financiadora` VARCHAR(100) default NULL,
    PRIMARY KEY  (`cod`)
);

CREATE TABLE RHlicenca (
    `cod` int(10) unsigned NOT NULL,
    `periodo` VARCHAR(151) default NULL,
    `datainicio` date default NULL,
    `dias` VARCHAR(4) default NULL,
    PRIMARY KEY  (`cod`)
);

CREATE TABLE RHferias (
    `cod` int(10) unsigned NOT NULL,
    `total` VARCHAR(30) default NULL,
    `datainicio` date default NULL,
    `pecunia` CHAR(1) default NULL,
    `adiantamento` CHAR(1) default NULL,
    PRIMARY KEY  (`cod`)
);

CREATE TABLE RHlinks (
    `cod` int(10) unsigned auto_increment,
    `url` VARCHAR(151) default NULL,
    `nome` VARCHAR(151) default NULL,
    PRIMARY KEY  (`cod`)
);

CREATE TABLE RHobjetivos (
    `cod` int(10) unsigned auto_increment,
    `texto` VARCHAR(151) default NULL,
    PRIMARY KEY  (`cod`)
);

#ALTER TABLE RHsolicitacoes ADD COLUMN dtaprova datetime NULL DEFAULT NULL;