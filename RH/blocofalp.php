<div class="container">
    <p class="alert alert-primary">Os pedidos de f&eacute;rias, licença pr&ecirc;mio e
        afastamentos no pa&iacute;s (at&eacute; 90 dias)
        dever&atilde;o ser solicitados atrav&eacute;s
        do m&oacute;dulo FALP, dentro do Sistema
        Vida Funcional Online da DGRH.
    </p>

    <strong>Para acessar</strong>
    <p>
        <a target="_blank" href="https://www.siarh.unicamp.br/passaporte" >
            1. Acesse o Vida Funcional e faça login com seu usuario e senha
        </a>
    </p>

    <p>2. Na tela inicial clicar em "Agendar ferias e Licença Prêmio</p>
    <img class="img-fluid"src="step2.png">
</div>