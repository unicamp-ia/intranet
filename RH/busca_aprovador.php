<?php
// Busca o aprovador da area onde o solicitante(usuario) eh lotado. Caso o solicitante seja chefe
// de sua area, retorna como aprovador o chefe imediato.
// Retorna tambem os dados do chefe/supervidor substituto, caso ele tenha sido indicado pelo RH,
// tanto para a area que se quer quanto para a area imediata
// Este procedimento eh incluso no Códdigo principal, portanto a variavel CODUSER, que identifica
// o solicitante, deve vir setada
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
// Alterado por Edson Giordani em Maio/2020 - Novo organograma ATU para CTUIA

include_once("../base/FuncoesOrganograma.php");
if ($coduser==$coduser_conected) {
    $areauser=$codarea_conected;
} else {
    $SQL="select * from tblusuarios where coduser='$coduser'";
    $res1=mysql_query("$SQL");
    if ($linha1=mysql_fetch_array($res1)) {
        $areauser=$linha1['codarea'];
    } else {
        $mensagem = "[busca_aprovador] Codigo do usuario nao mais presente na tabela que armazena os usuarios.<BR><BR><BR>";
        $mensagem .= "coduser=".$coduser."<BR>coduser_conected=".$coduser_conected."<BR>codarea_conected=".$codarea_conected;
        $mensagem .= "<BR>" . $sql;
        envia_email("[busca_aprovador} erro ferias", $mensagem, "giordani@unicamp.br");
        echo "[busca_aprovador] Codigo do usuario nao mais presente na tabela que armazena os usuarios.";
        die();
    }
}
$hierarquia=array();
//Monta a hierarquia para a area base(S), area acima(S) e nao para a area abaixo(N), detalhada (C)
MontaHierarquia($areauser,'S','S','N','C');

if (count($hierarquia) ==0) {
    echo "[busca_aprovador]. Erro 2 . Nao foi possivel montar a arvore hierarquica para a area $areauser.";
    die();
}
$codaprovador="";
$nomeaprovador="";
$emailaprovador="";
$codsubstituto="";
$nomesubstituto="";
$emailsubstituto="";

// Verifica se o solicitante eh chefe/supervisor ou substituto do departamento ao qual ele pertence,
// neste caso o aprovador eh o chefe imediato da area a qual o solicitante pertence.
if ($coduser==$hierarquia['chefia']['matr'] || $coduser == $hierarquia['substituto_chefia']['matr']) {
    // Assinala como aprovador o chefe imediato
    $codaprovador = $hierarquia['acima']['matr'];
    $nomeaprovador= $hierarquia['acima']['nome'];
    $emailaprovador = $hierarquia['acima']['email'];
    if ($hierarquia['substituto_acima']['matr']) {
        $codsubstituto=$hierarquia['substituto_acima']['matr'];
        $nomesubstituto = $hierarquia['substituto_acima']['nome'];
        $emailsubstituto = $hierarquia['substituto_acima']['email'];
    }
} else {
    // Assinala como aprovador o chefe do depto/secao/diretoria
    $codaprovador = $hierarquia['chefia']['matr'];
    $nomeaprovador= $hierarquia['chefia']['nome'];
    $emailaprovador = $hierarquia['chefia']['email'];
    if ($hierarquia['substituto_chefia']['matr']) {
        $codsubstituto=$hierarquia['substituto_chefia']['matr'];
        $nomesubstituto = $hierarquia['substituto_chefia']['nome'];
        $emailsubstituto = $hierarquia['substituto_chefia']['email'];
    }
}

// Caso nao definido definir a diretoria
if(!$codaprovador){
    $codaprovador = $hierarquia['17']['matr_chefe'];
    $nomeaprovador= $hierarquia['17']['nome_chefe'];
    $emailaprovador = $hierarquia['17']['email_chefe'];
    $codsubstituto=$hierarquia['17.36']['matr_substituto'];
    $nomesubstituto = $hierarquia['17.36']['nome_substituto'];
    $emailsubstituto = $hierarquia['17.36']['email_substituto'];
}

if (!$codaprovador) {
    echo "[busca_aprovador]. Nao foi possivel encontrar o aprovador para a area $areauser.";
    die();
}
