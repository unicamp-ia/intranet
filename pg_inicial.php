<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
    session_start();

    include("./base/db.php");
    db_connect() or die("Nao consigo me conectar ao Servidor!");
    $coduser_conected = $_SESSION["coduser_conected"];
    $usuario = $_SESSION["usuario"];
    $tipo_conected = $_SESSION["tipo_conected"];

    if(!(isset($_SESSION["usuario"]) AND isset($_SESSION["senha"]) AND isset($_SESSION["coduser_conected"]) AND isset($_SESSION["tipo_conected"])))
    {
        ?>
        <html>
                <head>
                        <title>INTRANET IA</title>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                        <link href="<? echo $dir_base;?>base/estilo.css" rel="stylesheet" type="text/css">
                </head>
        <SCRIPT LANGUAGE="JavaScript">alert('A sessao expirou, isto ocorre por ficar muito tempo sem utilizar a intranet. Entre novamente com seu e-mail e senha.'); parent.location.href='./index.php'; </SCRIPT>
        <?php
        exit(0);
    }

    $sistema = $_REQUEST['sist'];
    $pagina = $_REQUEST['pagina'];
    $parametros = $_REQUEST['parametros'];
    $parametros = str_replace("and","&",$parametros);

    // eh necessario manter a montagem da sessao com as permissoes de acesso
    // de cada usuario para cada modulo
    // INICIO ---------->
    if ($_SESSION["tipo_conected"] != "Aluno UNICAMP")
        {
        // Monta path basico para links do menu do sistema
        $dir_base = "./";
        $me = $_SERVER['PHP_SELF'];
        $Apathweb = explode("/", $me);
        $myFileName = array_pop($Apathweb); // nome do arquivo
        $dirName = array_pop($Apathweb);
        while ($dirName != "intranet") {
            $dir_base .= "../";
            $dirName = array_pop($Apathweb);
        }
        // Registra em secao as permissoes de acesso aos sistemas, atribuidas ao usuario
        $SQL = "Select u.*, s.codsistema from tblsistemas s left join tblususist u on s.idsistema=u.idsistema and coduser='$coduser_conected' and u.principal='S'";
        $rs_permissao_cad = mysql_query($SQL);

        while($linha = mysql_fetch_array($rs_permissao_cad)){
            $_SESSION[$linha['codsistema']]=$linha['permissao'];
        }
        // Se o usuario nao tem permissao de acesso ao modulo almoxarifado, entao verifica se o mesmo eh
        // secretario de departamento. Se for, o acesso eh autorizado para a funcao de PEDIR
        $perm_almox = $_SESSION["almox"];

        if (!$perm_almox) {
           if ($_SESSION['cod_secretario'] == $coduser_conected && $_SESSION["coduser_chefe"]!=$coduser_conected ) {
                $_SESSION['almox']=3;
            } else {
                if (isset($_SESSION["coduser_chefe"]) and $_SESSION["coduser_chefe"]==$coduser_conected) {
                    $_SESSION['almox']=2;
                }
            }
        }
        // Se o usuario nao tem permissao de acesso ao modulo financeiro mas eh diretor ou chefe de depto, nesse
        // caso atribui todas perrogativas ao diretor e a perrogativa de visualizar relatorios ao chefe de depto
        $perm_financeiro= $_SESSION["financeiro"];
        if (!$perm_financeiro) {
            if ($coduser_conected == $_SESSION["cod_diretor"] || $coduser_conected==$_SESSION["cod_associado"]) {
                $_SESSION["financeiro"]=7;
            } else {
                // Chefe de departamento ou secretario de departamento
                if((isset($_SESSION["coduser_chefe"]) and $_SESSION["coduser_chefe"]==$coduser_conected) || ($_SESSION['cod_secretario'] == $coduser_conected )) {
                    $_SESSION["financeiro"]=2;
                }
            }
        }
        $perm_expediente= $_SESSION["expediente"];
        if (!$perm_expediente && ($_SESSION['cod_secretario'] == $coduser_conected || $_SESSION["coduser_chefe"]==$coduser_conected )) {
            $_SESSION['expediente']=1;
        }
        $perm_patrimonio= $_SESSION["patrimonio"];
        if (!$perm_patrimonio) $nivel=3;
        if (!$perm_patrimonio && ($_SESSION['cod_secretario'] == $coduser_conected || $_SESSION["coduser_chefe"]==$coduser_conected )) {
            $_SESSION['patrimonio']=1;
        }

        if($tipo_conected=="DOCENTE" || $tipo_conected=="FUNCIONARIO" || $tipo_conected=="BOLSISTA" || $tipo=='ESTAGIARIO'){
            $SQL = "SELECT nome from tblusuarios where coduser ='$coduser_conected'";
            $prnt = mysql_query($SQL);
            $linha_prnt = mysql_fetch_array($prnt);
            $nomeusar = $linha_prnt['nome'];
            $firstname = "";
            $firstname = explode(" ", $nomeusar);
            $arrayReverso = $firstname;
        } else {
            if($tipo_conected=="ALUNO" || $tipo_conected=="INSTITUCIONAL"){
                echo "Utiliza&ccedil;&atilde;o n&atilde;o permitida.";
            } else {
                die();
            }
        }
    }
    // <---------- FIM

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Bootstrap CSS -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">    <title>INTRANET IA</title>
</head>
<?php
$timehoje=date("Y-m-d H:i:s");
$SQLlog="insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','Autenticacao','Acesso ao modulo $sistema','Nenhum','$timehoje')" or die("erro");
$res=mysql_query($SQLlog) or die("erro!" . mysql_error());

$rows=200;
/* if ($sistema=='financeiro' || $sistema=='RH') {
    $rows=210;
    if ($sistema=='RH') { $rows=120; }
    $MesGerencial=date('m');
    $MesAnoGerencial=date('Y');
    $ModuloFinanceiro=$_REQUEST['ModuloFinanceiro'];
} */
if ($sistema=='RH') {
    $rows=120;
}

?>
<link href="./base/estiloprint.css" rel="stylesheet" type="text/css" media="print">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>    <!-- Responsividade menu fim !-->

<!-- primeiro frameset rows="< ?echo $rows;?>,*" cols="793*" framespacing="0" frameborder="NO" border="0"!-->

<frameset rows="<?echo $rows;?>,*" framespacing="0" frameborder="NO" border="0">

    <frame src="<?=$sistema?>/index.php?sistema=<?=$sistema . (isset($ModuloFinanceiro) ? "&ModuloFinanceiro=$ModuloFinanceiro" : '')?>" name="topFrame" frameset="top" scrolling="AUTO" noresize class="naoimprime">

	<?
        if ($sistema=="transportes") {
            $nivel=$_SESSION['transportes'];
            if ($nivel=='5') {
                $pagina="transportes/solicitacoes.php?";
            }
        } else if ($sistema=="cartaoconv") {
            $nivel=$_SESSION['cartaoconv'];
            if ($nivel < 4) {
                $pagina = "";
            }
        }

        if(!$pagina) { ?>
	        <iframe src="index_down.php" name="mainFrameIn" scrolling="yes" style="overflow:hidden;height:100%;width:100%" height="100%" width="100%">
	    <? } else {
            if ($sistema=="financeiro") {
                $parametros="FiltroData=3&MesGerencial=$MesGerencial&MesAnoGerencial=$MesAnoGerencial";
                if ($ModuloFinanceiro=="gerencial" || $ModuloFinanceiro=="adiantamento") { ?>
                    <frame src="<? print $pagina."?FromModulo=pg_inicial&".$parametros;?>" name="mainFrameIn" scrolling="yes">
                <? } elseif($pagina=='proap') {?>
                   <frame src="/intranet/financeiro/proap" name="mainFrameIn" scrolling="yes">
                <? } else {?>
                   <frame src="" name="mainFrameIn" scrolling="yes">
                <? }
            } else {
                if ($sistema=='apdeptos' ){
                    $transicao=$_REQUEST['transicao'];
                    if ($transicao) {
                        $pagina .= "?op=1&tipo=A";
                    } ?>
                    <frame src="<? print $pagina.$parametros;?>" name="mainFrameIn" scrolling="yes">
                <? } else { ?>
                    <frame src="<?=$pagina . '?' .$parametros;?>" name="mainFrameIn" scrolling="yes">
                <? } ?>
             <? }
        } ?>
</frameset>
<noframes><body>

</body></noframes>
</html>
