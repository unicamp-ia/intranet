                    function MostraVinculo(vinculo) {
                        var lbl="Matr�cula";
                        if (vinculo.value=='1') {
                            lbl="RA";
                        }
                        if (vinculo.value=='4') {
                            lbl="RG/RNE";
                        }
                         document.getElementById('label_doc').innerHTML = lbl; 
                    }
                   
                    function SalvaPassageiro() {
                        var op=document.ListaPassageiros.pass_op.value;
                        cod=document.formulario.cod.value;
                        ano=document.formulario.ano.value;
                        pass_regnum=document.ListaPassageiros.pass_regnum.value;
                        pass_tipo=document.ListaPassageiros.pass_tipo.value
                        pass_horario=document.ListaPassageiros.pass_horario.value;
                        pass_nome=document.ListaPassageiros.pass_nome.value;
                        pass_vinculo=document.ListaPassageiros.pass_vinculo.value;
                        pass_docnumero=document.ListaPassageiros.pass_docnumero.value;
                        pass_localapresentacao=document.ListaPassageiros.pass_localapresentacao.value;
                        pass_cep=document.ListaPassageiros.pass_cep.value;
                        pass_rua=document.ListaPassageiros.pass_rua.value;
                        pass_numero=document.ListaPassageiros.pass_numero.value;
                        pass_complemento=document.ListaPassageiros.pass_complemento.value;
                        pass_bairo=document.ListaPassageiros.pass_bairro.value;
                        pass_cidade=document.ListaPassageiros.pass_cidade.value;
                        pass_estado=document.ListaPassageiros.pass_estado.value;
                        pass_email=document.ListaPassageiros.pass_email.value;
                        pass_telefone=document.ListaPassageiros.pass_telefone.value;
                        if (pass_horario =="" || pass_nome=="" || pass_vinculo =="0" || pass_docnumero=="" || pass_localapresentacao=="" || pass_rua=="" || pass_numero=="" || pass_cidade=="") {
                            alert("Por favor, preencha corretamente os dados do passageiro");
                            document.getElementById('pass_horario').focus();
                            return(false);
                        }
                        parametros = "./TrataPassageiro.php?op=" + op + "&cod=" + cod + "&ano=" + ano + "&pass_regnum=" + pass_regnum + "&pass_horario=" + pass_horario + "&pass_nome=" + pass_nome ;
                        parametros=parametros + "&pass_tipo=" + pass_tipo + "&pass_vinculo=" + pass_vinculo + "&pass_docnumero=" + pass_docnumero + "&pass_localapresentacao="+pass_localapresentacao;
                        parametros=parametros + "&pass_cep="+pass_cep+"&pass_rua="+pass_rua+"&pass_numero="+pass_numero+"&pass_complemento="+pass_complemento;
                        parametros=parametros + "&pass_bairro="+pass_bairo+"&pass_cidade="+pass_cidade+"&pass_estado="+pass_estado+"&pass_email="+pass_email;
                        parametros=parametros + "&pass_telefone="+pass_telefone;
                        
                        http.open("GET", parametros, true);
                        http.onreadystatechange = handleHttpResponse;
                        http.send(null);
                       
                        return(true);
                    }
                    
                    function handleHttpResponse()
                    {
                      if (http.readyState == 4) {
                         
                        //campo_select.options.length = 0;
                        result = http.responseText;
                        if (result=="0") {
                            alert("Passageiro n�o encontrado no banco de dados.");
                        }
                       
                        colunas = result.split( "|" );
                        len=colunas.length;
                        
                        if (colunas[0]!="OK_A" && colunas[0]!="OK_B" && colunas[0]!="OK_C" && colunas[0]!="OK_D" && colunas[0]!="OK_E") {
                           alert("Retorno desconhecido.");
                           return(false);
                           
                        }
                      
                        // OK_A devolve os dados de um passageiro
                        if (colunas[0]=="OK_A" || colunas[0]=="OK_E" ) {
                            status=document.formulario.status.value;
                            if(status != "J" && status!="D" && status != "C" && document.formulario.habilita_passageiro.value=='S') { 
                                document.getElementById("opcoes").style.display="";
                                document.getElementById("salvar").value="Gravar altera��es";
                                document.ListaPassageiros.pass_op.value='3';
                            }
                            document.ListaPassageiros.pass_regnum.value=colunas[3];
                            //document.ListaPassageiros.pass_tipo.value=colunas[4];
                            document.ListaPassageiros.pass_horario.value=colunas[5];
                            document.ListaPassageiros.pass_nome.value=colunas[6];
                           
                            document.ListaPassageiros.nome_anterior.value=colunas[6];
                            document.ListaPassageiros.pass_vinculo.value=colunas[7];
                            document.ListaPassageiros.pass_docnumero.value=colunas[8];
                            document.ListaPassageiros.pass_localapresentacao.value=colunas[9];
                            document.ListaPassageiros.pass_cep.value=colunas[10];
                            document.ListaPassageiros.pass_rua.value=colunas[11];
                            document.ListaPassageiros.pass_numero.value=colunas[12];
                            document.ListaPassageiros.pass_complemento.value=colunas[13];
                            document.ListaPassageiros.pass_bairro.value=colunas[14];
                            document.ListaPassageiros.pass_cidade.value=colunas[15];
                            document.ListaPassageiros.pass_estado.value=colunas[16];
                            document.ListaPassageiros.pass_email.value=colunas[17];
                            document.ListaPassageiros.pass_telefone.value=colunas[18];
                            MostraVinculo(document.ListaPassageiros.pass_vinculo);
                        }
                        // O registro foi inserido no banco de dados (OK_B) ou deletado (OK_D). Insere ou remove
                        if (colunas[0]=="OK_B" || colunas[0]=="OK_D" || colunas[0]=="OK_E" ) {
                             if (colunas[0] !="OK_E") {
                                 LimpaFormulario();
                             }
                             if (colunas[0] =="OK_D") {
                                MostrarGravar('none');
                                AssinalaPropriedade("desabilitar");
                             } 
                             vv=document.getElementById("passageiros");
                             if (colunas[0]=="OK_B" || colunas[0]=="OK_E" ) {
                                if (colunas[0]=="OK_B") {
                                    vv.options[vv.options.length]= new Option(colunas[2], colunas[1], true, true)
                                    // Esconde o bot�o gravar endere�o
                                    MostrarGravar('none');
                                    
                                } else { 
                                    vv.options[vv.options.length]= new Option(colunas[6], colunas[3], true, true)
                                }
                             } else {
                                 vv.remove(vv.selectedIndex);
                             }
                        }
                        // O registro foi alterado no banco de dados (OK_B) ou deletado (OK_D)
                        if (colunas[0]=="OK_C") {
                            vv=document.getElementById("passageiros");
                            alert("Dados do passageiro alterados.")
                            //alert(document.ListaPassageiros.nome_anterior.value);
                            if (document.ListaPassageiros.pass_nome.value != document.ListaPassageiros.nome_anterior.value) {
                                 vv.remove(vv.selectedIndex);
                                 vv.options[vv.options.length]= new Option(document.ListaPassageiros.pass_nome.value, colunas[1], true, true)
                            }
                        }
                        return true;
                      }
                    }

                    function getHTTPObject() {
                      var xmlhttp;

                      if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
                        try {
                          xmlhttp = new XMLHttpRequest();
                        } catch (e) {
                          xmlhttp = false;
                        }
                      }
                      return xmlhttp;
                    }
                    
                    function MostrarGravar(Exi) {
                        var divOpcoes = document.getElementById("opcoes");
                        divOpcoes.style.display=Exi;
                    }
                    
                    function AssinalaPropriedade(Oque) {
                        if (Oque=='habilitar') {
                            
                            document.getElementById('pass_horario').removeAttribute("disabled");
                            document.getElementById('pass_nome').removeAttribute("disabled");
                            document.getElementById('pass_vinculo').removeAttribute("disabled");
                            document.getElementById('pass_docnumero').removeAttribute("disabled");
                            document.getElementById('pass_localapresentacao').removeAttribute("disabled");
                            document.getElementById('pass_cep').removeAttribute("disabled");
                            document.getElementById('pass_rua').removeAttribute("disabled");
                            document.getElementById('pass_numero').removeAttribute("disabled");
                            document.getElementById('pass_complemento').removeAttribute("disabled");
                            document.getElementById('pass_bairro').removeAttribute("disabled");
                            document.getElementById('pass_cidade').removeAttribute("disabled");
                            document.getElementById('pass_estado').removeAttribute("disabled");
                            document.getElementById('pass_email').removeAttribute("disabled");
                            document.getElementById('pass_telefone').removeAttribute("disabled"); 
                        } else {    
                            document.getElementById('pass_horario').setAttribute("disabled","disabled");
                            document.getElementById('pass_nome').setAttribute("disabled","disabled");
                            document.getElementById('pass_vinculo').setAttribute("disabled","disabled");
                            document.getElementById('pass_docnumero').setAttribute("disabled","disabled");
                            document.getElementById('pass_localapresentacao').setAttribute("disabled","disabled");
                            document.getElementById('pass_cep').setAttribute("disabled","disabled");
                            document.getElementById('pass_rua').setAttribute("disabled","disabled");
                            document.getElementById('pass_numero').setAttribute("disabled","disabled");
                            document.getElementById('pass_complemento').setAttribute("disabled","disabled");
                            document.getElementById('pass_bairro').setAttribute("disabled","disabled");
                            document.getElementById('pass_cidade').setAttribute("disabled","disabled");
                            document.getElementById('pass_estado').setAttribute("disabled","disabled");
                            document.getElementById('pass_email').setAttribute("disabled","disabled");
                            document.getElementById('pass_telefone').setAttribute("disabled","disabled");
                        }
                    }
                    function LimpaFormulario() {
                        document.ListaPassageiros.pass_horario.value="";
                        document.ListaPassageiros.pass_nome.value="";
                        document.ListaPassageiros.pass_vinculo.value="0";
                      
                        document.getElementById('pass_horario').focus(); 
                        document.ListaPassageiros.pass_nome.value="";
                        document.ListaPassageiros.pass_vinculo.value="0";
                        document.ListaPassageiros.pass_docnumero.value="";
                        document.ListaPassageiros.pass_localapresentacao.value="";
                        document.ListaPassageiros.pass_cep.value="";
                        document.ListaPassageiros.pass_rua.value="";
                        document.ListaPassageiros.pass_numero.value="";
                        document.ListaPassageiros.pass_complemento.value="";
                        document.ListaPassageiros.pass_bairro.value="";
                        document.ListaPassageiros.pass_cidade.value="";
                        document.ListaPassageiros.pass_estado.value="";
                        document.ListaPassageiros.pass_email.value="";
                        document.ListaPassageiros.pass_telefone.value="";
                        status=document.formulario.status.value;
                       
                        if(status != "J" && status!="D" && status != "C" && document.formulario.habilita_passageiro.value=='S') { 
                            MostrarGravar("");
                            document.getElementById("salvar").value="Gravar endere�o";
                        }
                        //var divOpcoes = document.getElementById("opcoes");
                        //divOpcoes.style.display="";
                        return true;
                    }
                   
                    function InserePassageiro() {
                        if (document.ListaPassageiros.veiculo.value=='0') {
                                      alert('Escolha o ve�culo.');
                                      return(false);
                        }
                        vv=document.getElementById('passageiros');
                        par=document.ListaPassageiros.participantes.value;
                        // Se capacidade do veiculo for para +8, ent\xe3o fixa em 2 o n\xfamero de respons\xe1veis^M
                        
                         if (par==0 && document.ListaPassageiros.pass_tipo.value=='1') {
                            alert('N�mero de passageiros informado � zero. Caso queira cadastrar, indique um valor acima de zero.')
                              return(false);
                        }
                        
                        if (document.ListaPassageiros.pass_tipo.value=='2') {
                            par=2;
                        }
                        //alert(par);
                        if (vv.options.length < par) {
                          
                            if (vv.options.length > 0) {
  
                               var objSelect = document.ListaPassageiros.passageiros;
                               ind=objSelect.selectedIndex;
                               if (ind==-1) {
                                   ind=0;
                               }
                               pass_regnum=objSelect[ind].value;
                               //pass_nome= objSelect[ind].getAttribute('nome');
                              // pass_nome=pass_nome.trim();
                               if(confirm('Gostaria de copiar os dados do primeiro endere�o registrado para o novo passageiro?')) {
                                    cod=document.formulario.cod.value;
                                    ano=document.formulario.ano.value;
                                    parametros="./TrataPassageiro.php?op=5&cod=" + cod + "&ano=" + ano + "&pass_regnum=" + pass_regnum;
                                    http.open("GET", parametros, true);
                                    http.onreadystatechange = handleHttpResponse;
                                    http.send(null);
                                    return(true);
                                } 
                            }    
                            LimpaFormulario();    
                            document.ListaPassageiros.pass_op.value='2';
                            AssinalaPropriedade("habilitar");
                        } else {
                                alert('Inserir no m�ximo ' + par + ' passageiros/respons�veis');
                                MostrarGravar('none');
                                AssinalaPropriedade("desabilitar");
                              }
                        
                    }
                    
                    function BuscaPassageiro(obj) {
                       var objSelect =document.ListaPassageiros.passageiros;
                       ind=objSelect.selectedIndex;
                        if (obj.value=="0" || ind <0) {
                          return false;
                        }
                        LimpaFormulario();
                        cod=document.formulario.cod.value;
                        ano=document.formulario.ano.value;
                        parametros="./TrataPassageiro.php?op=1&cod=" + cod + "&ano=" + ano + "&pass_regnum=" + obj.value;
                        http.open("GET", parametros, true);
                        http.onreadystatechange = handleHttpResponse;
                        http.send(null);
                        AssinalaPropriedade("habilitar");
                        return(true);
                    }
                    
                    function ApagaPassageiro(){
                       pass_regnum = document.getElementById("passageiros").value;
                       if (pass_regnum=="0") {
                            return(false);
                       }
                       var vv = document.ListaPassageiros.passageiros;
                       if (vv.options.length < 1) {
                           alert(vv.options.length);
                           return(false);
                       }
                       ind=vv.selectedIndex;
                       if (ind==-1) {
                           alert("Selecione o passageiro que voc� quer excluir.")
                           return(false);
                       }
                        if(confirm('Tem certeza que deseja remover o endere�o do passageiro selecionado')) {
                            cod=document.formulario.cod.value;
                            ano=document.formulario.ano.value;
                            pass_regnum = document.getElementById("passageiros").value
                            parametros="./TrataPassageiro.php?op=4&cod=" + cod + "&ano=" + ano + "&pass_regnum=" + pass_regnum;
                            http.open("GET", parametros, true);
                            http.onreadystatechange = handleHttpResponse;
                            http.send(null);

                            return(true);
                        }
                       
                    }   
                       
                    var http = getHTTPObject();
                    var tta=10;
