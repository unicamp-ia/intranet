<?

//
// Módulo de transportes - Menu Principal
// --------------------------------------
//

include("../base/VerifiqueLogin.php");
$tipo_conected = $_SESSION["tipo_conected"];
$codarea_conected = $_SESSION["codarea_conected"];
$cod_diretoria = $_SESSION["cod_diretoria"];
$coduser = $_SESSION["coduser_conected"];

if ($coduser=='84930') $_SESSION["transportes"]=5; // Remover assim que entrar em licença-prêmio ou deixar de desenvolver em definitivo a intranet

// Se usuário é o gestor de informatica, então ele deve ter permissão de gerenciamento deste módulo, isso para acompanhar o funcionamento
// do sistema
if ($_SESSION['GestorInformatica']==$coduser_conected) {
   $_SESSION["transportes"]=5;
}
$nivel = $_SESSION["transportes"];

// Busca a posicao do usuário no organograma do IA
//
$EoChefe = false;
$EoSecretario = false;
$EoSubstituto = false;
//echo $nivel;
include ("../RH/VerifiquePosicao.php");

function ShowOPC($OPC,$LNK,$FRA = false) {
    global $td;

    if ($td==0) echo "<tr>";
    $td ++;
    ?>
    <td align="center">
        <a href="<?
                    echo $LNK . "\"";
                    if (!$FRA) echo "target=\"mainFrameIn\" class=\"LMS\"";
                    echo ">" . $OPC;
                ?>
        </a>
    </td>
   <?
   if ($td==6) {
        echo "</tr>";
        $td=0;
    }
}

?>
<table border="0" width="100%" cellspacing="0">
	<tr>
		<td width="14%"></td>
		<td height="1%" colspan="5"><hr></td>
	</tr>
	<tr>
		<td width="14%"></td>
		<td height="1%" colspan="5">
            <table width="100%" border="0" class="mainMenu">
                <?

                    $td=0;

                     if($EoChefe || $EoSubstituto || $Echefe) {
                         ShowOPC("APROVAR SOLICITA&Ccedil;&Otilde;ES","./aprovar.php");
                     }


                    if($EoChefe) {
                        ShowOPC("HIST&Oacute;RICO DE SOLICITA&Ccedil;&Otilde;ES","./histdepto.php");
                        ShowOPC("RELAT&Oacute;RIOS","./relatorios.php");
                    }

                    if($nivel <> 2){
                        $cod_php="cadpedido.php";
                        ShowOPC("SOLICITAR VE&Iacute;CULO","./cadpedido.php");
                        ShowOPC("MINHAS SOLICITA&Ccedil;&Otilde;ES","./histpedidos.php");
                    }

                    if($nivel==1) {
                         ShowOPC("TODAS SOLICITA&Ccedil;&Otilde;ES","./todassolicitacoes.php");
                    }

                    if($nivel==2) {
                         ShowOPC("RELAT&Oacute;RIO","./relatfinanceiro.php");
                    }

                    if($nivel>3) {
                         ShowOPC("CADASTROS","./cadastros.php");
                         ShowOPC("SOLICITA&Ccedil;&Otilde;ES DE VE&Iacute;CULOS","./solicitacoes.php");
                         ShowOPC("RELAT&Oacute;RIOS","./relatorios.php");
                    }

                    ShowOPC("MAPA AGENDAMENTO","./exibeagenda.php?cod=1&tipo=veiculo");

                    if ($td >0 and $td < 6) {
                        echo "</tr>";
                    }
                ?>
          </table>
    </td>
	</tr>
	<tr>
		<td width="14%"></td>
		<td height="1%" colspan="5"> <hr></td>
	</tr>
</table>
