<?
class Autentica
{
	function autentica($servidor,$user, $pass)
	{
       $OK = "";
       $this->valida($servidor,$user,$pass);
    }

    /*
    valores de retorno
    -3 = servidor indisponível
    -4 = senha ou usuario invalido
    
    */

    function valida($servidor,$user,$pass)
    {

       @$porta = fsockopen($servidor,110, &$errno, &$errdesc);
       if(empty($porta))
       {
          $this->OK = "-3";
       }
       else
       {
           $resp = fgets($porta,100);

           fputs($porta,"user $user\r\n");
           $resp = fgets($porta,100);

           //verifica se o usuario é valido
           if (substr($resp,0,3) == "+OK")
           {
             fputs($porta,"pass $pass\r\n");

             $resp = fgets($porta,100);

             //verifica se a senha é valida
             if (substr($resp,0,3) == "+OK")
               $this->OK = "1";
             else
               $this->OK = "-4";

           }
           else
             $this->OK = "-4";

           fputs($porta,"quit\r\n");
           fclose($porta);
        }

     }

    function getResult()
    {
       return ($this->OK);
    
    }
     
}

?>
