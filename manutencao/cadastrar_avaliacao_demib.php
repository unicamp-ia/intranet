<?php 

        // O fluxo da validaï¿½ï¿½o manda que o validador (chefe de depto) selecione a acao ("em avaliacao", "validado", "excluido" ou "retonar ao usuario") em cada item todo
        // ou, de forma automatica, indica "Validat todos os itens" ou "Retornar todos os itens ao usuario" e clica no botao OK.
        // Em seguida, deve clicar no botao "SALVAR" para finalizar o processo de validacao
 	
 	$igualtipo = true;
	$sotipo = true;
	$naopersist = true;
 	include("../base/inicio.php"); /*Faz a verificaÃ§Ã£o se nivel[sist]>nivelcomp e conecta ao banco. Coloca o head do html.*/
	include("../base/email.php"); /* FunÃ§Ã£o envia_email($assuntoemail, $mensagem, $remetente_email, $destinatario)*/
	include("../base/transaction.php"); /* Classe que controla transacoes com o banco de dados */
        $nivel = $_SESSION["manutencao"];
        $tipo_conected = $_SESSION["tipo_conected"];
        $coduser = $_SESSION["coduser_conected"];
        $codarea_conected = $_SESSION["codarea_conected"];
        $cod_graduacao = $_SESSION["cod_graduacao"];
        $matr_chefe=  $_SESSION["matr_chefe"];
	
        //echo $codarea_conected . " " . $matr_chefe . " " . $nivel . " " . $coduser . " " . $tipo_conected . " " .  $cod_graduacao;

 	if($codarea_conected!="MANUT") { /*Se nao for da MANUT*/ 
 		die("&Aacute;rea restrita aos usuarios da MANUT/IA");
 	}
 	if(!$matr_chefe) { /*Se nÃ£o for chefe ou vice chefe do DEMIB.*/ 
            die("Prerrogativa restrita ao chefe da MANUT/IA");
        }
	$objTransaction = null;
	
	//Dados do pedido
	$numpedido = $_REQUEST['numpedido'];
	$ano = $_REQUEST['ano'];
	
	//Parametros do filtro e da lista (da pagina de origem)
	$origem = $_REQUEST['origem'];
	$parametros = $_REQUEST['parametros']; //Esta string jah vem desescapada (Ex.:  [parametros=%20teste] -> [$parametros == " teste"] )
	
	//Parametros das operacoes
	$op = $_REQUEST['op']; //1-gravar avaliacoes
	$op_itens = $_REQUEST['op_itens']; //100-todos os itens validados; 200-todos os itens retornados
	$parecer_demib = $_REQUEST['parecer_demib'];
	//Obtendo os dados do pedido (pedido novo [50,60] ou pedido a ser finalizado que foi alterado pelo comprador [52])
	$sqlPedido = "SELECT ped.alterado, ped.numpedido, ped.ano, ped.matric, ped.data, ped.area, ped.disciplina, ped.responsavel, ped.laboratorio, ped.obs, ped.justificativa, ped.codtmat, ped.codrecurso, ped.parecer_inf, ped.parecer_demib FROM compras_pedido AS ped WHERE ano='$ano' AND numpedido='$numpedido' AND EXISTS (SELECT numpedido, ano FROM compras_item_pedido WHERE numpedido=ped.numpedido AND ano=ped.ano AND status IN ('50','60','52')) ;";
	$rsPedido = mysql_query($sqlPedido);
	//Verifica se o pedido solicitato existe e a pessoa que deseja visualiza-lo tem permissao
	if (mysql_num_rows($rsPedido) > 0) { // IF - 1
		$rowPedido = mysql_fetch_array($rsPedido);
		//Alterando os itens do pedido
		if ($op == "1") { // IF - 2 - Salvar as acoes de validacao que o usuario selecionou
			$objTransaction = new transaction($linknaopersist);
			
			$data_hora_atual = date("Y-m-d H:i:s");
			$blnRetornouAlgum = false; //Esta variavel identifica se algum dos itens foi retornado ao usuario para alteracoes
			
			//Atualizando o pedido
			$sqlAux = "UPDATE compras_pedido SET parecer_demib='$parecer_demib' WHERE numpedido='$numpedido' AND ano='$ano';";
			$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
			
			$sqlAux = "INSERT INTO compras_historico_pedido (numpedido,ano,procedimento,data,usuario) VALUES ('$numpedido','$ano','Pedido avaliado pela MANUT','$data_hora_atual','$coduser_conected');";
			$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
			
			//Selecionando os itens do pedido que podem ser alterados pelo demib
			$sqlAux = "SELECT item.item, item.status, item.codproc, proc.tipo AS tipoproc FROM compras_item_pedido AS item LEFT JOIN compras_processo AS proc ON item.codproc=proc.codproc WHERE numpedido = '$numpedido' AND ano = '$ano' AND status IN ('50','60','52') ORDER BY item;";
			$rsItens1 = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
			
			while ($rowItens = mysql_fetch_array($rsItens1)) { // WH - 1
				$item = $rowItens['item'];
				$status = $_REQUEST['status'.$item]; //Usados para itens recebidos
				$op_orcamento = $_REQUEST['op_orcamento'.$item]; //Opcao de operacoes com os itens que jah tem orcamento (foram alterados pelo comprador e precisam de reavaliacao [status 52])
				//Verifica se eh um pedido novo que estah sendo analisado
				if (in_array($rowItens['status'], array("50","60"))) {
					//Inserindo um registro no historico caso o item atual seja Cancelado/Excluido do pedido e mudando a variavel que identifica se cancelou algum item
					if ($status == "65") {
						$sqlAux = "INSERT INTO compras_historico_pedido (numpedido, ano, procedimento, data, usuario) VALUES ('$numpedido','$ano','O item $item do pedido foi cancelado pela MANUT','$data_hora_atual','$coduser_conected');";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
					}
					
					//Verifica se estah retornando este item ao usuario para mandar um e-mail de aviso
					if ($status == "25") {
						$blnRetornouAlgum = true;
					}
					
					//Verificando se existe alguma condicao antes do item ser aprovado de fato
					if ($status == "1") { //Validado
						$sqlAux = "SELECT informatica FROM tbltipomat WHERE codtmat='". $rowPedido['codtmat'] ."';";
						$rsDependencias = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						if (mysql_num_rows($rsDependencias) > 0) {
							$rowDependencias = mysql_fetch_array($rsDependencias);
						}
						if ($rowDependencias['informatica'] == "1") {
							$status = "45"; //Informatica
							
							$sqlAux = "INSERT INTO compras_historico_pedido (numpedido,ano,procedimento,data,usuario) VALUES ('$numpedido','$ano','Enviado para aprovação da DTI(informï¿½tica)','$data_hora_atual','$coduser_conected');";
							$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
							// By DAN $sqlAux = "SELECT usuario, email FROM tblusuarios u LEFT JOIN tblususist s ON u.coduser=s.coduser WHERE s.idsistema='3' AND permissao='4';"; //Avaliadores da informatica
							$sqlAux = "SELECT usuario, email FROM tblusuarios u LEFT JOIN tblususist s ON u.coduser=s.coduser WHERE s.idsistema='3' AND permissao='5';"; //Avaliadores da informatica
							$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
							$destino = "";
							while ($rowEmail = mysql_fetch_array($rsEmail)){
								$destino .= $rowEmail['email'].", ";
							}
							
							if ($destino) {
								$assunto = "Sistema de compras";
								$mensagem = "<html>";
								$mensagem .= "<body>";
								$mensagem .= "Ol&aacute; .<br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
								$mensagem .= "Foi cadastrado o pedido de compra n&uacute;mero ".$numpedido."/".$ano.".<br>";
								$mensagem .= "Neste pedido hÃ¡ itens que dependem da aprovaÃ§Ã£o da inform&aacute;tica.<br>";
								$mensagem .= "<a href=\"".$URLemail."?sist=informatica&pagina=informatica/cadastrar_avaliacao_info.php&parametros=numpedido=".$numpedido."andano=".$ano."\">Acesse aqui o pedido.</a>";
								$mensagem .= "<br> Qualquer problema entre em contato com o administrador.";
								$mensagem .= "<br><br> Atenciosamente DIRFIN/IA";
								$mensagem .= "</body>";
								$mensagem .= "</html>";
				
								envia_email($assunto, $mensagem, $destino);
							}
							
						} 
					}
					
					//Verifica se o pedido precisa passar pela aprovacao de alguem
					if ($status != "1") {
						$sqlAux = "UPDATE compras_item_pedido SET status='$status' WHERE numpedido='$numpedido' AND ano='$ano' AND item='$item';";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						
					} else { //Neste else o item nÃ£o precisa passar pela aprovacao de ninguem
						//Verifica se jah foi atribuido comprador para este item do pedido
						$sqlAux = "SELECT numpedido, ano FROM compras_item_pedido WHERE numpedido='$numpedido' AND ano='$ano' AND item='$item' AND matricomp IS NOT NULL;";
						$rsPossuiComprador = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						
						if (mysql_num_rows($rsPossuiComprador) > 0) {
							//Verifica se jah foi atribuida grade a este item
							$sqlAux = "SELECT numpedido, ano FROM compras_item_pedido WHERE numpedido='$numpedido' AND ano='$ano' AND item='$item' AND codproc IS NOT NULL;";
							$rsPossuiProcesso = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
							
							if (mysql_num_rows($rsPossuiProcesso) > 0) {
								$sqlAux = "UPDATE compras_item_pedido SET status='10' WHERE numpedido='$numpedido' AND ano='$ano' AND item='$item';";
								$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
							} else { //Neste else o item nao possui processo
								$sqlAux = "UPDATE compras_item_pedido SET status='5' WHERE numpedido='$numpedido' AND ano='$ano' AND item='$item';";
								$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
							} //if (mysql_num_rows($rsPossuiComprador) > 0) else
							
						} else { //Neste else o item nao possui comprador
							$sqlAux = "UPDATE compras_item_pedido SET status='$status' WHERE numpedido='$numpedido' AND ano='$ano' AND item='$item';"; //Aqui $status=="1"
							$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						}
					} //if ($status_item != "1") else
					
				} elseif ($rowItens['status'] == "52") { //Verifica se o pedido que estah sendo analisado eh um pedido pronto para ser finalizado que foi alterado pelo compras (precisa de reavaliacao)
					if ($op_orcamento == "1") { //Aprovar 					
						$status_item = "23"; //Aprovado
						
						//Verifica se o pedido foi alterado pelo compras (necessaria aprovacao do usuario, demib e/ou informatica)
						if ($rowPedido['alterado'] == "S") {
							//Seleciona os dados do tipo de material do pedido
							$sqlAux = "SELECT cemeq, demib, informatica FROM tbltipomat WHERE codtmat = '". $rowPedido['codtmat'] ."';";
							$rsTipoMaterial = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
	
							if (mysql_num_rows($rsTipoMaterial) > 0) {
				   			 	$rowTipoMaterial = mysql_fetch_array($rsTipoMaterial);
							} else {
								$msgerro = "O tipo de material do pedido n&atilde;o corresponde a um tipo de material v&aacute;lido."."<!--".__LINE__."-->";
								$objTransaction->executaQuery("ROLLBACK", "<!---->");
								break;
							}
							
							
							/*if ($rowTipoMaterial['cemeq'] == "1") { ////CEMEQ
								? ><script language="JavaScript">alert('Este pedido passarï¿½ pela avaliaï¿½ï¿½o do CEMEQ, isto poderï¿½ levar algum tempo.');</script>< ?
							}*/
							
							if ($rowTipoMaterial['informatica'] == "1") { ////INFORMATICA
								$status_item = "47"; //Aguardando da Informatica a aprovacao da alteracao
								
								$sqlAux = "INSERT INTO compras_historico_pedido (numpedido,ano,procedimento,data,usuario) VALUES ('".$numpedido."','".$ano."','Enviado para aprovação da DTI(Inform&aacute;tica).','$data_hora_atual','$coduser_conected');";				
								$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								//BY DAN 06/08/12 $sqlAux = "SELECT usuario,email FROM tblusuarios u LEFT JOIN tblususist s ON u.coduser=s.coduser WHERE s.idsistema='3' AND permissao='4';"; //Avaliadores da informatica
								$sqlAux = "SELECT usuario,email FROM tblusuarios u LEFT JOIN tblususist s ON u.coduser=s.coduser WHERE s.idsistema='3' AND permissao='5';"; //Avaliadores da informatica
								$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								$destino = "";
								while ($rowEmail = mysql_fetch_array($rsEmail)){
									$destino .= $rowEmail['email'].", ";
								}
					
								if ($destino) {
									$assunto = "Sistema de Compras";
									$mensagem = "<html>";
									$mensagem .= "<body>";
									$mensagem .= "Ol&aacute; .<br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
									$mensagem .= "Verificamos que o pedido de compra n&uacute;mero ".$numpedido."/".$ano." foi alterado pelo comprador e aprovado pelo solicitante.<br>";
									$mensagem .= "&Eacute; necess&aacute;ria sua aprova&ccedil;&atilde;o para o andamento do processo.<br>";
									$mensagem .= "<a href=\"".$URLemail."?sist=informatica&pagina=informatica/cadastrar_avaliacao_info.php&parametros=numpedido=".$numpedido."andano=".$ano."\">Acesse aqui o pedido.</a>";
									$mensagem .= "<br> Qualquer problema entre em contato com o administrador.";
									$mensagem .= "<br> Atenciosamente DIRFIN/IA";
									$mensagem .= "</body>";
									$mensagem .= "</html>";
						
									envia_email($assunto, $mensagem, $destino);	
								}
								
							} elseif (($rowPedido['codrecurso'] == "3") && ($rowPedido['codtmat'] != "238")) { //recurso da DIRETORIA excluindo PASSAGEM AEREA
								$status_item = "48"; //Aguardando a aprovacao da diretoria
								$sqlAux = "INSERT INTO compras_historico_pedido (numpedido,ano,procedimento,data,usuario) VALUES ('". $numpedido ."','". $ano ."','Enviado para aprovação da Diretoria','$data_hora_atual','$coduser_conected');";				
								$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								//BY DAN - 06/08/12 $sqlAux = "SELECT usuario, email FROM tblusuarios WHERE codarea='$cod_diretoria' AND tipo='INSTITUCIONAL';";
                                                                // Busca na tabela areas o email do diretor (chefe do depto DIR)
                                                                $sqlAux = "SELECT u.email, a.CodArea FROM areas a left join tblusuarios u on u.coduser=a.MATR_chefe WHERE a.CodArea='$cod_diretoria'";
								$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								$destino = "";
								while ($rowEmail = mysql_fetch_array($rsEmail)){
									$destino .= $rowEmail['email'].", ";
								}
								
								if ($destino) {
									$assunto = "Sistema de Compras";
									$mensagem = "<html>";
									$mensagem .= "<body>";
									$mensagem .= "Ol&aacute;.<br>Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
									$mensagem .= "Verificamos que o pedido de compra n&uacute;mero ".$numpedido."/".$ano." com o or&ccedil;amento da diretoria j&aacute; foi or&ccedil;ado.<br>";
									$mensagem .= "&Eacute; necess&aacute;ria sua aprova&ccedil;&atilde;o para o andamento do processo.<br>";
									$mensagem .= "<a href=\"".$URLemail."?sist=compras2&pagina=compras2/cadastrar_avaliacao_diretoria_orc_grade.php&parametros=codproc=".$rowItens['codproc']."\"> Acesse aqui o processo.</a>";
									$mensagem .= "<br>Qualquer problema entre em contato com o administrador.";
									$mensagem .= "<br>Atenciosamente, DIRFIN/IA";
									$mensagem .= "</body>";
									$mensagem .= "</html>";
									
									envia_email($assunto, $mensagem, $destino);	
								}
													
							} elseif ($rowPedido['codrecurso'] == "1") { //GRADUACAO
								$status_item = "42"; //Aguardando da graduacao a aprovacao da alteracao
							
								$sqlAux = "INSERT INTO compras_historico_pedido (numpedido,ano,procedimento,data,usuario) VALUES ('". $numpedido ."','". $ano ."','Enviado para aprovação da Graduaï¿½ï¿½o.','$data_hora_atual','$coduser_conected');";				
								$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								//BY DAN 06/08/12 $sqlAux = "SELECT usuario, email FROM tblusuarios WHERE codarea='$cod_graduacao' AND tipo='INSTITUCIONAL';";
                                                                $sqlAux = "SELECT u.email, a.CodArea FROM areas a left join tblusuarios u on u.coduser=a.MATR_chefe WHERE a.CodArea='$cod_graduacao'";
								$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								$destino = "";
								while ($rowEmail = mysql_fetch_array($rsEmail)){
									$destino .= $rowEmail['email'].", ";
								}
					
								if ($destino) {
									$assunto = "Sistema de Compras";
									$mensagem = "<html>";
									$mensagem .= "<body>";
									$mensagem .= "Ol&aacute; .<br> Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
									$mensagem .= "Verificamos que o pedido de compra n&uacute;mero ". $numpedido ."/". $ano ." com o or&ccedil;amento da gradua&ccedil;&atilde;o j&aacute; foi or&ccedil;ado.<br>";
									$mensagem .= "&Eacute; necess&aacute;ria sua aprova&ccedil;&atilde;o para o andamento do processo.<br>";
									$mensagem .= "<a href=\"".$URLemail."?sist=compras2&pagina=compras2/cadastrar_avaliacao_graduacao_orc_grade.php&parametros=codproc=".$rowItens['codproc']."\"> Acesse aqui o processo.</a>";
									$mensagem .= "<br> Qualquer problema entre em contato com o administrador.";
									$mensagem .= "<br> Atenciosamente, DIRFIN/IA";
									$mensagem .= "</body>";
									$mensagem .= "</html>";
						
									envia_email($assunto, $mensagem, $destino);	
								}
								
							} elseif (($rowPedido['codrecurso'] == "2") || (($rowPedido['codtmat'] == "238") && ($rowPedido['codrecurso'] == "3"))) { //DEPARTAMENTAL ou PASSAGEM AEREA com recurso DIRETORIA
								$status_item = "37"; //Aguardando do chefe a aprovacao da alteracao
								
								$sqlAux = "INSERT INTO compras_historico_pedido (numpedido,ano,procedimento,data,usuario) VALUES ('". $numpedido ."','". $ano ."','Enviado para aprovação do chefe do departamento.','$data_hora_atual','$coduser_conected');";				
								$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								//$sqlAux = "SELECT usuario, email FROM tblusuarios WHERE codarea='".$rowPedido['area']."' AND tipo='INSTITUCIONAL';";
                                                                // Obtem o endereco do chefe do depto
                                                                $sqlAux = "SELECT u.email, a.CodArea FROM areas a left join tblusuarios u on u.coduser=a.MATR_chefe WHERE a.CodArea='" . $rowPedido['area'] . "'";
								$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
								
								$destino = "";
								while ($rowEmail = mysql_fetch_array($rsEmail)){
									$destino .= $rowEmail['email'].", ";
								}
					
								if ($destino) {
									$assunto = "Sistema de Compras";
									$mensagem = "<html>";
									$mensagem .= "<body>";
									$mensagem .= "Ol&aacute;.<br>Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
									$mensagem .= "Verificamos que o pedido de compra n&uacute;mero ". $numpedido ."/". $ano ." j&aacute; foi or&ccedil;ado.<br>";
									$mensagem .= "&Eacute; necess&aacute;ria sua aprova&ccedil;&atilde;o para o andamento do processo.<br>";
									$mensagem .= "<a href=\"".$URLemail."?sist=compras2&pagina=compras2/cadastrar_avaliacao_depto_orc_grade.php&parametros=codproc=".$rowItens['codproc']."\"> Acesse aqui o processo.</a>";
									$mensagem .= "<br> Qualquer problema entre em contato com o administrador.";
									$mensagem .= "<br> Atenciosamente, DIRFIN/IA";
									$mensagem .= "</body>";
									$mensagem .= "</html>";
						
									envia_email($assunto, $mensagem, $destino);	
								}
							}
							
						//Estes elses soh serao executados se o pedido nao tiver sido alterado, o que por enquanto nao acontece (hoje o pedido soh vem para esta pagina se ele tiver sido alterado e der o andamento na grade)
						} 
						
						$sqlAux = "UPDATE compras_item_pedido AS item INNER JOIN compras_pedido AS ped ON (item.numpedido=ped.numpedido AND item.ano=ped.ano) SET item.status='$status_item', item.dt_alteracao='$data_hora_atual' WHERE item.status='52' AND item.codproc='".$rowItens['codproc']."' AND ped.numpedido='".$numpedido."' AND ped.ano='".$ano."' AND item.item='$item';";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						
					} elseif ($op_orcamento == "2") { //Retornar para alteraÃ§Ã£o
						//Alerta os compradores dos itens deste pedido, antes que os itens sejam atualizados
						$sqlAux = "SELECT usuario, email FROM tblusuarios WHERE coduser IN (SELECT DISTINCT item.matricomp FROM compras_item_pedido AS item INNER JOIN compras_pedido AS ped ON (item.numpedido=ped.numpedido AND item.ano=ped.ano) WHERE item.numpedido='". $numpedido ."' AND item.ano='". $ano ."' AND item.codproc='".$rowItens['codproc']."' AND item.status='52' AND item.item='$item');";
						$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						$destinatario = "";
						if ($rowEmail = mysql_fetch_array($rsEmail)) {
							$destinatario .= $rowEmail['email'].", ";
						}
						
						if ($destinatario) {
							$assunto = "Sistema de Compras";
							$mensagem = "<html>";
							$mensagem .= "<body>";
							$mensagem .= "Ol&aacute;.<br>Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
							$mensagem .= "Informamos que o pedido de compra n&uacute;mero: ". $numpedido ."/". $ano ." foi devolvido para que voc&ecirc; fa&ccedil;a algumas altera&ccedil;&otilde;es.";
							
							if ($rowItens['tipoproc'] == "P") { //Processo
								$mensagem .= "<a href=\"".$URLemail."?sist=compras2&pagina=compras2/cadastrar_proc_processo.php&parametros=codproc=".$rowItens['codproc']."\"> Acesse aqui o processo.</a>";
							} elseif ($rowItens['tipoproc'] == "A") { //Adiantamento
								$mensagem .= "<a href=\"".$URLemail."?sist=compras2&pagina=compras2/cadastrar_proc_adiantamento.php&parametros=codproc=".$rowItens['codproc']."\"> Acesse aqui o adiantamento.</a>";
							}
							
							$mensagem .= "<br> Atenciosamente, DIRFIN/IA";
					
							envia_email($assunto, $mensagem, $destinatario);
						}
						
						$sqlAux = "INSERT INTO compras_historico_pedido (numpedido, ano, procedimento, data, usuario) VALUES ('".$numpedido."','".$ano."','Alteraï¿½ï¿½o retornada pela MANUT para revisï¿½o','$data_hora_atual','$coduser_conected');";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						
						$sqlAux = "UPDATE compras_item_pedido AS item INNER JOIN compras_pedido AS ped ON (item.numpedido=ped.numpedido AND item.ano=ped.ano) SET item.status='27', item.dt_alteracao='$data_hora_atual' WHERE item.status='52' AND item.codproc='".$rowItens['codproc']."' AND ped.numpedido='".$numpedido."' AND ped.ano='".$ano."' AND item.item='$item';";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						
						
					} elseif ($op_orcamento == "3") { //Cancelar 
						$sqlAux = "INSERT INTO compras_historico_pedido (numpedido, ano, procedimento, data, usuario) VALUES ('".$numpedido."','".$ano."','Pedido cancelado pelo solicitante','$data_hora_atual','$coduser_conected');";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						
						$sqlAux = "DELETE FROM compras_preco_orcamento WHERE (numpedido, ano, item) IN (SELECT ped.numpedido, ped.ano, item.item FROM compras_item_pedido AS item INNER JOIN compras_pedido AS ped ON (item.numpedido=ped.numpedido AND item.ano=ped.ano) WHERE item.status='52' AND item.codproc='".$rowItens['codproc']."' AND ped.numpedido='".$numpedido."' AND ped.ano='".$ano."' AND item.item='".$item."');";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
						
						$sqlAux = "UPDATE compras_item_pedido AS item INNER JOIN compras_pedido AS ped ON (item.numpedido=ped.numpedido AND item.ano=ped.ano) SET item.status='65', item.codproc=NULL, item.dt_alteracao='$data_hora_atual' WHERE item.status='52' AND item.codproc='".$rowItens['codproc']."' AND ped.numpedido='".$numpedido."' AND ped.ano='".$ano."' AND item.item='$item';";
						$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
					}
				
				}
			} // WH - 1
			exit;				
			//Verifica se retornou algum item ao solicitante
			if ($blnRetornouAlgum) {
				$sqlAux = "SELECT usuario, email FROM tblusuarios AS usu LEFT JOIN compras_pedido AS ped ON usu.coduser=ped.matric WHERE ped.numpedido='$numpedido' AND ped.ano='$ano';";
				$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
				$destino = "";
				while ($rowEmail = mysql_fetch_array($rsEmail)){
					$destino .= $rowEmail['email'].", ";
				}
				
				if ($destino) {
					$assunto = "Sistema de Compras";
					$mensagem = "<html>";
					$mensagem .= "<body>";
					$mensagem .= "Ol&aacute;.<br>Esta mensagem foi gerada automaticamente, n&atilde;o responda.<br>";
					$mensagem .= "Informamos que o pedido de compra n&uacute;mero: ".$numpedido." / ".$ano." foi devolvido para que voc&ecirc; fa&ccedil;a algumas altera&ccedil;&otilde;es.<br>";
					$mensagem .= "Clique no link abaixo para realizar estas altera&ccedil;&otilde;es.<br>";
					$mensagem .= "<a href=\"".$URLemail."?sist=compras2&pagina=compras2/cadastrar_pedido_itens.php&parametros=numpedido=".$numpedido."andano=".$ano."andop_alteracao=3\"> Acesse aqui o pedido.</a>";
					$mensagem .= "<br>Qualquer problema entre em contato com o chefe da MANUT/IA.";
					$mensagem .= "<br>Atenciosamente DIRFIN/IA";
					$mensagem .= "</body>";
					$mensagem .= "</html>";
	
					envia_email($assunto, $mensagem, $destino);
				}
			}
						
			//Itens cadastrados
			$sqlAux = "SELECT COUNT(*) AS qtd_itens_cadastrados FROM compras_item_pedido WHERE numpedido='$numpedido' AND ano='$ano';";
			$rsItens2 = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
			$qtd_itens_cadastrados = 0;
			if ($rowItens = mysql_fetch_array($rsItens2)) {
				$qtd_itens_cadastrados = $rowItens['qtd_itens_cadastrados'];
			}
			
			//Itens cancelados
			$sqlAux = "SELECT COUNT(*) AS qtd_itens_cancelados FROM compras_item_pedido WHERE numpedido='$numpedido' AND ano='$ano' AND status='65';";
			$rsItens3 = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
			$qtd_itens_cancelados = 0;
			if ($rowItens = mysql_fetch_array($rsItens3)) {
				$qtd_itens_cancelados = $rowItens['qtd_itens_cancelados'];
			}
			
			//Verificando se cancelou todos os itens existentes no pedido
			if (($qtd_itens_cadastrados > 0) && ($qtd_itens_cadastrados == $qtd_itens_cancelados)) {
				$sqlAux = "INSERT INTO compras_historico_pedido (numpedido,ano,procedimento,data,usuario) VALUES ('$numpedido','$ano','$data_hora_atual','Pedido cancelado','$coduser_conected');";
				$objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
				
				$sqlAux = "SELECT usuario, email FROM tblusuarios AS usu LEFT JOIN compras_pedido AS ped ON usu.coduser=ped.matric WHERE ped.numpedido='$numpedido' AND ped.ano='$ano';";
				$rsEmail = $objTransaction->executaQuery($sqlAux, "<!--".__LINE__."-->");
				$destino = "";
				while ($rowEmail = mysql_fetch_array($rsEmail)){
					$destino .= $rowEmail['email'].", ";
				}
				
				if ($destino) {
					$assunto = "Sistema de Compras";
					$mensagem .= "Informamos que o pedido de compra n&uacute;mero: ".$numpedido." foi cancelado pela MANUT. Foi descrita a seguinte justificativa.<br><br>";
					$parecer_demib_aux = str_replace($search, $replace, $parecer_demib); /*$search e $replace sÃ£o definidos no email.php*/
					$mensagem .= $parecer_demib_aux;
					$mensagem .= "<br><br> Qualquer problema entre em contato com o chefe da MANUT/IA.";
					$mensagem .= "<br> Atenciosamente DIRFIN/IA";
					$mensagem .= "</body>";
					$mensagem .= "</html>";
	
					envia_email($assunto, $mensagem, $destino);
				}
			}
			
			//Verificando se alguma query nao foi executada corretamente
			$strFalha = $objTransaction->finalizaTransaction();
			if ($strFalha) {
			    $msgerro = $strFalha;
			} else {
			    $msgfinal = "Pedido alterado com sucesso!";
			}
			
			$objTransaction = null;
		} else { // IF - 2 (Verifica se OP=1 
                 //echo "OP=branco";
                }
                
		
	} else { // IF - 1
		$msgerro = "O pedido solicitado &eacute; inv&aacute;lido!";
	}
?>
<script type="text/javascript">

function goTo(strUrl) {
    var arrUrl;
    var objForm;
    var arrParams;
    var arrCampo;
    var objInput;
    
    arrUrl = strUrl.split("?");
    
    objForm = document.createElement("form");
    objForm.setAttribute("method", "post");
    objForm.setAttribute("target", "mainFrameIn");
    objForm.setAttribute("action", arrUrl[0]); //arrUrl[0] = string antes do "?"
    
    //Verifica se tem parametros
    if (arrUrl.length > 1) {
        arrParams = arrUrl[1].split("&"); ////arrUrl[1] = string depois do "?"
    
        for (var i = 0; i < arrParams.length; i++) {
            arrCampo = arrParams[i].split("=");
            
            objInput = document.createElement("input");
            objInput.setAttribute("type", "hidden");
            objInput.setAttribute("name", arrCampo[0]); //arrCampo[0] = string antes do "="
            if (arrCampo.length > 1) {
                objInput.setAttribute("value", arrCampo[1]); //arrCampo[1] = string depois do "="
            }
            objForm.appendChild(objInput);
        }
    }
    
    document.body.appendChild(objForm);
    objForm.submit();
}

function ok() {
	form = document.formulario;
	
	var op_itens = "";
	for (var i = 0; i < form.op_itens.length; i++) {
		if (form.op_itens[i].checked) {
			op_itens = form.op_itens[i].value;
			break;
		}
	}
	
	if (op_itens == "") {
		alert('Escolha uma opï¿½ï¿½oo.');
		form.op_itens[0].focus();
		return false;
	}
	
	form.submit();
	return true;
}

function salvar() {
	form = document.formulario;
	
	if (form.parecer_demib.value == "") {
		alert('Preencha o parecer.');
		form.parecer_demib.focus();
		return false;
	}
	
	form.op.value = "1"; //Alterar itens
	form.submit();
	return (true);
}

</script>
<?	
if (($msgerro) || ($msgfinal)) { ?>
<body>
	<div align="center">
		<font class="fontetitulo"><? print (($msgerro)?$msgerro:$msgfinal); ?></font>
	</div>
</body>
<?
	die();
} ?>
<body>
<form name="formulario" action="cadastrar_avaliacao_demib.php" method="post" target="mainFrameIn">
<input type="hidden" name="op" value="" />

<input type="hidden" name="numpedido" value="<? print $numpedido; ?>" />
<input type="hidden" name="ano" value="<? print $ano; ?>" />
<input type="hidden" name="origem" value="<? print $origem; ?>" />
<input type="hidden" name="parametros" value="<? print $parametros; ?>" />

	<table align="center" cellspacing="0" class="tabela_conteudo" width="80%">
        <tr class="titulo">
        	<td colspan="4" align="center">
        		<font class="fontetitulo">Dados do pedido de compra</font>
			</td>
        </tr>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">Pedido</font>
			</td>
        	<td colspan="3" align="left">
        		<font class="fonte1"><? print $numpedido ."/". $ano; ?></font>
			</td>
        </tr>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">&Oacute;rg&atilde;o requisitante</font>
			</td>
        	<td colspan="3" align="left">
        		<? 
					$rsArea = mysql_query("SELECT Descricao FROM areas WHERE CodArea = '". $rowPedido['area'] ."';") ?>
        			<font class="fonte1"><? if ($rowArea = mysql_fetch_array($rsArea)) print $rowArea['Descricao']; else print "-"; ?></font>
			</td>
        </tr>

		<?	$rsRequisitante = mysql_query("SELECT usu.nome, usu.usuario, usu.email, serv.ramal FROM tblusuarios usu LEFT JOIN servidores serv ON usu.coduser = serv.MATR WHERE usu.coduser = '". $rowPedido['matric'] ."';");
			if (mysql_num_rows($rsRequisitante)) $rowRequisitante = mysql_fetch_array($rsRequisitante); ?>
        <tr class="linha1">
        	<td align="left" width="15%">
        		<font class="fontetitulo">Requisitante</font>
			</td>
        	<td align="left" width="35%">
        			<font class="fonte1"><? print $rowRequisitante['nome']; ?></font>
			</td>
        	<td align="left" width="15%">
        		<font class="fontetitulo">Ramal</font>
			</td>
        	<td align="left" width="35%">
        			<font class="fonte1"><? print $rowRequisitante['ramal']; ?></font>
			</td>
        </tr>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">E-mail</font>
			</td>
        	<td align="left">
        			<font class="fonte1"><? print $rowRequisitante['email']; ?></font>
			</td>
        	<td align="left">
        		<font class="fontetitulo">Data</font>
			</td>
        	<td align="left">
        			<font class="fonte1"><? print date("d/m/Y"); ?></font>
			</td>
        </tr>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">Recurso</font>
			</td>
        	<td align="left">
				<?	$rsRecurso = mysql_query("SELECT descricao FROM compras_recurso WHERE codrecurso = '". $rowPedido['codrecurso'] ."';") ?>
        			<font class="fonte1"><? if ($rowRecurso = mysql_fetch_array($rsRecurso)) print $rowRecurso['descricao']; else print "-"; ?></font>
			</td>
        	<td align="left">
        		<font class="fontetitulo">Tipo material</font>
			</td>
        	<td align="left">
				<?	$rsTipoMat = mysql_query("SELECT tipomat FROM tbltipomat WHERE codtmat = '". $rowPedido['codtmat'] ."';"); ?>
        			<font class="fonte1"><? if ($rowTipoMat = mysql_fetch_array($rsTipoMat)) print $rowTipoMat['tipomat']; else print "-"; ?></font>
			</td>
        </tr>
	<?	if ($rowPedido['codrecurso'] == "1") { //Orcamentario - graduacao ?>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">Disciplina</font>
			</td>
        	<td align="left">
       			<font class="fonte1"><? print $rowPedido['disciplina']; ?></font>
			</td>
        	<td align="left">
        		<font class="fontetitulo">Respons&aacute;vel</font>
			</td>
        	<td align="left">
       			<font class="fonte1"><? print $rowPedido['responsavel']; ?></font>
			</td>
        </tr>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">Laborat&oacute;rio</font>
			</td>
        	<td colspan="3" align="left">
        		<? 
					$laboratorio_temp = "";
					if (strstr($rowPedido['laboratorio'], "LB1_2")) $laboratorio_temp .= "LB1 ou 2";
					if (strstr($rowPedido['laboratorio'], "LB3_4")) $laboratorio_temp .= ($laboratorio_temp?", ":"")."LB3 ou 4";
					if (strstr($rowPedido['laboratorio'], "LB8")) $laboratorio_temp .= ($laboratorio_temp?", ":"")."LB8";
					if (strstr($rowPedido['laboratorio'], "MBS")) $laboratorio_temp .= ($laboratorio_temp?", ":"")."MBS";
				?>
       			<font class="fonte1"><? print $laboratorio_temp; ?></font>
			</td>
        </tr>
	<?	} ?>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">Justificativa</font>
			</td>
        	<td colspan="3" align="left">
       			<font class="fonte1"><? print $rowPedido['justificativa']; ?></font>
			</td>
        </tr>
        <tr class="linha1">
        	<td align="left">
        		<font class="fontetitulo">Observa&ccedil;&atilde;o</font>
			</td>
        	<td colspan="3" align="left">
       			<textarea name="obs" cols="75" rows="2" disabled><? print $rowPedido['obs']; ?></textarea>
			</td>
        </tr>
	</table>

<? //Verifica se o pedido nao eh de um tipo especial (ajuda de custo, estadia, passagem aerea)
if (!in_array($rowPedido['codtmat'], array("263", "262", "238", "266", "280"))) { //============================  PEDIDO COMUM ?>
	<font class="titulo"><br/></font>
	<table align="center" cellspacing="0" cellpadding="0"  border="0" width="500px">
        <tr>
        	<td align="center">
        		<input type="radio" name="op_itens" value="100" />
			</td>
        	<td align="left">
				<font class="fonte1">
					Validar todos os itens
				</font>
			</td>
        	<td align="center">
        		&nbsp;
			</td>
        	<td align="center">
        		<input type="radio" name="op_itens" value="200" />
			</td>
        	<td align="left">
				<font class="fonte1">
					Retornar todos os itens ao usu&aacute;rio
				</font>
			</td>
        	<td align="center">
				<input type="button" name="btnOk" value="OK" onclick="ok();">
			</td>
		</tr>
	</table>
			
	
	<font class="titulo"><br/></font>
	<table align="center" cellspacing="0" class="tabela_conteudo" width="80%">
        <tr class="titulo">
        	<td colspan="5" align="center">
        		<font class="fontetitulo">Lista de materiais</font>
			</td>
        </tr>
        <tr class="titulo">
        	<td align="center">
        		<font class="fontetitulo">Item</font>
			</td>
        	<td align="center">
        		<font class="fontetitulo">Descri&ccedil;&atilde;o do material</font>
			</td>
        	<td align="center">
        		<font class="fontetitulo">Qtd. pedida</font>
			</td>
        	<td align="center">
        		<font class="fontetitulo">Unidade</font>
			</td>
        	<td align="center">
        		<font class="fontetitulo">Valida&ccedil;&atilde;o</font>
			</td>
        </tr>
	<?	
		$sqlItens = "SELECT item, codmerc, descricao, unidade, qtd, prioridade, status FROM compras_item_pedido WHERE numpedido = '$numpedido' AND ano = '$ano' AND status IN ('50','60','52') ORDER BY item;";
		$rsItens = mysql_query($sqlItens);
		
		while ($rowItens = mysql_fetch_array($rsItens)) {
			if ($fonte != "fonte1") {$fonte = "fonte1"; $linha = "linha1";} else {$fonte = "fonte2"; $linha = "linha2";} ?>
		<tr class="<? print $linha; ?>">
        	<td align="center">
        		<font class="<? print $fonte; ?>">
        			<? print $rowItens['item'].(($rowItens['status'] == "52")? "<!--pedido orcado-->": "<!--pedido novo-->"); ?>
				</font>
			</td>
        	<td align="left">
        		<font class="<? print $fonte; ?>">
        		<? if ($rowItens['codmerc'] == "0") {
        			print "OUTRO";
        		} else {
					$rsMaterial = mysql_query("SELECT codmerc, descricao FROM compras_mercadoria WHERE codtmat='". $rowPedido['codtmat'] ."' AND codmerc='". $rowItens['codmerc'] ."';");
					if ($rowMaterial = mysql_fetch_array($rsMaterial)) { print $rowMaterial['descricao']; } else { print "-"; }
        		} 

				if ($rowItens['descricao']) { print " - ".$rowItens['descricao']; } ?>
				</font>
			</td>
        	<td align="center">
        		<font class="<? print $fonte; ?>">
        			<? print $rowItens['qtd']; ?>
				</font>
			</td>
        	<td align="center">
        		<font class="<? print $fonte; ?>">
        			<? print $rowItens['unidade']; ?>
				</font>
			</td>
        	<td align="center">
        		<font class="<? print $fonte; ?>">
    				<?
					//Verifica se o item eh um item recem solicitado
					if (in_array($rowItens['status'], array("50","60"))) { ?>
	        			<select name="status<? print $rowItens['item']; ?>" >
	        				<option value="50" <?  if ((!$op_itens) && ($rowItens['status'] == "50")) { print "selected"; } ?>>-- selecione --</option>
	        				<option value="60" <?  if ((!$op_itens) && ($rowItens['status'] == "60")) { print "selected"; } ?>>Em avalia&ccedil;&atilde;o</option>
	        				<option value="1"  <? if (((!$op_itens) && ($rowItens['status'] == "1")) || ($op_itens=="100"))  { print "selected"; } ?>>Validado</option>
	        				<option value="65" >Exclu&iacute;do</option>
	        				<option value="25" <? if (((!$op_itens) && ($rowItens['status'] == "25")) || ($op_itens=="200")) { print "selected"; } ?>>Retornar ao usu&aacute;rio</option>
	        			</select>
    				<?
					} elseif ($rowItens['status'] == "52") { //Verifica se o item eh um item prestes a ser finalizado, que foi alterado pelo comprador?>
	        			<select name="op_orcamento<? print $rowItens['item']; ?>" >
	        				<option value="" <?  if ((!$op_itens) && ($rowItens['status'] == "52")) { print "selected"; } ?>>-- selecione --</option>
	        				<option value="1"  <? if ($op_itens=="100")  { print "selected"; } ?>>Aprovado</option>
	        				<option value="2" <? if ($op_itens=="200") { print "selected"; } ?>>Retornar para altera&ccedil;&atilde;o</option>
	        				<option value="3" >Cancelar</option>
	        			</select>
    				<?
					} ?>
				</font>
			</td>
		</tr>
	<?	} //while ($rowItens = mysql_fetch_array($rsItens))
	?>
	</table>
	
	
	
<? } //if (!in_array($rowPedido['codtmat'], array("263", "262", "238"))) ?>


	<font class="titulo"><br/><br/></font>
	<table align="center" cellspacing="0" class="tabela_conteudo" width="80%">
		<tr class="linha1">
        	<td align="left">
				<font class="fontetitulo">
					Parecer MANUT:
				</font>
			</td>
			<td>
		  		<textarea name="parecer_demib" cols="60" rows="5"><? print (($parecer_demib)?$parecer_demib:$rowPedido['parecer_demib']); ?></textarea>
			</td>
		</tr>
		<tr class="linha1">
        	<td align="left">
				<font class="fontetitulo">
					Observa&ccedil;&atilde;o:
				</font>
			</td>
			<td>
		  		<textarea name="obs" cols="60" rows="5" readonly><? print $rowPedido['obs']; ?></textarea>
			</td>
		</tr>
	</table>
	
	
	<font class="titulo"><br/><br/></font>
	<table align="center" cellspacing="0" cellpadding="0" width="80%">
        <tr>
        	<td align="center">
					<input type="button" name="btnSalvar" value="Salvar" onclick="salvar();" />
        		<?
				if ($origem) { ?>
					<input type="button" name="btnVoltar" value="Voltar" onClick="javascript: goTo('<? print $origem; ?>?<? print $parametros; ?>');" />
				<? 
				} ?>
			</td>
        </tr>
	</table>

</form>
</body>
</html>
<?
	Desconectar($linknaopersist);
?>
