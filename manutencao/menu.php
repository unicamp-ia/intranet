<?
        // Permissões de acesso ao módulo MANUTENCAO
        // ------------------------------------------
        //   $nivel representa a permissao
        //   1 = e-mail avaliacao
        //   2 = Consulta financeiro
        //   3 = Tecnico
        //   4 = Funcionario almoxarifado
        //   5 = Gerenciamento e técnico
        //   6 = Gerenciamento
        include("../base/VerifiqueLogin.php");
        function ShowTD($href,$target,$txt)
        {
                echo "<td align=\"center\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">";
                echo "<a href=\"$href\" target=\"$target\" class=\"LMS\">$txt</a>";
                echo "</font></strong></td>";
        }
 	
	$tipo_conected = $_SESSION["tipo_conected"];
	$coduser = $_SESSION["coduser_conected"];
	$codarea_conected = $_SESSION["codarea_conected"];
	$cod_graduacao = $_SESSION["cod_graduacao"];
        
        if ($coduser=='119873') $_SESSION["manutencao"]=6; // Remover assim que entrar em licença-prêmio ou deixar de desenvolver em definitivo a intranet
        
        // Se usuário é o gestor de informatica, então ele deve ter permissão de gerenciamento deste módulo, isso para acompanhar o funcionamento 
        // do sistema
        if ($_SESSION['GestorInformatica']==$coduser_conected) {
           $_SESSION["manutencao"]=6;
        }  
        $nivel = $_SESSION["manutencao"]; 
        // Alteracao inserida por Daniel
        // Verifica se o usuario e` chefe do departamento ao qual ele esta ligado. Se for, disponibiliza menu de administracao.
        $EoChefe=false;
        if ($_SESSION["coduser_chefe"] && $_SESSION["coduser_chefe"]==$coduser) {
		$EoChefe=true;
	}
        $matr_chefe= $_SESSION["coduser_chefe"];
        
        function ShowOPC($OPC,$LNK) {
            global $td;
            if ($td==0) echo "<tr>";
            $td ++;
           
           
            ?>     
            <td align="center">
                <font color="#990000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>
                        <a href="<?echo $OPC; ?>" target="mainFrameIn" class="LMS"> <? echo $LNK; ?></a>
                </strong></font>
            </td>
           <? 
           if ($td==7) {
                echo "</tr>";
                $td=0;
           }
       }
      
?>
<table border="0" width="100%" cellspacing="0">
	<tr>
		<td width="14%"></td>
		<td height="1%" colspan="5"><hr></td>
	</tr>
	<tr>
		<td width="14%"></td>
		<td colspan="5">

<table width="100%" border="0" class="mainMenu">
	
	<?
           $td=0;
           if($nivel == 5 || $nivel == 6 || $EoChefe ){ // Se gerente ou gerente+tecnico, pode avaliar
             ShowOPC("veravaliacoes.php","AVALIA&Ccedil;&Otilde;ES");
           }
           //echo "n=" . $nivel;
           ShowOPC("cadordem.php","SOLICITAR SERVI&Ccedil;OS");
           $MsgID="MEUS SERVI&Ccedil;OS CADASTRADOS";
           if ($EoChefe || $nivel >=5) {
              $MsgID="SERVI&Ccedil;OS CADASTRADOS"; 
           }
          
           ShowOPC("ordens.php",$MsgID);
           ShowOPC("cadavaliacao.php","PLANEJAR OBRAS");
           ShowOPC("avalanteriores.php","MINHAS OBRAS CADASTRADAS");
	   if (($nivel==5 || $nivel==6 ) || ($EoChefe && $codarea_conected=="MANUT")) { // Somente tem acesso Chefe da MANUT e os gerentes.  
             ShowOPC("listas.php","SERVI&Ccedil;OS");
             ShowOPC("verifAvaliacoes.php","VERIFICAR OBRAS");
             ShowOPC("cadservicos.php","CUSTO DE SERVI&Ccedil;OS");
           }  
          
           if ($EoChefe and $codarea_conected=="MANUT") { /* Se usuario e' chefe da MANUT */ 
              ShowOPC("cad_avaliacao_demib.php","AUTORIZAR COMPRAS");
           }
           if ($EoChefe) { 
               if ($codarea_conected <> "CG") { 
                   ShowOPC("autorizaOS.php","AUTORIZAR ORDENS");
               } else {
                   ShowOPC("autorizagraduacao.php","AUTORIZAR ORDENS");
               } 
           } ?>

  
     <?
     if($nivel>=5 || ($EoChefe && $codarea_conected=="MANUT")){ // Almorarifafo, gerentes e chefe
         ShowOPC("almoxarifado.php","ALMOXARIFADO");
         ShowOPC("cadrecurso.php","RECURSOS");
         ShowOPC("relatorios.php","RELAT&Oacute;RIOS");
     } 
    if($nivel>=5 || ($EoChefe && $codarea_conected=="MANUT")){ // Somente gerentes ou chefe
        ShowOPC("cadautorizados.php","AUTORIZADOS");
    }
    if ($td >0 and $td < 7) {
        echo "</tr>";
    }
     ?>

</table>

		</td>
	</tr>
	<tr>
		<td width="14%"></td>
		<td height="1%" colspan="5"> <hr></td>
	</tr>
</table>

