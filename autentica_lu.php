<?php

include_once("./base/db.php");
require_once('sanitize.php');
header("Location: " .SI_ROOT);
header("Content-type: text/html; charset=utf-8");

// Prevencao de HTML e SQL Injection 
// Importa classe

// Filtra HTML e SQL Injection em todos os campos
// echo $_GET."<BR>";
// echo $_POST."<BR>";
$_GET = Sanitize::filter($_GET);
$_POST = Sanitize::filter($_POST);
$_REQUEST = Sanitize::filter($_REQUEST);
// echo $_GET."<BR>";
// echo $_POST."<BR>";
// Filtra somente HTML Injection
// $_POST = Sanitize::filter($_POST, array('html'));
// Filtra somente SQL Injection
// $_POST = Sanitize::filter($_POST, array('sql'));
// Filtra somente campos especificos
// $_GET['campo'] = Sanitize::filter($_GET['campo']);
// $_POST['campo'] = Sanitize::filter($_POST['campo']);


 function StringToAscii($str)
{
    //Separa cada caracter digitado
    $arrStr = str_split($str); //Debug($arrStr,"\$arrS",__LINE__);

    //Converte para ascii, com preenchimento de zeros \xe0 esquerda para que cada valor tenha 3 caracteres
    $strOrd=null; 
    foreach($arrStr AS $val) 
        $strOrd .= sprintf("%03s",ord($val)); $lenOrd = strlen($strOrd);
    return $strOrd;
}

/*
   Funcao escrita por Josue Samuel
 */
 function DecimalToHexadecinal($str, $conjuntoBits=3)
{
    //Converte de ascii para a strin original
    $arrAscii = str_split($str, $conjuntoBits); //Debug($arrAscii,"\$arrAscii",__LINE__,'Green');

    //Converte do c\xf3digo ascii p/ hexa
    $strHex=null; 
    foreach($arrAscii AS $val) { 
        $strHex .= base_convert($val, 10, 16); 
    } 
    $lenHex = strlen($strHex);
    return $strHex;
}

/*
   Funcao escrita por Josue Samuel
 */
 function CharacterToHexadecimal($str, $conjuntoBitsAscii=3, $conjuntoBitsHexa=3)
{
    $strAscii = StringToAscii($str, $conjuntoBitsAscii);
    $strHex = DecimalToHexadecinal($strAscii, $conjuntoBitsHexa);
    return $strHex;
}

if ($_SERVER['SERVER_PORT']!=443) {
    $sslport=443; //whatever your ssl port is
    $url = "https://". $_SERVER['SERVER_NAME'] . ":" . $sslport . $_SERVER['REQUEST_URI'];
    header("Location: $url");
    exit;
}

$sist = $_REQUEST['sist'];
if (!$sist) {
    //header("Location: ".$_SERVER['SERVER_NAME']);
    header('Location: ../../');
    die();
}
  

include("./ldap/codigos_areas");
include("./base/email.php");
$usuario = filter_var($_POST['usuario']);
$usuario  = lcfirst($usuario);
$senha = filter_var($_POST['senha']);
$submiter = filter_var($_POST['submiter']);
$pagina = filter_var($_REQUEST['pagina']);
$parametros = filter_var($_REQUEST['parametros']);
$timehoje = date("Y-m-d H:i:s");
$op = filter_var($_REQUEST['op']);
$passo = filter_var($_REQUEST['passo']);
$login = filter_var($_REQUEST['login']);
$sucesso = null;
$ocultarClique = null;
$msgEmail = null;

$tipo_erro=0;

$excetos=array('086362'=>'feagri','082301'=>'iel','094602'=>'dac','150774'=>'fe');

//op=AU: autenticaÃ§Ã£o utilizando as credenciais do Login Unico
if ($op=="AU") { /* Se o formulario for enviado, ele entra nesta parte do script senao ele irï¿½ mostrar o formulï¿½rio novamente atï¿½ ser enviado corretamente!! */
    if ((!$usuario) || (!$senha)) {
        $erro = "Voc&ecirc; deixou algum campo em branco no formul&aacute;rio de autenticaÃ§Ã£o.";
    } else  {
        db_connect() or die("N&atilde;o consigo me conectar ao Servidor!");
        
        /* BY DAN $if(substr_count($usuario, '@')==0) {
                $usuario = $usuario."@unicamp.br";
        }*/
        
        /* Verifica se existe usuarios com aquela senha digitada!! */
        $lu_usuario = addslashes($usuario); // Contra sql injection
        $lu_senha = addslashes($senha); // Contra sql injection
        $senha_md5 = md5($lu_senha);

        $SQL = "select * from dbcorp.usuarios where login = '$lu_usuario'";
        $res = mysql_query($SQL);
         

        if ($linha = mysql_fetch_array($res)) {
            if ($linha['ativo'] <> 'Sim') { 
                $erro = "Usuário desativado no banco de usuários do IA. Procure a Diretoria TÃ©cnica de InformÃ¡tica para ativÃ¡-lo!";
            }

            if (!$erro and !authenticate($lu_usuario, $lu_senha)) {
                $erro = "A senha informada Ã© incorreta. Digite-a novamente";
            }
        } else {
            $tipo_erro = 1;
            $erro = "Usuário nÃ£o encontrado na base de dados de usuários do IA. Caso seja aluno regular matriculado no IA, execute o procedimento de ativaÃ§Ã£o do seu login na base de dados do IA.";
        }

        if (!$erro) {
            $username = $linha['login'];
            $nome = $linha['nome'];
            $usuarioID = $linha['usuarioID'];
            $SQL = "select * from dbcorp.usuarios_tipo where usuarioID=$usuarioID and (tipo='Discente' || tipo='Participante Externo')";
            $res1 = mysql_query($SQL);
            if (!$linha1 = mysql_fetch_array($res1)) {
                $erro = "NÃ£o encontrado o perfil Discente no banco de dados de usuários do IA. Procure suporte na Diretoria TÃ©cnica de InformÃ¡tica do IA.";
            } else {   
                $codarea_conected = $linha1['depto'];
                session_start(); /* A session_start deve estar antes de qualquer codigo senao dara erro !!  e ele pode ter qualquer nome  */
                $_SESSION["base"] = "./base";
                $_SESSION["usuario"] = $lu_usuario;
                $_SESSION["senha"] = $lu_senha;
                
                // Apenas para teste. Retirar depois
                //
                $tipo_1 = "Aluno UNICAMP";
                $_SESSION["tipo_conected"] = $tipo_1;
                $_SESSION["codarea_conected"] = $codarea_conected;
                $_SESSION["coduser_conected"] = $username;
                $_SESSION["nomeuser_conected"] = utf8_decode($nome);

                // Newton: alteraÃ§Ã£o para atender ao modelo antigo de username 123456 => x123456
                if(is_numeric($_SESSION["coduser_conected"])){
                    $_SESSION["coduser_conected_old"] = strtolower(substr($_SESSION['nomeuser_conected'],0,1)) . $_SESSION["coduser_conected"];
                }

                if ($username=='u000003') {
                     $_SESSION["emailuser_conected"] = "roseno@iar.unicamp.br";
                } else {
                $_SESSION["emailuser_conected"] = $username . "@dac.unicamp.br";
                }
                $_SESSION["cod_diretoria"] = "DIR"; /* Registra o cÃ³digo da diretoria na sessÃ£o */
                $_SESSION["cod_graduacao"] = "CGRAD"; /* Registra o cÃ³digo da diretoria na sessÃ£o */
                $_SESSION["cod_pos"]="CPG"; /* Registra o cÃ³digo da diretoria na sessÃ£o */
                                
                //Coloca em sessï¿½o os dados do chefe do departamento ao qual o usuário estÃ¡ lotado
                $SQL = "Select a.*, u.email, u.nome from areas a left join tblusuarios u on a.MATR_chefe=u.coduser where a.CodArea='$codarea_conected'";
                $func = mysql_query($SQL);
                if (mysql_num_rows($func) > 0) {
                    $res = mysql_fetch_array($func);
                    $_SESSION["coduser_chefe"] = $res['MATR_chefe'];
                    $_SESSION["coduser_imediato"] =$res['MATR_imediato'];
                    $_SESSION["email_chefe"] = $res['email'];
                    $_SESSION["nomeuser_chefe"] = $res['nome'];
                    $_SESSION["nome_departamento"] = $res['Descricao'];
                    $SQL = "SELECT  nome,coduser,email from tblusuarios where coduser='" . $res['MATR_imediato'] . "'";
                    $res = mysql_query($SQL);
                    if ($linha = mysql_fetch_array($res)) {
                        $_SESSION["email_imediato"] = $linha['email'];
                        $_SESSION["nomeuser_imediato"] = $linha['nome'];
                    }
                }
                                
                //Busca o secretario do departamento
                $SQL = "select s.*,u.nome,u.email,u.codarea from RHsecretarios s left join tblusuarios u on codsecretario=coduser where u.codarea='$codarea_conected'";
                $res = mysql_query($SQL);
                if ($lin = mysql_fetch_array($res)) {
                    $_SESSION["cod_secretario"] = $lin['codsecretario'];
                    $_SESSION["nome_secretario"] = $lin['nome'];
                    $_SESSION["email_secretario"] = $lin['email'];
                }
                header("Location: ./pg_inicial.php?sist=".$sist);
                exit;  /* Finaliza este script aqui */
            }
        } 
    }
}
  
$parar=false;
$login_ativado=false;

if ($op=="AS" && $passo==5) {
    $ldap_senha = filter_var($_POST['senha0']);
    $senhaNova = filter_var($_POST['senha1']);
    $senhaNovaC = filter_var($_POST['senha2']);
    
    if (($ldap_senha) && ($senhaNova) && ($senhaNovaC)) {
        if ($senhaNova === $senhaNovaC) {
            if (validaSenha($senhaNova)) {
                $ldap_usuario = filter_var($_POST['usuario']);
                $ldaprdn = "uid=" . $ldap_usuario. ",ou=people,dc=unicamp,dc=br";
                $ldapconn = ldap_connect("ldaps://ldap1.unicamp.br ldaps://ldap1.unicamp.br") or die("Servidor nao pode ser encontrado");
                $tt = ldap_start_tls($ldapconn);
                if ($ldapconn) { 
                    ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
                    $ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldap_senha);
                    if ($ldapbind) { 
                        db_connect() or die("N&atilde;o consigo me conectar ao Servidor MYSQL!");
                        $usuarioID = 0;
                        $SQL = "select * from dbcorp.usuarios where login='$login'";
                        $res = mysql_query($SQL);
                        if (mysql_num_rows($res) === 1) {
                            if ($linha = mysql_fetch_array($res)) {
                                $usuarioID = $linha['usuarioID'];
                            }
                            if ($usuarioID <> 0) {
                                $senha_md5 = md5($senhaNova);
                                $senha_hexa = CharacterToHexadecimal($senhaNova);
                                $Adata = date("Y-m-d H:i:s");
                                $CA = md5($Adata);
                                $SQL = "update dbcorp.usuarios set senha='$senha_md5',senha2='$senha_hexa',cod_ativacao='$CA',evento_replicar='AtualizarSenha'";
                                $SQL .= ",atualizado_por=1210,atualizado_em='$Adata' where usuarioID=$usuarioID";
                                mysql_query($SQL) or die(mysql_error());
                                $sucesso = "A senha do login <b>" . $login . " </b> foi alterada com sucesso.";
                                $ocultarClique = True;
                                $parar = true;
                                $passo = 0;
                                $eM1 = "<html><body>";
                                $eM2 = "</body></html>";
                                $mensagem = "Alteraï¿½ï¿½o de senha:<br><b>Registro existente</b><br>Login " . $login;
                                envia_email("Alteraï¿½ï¿½o de senha de Login ï¿½nico", $eM1. $mensagem. $eM2, "intra.ia@iar.unicamp.br"); 
                                
                                $msgEmail = $sucesso . '<br><br><b>VocÃª declarou que leu e estÃ¡ ciente do termo de aceite que contÃ©m as normas de acesso aos serviÃ§os de InformÃ¡tica, replicado abaixo:</b>'; 
                                $msgEmail = $msgEmail . '<br><br><a href="https://www.iar.unicamp.br/lab/informatica/aceite.php" target="_blank">Termo de Aceite</a>';
                                //Enviando email ao dicente
                                envia_email("Alteraï¿½ï¿½o da senha de login nos sistemas do IA", $eM1 . $msgEmail . $eM2, $login . "@dac.unicamp.br"); 
                                //envia_email("Alteraï¿½ï¿½o da senha de login nos sistemas do IA", $eM1 . $msgEmail . $eM2, "marcelo.silva@iar.unicamp.br"); 
                            } else {
                                $erro = "Login nÃ£o localizado em nossa base de dados";
                            }
                        } else {
                            $erro = "Desculpe!! Ocorreu um erro os atualizar os dados";
                        }
                    } else {
                        $erro = "RA inexistente ou senha DAC inválida.<br><br>Se esqueceu sua senha da DAC procure a DAC (Diretoria AcadÃªmica)";
                    }
                } else {
                    $erro = "Problema na conexÃ£o com o servidor LDAP da Unicamp. <br>Por favor, comunique esse problema aos administradores de redes do IA (Seï¿½ï¿½o de Apoio ï¿½ InformÃ¡tica)";
                }  
            } else {
                $erro = "Senha deve conter letras e nÃºmeros";
            }
        } else {
            $erro = "Nova senha nÃ£o conferem";
        }
    } else {
        $erro = "Por favor, preencha todos os campos";
    }
}

function validaSenha($senha) {
    if ((preg_match('/[0-9]+/', $senha)) && (preg_match('/[a-zA-Z]+/', $senha))) {
        return true;
    } else {
        return false;
    }
}

if ($op=="AT" && $passo==2) {
    /* Verifica se existe usuario com aquela senha digitada */
    $ldap_usuario = addslashes($usuario);// Contra sql injection
    $ldap_senha = addslashes($senha);// Contra sql injection
    $ldaprdn = "uid=" . $ldap_usuario. ",ou=people,dc=unicamp,dc=br";
    $ldapconn = ldap_connect("ldaps://ldap1.unicamp.br ldaps://ldap1.unicamp.br") or die("Servidor nao pode ser encontrado");
    $tt = ldap_start_tls($ldapconn);
    if ($ldapconn) {
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldapbind = ldap_bind($ldapconn, $ldaprdn,$ldap_senha);
        if ($ldapbind) { 
            $basedn = "ou=people,dc=unicamp,dc=br";
            $filtro = "(uid=" . $ldap_usuario . ")";
            $justthese = array("ou", "sn", "givenName", "employeeNumber");
            $sr = ldap_search($ldapconn, $basedn, $filtro);
            $info_ldap_user = ldap_get_entries($ldapconn, $sr);
            if ($info_ldap_user["count"] > 0) { 
                $username = $info_ldap_user[0]['uid'][0];
                $nome = utf8_decode($info_ldap_user[0]['cn'][0]);
                $nome = ucwords(strtolower($nome));
                $shadowflag = $info_ldap_user[0]['shadowflag'][0];
                $tipo_1 = $info_ldap_user[0]['employeetype'][0];
                $tipo_2 = $info_ldap_user[0]['employeetype'][1];
                $departamento = $info_ldap_user[0]['departmentnumber'][0];
                $ra = $info_ldap_user[0]['employeenumber'][0];
                $unidade = $info_ldap_user[0]['ou'][0];
                
                if ($unidade <> 'ia' && $unidade <> 'fec') {
                    if (!$excetos[$ldap_usuario]) { 
                        $erro = "Usuário nÃ£o autorizado por nÃ£o pertencer ao quadro discente do IA. Procure o suporte da Diretoria TÃ©cnica de InformÃ¡tica do IA.";
                    }
                }
                
                if ($username && strlen($username) > 0 && $nome && $departamento && strlen($departamento) > 0 && $shadowflag && strlen($shadowflag) > 0 && $ra && strlen($ra) > 0  && $unidade && strlen($unidade) > 0 && !$erro ) { // && $tipo_1=="Aluno UNICAMP" 
                    if ($shadowflag == "2") 
                        $erro = "Senha expirada. Verifique o procedimento de alteraï¿½ï¿½o de senha no site da DAC.";
                    if ($shadowflag == "3") 
                        $erro = "Usuário inativo no sistema DAC. Verifique o problema diretamente na DAC.";
                    $curso_depto = $ArrAreas[$departamento][0];
                    
                    /*if ($curso_depto == "IA" && !$erro) {
                        $erro = "Aluno de pï¿½s-graduaï¿½ï¿½o jï¿½ possui Login ativado. Em caso negativo, procurar o suporte da Diretoria TÃ©cnica de InformÃ¡tica para ativaÃ§Ã£o do seu Login.";
                        $parar = true;
                    }*/
                    
                    if ((!$curso_depto || strlen($curso_depto)==0) && !$erro) 
                        $erro = "NÃ£o encontrado o curso ao qual o usuário estÃ¡ matriculado no IA. Procure o suporte da Diretoria TÃ©cnica de InformÃ¡tica do IA.";
                    if (!$erro) {
                        $codigo_curso = $ArrCursos[$departamento][0];
                        $nome_curso = $ArrCursos[$departamento][1];
                        $aluno_tipo = "Discente";
                        if ($curso_depto == "ARQ") {
                            $curso_depto = "DAP";
                        }
                        session_start(); /* A session_start deve estar antes de qualquer codigo senao dara erro !!  e ele pode ter qualquer nome  */

                        $letra = substr($nome,0,1);
                        $letra = ereg_replace("[ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½]","a",$nome);
                        $letra = ereg_replace("[ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½]","e",$nome);
                        $letra = ereg_replace("[ï¿½ï¿½ï¿½ï¿½]","i",$nome);
                        $letra = ereg_replace("[ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½]","o",$nome);
                        $letra = ereg_replace("[ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½]","u",$nome);
                        $letra = str_replace("ï¿½ï¿½","c",$nome);
                        $letra = strtolower($letra);
                        //$login=$letra.$username;
                        $login = substr($letra,0,1).$username;
                        $email = $login . "@dac.unicamp.br";
                        $Adata = date("Y-m-d H:i:s");
                        $CA = md5($Adata);
                        $senha_md5 = md5($ldap_senha);
                        $senha_hexa = CharacterToHexadecimal($ldap_senha);
                        db_connect() or die("N&atilde;o consigo me conectar ao Servidor MYSQL!");
                        
                        //De posse do login, procura no siscorp para ver se o mesmo jï¿½ estÃ¡ cadastrado
                        $usuarioID = 0;
                        $SQL = "select * from dbcorp.usuarios where login='$login'";
                        $res = mysql_query($SQL);
                        if ($linha = mysql_fetch_array($res)) {
                            $usuarioID = $linha['usuarioID'];
                        }
                        $eM1 = "<html><body>";
                        $eM2 = "</body></html>";
                        if ($usuarioID <> 0) {
                            
                            //Se era participante externo e mudou para o IA
                            $msgParcDiscente = "";
                            $SQL = "SELECT count(usuarioID) as cont FROM dbcorp.usuarios_tipo WHERE tipo = 'Discente' and usuarioID=$usuarioID";
                            $res = mysql_query($SQL) or die(mysql_error());
                            $lin = mysql_fetch_array($res);
                            //echo $SQL;
                            if ($lin['cont'] == '0') {
                                $SQL = "UPDATE dbcorp.usuarios_tipo SET tipo='Discente', depto='$curso_depto', atualizado_por=1210, atualizado_em='$Adata' where usuarioID=$usuarioID";
                                $result = mysql_query($SQL) or die(mysql_error());
                                $SQL = "update dbcorp.usuarios set contador_replicado=0, atualizado_por=1210, atualizado_em='$Adata' where usuarioID=$usuarioID";
                                $result = mysql_query($SQL) or die(mysql_error());
                                $msgParcDiscente = "<br><b>Login alterado para Discente</b><br>";
                            }
                            
                            //Se o aluno for de pï¿½s e o mesmo estiver cadastrado e sem senha
                            $SQL = "select u.senha, p.sigla from dbcorp.usuarios u left join dbcorp.usuarios_lotacao p on u.usuarioID = p.usuarioID where u.usuarioID=$usuarioID";
                            $res = mysql_query($SQL) or die(mysql_error());
                            $lin = mysql_fetch_array($res);
                            if (($lin['senha'] == '') && (strtoupper($lin['sigla']) == 'CPG')) {
                                $SQL = "update dbcorp.usuarios set situacao_acesso='Habilitado',situacao_conta='Desbloqueado',ativo='Sim',situacao_institucional='Ativo',contador_replicado=0,senha='$senha_md5',senha2='$senha_hexa',cod_ativacao='$CA',evento_replicar='AtualizarSenha'";
                                $SQL .= ",atualizado_por=1210,atualizado_em='$Adata' where usuarioID=$usuarioID";
                                mysql_query($SQL) or die(mysql_error());
                                $sucesso = "Caro(a) $nome,<br>Seu login <b> $login foi cadastrado em nosso sistema com sucesso e foi registrado com a mesma senha da DAC. 
<br>Utilize este Login para acessar os serviÃ§os disponibilizados pelo IA: acesso aos computadores do LaboratÃ³rio de InformÃ¡tica e LaboratÃ³rio VirgÃ­lio Noya, sistema de eletivas e reservas de salas e equipamentos.</b>";
                                
                                $op = 'AS';
                                $mensagem = "AtivaÃ§Ã£o de Login:<br><b>Registro existente</b><br>Login " . $login . "<br>Nome: $nome<br>RA: $ra<br>Curso: $codigo_curso - $curso_depto - $nome_curso";
                                $mensagem = $mensagem . $msgParcDiscente;
                                envia_email("AtivaÃ§Ã£o de Login ï¿½nico", $eM1. $mensagem. $eM2, "intra.ia@iar.unicamp.br"); 
                                
                                $msgEmail = $sucesso . '<br><br><b>VocÃª declarou que leu e estÃ¡ ciente do termo de aceite que contÃ©m as normas de acesso aos serviÃ§os de InformÃ¡tica, replicado abaixo:</b>'; 
                                $msgEmail = $msgEmail . '<br><br><a href="https://www.iar.unicamp.br/lab/informatica/aceite.php" target="_blank">Termo de Aceite</a>';
                                //Enviando email ao discente
                                envia_email("AtivaÃ§Ã£o de login nos sistemas do IA", $eM1 . $msgEmail . $eM2, $email); 
                                //envia_email("AtivaÃ§Ã£o de login nos sistemas do IA", $eM1 . $msgEmail . $eM2, 'marcelo.silva@iar.unicamp.br'); 
                            } else {
                                if (($linha['situacao_acesso'] != 'Habilitado') || ($linha['situacao_conta'] != 'Desbloqueado') || ($linha['ativo'] != 'Sim') || ($linha['situacao_institucional'] != 'Ativo')){
                                    $SQL = "update dbcorp.usuarios set situacao_acesso='Habilitado',situacao_conta='Desbloqueado',ativo='Sim',situacao_institucional='Ativo',contador_replicado=0";
                                    $SQL .= ",atualizado_por=1210,atualizado_em='$Adata' where usuarioID=$usuarioID";
                                    mysql_query($SQL) or die(mysql_error());
                                    $op = 'AS';
                                    $mensagem = "AtivaÃ§Ã£o de Login:<br><b>Registro existente</b><br>Login " . $login . "<br>Nome: $nome<br>RA: $ra<br>Curso: $codigo_curso - $curso_depto - $nome_curso";
                                    $mensagem = $mensagem . $msgParcDiscente;
                                    envia_email("AtivaÃ§Ã£o de Login ï¿½nico", $eM1. $mensagem. $eM2, "intra.ia@iar.unicamp.br");
                                    
                                    $sucesso = "Caro(a) $nome,<br>Seu login <b>" . $login . " </b> foi reativado com sucesso";
                                    $msgEmail = $sucesso . '<br><br><b>VocÃª declarou que leu e estÃ¡ ciente do termo de aceite que contÃ©m as normas de acesso aos serviÃ§os de InformÃ¡tica, replicado abaixo:</b>'; 
                                    $msgEmail = $msgEmail . '<br><br><a href="https://www.iar.unicamp.br/lab/informatica/aceite.php" target="_blank">Termo de Aceite</a>';
                                    //Enviando email ao dicente
                                    envia_email("ReativaÃ§Ã£o de login nos sistemas do IA", $eM1 . $msgEmail . $eM2, $email); 
                                } else {
                                    $sucesso = "Seu login para os serviÃ§os de TIC do IA ï¿½ <b>" . $login . " </b>";
                                    $op = 'AS';
                                }
                            }
                        } else {
                            // Inserir o aluno no Login Unico
                            $SQL = "INSERT INTO dbcorp.usuarios (nome, login, cod_ativacao, situacao_acesso, situacao_conta, evento_replicar, contador_replicado, situacao_institucional, ativo, inserido_em, senha,senha2,inserido_por)";   
                            $SQL .= "VALUES ('$nome','$login','$CA','Habilitado','Desbloqueado','Inserir', '0', 'Ativo', 'Sim','$Adata','$senha_md5','$senha_hexa',1210)";
                            mysql_query($SQL);
                            $usuarioID = mysql_insert_id(); // Obtem  o valor do campo UsuarioID
                            $result = mysql_query("select * from dbcorp.usuarios  where usuarioID = ".$usuarioID);
                            $QC = mysql_fetch_array($result); // Verifica se ele foi mesmo inserido no BD
                            if ($QC) { // Se sim, completa as demais tabelas
                                $SQL = "INSERT INTO dbcorp.usuarios_contatos (usuarioID, tipo_contato, detalhamento, principal, atualizado_em, atualizado_por, inserido_em, inserido_por)
                                        VALUES($usuarioID,'E-mail','$email','Sim','$Adata',1210,'$Adata',1210)";
                                
                                $result = mysql_query($SQL) or die(mysql_error());
                                $SQL = "INSERT INTO dbcorp.usuarios_documentos (usuarioID, documento_tipo, documento_numero, possui_copia, ativo, inserido_por, inserido_em, atualizado_por, atualizado_em)
                                        VALUES ($usuarioID, 'RA','$ra', 'Nao', 'Sim', 1210,'$Adata', 1210,'$Adata')";   
                                
                                $result = mysql_query($SQL) or die(mysql_error());
                                $SQL = "INSERT INTO dbcorp.usuarios_tipo (usuarioID, tipo, depto, data_inicio, data_termino, inserido_em, inserido_por, atualizado_em)
                                        VALUES ($usuarioID,'Discente','$curso_depto',  NULL, NULL, '$Adata', 1210, '$Adata')";
                                
                                $result = mysql_query($SQL) or die(mysql_error());
                                $cursoID = '0';
                                
                                //Obtem a chave do curso em cursos+unbicamp
                                $SQL = "select * from dbcorp.cursos_unicamp where codigo_curso='$codigo_curso'";
                                $Rs = mysql_query($SQL);
                                if ($ln = mysql_fetch_array($Rs)) {
                                    $cursoID = $ln['cursoID'];
                                    $nivel_curso = $ln['nivel_curso'];
                                    $SQL = "INSERT INTO dbcorp.alunos_cursos (cursoFK,curso,nivel,usuarioID,ativo,inserido_em, inserido_por)
                                            VALUES ($cursoID,$codigo_curso,'$nivel_curso',$usuarioID,'Sim','$Adata', 1210)";
                                    $result = mysql_query($SQL) or die(mysql_error());
                                }
                            }
                            $mensagem = "AtivaÃ§Ã£o de Login:<br><b>Novo registro</b><br>Login " . $login . "<br>Nome: $nome<br>RA: $ra<br>Curso: $codigo_curso - $curso_depto - $nome_curso";
                            
                            if (!$erro) {
                                $login_ativado = true;
                                envia_email("AtivaÃ§Ã£o de Login ï¿½nico", $eM1. $mensagem. $eM2, "intra.ia@iar.unicamp.br"); 
                                $sucesso = "Caro(a) $nome,<br>Seu login <b> $login foi cadastrado em nosso sistema com sucesso e foi registrado com a mesma senha da DAC. <br>Utilize este Login para acessar os serviÃ§os disponibilizados pelo IA: acesso aos computadores do LaboratÃ³rio de InformÃ¡tica e LaboratÃ³rio VirgÃ­lio Noya, sistema de eletivas e reservas de salas e equipamentos.</b>";
                                $op = 'AS';
                                $msgEmail = $sucesso . '<br><br><b>VocÃª declarou que leu e estÃ¡ ciente do termo de aceite que contÃ©m as normas de acesso aos serviÃ§os de InformÃ¡tica, replicado abaixo:</b>'; 
                                $msgEmail = $msgEmail . '<br><br><a href="https://www.iar.unicamp.br/lab/informatica/aceite.php" target="_blank">Termo de Aceite</a>';
                                //Enviando email ao dicente
                                envia_email("AtivaÃ§Ã£o de login nos sistemas do IA", $eM1 . $msgEmail . $eM2, $email); 
                            }  
                        }
                        $parar = true;
                        if ($op != 'AS') {
                            $op = 0;
                        }
                    }
                } else {
                    $parar = true;
                    $op = 0;
                }
            } else {
                $erro = "Usuário (RA) nÃ£o encontrado na base de dados da DAC. Digite o RA correto ou procure a DAC (Diretoria AcadÃªmica) para verificar o problema.</b>";
            }
        } else {
           $erro = "RA inexistente ou senha inválida.<br><br>Se esqueceu sua senha da DAC procure a DAC (Diretoria AcadÃªmica)";
        }
    } else {
        $erro = "Problema na conexÃ£o com o servidor LDAP da Unicamp. <br>Por favor, comunique esse problema aos administradores de redes do IA (SeÃ§Ã£o de Apoio Ã  InformÃ¡tica)";
    }     
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html lang="pt-BR">
    <head>
        <script language="JavaScript">
        var alfaMin = /^[a-z]$/;
        var numm = /^d$/;

        function putFocus(formInst, elementInst) {
            if (document.forms.length > 0) {
                document.forms[formInst].elements[elementInst].focus();
            }
        }
        function checa_usuario() {
            var u = document.form.usuario.value; 
            var s = document.form.senha.value; 
            if (u == "" || s == "") { 
                alert("Por favor, informe corretamente suas credenciais.");
                return(false);
            }
            /*var letra = u.substring(0,1);
            var resto = u.substring(1);
            if (!alfaMin.test(letra) || resto.length != 6) {
                alert("Login fora do padrï¿½o. Digite como login a primeira letra do seu nome seguinda pelo RA.");
                document.form.usuario.focus();
                return(false);
            }

            if (isNaN(resto)) {
               alert("Login fora do padrï¿½o. Digite como login a primeira letra do seu nome seguida pelo RA.");
               return(false);
            }

            if (s.length < 8 ) {
                alert("Senha fora do padrï¿½o, deve conter ao menos 8 caracteres.")
                document.form.senha.focus();
                return(false);
            }*/
            document.form.op.value = "AU";
            document.form.submit();            
        }

        function ativa_usuario(passo) {
            if (passo=='1') {
                if(confirm('Este procedimento sÃ³ deve ser executado se vocÃª for aluno regular de graduaï¿½ï¿½o ou pï¿½s graduaï¿½ï¿½o do IA. Para cadastrar login e senha para os serviÃ§os de TIC do IA (acesso aos computadores do LaboratÃ³rio de InformÃ¡tica e LaboratÃ³rio VirgÃ­lio Noya, sistema de eletivas, reserva de salas e equipamentos) serï¿½ necessï¿½rio fornecer suas credenciais da DAC/SISE.') == false) {
                    return(false);
                }
            }
            if (passo != "3") {
                if (passo == "2") {
                    var u = document.form.usuario.value; 
                    var s = document.form.senha.value; 
                    if (u == "" || s == "") { 
                        alert("Por favor, informe corretamente suas credenciais.");
                        return(false);
                    }
                    if (isNaN(u) || u.length != 6) {
                       alert("RA fora do padrï¿½o. Digite corretamente seu RA com 6 digitos nï¿½mericos.");
                       return(false);
                    }
                    /*if (s.length != 8 ) {
                       alert("Senha fora do padrï¿½o, deve conter 8 caracteres.")
                       document.form.senha.focus();
                       return (false);
                    }*/
                }
                document.form.op.value = "AT";
                document.form.passo.value = passo;
                document.form.submit();
            } else {
                window.location.href = "/";
            }
            
            if (passo == "4") {
                document.form.op.value = "AT";
                document.form.passo.value = "0";
                document.form.submit();
            }
            
        }
        
        function ativa_ok(){
            if (document.getElementById("chkConcordo").checked == true){
                document.getElementById("login-button").disabled = false;
            } else {
                document.getElementById("login-button").disabled = true;
            }
        }
        
        function checa_nova_senha(){
            var s0 = document.form.senha0.value; 
            var s1 = document.form.senha1.value; 
            var s2 = document.form.senha2.value; 
            if (s0 === "") {
                alert("Preencha o campo senha atual");
                return(false);
            }
            /*if (s0.length !== 8) {
                alert("Senha atual fora do padrï¿½o, deve conter 8 caracteres");
                return(false);
            }*/
            if (s1 === "") {
                alert("Preencha o campo 'Digite sua nova senha'");
                return(false);
            }
            /*if (s1.length !== 8) {
                alert("Nova senha fora do padrï¿½o, deve conter 8 caracteres");
                return(false);
            }*/
            if (s2 === "") {
                alert("Preencha o campo 'Digite sua nova senha'");
                return(false);
            }
            /*if (s2.length !== 8) {
                alert("Confirmaï¿½ï¿½o de nova senha do padrï¿½o, deve conter 8 caracteres");
                return(false);
            }*/
            if (s1 !== s2) {
                alert("Nova senha nÃ£o conferem");
                return(false);
            }
            document.form.passo.value = "5";
            document.form.submit();
        }
        </script>

        <link href="../favicon.ico" rel="SHORTCUT ICON" />
        <link href="./css/screend.css" rel="stylesheet" type="text/css" />
        <link href="css_a/screen.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="prototype.js"></script>
        <script type="text/javascript" src="accesskeys.js"></script>
        <title>SERVIÃOS IA</title>
    </head>

    <body class="modal-form" onLoad="putFocus(0,0);">
        <div class="modal-form">
            <div>
                <table> 
                    <tr>
                        <td> 
                            <img src="base/imagens/marcaIA_oficial.jpg" width="130" heigth="80">
                        </td>
                        <td>
                            <h2><font color="purple">ServiÃ§os</h2> 
                            <table width="100%">
                                <tr>
                                    <td align="center"><br></td>
                                </tr>
                                <td>
                                    <center><font color="blue" size="2">Cadastro de usuários para os serviÃ§os de informÃ¡tica do IA</font></center>
                                </td>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            
            <form name="form" id="form" method="post" action="autentica_lu.php">
            <? if($sist) {?>
                <input type="hidden" name="sist" value="<?print $sist;?>">
                <input type="hidden" name="pagina" value="<?print $pagina;?>">
                <input type="hidden" name="parametros" value="<?print $parametros;?>">
                <input type="hidden" name="op" value="<?echo $op;?>">
                <input type="hidden" name="passo" value="<?echo $passo;?>">

            <? }
            if($erro){ /* Se existir algum erro ele entra neste if e mostra o erro antes de mostrar o formulario novamente */
                    echo "<div><b><font size=4 color=red>". $erro . "</font></div></b><br>";
            }
            if($sucesso){ /* Se foi cadastrado com sucesso entao mostra a mensagem */
                    echo "<div><b><font size=4 color=blue>". $sucesso . "</font></div></b><br>";
            }
            if ($parar) { ?>
                <input type="hidden" name="usuario" value="<?print $usuario;?>"> <!--Para recuperar o RA que usuário digitou-->
                <? if($sucesso) {
                ?>
                    <input type="hidden" name="login" value="<?print $login;?>"> <!--Para recuperar o login do usuário-->
                    <div>&nbsp;</div>
                    <?php if (!$ocultarClique) { ?>
                        <font size="3"><b>Para (re)definir sua senha, <a href="#" onClick="document.getElementById('form').submit();">clique aqui</a>.</b></font>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                    <?php } ?>
                    <input name="ok" class="button submit-button" type="button" value="Cancelar" onclick="ativa_usuario('3');"/>
                <?} else if($erro) { ?>
                    <input name="retornar" class="button submit-button" value="Sair" type="button" onclick="ativa_usuario('4');"/>
                <?}
                exit;
            }
            if (!$op || $op=="AU") { 
                if ($sist === "login"){ //Entra para ativar o login de acesso
                ?>
                    <script>
                    if (ativa_usuario('1') === false){
                        window.location.href = "/";
                    }    
                    </script>
                <?
                    exit;
                }
                ?>
                <div>
                    <label for="lbl_user">Digite seu Login no IA</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="./imagens/help.png" alt="" title="Sua identificaï¿½ï¿½o (primeira letra do seu nome seguido pelo RA), utilizada para acessar os serviÃ§os disponï¿½veis no IA: computadores do LaboratÃ³rio de InformÃ¡tica e  LaboratÃ³rio VirgÃ­lio Noya, rede sem fio, sistema de eletivas, reserva de salas e equipamentos. Caso nÃ£o tenha ativado seu login, clique no link abaixo para ativÃ¡-lo." onclick="alert('Sua identificaï¿½ï¿½o (primeira letra do seu nome seguido pelo RA), utilizada para acessar os serviÃ§os disponï¿½veis no IA: computadores do LaboratÃ³rio de InformÃ¡tica e LaboratÃ³rio VirgÃ­lio Noya, rede sem fio, sistema de eletivas, reserva de salas e equipamentos. Caso nÃ£o tenha ativado seu login, clique no link abaixo para ativÃ¡-lo.');">
                </div>
                <div>
                    <input type="text" class="login" autocapitalize="off" autocorrect="off"  id="intra_user" name="usuario" value="" size="20" style="direction:ltr" />  
                    &nbsp;
                    <span style="font-size:80%;color:#333;font-weight:bold"></span>
                </div>

                <div>
                    <label for="lbl_pass">Digite sua Senha</label>
                </div>
                <div>
                    <input type="password" class="login" id="intra_pass" name="senha" value="" style="direction:ltr" />
                </div>
                <table width=100%>
                    <tr>
                        <td>
                            <div>
                                <input  type="button" name="submeter" class="button submit-button" value="Acessar" onclick="checa_usuario();">
                                <!--<br>
                                <?//if ($_SERVER['REMOTE_ADDR']=="143.106.55.33"){?>
                                <center><a href="#" onclick="ativa_usuario('1');">Ativar login de acesso</a></center>
                                <?//}?>-->
                            </div>
                        </td>
                    </tr>
                </table>
            <?}?>
            
            <? if ($op=="AT") {?>
                <div>&nbsp;</div>
                <div>
                    <label for="lbl_user">Digite seu RA</label>
                </div>
                <div>
                    <input type="text" class="login" autocapitalize="off" autocorrect="off"  id="intra_user" name="usuario" value="<?=$usuario?>" size="6" maxlength="6" style="direction:ltr" />  
                </div>
                <div>
                    <label for="lbl_pass">Senha (DAC)</label>
                </div>
                <div>
                    <input type="password" class="login" id="intra_pass" name="senha" value="" maxlength="255" style="direction:ltr" />
                </div>
                <div>&nbsp;</div>
                <div>
                    <input id="chkConcordo" name="chkConcordo" value="" type="checkbox" style="width:20px;" onclick="ativa_ok();">
                    <font size="2"> Declaro que li e estou ciente do <a href="https://<?=$_SERVER['SERVER_NAME']?>/lab/informatica/aceite.php" target="_blank">termo de aceite</a> que contÃ©m as normas de acesso aos serviÃ§os de InformÃ¡tica</font>
                <div>&nbsp;</div>
                <table width="100%">
                    <tr>
                        <td align="center">
                            <input id="login-button" name="ativar" class="button submit-button" value="    OK    " type="button" disabled onclick="ativa_usuario('2');"/>
                        </td>
                        <td align="center">
                            <input  name="cancelar" class="button submit-button" value="Cancelar" type="button" onclick="ativa_usuario('3');"/>
                        </td>
                    </tr>
                </table>
            <?}?>
            <? if ($op=="AS") {?>
                <input type="hidden" name="usuario" value="<?print $usuario;?>">
                <input type="hidden" name="login" value="<?print $login;?>"> <!--Para recuperar o login do usuário-->
                <div>
                    <font size=2 color=green>A (re)definiï¿½ï¿½o desta senha nÃ£o altera a senha nos sistemas da DAC, somente dos serviÃ§os de TIC do IA.<br>A senha deve conter no mï¿½nimo oito caracteres, entre letras e nï¿½meros</font>
                </div>
                <div>&nbsp;</div>
                <div>
                    <label for="lbl_pass">Digite sua senha da DAC:</label>
                </div>
                <div>
                    <input type="password" class="login" id="intra_pass1" name="senha0" value="<?=$_POST['senha0']?>" maxlength="255" style="direction:ltr" />
                </div>
                <div>&nbsp;</div>
                <div>
                    <label for="lbl_pass">Digite sua nova senha para acesso aos sistemas do IA:</label>
                </div>
                <div>
                    <input type="password" class="login" id="intra_pass1" name="senha1" value="<?=$_POST['senha1']?>" maxlength="255" style="direction:ltr" />
                </div>
                <div>&nbsp;</div>
                <div>
                    <label for="lbl_pass">Confirme sua nova senha para acesso aos sistemas do IA:</label>
                </div>
                <div>
                    <input type="password" class="login" id="intra_pass2" name="senha2" value="<?=$_POST['senha2']?>" maxlength="255" style="direction:ltr" />
                </div>
                <div>&nbsp;</div>
                <table width=100%>
                    <tr>
                        <td>
                            <div>
                                <input type="button" name="submeter" class="button submit-button" value="Confirmar" onclick="checa_nova_senha();">
                            </div>
                        </td>
                        <td>
                            <div>
                                <input name="retornar" class="button submit-button" value="Cancelar" type="button" onclick="ativa_usuario('4');"/>
                            </div>
                        </td>
                    </tr>
                </table>
            <?}?>

            </form>
        </div>
    </body>
</html>

<html><head></head><body></body></html>
