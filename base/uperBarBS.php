<?
// reponsividade
$total_paginas = $total_linhas/$num_linhas;
$total_paginas = ceil($total_paginas);
?>

<SCRIPT LANGUAGE="JavaScript">
    function recarrega(select) {
        document.location = '<? echo $_SERVER['PHP_SELF']; ?>?&num_linhas=' + select.value + '&pagina=1&busca=<?print $busca.$parametros;?>&order=<? print $order;?>';
    }
</script>

<table class="table">
    <tbody>
    <tr>
        <td align="left">
            <font size="1" face="Verdana, Arial, Helvetica, sans-serif">
                p&aacute;gina <?echo $pagina;?> de <?echo $total_paginas;?>
            </font>
        </td>
        <td align="right">
            <? if($busca) {?>
                <font size="1" align="left" face="Verdana, Arial, Helvetica, sans-serif">
                    <a href="<? echo $_SERVER['PHP_SELF'] ?>?order=<?print $order?>&busca=busca&num_linhas=<?print $num_linhas;?>&pagina=1<?print $parametros;?>">
                        Exibir todos
                    </a>
                </font>
            <? } ?>
            <font size="1" face="Verdana, Arial, Helvetica, sans-serif">
                linhas por p&aacute;gina
                <select name="linhas2" id="linhas2" onChange="recarrega(this);">
                    <option value="10" <? if($num_linhas == 10) {echo "selected";}?>>10</option>
                    <option value="20" <? if($num_linhas == 20) {echo "selected";}?>>20</option>
                    <option value="25" <? if($num_linhas == 25) {echo "selected";}?>>25</option>
                    <option value="50" <? if($num_linhas == 50) {echo "selected";}?>>50</option>
                    <option value="100" <? if($num_linhas == 100) {echo "selected";}?>>100</option>
                    <option value="1000" <? if($num_linhas == "1000") {echo "selected";}?>>1000</option>
                </select>
            </font>
        </td>
    </tr>
    </tbody>
</table>
