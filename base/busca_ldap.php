<?php
function busca_ldap($ldap_user) {
    //cursoCSolicitante=codigo do departamento/curso 
    //cursoDsolicitante=descricao do departamento/cusro
    global $nomeSolicitante,$emailSolicitante,$cursoCSolicitante,$cursoDSolicitante;
    include("../ldap/codigos_areas");
    $ldapconn = ldap_connect("ldaps://ldap1.unicamp.br ldaps://ldap1.unicamp.br") or die("Servidor nao pode ser encontrado");
    //$tt=ldap_start_tls($ldapconn);
    $bu=false;
    if ($ldapconn) {
        // ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 2) or die("Erro LDAP_SET");
        // Faz  BIND anonimo com o servidor LDAP
        $ldapbind = ldap_bind($ldapconn);
        if ($ldapbind) {
            $basedn="ou=people,dc=unicamp,dc=br";
            //$filtro="(&(uid=" . $ldap_usuario . ")(ou=ia))";
            $filtro="(uid=" . $ldap_user . ")";
            $justthese = array("ou", "sn", "givenName", "employeeNumber");
            //$sr=ldap_search($ldapconn, $basedn, $filtro,$justthese);
            $sr=ldap_search($ldapconn, $basedn, $filtro);
            $info_ldap_user = ldap_get_entries($ldapconn, $sr);
            if ($info_ldap_user["count"] > 0) {
                    $username=$info_ldap_user[0]['uid'][0];
                    $nome_ldap=$info_ldap_user[0]['cn'][0];
                    $nomeSolicitante=utf8_decode($nome_ldap);
                    $departamento=$info_ldap_user[0]['departmentnumber'][0];
                    $letra = substr($nomeSolicitante,0,1);
                    $letra = ereg_replace("[ÁÀÂÃáàâãª]","a",$nomeSolicitante);
                    $letra = ereg_replace("[ÉÈÊéèê]","e",$nomeSolicitante);
                    $letra = ereg_replace("[Íí]","i",$nomeSolicitante);
                    $letra = ereg_replace("[ÓÒÔÕóòôõº]","o",$nomeSolicitante);
                    $letra = ereg_replace("[ÚÙÛúùû]","u",$nomeSolicitante);
                    $letra = str_replace("Çç","c",$nomeSolicitante);
                    $letra = strtolower($letra);
                    $emailSolicitante=substr($letra,0,1).$ldap_user . "@dac.unicamp.br";
                    $cursoCSolicitante = $ArrAreas[$departamento][0];
                    $cursoDSolicitante = $ArrAreas[$departamento][1];
                    //echo "<br>". print_r($info_ldap_user);
                    $bu=true;
            }
        } 
    }
    return($bu);
}
?>
