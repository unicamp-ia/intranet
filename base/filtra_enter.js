var inputs = document.getElementsByTagName('input');
var buttons = Array();
var btngo = null;
var reFiltrar = /^localizar|filtrar|enviar$/;

for (var i=0; i<inputs.length; i++) {
	if (inputs[i].type.toLowerCase() == 'button') {
		buttons[buttons.length] = inputs[i];
	}
}
for (var i=0; i<buttons.length; i++) {
	if (buttons[i].value.toLowerCase().match(reFiltrar)) {
		if (buttons[i].onclick) {
			btngo = buttons[i];
		}
	}
}
//Verifica se achou o botao de localizar
if (btngo != null) {
	for (var i=0; i<inputs.length; i++) {
		if (inputs[i].type.toLowerCase() == 'text') {
			inputs[i].onkeypress = newkeypress;
		}
	}
}


function newkeypress(e) { //unixpapa.com/js/testkey.html
	if (!e) {
		e = event;
	}
	var code = (e.keyCode? e.keyCode:
					(e.which? e.which:
						(e.charCode? e.charCode: null)
					)
				);
	if (code == 13) {
		btngo.onclick();
	} else {
		return true;
	}
	
}
