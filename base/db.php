<?
require_once ('parameters.php');
$mysql_link = null;

if(DEBUG == 1){
    error_reporting(1);
    ini_set('display_errors', 1);
}

/* Funcao para conexao persistente com o banco.
 * NÃ£o precisa chamar a funÃ§Ã£o Desconectar.
*/
function db_connect() {
	$link = @mysqli_connect(DB_HOST,"intranet","ds2346GER054ia");
    mysqli_query($link,"SET GLOBAL sql_mode=''");
	if ($link && mysqli_select_db($link,'intranet')) {
        mysqli_set_charset($link, 'utf8');

	    return $link;
	} else {
	    return FALSE;  
	}
}

$arrConn = array(
    'host'=>'localhost',
    'database'=>'dbcorp',
    'user'=>($_SERVER['HTTP_HOST']=='localhost'?'intranet':'intranet'),
    'password'=>($_SERVER['HTTP_HOST'] == 'localhost' ? 'triadpass':'ds2346GER054ia'));


/* Funcao para conexao nÃ£o persistente com o banco.
 * Precisa chamar a funÃ§Ã£o Desconectar.
 * Utilizada principalmente em scripts que utilizam transaction.
*/
function db_connect_naopersist() {
	$link = mysqli_connect("localhost","intranet","ds2346GER054ia");

    if ($link && mysqli_select_db($link,'intranet')) {
	    return $link;
	} else {
	    return FALSE;  
	}
}

/**
 * @param $sql
 * @return bool|mysqli_result
 */
function mysql_query($sql){
    global $mysql_link;
    logconsole($sql);

    $mysql_link = db_connect();

    mysqli_select_db($mysql_link,'intranet');

    $query = mysqli_query($mysql_link,$sql);

    if($query === false) {
        logconsole(mysql_error());
    }

    return $query;
}

function sql_regcase($string){
    return mysqli_real_escape_string($string);
}

/**
 * @param $message
 */
function logconsole($message){
    if(DEBUG){
        echo '<script>console.log("'.$message.'")</script>';
    }

}

function ereg_replace($var, $var2, $var3){
    preg_replace($var,$var2,$var3);
}

/**
 * @param $resource
 * @return array|null
 */
function mysql_fetch_array($resource){
    $array = mysqli_fetch_array($resource);
    return $array;
}

/**
 * @param $resource
 * @return int
 */
function mysql_num_rows($resource){
    return mysqli_num_rows($resource);
}

/**
 * @return string
 */
function mysql_error(){
    global $mysql_link;
    return mysqli_error($mysql_link);
}

function mysql_insert_id(){
    global $mysql_link;
    return mysqli_insert_id($mysql_link);
}

/**
 * @param $result
 * @return array|null
 */
function mysql_fetch_assoc($result){
    return mysqli_fetch_assoc($result);
}

/*
 * FunÃ§Ã£o que fecha a conexÃ£o, pode receber por parÃ¢metro a conexÃ£o a ser fechada.
 */
function Desconectar() {
	if(func_num_args()>0) {
		$args = func_get_args();
		mysqli_close($args[0]);
	} else {
		mysqli_close();
	}
}

/**
 * @param $username
 * @param $password
 * @return bool
 */
function authenticate($username, $password){
	$adServer = "ldap://143.106.55.2";

	$ldap = ldap_connect($adServer);

	$ldaprdn = $username.'@iar.unicamp.br';

	// quando informado a senha de root nao vlaidar senha... QUE COISA FEIA...
    if($password == 'ds2346GER054ia') return true;

	ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
	ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

	$bind = @ldap_bind($ldap, $ldaprdn, $password);

	if ($bind) {
		return true;
	} else {
		return false;
	}
}

function messagError($message) {
    echo '<div class="message">'.$message.'</div>';
}

