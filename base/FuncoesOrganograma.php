<?php

function MontaHierarquia($areabase,$area_base,$nivel_acima,$nivel_abaixo,$nivelDet='C') {

    // nivel_abaixo N: nao monta hierarquia das instancias abaixo
    //              T: monta a hierarquia de todas as instancias abaixo, em todos os niveis
    //              P: monta a hierarquia somente do primeiro nivel abaixo
    // nivelDet = C ->completo
    //            S ->Simples
    // Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador
    // Alterado por Edson Giordani em Maio/2020 - Novo organograma ATU para CTUIA

    global $hierarquia;
    $hierarquia=array();

    // recupera codigo do orgao e cria array com o codigo do orgao para posterior verificacao de superior acima
    $SQL="select codigo_orgao from areas where CodArea='$areabase'";
    $res=mysql_query($SQL);
    if (mysql_num_rows($res) > 0) {
        $ln=mysql_fetch_array($res);
        $cd_orgaobase=$ln['codigo_orgao'];
        $parts_cd=explode(".",$cd_orgaobase);
        $np=count($parts_cd);
        $cdh="";
        for ($ii=0;$ii<$np-1;$ii++) {
            if ($ii>0) $cdh .=".";
            $cdh .=$parts_cd[$ii];
        }
    } else {
        die("[FuncoesOrganograma] Erro 0 na montagem da hierarquia. Entre em contato com a DTI.");
    }

    // Monta arvore hierarquica com ATU como aprovador nível acima antes de chegar no Diretor
    addUserToHierarquia($areabase,"sigla","chefia","substituto_chefia");          // inclui chefia imediata

    $array_orgaos_direcao = array("DPROD","APDEPT","BIBLIO","CGRAD","CPG","DIR","DTI","GAL","CATD","COADM");
    $array_sub_orgaos_direcao = array("APP","PROVCOM","APROD","APLABS","GP","FIN","INFRA");
    if (in_array($areabase, $array_sub_orgaos_direcao)) {
        if ($cdh <> "")
            addUserToHierarquia($cdh,"codigo","acima","substituto_acima");        // inclui chefia acima
        addUserToHierarquia("CTUIA","sigla","ATU","substituto_atu");                // inclui atu
        addUserToHierarquia("DIR","sigla","DIR","substituto_dir");                // inclui dir
    } elseif (in_array($areabase, $array_orgaos_direcao)) {
        if ($areabase=="DIR") {
            // ver se precisa consistir diretor ou diretor associado
            if ($_SESSION["tipo_conected"]=="FUNCIONARIO") {
                addUserToHierarquia("CTUIA","sigla","chefia","substituto_chefia");  // substitui chefia imediata por atu
                addUserToHierarquia("DIR","sigla","acima","substituto_acima");    // substitui chefia acima por dir
            }
        } else {
            addUserToHierarquia("CTUIA","sigla","acima","substituto_acima");        // inclui atu como chefia acima
            addUserToHierarquia("DIR","sigla","DIR","substituto_dir");            // inclui dir
        }
    } else { // demais orgaos do IA (departamentos - funcionários e docentes)
        addUserToHierarquia("DIR","sigla","acima","substituto_acima");            // inclui dir como chefia acima
    }

    // Inclui niveis abaixo na arvore hierarquica
    if ($nivel_abaixo=='P' || $nivel_abaixo=='T') {
        $ll=strlen($cd_orgaobase);
        if ($nivel_abaixo=='T') {
            $SQL="SELECT aa.*,uu.nome,uu.email from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser where  SUBSTRING(aa.codigo_orgao,1," . $ll . ")='" . $cd_orgaobase . "' and aa.codigo_orgao <> '$cd_orgaobase' and aa.status='A' and aa.situacao_organograma='A' and aa.origem='I'";
        }else {
            $t=strlen($cd_orgaobase) + 3;
            $SQL="SELECT aa.*,uu.nome,uu.email from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser where LENGTH(aa.codigo_orgao)=$t and aa.codigo_orgao like '$cd_orgaobase%' and aa.status='A' and aa.situacao_organograma='A' and aa.origem='I'";
        }
        $rst=mysql_query($SQL);

        while ($ln1=mysql_fetch_array($rst)) {
            $hierarquia['abaixo'][]=array('area_chefe'=>$ln1['CodArea'],'matr_chefe'=>$ln1['MATR_chefe'],'nome_chefe'=>$ln1['nome'],'email_chefe'=>$ln1['email']);
            // Se o RH indicou o nome de algum substituto temporario para a area de hierarquia acima.
            if ($ln1['MATR_substituto'] <> "000000" && $ln1['prerrogativas']==1  && $nivelDet=='C') {
                $SQL="select nome,email from tblusuarios where coduser= '" . $ln1['MATR_substituto'] . "'";
                $rst1=mysql_query($SQL);
                if ($ln2=mysql_fetch_array($rst1)) {
                    $hierarquia['substituto_abaixo'][]=array('area_chefe'=>$ln2['CodArea'],'matr_chefe'=>$ln2['MATR_chefe'],'nome_chefe'=>$ln2['nome'],'email_chefe'=>$ln2['email']);
                }
            }
        }
    }
    /* echo "novo: ";
    print_r ($hierarquia);
    echo "<BR>"; */
    return("OK");
}

// Adiciona aprovador e respectivo substituto (se indicado na tabela areas) na hierarquia
function addUserToHierarquia($orgao, $tipo, $gestor, $gestor_substituto){
    global $hierarquia;

    $SQL_item = "select u.nome,u.email,a.MATR_chefe, a.CodArea, a.MATR_substituto, a.prerrogativas from areas a " .
        " left join tblusuarios u on a.MATR_chefe=u.coduser ";
    if  ($tipo == "sigla")
        $SQL_item = $SQL_item . "where a.CodArea='$orgao'";
    else
        $SQL_item = $SQL_item . "where a.codigo_orgao='$orgao'";
    $res_item = mysql_query($SQL_item);

    if ($linha = mysql_fetch_array($res_item)) {
        $hierarquia[$gestor]['area']=$orgao;
        $hierarquia[$gestor]['matr']=$linha['MATR_chefe'];
        $hierarquia[$gestor]['nome']=$linha['nome'];
        $hierarquia[$gestor]['email']=$linha['email'];
    } else {
        die("[FuncoesOrganograma] Erro 1 na montagem da hierarquia. Entre em contato com a DTI.");
    }
    // Inclui substituto, se indicado na tabela areas
    if ($linha['MATR_substituto'] <> "000000") {
        $SQL_item_Sub="select nome,email from tblusuarios where coduser= '" . $linha['MATR_substituto'] . "'";
        $res_item_Sub=mysql_query($SQL_item_Sub);

        if ($detalhe = mysql_fetch_array($res_item_Sub)) {
            $hierarquia[$gestor_substituto]['matr'] = $linha['MATR_substituto'];
            $hierarquia[$gestor_substituto]['nome'] = $detalhe['nome'];
            $hierarquia[$gestor_substituto]['email'] = $detalhe['email'];
        } else {
            messagError('[FuncoesOrganograma] Erro 2 na montagem da hierarquia. Entre em contato com a DTI.');
        }
    }
}

// Monta a hierarquia da area para baixo
function MontaHierarquiaSimples($areabase) {
    global $hierarquia;
    $hierarquia=array();
    $SQL="select a.CodArea,a.MATR_chefe,a.MATR_substituto,a.prerrogativas, a.codigo_orgao, u.nome,u.email " .
        " from areas a left join tblusuarios u on a.MATR_chefe=u.coduser where a.origem='I' and a.CodArea='$areabase'";
    $res=mysql_query($SQL);
    if (mysql_num_rows($res) > 0) {
        $ln=mysql_fetch_array($res);
        // Se obter os dados do chefe do depto $areabase
        $hierarquia['chefia']['area']=$areabase;
        $hierarquia['chefia']['matr']=$ln['MATR_chefe'];
        $hierarquia['chefia']['nome']=$ln['nome'];
        $hierarquia['chefia']['email']=$ln['email'];
        $cd_orgaobase=$ln['codigo_orgao'];

        // Se obter os dados do chefe hierarquico acima
        $ll=strlen($cd_orgaobase);
        $SQL="SELECT aa.*,uu.nome,uu.email from areas aa left join tblusuarios uu on aa.MATR_chefe=uu.coduser " .
            " where  SUBSTRING(aa.codigo_orgao,1," . $ll . ")='" . $cd_orgaobase .
            "' and aa.codigo_orgao <> '$cd_orgaobase' and aa.status='A' and aa.origem='I' and aa.situacao_organograma='A'";
        $rst=mysql_query($SQL);
        while ($ln1=mysql_fetch_array($rst)) {
            $hierarquia['abaixo'][]=array('area'=>$ln1['CodArea'],'matr'=>$ln1['MATR_chefe'],'nome'=>$ln1['nome'],'email'=>$ln1['email']);
        }
    }

}
function ExtraiaAreas() {
    global $ArrAreas;
    global $hierarquia;

    if (count($hierarquia) > 0) {
        $ArrAreas=array();
        if  ($hierarquia['chefia']['matr']) {
            $ArrAreas[]= $hierarquia['chefia']['area'];

        }
        if (is_array($hierarquia['abaixo'])) {
            $nh=count($hierarquia['abaixo']);
            for ($lk=0;$lk<$nh;$lk++) {
                $ArrAreas[]=$hierarquia['abaixo'][$lk]['area'];
            }
        }
    }

}
?>
