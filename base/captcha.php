<?php
session_start();
header("Content-type: image/jpeg");
$largura = 150;
$altura = 50;
$tamanho_fonte = 20;
$quantidade_letras = 5;
$palavra = substr(str_shuffle("AaBbCcDdEeFfGgHhJjKkLMmNnPpQqRrSsTtUuVvYyXxWwZz23456789"),0,$quantidade_letras);
$_SESSION["palavra"] = $palavra;
$imagem = imagecreate($largura,$altura);
$preto = imagecolorallocate($imagem,0,0,0);
$branco = imagecolorallocate($imagem,255,255,255);
for($i = 1; $i <= $quantidade_letras; $i++){
    $coordx = $tamanho_fonte*$i;
    $coordy = !($i % 2) ? ($tamanho_fonte-10) : ($tamanho_fonte);
    imagestring($imagem, $tamanho_fonte, $coordx, $coordy, substr($palavra,($i-1),1), $branco);
}
imagejpeg($imagem);
imagedestroy($imagem);
?>