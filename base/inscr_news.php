<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Inscrição na Newsletter</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>

<body>

<?
$resposta = "";
$nome = "";
$email = "";

if ($_POST['submitted']) {

    // recupera post de variaveis
    $nome = $_POST['nome'];
    $email = $_POST['email'];
         
    // AntiXSS
    $char_XSS=array("<",">","/","(",";",")","script","javascript:","alert","font","div","style","body");
    include("AntiXSS.php");
    $nome = AntiXSS::setEncoding($nome, "UTF-8");
    $nome = AntiXSS::setFilter($nome, "black");
    $email = AntiXSS::setEncoding($email, "UTF-8");
    $email = AntiXSS::setFilter($email, "black");
    if (($nome=="XSS Detected!") || ($email=="XSS Detected!")) die();
    $nome=str_replace($char_XSS, "", $nome);
    $email=str_replace($char_XSS, "", $email);


    session_start();

    // Consistencias de campos 
    if ($_POST["palavra"] != $_SESSION["palavra"]){
        $resposta = "Preencha todos os campos corretamente."; }
    elseif (empty($nome) || empty($email) ) {
        $resposta = "Preencha todos os campos."; }
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $resposta = "Email Inválido."; }
    else {
 	    include("email.php");
        $assunto = "Inscrever na Newsletter do IA";
        $IPsender = $_SERVER['REMOTE_ADDR'];
        $mensagem .= "<br>Foi solicitada a inscrição na Newsletter do IA:<br>"; 
        $mensagem .= "<br>Nome: {$nome}";
        $mensagem .= "<br>Email: {$email}";
        $mensagem .= "<br>IP do solicitante: {$IPsender}";
        $email_notif = "giordani@unicamp.br";
        envia_email($assunto, $mensagem, $email_notif);
        $resposta = "Sua inscrição foi solicitada com sucesso.";
        $nome = "";
        $email = "";
    }
}
?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <p class="navbar-text"><font size="5"><strong>Inscrição na Newsletter</strong></font></p>
        </div>
    </div>
</nav>

<? if (!(empty($resposta))) { ?>
    <div class="alert alert-info" role="alert">
        <? echo "&nbsp;&nbsp;&nbsp;" . $resposta; ?>
    </div>
<? }
if ($resposta=="Sua inscrição foi solicitada com sucesso.")
    die();
?>

<div class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <form name="inscricao" action="inscr_news.php" method="post">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Nome completo: </label>
                    <?php
                    echo "<input class=\"form-control\" type=\"text\" name=\"nome\" id=\"nome\" maxlength=\"60\"";
                    if ($nome) echo " value=\"$nome\"";
                    echo ">";
                    ?>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Email: </label>
                    <?php
                    echo "<input class=\"form-control\" type=\"text\" name=\"email\" id=\"email\" maxlength=\"60\"";
                    if ($email) echo " value=\"$email\"";
                    echo ">";
                    ?>
                </div>
                <img src="captcha.php">
                <input type="text" name="palavra"  />
                <p>&nbsp;</p>
                <input class="btn btn-primary" type="submit" value="Inscrever"/>
                <input type="hidden" name="submitted" id="submitted" value="1">
                <p>&nbsp;</p>
            </form>
        </div>
    </div>
</div>

</body>
</html>

