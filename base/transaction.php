<?php
/* Classe que implementa uma transaÃ§Ã£o ao finalizar repassa true caso tenha executado com sucesso ou a menssagem de erro. */
class transaction {
	var $continua = TRUE; /* Variavel que informa se continua a executar os comandos SQL quando houver erro. */
	var $mensagem = ""; /* Guarda a mensagem de erro quando algum comando SQL executou de maneira inadequada. */
	var $contador = 0; /* Conta quandos comandos SQL foram executados neste comando. */
	var $query_erro = ""; /* Guarda a query que gerou erro no banco de dados. */
	var $sql_error = ""; /* Guarda a mensagem de erro do banco de dados. */
	var $linkconect = null;

    /*!
     * Constructor2: Inicia a transaction com o bd.
     */
    function transaction($linknaopersist) {
		$query = "BEGIN";
		$this->continua = mysql_query($query, $linknaopersist);
		$this->linkconect = $linknaopersist;
	}
	
	 /*!
     * Altera valor da mensagem.
     */
    function SetNaoContinua() {
        $this->continua = false;
		$this->mensagem = "Erro de lï¿½gica.";
    }
	
	/*!
     * Altera valor da mensagem.
     */
    function SetMensagem($msg) {
        $this->mensagem = $msg;
    }

	
	 /*!
     * Altera valor da query de erro.
     */
    function SetErroSql($error) {
        $this->sql_error = $error;
    }
	
	 /*!
     * Retorna valor da query de erro.
     */
    function GetErroSql() {
        return $this->sql_error;
    }
	
	 /*!
     * Altera valor da query de erro.
     */
    function SetQueryErro($query) {
        $this->query_erro = $query;
    }
	
	 /*!
     * Retorna valor da query de erro.
     */
    function GetQueryErro() {
        return $this->query_erro;
    }
	
    /*!
     * Executa a query passada por parametro, caso ainda nÃ£o tenha erro em nenhuma execuÃ§Ã£o anterior.
     * $query = comando SQL a ser executado no banco
     * $msg = Mensagem de erro a ser exibida caso tenha dado erro neste comando SQL
     */
    function executaQuery($query, $msg) {
		if($this->continua) {
			$this->contador++;
			if($this->linkconect) {
				$this->continua = mysql_query($query,$this->linkconect);
			} else {
				$this->continua = mysql_query($query);
			}
			if(!$this->continua) {
				$this->SetMensagem($msg);
				$this->SetQueryErro($query);
				$this->SetErroSql("(".mysql_errno()."-".mysql_error().")");
			}
		}
		return $this->continua;
	}
	
	
    /*!
     * Envia sinal de final de transaction.
     * Retorna uma string vazia caso tenha sido com sucesso ou a mensagem erro quando houver algum problema.
     */
    function finalizaTransaction() {
		if($this->continua) {
			$query = "COMMIT";
			if($this->linkconect) {
				$this->continua = mysql_query($query,$this->linkconect);
			} else {
				$this->continua = mysql_query($query);
			}
			return '';
		} else {
			$query = "ROLLBACK";
			if($this->linkconect) {
				$this->continua = mysql_query($query,$this->linkconect);
			} else {
				$this->continua = mysql_query($query);
			}
			return "Erro".$this->contador.": ".$this->mensagem." Tente novamente, se o problema persistir entre em contato com o administrador do sistema.";
		}
	}
}
?>