<?php
require_once("../fpdf/fpdf.php");
function PreparaPDF($modulo,$preju = NULL) {
    // Definindo Fonte
    global $relPDF;
    global $Mes,$Dia,$Ano,$NomeMeses;
    $relPDF->SetFont('arial','',15);
    //posicao vertical no caso -1 e o limite da margem
    $relPDF->SetY("-2");
    //::::::::::::::::::Cabecalho::::::::::::::::::::
    if ($modulo=='adiantamento' || $modulo=='gerencial') {
        $relPDF->Cell(0,5,'Intranet - Módulo Financeiro',0,0,'L');
        $relPDF->Cell(0,5,'Instituto de Artes',0,1,'R');
        $relPDF->Cell(0,0,'',1,1,'L');

        $relPDF->Ln(3);
        if ($modulo=='adiantamento') {
            $relPDF->Cell(0,5,'Controle Adiantamento',0,0,'L');
        } else {
           $relPDF->Cell(0,5,'Programas Gerenciais',0,0,'L'); 
        }
    }
    if ($modulo=='equip_informa') {
        $relPDF->Cell(0,5,'Intranet - Cadastro de equipamentos',0,0,'L');
        $relPDF->Cell(0,5,'Instituto de Artes',0,1,'R');
        $relPDF->Cell(0,0,'',1,1,'L');
         $relPDF->Ln(3);
        $relPDF->Cell(0,5,'Cadastro de equipamentos',0,0,'L'); 
    }
     if ($modulo=="transportes") {
        $relPDF->Cell(0,5,'Intranet - Módulo Transportes',0,0,'L');
        $relPDF->Cell(0,5,'Instituto de Artes',0,1,'R');
        $relPDF->Cell(0,0,'',1,1,'L');
        $relPDF->Ln(3);
        
    }
    //$modulo="afastamento";
    if ($modulo=='almox') {
        $relPDF->Cell(0,5,'Intranet - Módulo Almoxarifado',0,0,'L');
        $relPDF->Cell(0,5,'Instituto de Artes',0,1,'R');
        $relPDF->Cell(0,0,'',1,1,'L');
        $relPDF->Ln(3);
        
    }
    if ($modulo=="afastamento") {
        if ($preju) {
            // Substituido pelo termo de confissão de responsabilidade, por solicitação do supervisor, sr Edimilson
            /*$relPDF->SetFont('','',13);
            $relPDF->Cell(0,0,'TERMO DE COMPROMISSO E ASSUNÇÃO DE ENCARGO FINANCEIRO',0,0,'R');
            $relPDF->Ln(4);
            $relPDF->Cell(0,2,'AFASTAMENTO SEM PREJUÍZO DE VENCIMENTOS',0,0,'R');
            */
            $relPDF->SetFont('','',13);
            $relPDF->Cell(0,0,'TERMO DE CONFISSÃO DE RESPONSABILIDADE',0,0,'R');
            $relPDF->Ln(4);
            $relPDF->Cell(0,2,'E ASSUNÇÃO DE ENCARGO FINANCEIRO',0,0,'R');
    
        } else {
            $relPDF->SetFont('','',10);
            $relPDF->Cell(0,0,'Universidade Estadual de Campinas',0,0,'R');
            $relPDF->Ln(3);
            $relPDF->Cell(0,2,'Instituto de Artes',0,0,'R');
            //$relPDF->Ln(2);
            $relPDF->Cell(0,10,'Secão de Apoio aos Departamentos',0,0,'R');
            $relPDF->Ln(12);
            $relPDF->SetFont('','',8);
            $relPDF->Cell(0,0,'email: apdeptos@iar.unicamp.br',0,0,'R');
            $relPDF->Ln(4);
            $relPDF->Cell(0,0,'Tel.: (19) 35217081',0,0,'R');
            $relPDF->Ln(4);
            $relPDF->Cell(0,0,'Tel.: (19) 35217827',0,0,'R');
            $relPDF->SetLineWidth(0.8);
            $relPDF->Line(13,37, 200,37)  ; 
            $relPDF->Ln(10);
            $relPDF->SetFont('','',12);
            $relPDF->Cell(0,0,'Cidade Universitária "Zeferino Vaz"',0,0,'R');
            $relPDF->Ln(5);
            $relPDF->SetFont('','',10);
            $relPDF->Cell(0,0,'Campinas, ' . $Dia . ' de ' . $NomeMeses[$Mes] . ' de ' . $Ano,0,0,'R');
        }
        //$relPDF->Cell(0,100,'',0,101,'L');
        
    }
    
    //$relPDF->Cell(0,5,date("d/m/Y"),0,0,'R');
    //$relPDF->Cell(0,0,'',1,1,'L');
     /*
    $relPDF-> SetFont('arial','B',10);
    $relPDF->SetFillColor(122,122,122);
    $relPDF->SetFont('','',12);
      */
}
function ImprimaClausula($tit,$mm) {
    global $relPDF;
     //$relPDF->MultiCell(0,5,$tit.chr(10).chr(10),0,'C');
     $relPDF->MultiCell(0,5,$tit.$mm.chr(10).chr(10),0,'J');
                 //$relPDF->Ln(5);
}
    function ShowCabecalho($header,$w){
            global $relPDF;
            // Colors, line width and bold font
            $relPDF->SetFillColor(255,0,0);
            $relPDF->SetTextColor(255);
            $relPDF->SetDrawColor(128,0,0);
            $relPDF->SetLineWidth(.3);
            $relPDF->SetFont('','B');
            // Header
            for($i=0;$i<count($header);$i++)
                $relPDF->Cell($w[$i],7,$header[$i],1,0,'C',true);
            $relPDF->Ln();
   
    }
    function ShowLinhaUnica($linha,$w){
        global $preenchimento,$relPDF;
        
        // Color and font restoration
        $relPDF->SetFillColor(224,235,255);
        $relPDF->SetTextColor(0);
        $relPDF->SetFont('');
        // Data
        for($i=0;$i<count($linha);$i++){
           // if (is_numeric($linha[$i])) {
            //     $relPDF->Cell($w[2],6,$linha[$i],'LR',0,'R',$preenchimento  );
           // } else {
                $relPDF->Cell($w[$i],6,$linha[$i],'LR',0,'C',$preenchimento);
           // }
        }
        $relPDF->Ln();
        $preenchimento = !$preenchimento;
    }
    function ShowLN($w,$ln) {
        global $relPDF;
        //$relPDF->SetLineWidth(2);
        
        $relPDF->SetTextColor(0);
        $relPDF->SetFont('',"B");
        // Data
        $relPDF->Cell($w[0],6,$ln,'',0,'L');
        $relPDF->Ln();
    }
?>
