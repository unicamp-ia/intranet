<?php

 function dtSomar($data,$dias) {
            $data = str_replace("-","",$data);
            $ano = substr ( $data, 0, 4 );
            $mes = substr ( $data, 4, 2 );
            $dia = substr ( $data, 6, 2 );
            $novaData = mktime ( 0, 0, 0, $mes, $dia + $dias, $ano );
            return strftime("%Y-%m-%d", $novaData);
        }
function dtSomarTudo($data,$dias) {
    $data = str_replace("-","",$data);
    $ano = substr ( $data, 0, 4 );
    $mes = substr ( $data, 4, 2 );
    $dia = substr ( $data, 6, 2 );
    $p=strpos($data,":");
    $hh="00";
    $mm="00";
    if ($p) {
      // $p -=2;
       $hh=substr($data,$p-2,2);
       //echo $hh;
       $mm=substr($data,$p+1,2);
      // echo $mm;
    }
    $novaData = mktime ( $hh, $mm, 0, $mes, $dia + $dias, $ano );
    return strftime("%Y-%m-%d %H:%M:00", $novaData);
}
 function MontaDataSimples($a,$m,$d) {
     $a=$a+0;
     $m=$m+0;
     $d=$d+0;
     $mm = ($m<10?"0".$m:$m);
     $dd = ($d<10?"0".$d:$d);
     $dt = $a . "-" . $mm . "-" . $dd;
     return $dt;
}
function MontaDataCompleta($a,$m,$d,$ho,$mi,$se) {
     $a=$a+0;
     $m=$m+0;
     $d=$d+0;
     $ho=$ho+0;
     $mi=$mi+0;
     $se=$se+0;
     $mm=($m<10?"0".$m:$m);
     $dd=($d<10?"0".$d:$d);
     $hora=($ho<10?"0".$ho:$ho);
     $min=($mi<10?"0".$mi:$mi);
     $seg=($se<10?"0".$se:$se);
     $dt = $a."-".$mm."-".$dd." ".$hora.":".$min.":" . $seg;
     //echo "<br>dt=" . $dt . " $ho";
     return $dt;
}
?>
