<?
require '/site/intranet/PHPMailer/src/Exception.php';
require '/site/intranet/PHPMailer/src/PHPMailer.php';
require '/site/intranet/PHPMailer/src/SMTP.php';
require('/site/intranet/base/parameters.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

$search = explode(",","Ã§,Ã¦,Å,Ã¡,Ã©,Ã­,Ã³,Ãº,Ã ,Ã¨,Ã¬,Ã²,Ã¹,Ã¤,Ã«,Ã¯,Ã¶,Ã¼,Ã¿,Ã¢,Ãª,Ã®,Ã´,Ã»,Ã¥,e,i,Ã¸,u,Ã£,Ãµ,Ä©,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Å¸,Ã,Ã,Ã,Ã,Ã,Ã,Ã,Ä¨");
$replace =explode(",","&ccedil;,ae,oe,&aacute;,&eacute;,&iacute;,&oacute;,&uacute;,&agrave;,&egrave;,&igrave;,&ograve;,&ugrave;,a,e,i,o,u,y,&acirc;,&ecirc;,&icirc;,&ocirc;,&ucirc;,a,e,i,o,u,&atilde;,&otilde;,&itilde;,&Ccedil;,&Aacute;,&Eacute;,&Iacute;,&Oacute;,&Uacute;,A,E,I,O,U,A,E,I,O,U,Y,&Acirc;,&Ecirc;,&Icirc;,&Ocirc;,&Ucirc;,&Atilde;,&Otilde;,&Itilde;");

$URLemail = "http://www.iar.unicamp.br/intranet/index.php";

function envia_email($assunto, $mensagem, $destinatario, $replyTo = null){

	$mail = new PHPMailer(true);

	try {
		//Server settings
		//$mail->SMTPDebug = SMTP::DEBUG_SERVER;                    // Enable verbose debug output
		$mail->isSMTP();                                            // Send using SMTP
		$mail->Host       = 'smtp-imp.unicamp.br';                  // Set the SMTP server to send through
		$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
		$mail->Username   = 'intraia';                              // SMTP username
		$mail->Password   = 'r33coNsTruIr';                         // SMTP password
		$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
		$mail->Port       = 587;                                    // TCP port to connect to

		//Recipients
		$mail->setFrom('intraia@unicamp.br', 'IA');
		if(DEBUG) {
			$mail->addAddress('intraia@unicamp.br');     // Add a recipient
		} else {
                        $destinatarioArray = explode(',', $destinatario);
                        foreach ($destinatarioArray as $value) {
                            $mail->addAddress($value);     // Add a recipient
                        }
		}
		if($replyTo) $mail->addReplyTo($replyTo);

		// Content
		$mail->isHTML(true);                                  // Set email format to HTML
		$mail->Subject = $assunto;
		$mail->Body    = $mensagem;

		$mail->send();
	} catch (Exception $e) {
		echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}

/*
 * Funcao que envia um e-mail sem remetente.
 * @param object $assunto - Assunto da mensagem
 * @param object $mensagem - Mensagem em formato html a ser enviada.
 * @param object $destinatario - Destinatario do email.
 */
function envia_emailOLD($assunto, $mensagem, $destinatario) {
	$remetente_email = "NaoResponda@iar.unicamp.br";

	$hdrs = array(
		'From'    => $remetente_email,
		'Subject' => $assunto
	);

	$text = 'Text version of email';
	$html = '<html><body>HTML version of email</body></html>';
	$file = '/home/richard/example.php';
	$crlf = "\n";
	$hdrs = array(
		'From'    => $remetente_email,
		'Subject' => $assunto
	);

	$mime = new Mail_mime(array('eol' => $crlf));

	$mime->setTXTBody($text);
	$mime->setHTMLBody($html);
	$body = $mime->get();
	$hdrs = $mime->headers($hdrs);

	$mail =& Mail::factory('mail');
	$mail->send($destinatario, $hdrs, $body);
	;
	exit;

	//$mime->setTXTBody($mensagem);
	$mime->setHTMLBody($mensagem);
	//$mime->addAttachment($file, 'text/html');

	//do not ever try to call these lines in reverse order
	$body = $mime->get();
	$hdrs = $mime->headers($hdrs);

	$mail = Mail::factory('mail');

	$mail->send($destinatario, $hdrs, $body);
	echo 'oi2';
	exit;

}

/*
 * Funcao que envia um e-mail com remetente.
 * @param object $assunto - Assunto da mensagem
 * @param object $mensagem - Mensagem em formato html a ser enviada.
 * @param object $destinatario - Destinatario do email.
 * @param object $remetente - Remetente do email.
 */
function envia_email_remetente($assunto, $mensagem, $destinatario, $remetente) {
	envia_email($assunto,$mensagem,$destinatario,$remetente);
}

//http://www.php.net/manual/en/function.mail.php#27997
//gordon at kanazawa-gu dot ac dot jp
//29-Dec-2002 02:04
/* Funcao utilizada para assunto do e-mail poder ter acentos. */
function encode($in_str, $charset) {
	$out_str = $in_str;
	if ($out_str && $charset) {

		// define start delimimter, end delimiter and spacer
		$end = "?=";
		$start = "=?" . $charset . "?B?";
		$spacer = $end . "\r\n " . $start;

		// determine length of encoded text within chunks
		// and ensure length is even
		$length = 75 - strlen($start) - strlen($end);

		/*
			[EDIT BY danbrown AT php DOT net: The following
			is a bugfix provided by (gardan AT gmx DOT de)
			on 31-MAR-2005 with the following note:
			"This means: $length should not be even,
			but divisible by 4. The reason is that in
			base64-encoding 3 8-bit-chars are represented
			by 4 6-bit-chars. These 4 chars must not be
			split between two encoded words, according
			to RFC-2047.
		*/
		$length = $length - ($length % 4);

		// encode the string and split it into chunks
		// with spacers after each chunk
		$out_str = base64_encode($out_str);
		$out_str = chunk_split($out_str, $length, $spacer);

		// remove trailing spacer and
		// add start and end delimiters
		$spacer = preg_quote($spacer);
		$out_str = preg_replace("/" . $spacer . "$/", "", $out_str);
		$out_str = $start . $out_str . $end;
	}
	return $out_str;
}
?>
