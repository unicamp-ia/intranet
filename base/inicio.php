<?php
/* Setar variavel $nivelcomp. Se todos podem ver a pÃ¡gina $nivelcomp tem que ser = -1
 * A variavel sist deve ser setada no index.php de cada sistema.
 * Para verificar se eh um usuário institucional voce deve setar
 *	$tipocon = "INSTITUCIONAL"
 *	$igualtipo = true
 * Para verificar se nao eh um usuário institucional voce deve setar $igualtipo = false
 * Para verificar apenas o tipo e nao o nivel sotipo= true
 * Para verificar se pode abrir a pagina sem estar logado voce deve setar a variavel [$passa_deslogado = true] e [$nivelcomp = -1]
 * 
 * Se $htmlescape_request tiver valor transforma os valores do $_REQUEST em caracteres html (Ã¡ => &aacute)
 * Se $no_add_slashes tiver valor nao faz a escapagem dos valores do $_REQUEST com backslash
 * */
	session_start();
	if(!(isset($_SESSION["usuario"]) AND isset($_SESSION["senha"]) AND isset($_SESSION["coduser_conected"]) AND isset($_SESSION["tipo_conected"]) AND isset($_SESSION["nomeuser_conected"])))
        {
        // Provavelmente se trata de um acesso indevido
        die("Acesso indevido");
    }
	$pastas = encontrar_path("intranet");
	$sistget = $pastas["subpasta"]; //Representa o sistema em que este arquivo esta contido
	$nivel = $_SESSION[$sistget]+0;
    //include '../functionsInc.php';
    //Debug2($_SESSION, null, 'lime');
	$coduser_conected = $_SESSION["coduser_conected"];
	$codarea_conected = $_SESSION["codarea_conected"];
    $login_conected=$_SESSION["usuario"];
	$tipo_conected = $_SESSION["tipo_conected"];
	$cod_diretoria = $_SESSION["cod_diretoria"];
	$cod_graduacao = $_SESSION["cod_graduacao"];
    $cod_diretor=$_SESSION["cod_diretor"];
    $cod_associado=$_SESSION["cod_associado"];
    $codigouser_orgao = $_SESSION["codigouser_orgao"];
	$cod_pos = $_SESSION["cod_pos"];
        
    //print(phpinfo());
       
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>INTRANET IA</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="estilo.css" rel="stylesheet" type="text/css">
        <link href="../base/estilo.css" rel="stylesheet" type="text/css">
        <link href="../base/estiloprint.css" rel="stylesheet" type="text/css" media="print">
        <!-- Bootstrap CSS -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <?php
    //exit;
    /*	if(!(isset($_SESSION["usuario"]) AND isset($_SESSION["senha"])) && !$passa_deslogado) { ?>
		<SCRIPT LANGUAGE="JavaScript">alert('A sessï¿½o expirou, isto ocorre por ficar muito tempo sem utilizar a intranet. Entre novamente com seu e-mail e senha.'); if(this.name=='mainFrameIn') { parent.location.href='../index.php';} else { window.opener.parent.location.href='../index.php'; window.close();}</SCRIPT>
		<? exit(0);
	}
    */
    /*	if(!$sotipo && !($nivel>$nivelcomp || ($diretor && $codarea_conected==$cod_diretoria && $tipo_conected=="INSTITUCIONAL"))) { ?>
		<SCRIPT LANGUAGE="JavaScript">alert('Seu acesso nï¿½o estï¿½ liberado para esta pï¿½gina. Entre em contato com o administrador.'); </SCRIPT>
		<? exit(0);
	}
	if(($tipocon && (($igualtipo && $tipo_conected!=$tipocon) || (!$igualtipo && $tipo_conected==$tipocon))) && !($diretor && $codarea_conected==$cod_diretoria && $tipo_conected=="INSTITUCIONAL")) {?> 
		<SCRIPT LANGUAGE="JavaScript">alert('Seu acesso nï¿½o estï¿½ liberado para esta pï¿½gina. Entre em contato com o administrador.'); if(this.name=='mainFrameIn') { parent.location.href='../index.php';} else { window.opener.parent.location.href='../index.php'; window.close();}</SCRIPT>
		<? exit(0);
	}
    */
    include ("../base/ChecaTipo.php");
 	include("../base/db.php");
	if(!$naopersist) {
	  	db_connect()
    		or die("N&atilde;o consigo me conectar ao Servidor!");
		//$prnt = mysql_query("SET NAMES 'LATIN'");
	} else {
  		$linknaopersist = db_connect_naopersist()
    		or die("N&atilde;o consigo me conectar ao Servidor!");
		//$prnt = mysql_query("SET NAMES 'LATIN'", $linknaopersist);
	}

//Funcao que retorna [caminho relativo da pasta procurada em relacao a pasta atual, caminho absoluto da pasta procurada, sub pasta mais proxima da pasta procurada]
function encontrar_path($strPastaprocurada = "public_html") {
	$procurada = $strPastaprocurada;
	$encontrou = false;
	$tentativas = 0;
	$relativo = ".";
	$absoluto = dirname($_SERVER["SCRIPT_FILENAME"]); //Caminho relativo ao script que chamou o inicio.php (include("../base/inicio.php")
	
	$umdentro_rel = ""; //Variavel que guarda 1 pasta a mais (1 nivel mais profundo) em relacao ao script atual
	$umdentro_abs = "";
	
	
	while ((!$encontrou) && ($tentativas < 10)) {
		$pasta_atual = substr($absoluto, strrpos($absoluto, "/")+1); //Seleciona a ultima pasta (a mais a direita
		
		if ($pasta_atual == $procurada) {
			$encontrou = true;
		} else {
			$umdentro_abs = $absoluto; //Guarda a pasta atual para reconhecer a
            $umdentro_rel = $relativo;
			
			$absoluto = substr($absoluto, 0, strrpos($absoluto, "/"));
			$relativo .= "/..";
		}
		
		$tentativas++;
	}
	
	if ($encontrou) { 
		return array("relativo"=>$relativo, "absoluto"=>$absoluto, "subpasta"=>substr($umdentro_abs, strrpos($umdentro_abs, "/")+1)    );
	} else {
		return false;
	}
}//funciton

function slashescape_r(&$in) {
	if (is_array($in)) {
		foreach ($in as $key=>$value) {
			if (is_array($value)) {
				slashescape_r($in[$key]);
			} else {
				$in[$key] = addslashes($value);
			}
		}
	}
}


function slasheunscape_r(&$in) {
	if (is_array($in)) {
		foreach ($in as $key=>$value) {
			if (is_array($value)) {
				slasheunscape_r($in[$key]);
			} else {
				$in[$key] = stripslashes($value);
			}
		}
	}
}


function htmlescape_r(&$in, $quotestyle = ENT_COMPAT) {
	if (!in_array($quotestyle, array(ENT_COMPAT, ENT_QUOTES, ENT_NOQUOTES))) {
		$quotestyle = ENT_COMPAT;
	}
	
	if (is_array($in)) {		
		foreach ($in as $key=>$value) {
			if (is_array($value)) {
				htmlescape_r($in[$key], $quotestyle);
			} else {
				$in[$key] = htmlentities($value,$quotestyle,'UTF-8');
			}
		}
	} else {
		$in = htmlentities($in,$quotestyle,'UTF-8');
		return $in;
	}
}


/*if ($htmlescape_request) {
	htmlescape_r($_REQUEST);
} */

/*if (!$no_add_slashes) { //Este if soh deve existir quando o magic_quotes for desligado
	slashescape_r($_REQUEST);
}*/

//echo "U=" . $_SESSION["usuario"] . " S=" . $_SESSION["senha"];
?>
