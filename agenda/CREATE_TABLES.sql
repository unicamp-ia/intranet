CREATE TABLE AgendaUsuario(
`coduser` varchar(14) NOT NULL,
`codcomp` int(5) default NULL,
`status` varchar(1) default NULL
);


CREATE TABLE AgendaCompr(
`codcomp` int(10) unsigned auto_increment,
`codcompconj` int(10) unsigned NOT NULL,
`local` varchar(30) default NULL,
`datainicial` datetime default NULL,
`datafinal` datetime default NULL,
`datafinalint` date default NULL,
`solicitante` varchar(14) default NULL,
`observacao` text,
`datacadastro` date default NULL,
`assunto` varchar(30) default NULL,
`tipo` varchar(1) default NULL,
PRIMARY KEY (codcomp)
);

CREATE TABLE AgendaPermissao(
`nivel` int(1) NOT NULL,
`codpessoa` varchar(14) default NULL,
`codcedente` varchar(14) default NULL,
`notifemail` varchar(1) default NULL
);


CREATE TABLE AgendaConfig(
`Matricula` varchar(14) NOT NULL,
`Cor` varchar(20) default NULL,
`Lembretes` int(4) default NULL,
`Exibicao` varchar(1) default NULL,
`notifemail` varchar(1) default NULL,
PRIMARY KEY (`Matricula`)
);

CREATE TABLE AgendaGrupo(
`codgrupo` int(5) auto_increment,
`grupo` varchar(25) default NULL,
`matricula` varchar(14) default NULL,
PRIMARY KEY (`CodGrupo`)
);

CREATE TABLE AgendaGrupoPart(
`codgrupo` int(5) default NULL,
`matricula` varchar(14) default NULL,
PRIMARY KEY (`CodGrupo`,`Matricula`)
);
