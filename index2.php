<?PHP
//error_reporting(E_ALL);
//ini_set('display_errors', 1);


header("Content-type: text/html; charset=utf-8");
if ($_SERVER['SERVER_PORT']!=443) {
    $sslport=443; //whatever your ssl port is
    $url = "https://". $_SERVER['SERVER_NAME'] . ":" . $sslport . $_SERVER['REQUEST_URI'];
    header("Location: $url");
}
include("./base/db.php");
$usuario = $_REQUEST['usuario'];
$senha = $_REQUEST['senha'];
$sist = $_REQUEST['sist'];
$token = $_REQUEST['token'];
$pagina = $_REQUEST['pagina'];
$parametros = $_REQUEST['parametros'];

//
// Consistir Token ???
//
if ((!$usuario) || (!$token)) {
    $erro = "Desculpe! Mas voc&ecirc; deixou algum campo em branco no formul&aacute;rio.";
} else {
    // Verifca token
    if($token != md5($usuario.$usuario)) {
        $erro = 'Token invalido';
        exit;
    }


    $tamvar_usuario = strlen($usuario);
    if (($tamvar_usuario==6 && is_numeric($usuario)) || ($tamvar_usuario==7 && is_numeric(substr($usuario,1,6))))
        {
        // se $usuario tem seis caracteres e eh numerico ou
        // se $usuario tem sete caracteres e a substr do segundo ao setimo cartacter eh numerica
        // entao eh discente
        /* conecta ao banco */
        $linksys = db_connect();
        /* Verifica se existe usuario */
        $SQL = "select * from dbcorp.usuarios T1, dbcorp.usuarios_tipo T2 where T1.login='$usuario' and T1.usuarioID=T2.usuarioID and (tipo='Discente' || tipo='Participante Externo')";
        $res1 = mysqli_query($linksys,$SQL);
        if (!$linha1 = mysqli_fetch_array($res1)) {
            $erro = "Nao encontrado o perfil Discente no banco de dados de usuarios do IA. Procure suporte na Diretoria Tecnica de Informatica do IA.";
        } else {
            session_start(); /* A session_start deve estar antes de qualquer codigo senao dara erro !!  */
            $_SESSION["base"] = "./base";
            $_SESSION["usuario"] = $usuario;
            $_SESSION["senha"] = "informada";;
            $_SESSION["tipo_conected"] = "Aluno UNICAMP";
            $_SESSION["codarea_conected"] = $linha1['depto'];
            $_SESSION["coduser_conected"] = $usuario;
            $_SESSION["nomeuser_conected"] = $linha1['nome'];

            // Newton: alteracao para atender ao modelo antigo de username 123456 => x123456
            if(is_numeric($_SESSION["coduser_conected"])){
                $_SESSION["coduser_conected_old"] = strtolower(substr($_SESSION['nomeuser_conected'],0,1)) . $_SESSION["coduser_conected"];
                $_SESSION["emailuser_conected"] = $_SESSION["coduser_conected_old"] . "@dac.unicamp.br";
            } else {
                $_SESSION["emailuser_conected"] = $_SESSION["coduser_conected"] . "@dac.unicamp.br";
            }

            $_SESSION["cod_diretoria"] = "DIR"; /* Registra o codigo da diretoria na sessao */
            $_SESSION["cod_graduacao"] = "CGRAD"; /* Registra o codigo da graduacao na sessao */
            $_SESSION["cod_pos"] = "CPG"; /* Registra o codigo da CPG na sessao */

            //Coloca em sessao os dados do chefe do departamento ao qual o usuario estah lotado
            $SQL = "Select a.*, u.email, u.nome from areas a left join tblusuarios u on a.MATR_chefe=u.coduser where a.CodArea='".$linha1['depto']."'";
            $func = mysql_query($SQL);
            if (mysql_num_rows($func) > 0) {
                $res = mysql_fetch_array($func);
                $_SESSION["coduser_chefe"] = $res['MATR_chefe'];
                $_SESSION["coduser_imediato"] =$res['MATR_imediato'];
                $_SESSION["email_chefe"] = $res['email'];
                $_SESSION["nomeuser_chefe"] = $res['nome'];
                $_SESSION["nome_departamento"] = $res['Descricao'];
                $SQL = "SELECT  nome,coduser,email from tblusuarios where coduser='" . $res['MATR_imediato'] . "'";
                $res = mysql_query($SQL);
                if ($linha = mysql_fetch_array($res)) {
                    $_SESSION["email_imediato"] = $linha['email'];
                    $_SESSION["nomeuser_imediato"] = $linha['nome'];
                }
            }

            // header("Location: ./pg_inicial.php?sist=".$sist);
            header("Location: ./pg_inicial.php?sist=".$sist."&pagina=".$pagina."&parametros=".$parametros);
            exit;  /* Finaliza este script aqui */
        }
    } else {
        $rsconect = mysql_query("SELECT * FROM tblusuarios WHERE usuario='$usuario'");
        $linhausu = mysql_fetch_array($rsconect);




        // serah que vai precisar consistir se estah ativo ???
        if ($linhausu['status'] == "A") {
            $tipo_conected = $linhausu['tipo'];
            $coduser_conected = $linhausu['coduser'];
            $nomeuser_conected = $linhausu['nome'];
            $codarea_conected = $linhausu['codarea'];
            $regime_trabalho = $linhausu['regime'];
            $emailuser_conected = $linhausu['email'];
            // A session_start deve estar antes de qualquer codigo, senao darah erro !!
            session_start();
            $_SESSION["base"] = "./base";
            $_SESSION["usuario"] = $usuario;
            $_SESSION["senha"] = "informada";
            $_SESSION["tipo_conected"] = $tipo_conected;
            $_SESSION["codarea_conected"] = $codarea_conected;
            $_SESSION["regime_trabalho"] = $regime_trabalho;
            $_SESSION["coduser_conected"] = $coduser_conected; /* Registra a matricula do usuario */
            $_SESSION["nomeuser_conected"] = $nomeuser_conected; /* Registra o nome do usuario */
            $_SESSION["emailuser_conected"] = $emailuser_conected; /* Registra o nome do usuario */
            $_SESSION["cod_diretoria"] = "DIR"; /* Registra o codigo da diretoria na sessao */
            $_SESSION["cod_graduacao"] = "CGRAD"; /* Registra o codigo da diretoria na sessao */
            $_SESSION["expediente_central"] = "RECHUM"; /* Registra o codigo do RH da unidade */
            $_SESSION["cod_pos"] = "CPG"; /* Registra a sigla da pos-graduacao na sessao */
            $_SESSION["SQL_PDF"] = "N";
            $_SESSION["SQL_STR_SELECAO"] = "N";

            // Verifica quem eh o diretor do IA e quem eh o diretor associado
            // Para o diretor associado: a matricula eh armazenada no campo SUPERIOR imediato.
            $SQL = "select * from areas where CodArea='DIR' and origem='I'";
            $res = mysql_query($SQL);
            if ($lnd = mysql_fetch_array($res)) {
                $_SESSION["cod_diretor"] = $lnd['MATR_chefe'];
                $_SESSION["cod_associado"] = $lnd['MATR_imediato'];
            } else {
                $_SESSION["cod_diretor"] = "296644";
                $_SESSION["cod_associado"] = "304385";
            }

            //Coloca em sessao os dados do chefe do departamento ao qual o usuario estah lotado,
            // bem como os dados da pessoa indicada pelo RH para suplencia
            $SQL = "Select a.*, u.email, u.nome from areas a left join tblusuarios u on a.MATR_chefe=u.coduser where a.CodArea='$codarea_conected' and a.status='A'";
            $func = mysql_query($SQL);
            if (mysql_num_rows($func) > 0) {
                $res = mysql_fetch_array($func);
                $_SESSION["coduser_chefe"] = $res['MATR_chefe'];
                $_SESSION["nomeuser_chefe"] = $res['nome'];
                $_SESSION["email_chefe"] = $res['email'];
                $_SESSION["coduser_imediato"] = $res['MATR_imediato'];
                $_SESSION["nome_departamento"] = $res['Descricao'];
                $_SESSION["codigouser_orgao"] = $res['codigo_orgao'];

                if ($res['MATR_substituto']) {
                    if ($res['MATR_substituto'] <> "000000" && $res['prerrogativas'] == 1) {
                        $_SESSION["coduser_substituto"] = $res['MATR_substituto'];
                        $_SESSION["codarea_substituto"] = $codarea_conected;
                    }
                }
                $SQL = "SELECT  nome,coduser,email from tblusuarios where coduser='" . $res['MATR_imediato'] . "'";
                $res = mysql_query($SQL);
                if ($linha = mysql_fetch_array($res)) {
                    $_SESSION["email_imediato"] = $linha['email'];
                    $_SESSION["nomeuser_imediato"] = $linha['nome'];
                }
            }
            // Verifica se o usuario eh substituto de algum outro departamento,
            // caso nao seja substituto indicado do seu proprio departamento
            if (!$_SESSION["coduser_substituto"]) {
                $SQL = "SELECT * from areas where  MATR_substituto='$coduser_conected' and prerrogativas=1";
                $func = mysql_query($SQL);
                if ($linha = mysql_fetch_array($func)) {
                    $_SESSION["coduser_substituto"] = $linha['MATR_substituto'];
                    $_SESSION["codarea_substituto"] = $linha['CodArea'];
                }
            }
            // Verifica se o usuario eh um dos responsaveis pelo modulo Reserva de Salas
            $SQL = "SELECT * from salasresponsaveis where coduser='$coduser_conected'";
            $func = mysql_query("$SQL");
            if ($row_alt = mysql_fetch_array($func)) {
                $_SESSION['EoRespSalas'] = $coduser_conected;
            }

            // Verifica se o usuario eh o responsavel pelo modulo Reserva de Equipamentos
            $SQL = "SELECT * from equipamentosresponsaveis where coduser='$coduser_conected'";
            $func = mysql_query("$SQL");
            if ($row_alt = mysql_fetch_array($func)) {
                $_SESSION['EoRespEquipamentos'] = $coduser_conected;
            }

            // Coloca em sessao se o usuario eh gestor de informatica (Diretor de TI do IA).
            // pois, para solucao de problemas, o gestor de informatica deve ter acesso
            // com o mesmos privilegios dos responsaveis/gestores dos demais modulos.
            $SQL = "Select idsistema,coduser from tblususist where idsistema=3 and permissao='5' and coduser='$coduser_conected'";
            $rs_gestor_info = mysql_query($SQL);
            if ($linha = mysql_fetch_array($rs_gestor_info)) {
                $_SESSION['GestorInformatica'] = $coduser_conected;
            }

            $SQLlog = "insert into logs (coduser,modulo,acao,detalheacao,data_hora) values ('$coduser_conected','Autentica??o','Login','','$timehoje')";
            $res = mysql_query($SQLlog);

            $useragent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('|MSIE ([0-9].[0-9]{1,2})|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'IE';
            } elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Opera';
            } elseif (preg_match('|Firefox/([0-9\.]+)|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Firefox';
            } elseif (preg_match('|Chrome/([0-9\.]+)|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Chrome';
            } elseif (preg_match('|Safari/([0-9\.]+)|', $useragent, $matched)) {
                $browser_version = $matched[1];
                $browser = 'Safari';
            } else {
                // browser not recognized!
                $browser_version = 0;
                $browser = 'other';
            }
            $_SESSION['browser'] = $browser;

            if (!$sist) {
                header("Location: ./menu_inicial.php"); // Redireciona o usuario para o php protegido
            } else {
                $coduser_conected = $_SESSION["coduser_conected"];
                $tipo_conected = $_SESSION["tipo_conected"];
                $usuario = $_SESSION["usuario"];
                $SQL = "Select u.*, s.codsistema from tblususist u left join tblsistemas s on u.idsistema=s.idsistema where coduser='$coduser_conected' and u.principal='S'";
                $rs_permissao_cad = mysql_query($SQL);
                while ($linha = mysql_fetch_array($rs_permissao_cad)) {
                    $_SESSION[$linha['codsistema']] = $linha['permissao'];
                }

                header("Location: ./pg_inicial.php?sist=".$sist."&pagina=".$pagina."&parametros=".$parametros);
            }
            exit;  // Finaliza este script aqui
        } else {
            $erro = "<font color=green>Usu&aacute;rio inativo na Intranet. Por favor, procure a se&ccedil;&atilde;o de Recursos Humanos do IA.</font><br><br>";
        }
    }
}
?>
