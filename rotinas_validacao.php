<?php
?>
<script type="text/javascript">
var reDigits = /^(score:).+\d+/;
var reDigits = /(score:).+\d+/;

function doDigits(pStr)
{
	if (reDigits.test(pStr)) {
		alert(pStr + " contém apenas dígitos.");
	} else if (pStr != null && pStr != "") {
		alert(pStr + " NÃO contém apenas dígitos.");
	}
}

var reDecimalPt = /^[+-]?((\d+|\d{1,3}(\.\d{3})+)(\,\d*)?|\,\d+)$/;
var reDecimalEn = /^[+-]?((\d+|\d{1,3}(\,\d{3})+)(\.\d*)?|\.\d+)$/;
var reDecimal = reDecimalPt;
//<form class="boxLeft" id="frmDecimal" action="#"
// onsubmit="doDecimal(this.txtDecimal.value, this.selLang.value); return false;">
//<div>
//<label for="txtDecimal">Decimal:</label>
//<input type="text" size="10" id="txtDecimal" name="txtDecimal" />
//<select name="selLang" id="selLang">
//  <option value="Pt" selected="selected">Português</option>
//  <option value="En">Inglês</option>
//</select>
//<input type="submit" value="Validar" />
//</div>
//</form>
function doDecimal(pStr, pLang)
{
	charDec = ( pLang != "En"? ",": "." );
	eval("reDecimal = reDecimal" + pLang);
	if (reDecimal.test(pStr)) {
		pos = pStr.indexOf(charDec);
		decs = pos == -1? 0: pStr.length - pos - 1;
		alert(pStr + " é um float válido (" + pLang + ") com " + decs + " decimais.");
	} else if (pStr != null && pStr != "") {
		alert(pStr + " NÃO é um float válido.");
	}
} // doDecimal

var reMoeda = /^\d{1,3}(\.\d{3})*\,\d{2}$/;
//<form class="boxLeft" id="frmMoeda" action="#"
// onsubmit="doMoeda(this.txtMoeda.value); return false;">
//<div>
//<label for="txtMoeda">Valor (financeiro, Português):</label>
//<input type="text" size="10" id="txtMoeda" name="txtMoeda" />
//<input type="submit" value="Validar" />
//</div>
//</form>
function doMoeda(pStr)
{
	if (reMoeda.test(pStr)) {
		alert(pStr + " é um valor financeiro válido.");
	} else if (pStr != null && pStr != "") {
		alert(pStr + " NÃO é um valor financeiro válido.");
	}
}

var reDate1 = /^\d{1,2}\/\d{1,2}\/\d{1,4}$/;
var reDate2 = /^[0-3]?\d\/[01]?\d\/(\d{2}|\d{4})$/;
var reDate3 = /^(0?[1-9]|[12]\d|3[01])\/(0?[1-9]|1[0-2])\/(19|20)?\d{2}$/;
var reDate4 = /^((0?[1-9]|[12]\d)\/(0?[1-9]|1[0-2])|30\/(0?[13-9]|1[0-2])|31\/(0?[13578]|1[02]))\/(19|20)?\d{2}$/;
var reDate5 = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
var reDate = reDate4;
//  pFmt
//  "1">Simples</option>
//  2">Média</option>
//  3">Avançada</option>
//  4">Completa</option>
//  5" selected="selected">dd/mm/aaaa</option>
function doDate(pStr, pFmt)
{
	eval("reDate = reDate" + pFmt);
	if (reDate.test(pStr)) {
		alert(pStr + " é uma data válida.");
	} else if (pStr != null && pStr != "") {
		alert(pStr + " NÃO é uma data válida.");
	}
} // doDate

var reTime1 = /^\d{2}:\d{2}$/;
var reTime2 = /^([0-1]\d|2[0-3]):[0-5]\d$/;
var reTime3 = /^(0[1-9]|1[0-2]):[0-5]\d$/;
var reTime4 = /^\d+:[0-5]\d:[0-5]\d$/;
var reTime5 = /^\d+:[0-5]\d:[0-5]\.\d{3}\d$/;
//<form class="boxLeft" id="frmTime" action="#"
// onsubmit="doTime(this.txtTime.value, this.selTime.value); return false;">
//<div>
//<label for="txtTime">Tempo:</label>
//<input type="text" size="10" maxlength="10" id="txtTime" name="txtTime" />
//<select name="selTime" id="selTime">
//  <option value="1">Horário HH:MM simples</option>
//  <option value="2" selected="selected">Horário HH:MM 24h</option>
//  <option value="3">Horário HH:MM 12h</option>
//  <option value="4">Tempo horas:MM:SS</option>
//  <option value="5">Tempo horas:MM:SS.mili</option>
//</select>
//<input type="submit" value="Validar" />
//</div>
//</form>
function doTime(pStr, pFmt)
{
	eval("reTime = reTime" + pFmt);
	if (reTime.test(pStr)) {
		alert(pStr + " é um horário/tempo válido.");
	} else if (pStr != null && pStr != "") {
		alert(pStr + " NÃO é um horário/tempo válido.");
	}
} // doTime

var reEmail1 = /^[\w!#$%&'*+\/=?^`{|}~-]+(\.[\w!#$%&'*+\/=?^`{|}~-]+)*@(([\w-]+\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
var reEmail2 = /^[\w-]+(\.[\w-]+)*@(([\w-]{2,63}\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
var reEmail3 = /^[\w-]+(\.[\w-]+)*@(([A-Za-z\d][A-Za-z\d-]{0,61}[A-Za-z\d]\.)+[A-Za-z]{2,6}|\[\d{1,3}(\.\d{1,3}){3}\])$/;
var reEmail = reEmail3;
//  <option value="1">Livre</option>
//  <option value="2">Compacto</option>
//  <option value="3" selected="selected">Restrito</option>
function doEmail(pStr, pFmt)
{
	eval("reEmail = reEmail" + pFmt);
	if (reEmail.test(pStr)) {
		alert(pStr + " é um endereço de e-mail válido.");
	} else if (pStr != null && pStr != "") {
		alert(pStr + " NÃO é um endereço de e-mail válido.");
	}
} // doEmail
</script>
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
