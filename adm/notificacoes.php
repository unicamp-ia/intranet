#!/usr/local/bin/php
<?

        /* 
            Intranet  Sistema de Notificacoes periodicas
            --------------------------------------------
            Notificacoes aos aprovadores desatentos de RH e Transportes
        
            Execucao agendada no Cron da maquina Web, todos os dias as 02:00
            D.R.S          13/12/2013
        */

        // Execura o script somente se chamado pelo cron
        if ($argv[1] !="cron") {
             exit;
        }
        
        $pa="/usr/local/www/data/intranet";
        include ($pa. "/base/FuncoesUteis.php");
        include($pa . "/base/db.php");
        db_connect() or die("N&atilde;o consigo me conectar ao Servidor!");
        include($pa . "/base/email.php");
        include($pa . "/base/transaction.php");

        function IdentifiqueOaprovador($Oaprovador) {
             global $nomeaprovador,$emailaprovador,$codsubstituto,$nomesubstituto,$emailsubstituto;
             $nomeaprovador="";
             $emailaprovador="";
             $nomesubstituto="";
             $emailsubstituto="";
             $codsubstituto="";
             // Busca o depto do chefe imediato, para verificar se ha algum substituto indicado naquele depto.
             // select u.*,a.* from tblusuarios u left join areas a on u.codarea=a.CodArea where u.coduser='284854'

             $SQL="select u.nome,u.coduser,u.email,a.* from tblusuarios u left join areas a on u.codarea=a.CodArea where u.coduser='$Oaprovador'";
             $res1=mysql_query($SQL);
             if ($linha=mysql_fetch_array($res1)) {
                   $nomeaprovador=$linha['nome'];
                   $emailaprovador=$linha['email'];
                   if ($linha['MATR_substituto'] <> "000000" and $linha['prerrogativas']==1) {
                        $emailsubstituto =  $linha['email_substituto'];
                        $SQL_A="select * from tblusuarios where coduser='$codsubstituto'";
                        $res = mysql_query("$SQL_A");
                        if($linha = mysql_fetch_array($res)) {
                              $nomesubstituto = $linha['nome'];
                              $codsubstituto = $linha['coduser'];
                        }    
                    }
              }     
        }
        //envia_email("[cron]Sistema de notificacao periodica", "Iniciando as notificacaes", "intra.ia@iar.unicamp.br");
 
	$timestamp = time();
        $atenciosamenteRH="<br> Atenciosamente, RH/IA (Recursos Humanos)";
        
        /*
             Notificacoes aos aprovadores - Afastamento
             Quantidade em segundos de hoje a 3 dias apos
        */
	$timestampdepois = $timestamp + 259200;
	$data_mais_3d = date('Y-m-d', $timestampdepois);
	$SQL = "select u.nome,u.email,s.codaprovador, s.codsolicitante,date_format(a.datasaida, '%d/%m/%Y' ) as dtsaida from RHafastamento a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A' and a.datasaida <='$data_mais_3d'";
	$res = mysql_query($SQL);
        $assunto = "Sistema de Recursos Humanos";
       
	while ($linha=mysql_fetch_array($res)) {
                $cod=$linha['cod'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datasaida = $linha['dtsaida'];
		$mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de afastamento para inicio em $datasaida at&eacute; hoje n&atilde;o foi aprovada pelo(a) chefe. Consulte-o para saber os motivos da demora na an&aacute;lise, pois o atraso pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                IdentifiqueOaprovador($codaprovador);
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .=  "<br> Verificamos a exist&ecirc;ncia de uma solicita&ccedil;&atilde;o de afastamento do colaborador $nomesolicitante para inicio";
                $mensagem .= " em $datasaida ate o momento nao aprovada. Pedimos-lhe a gentileza de analis&aacute;-la pois a demora pode comprometer os prazos exigidos pelo"; 
                $mensagem .= " RH da Unicamp e, como consequencia, prejudicar o solicitante. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                // envia_email("[cron]Sistema RH - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
	}
        /*
         Notificacoes aos aprovadores - Licenca-premio
         Quantidade em seguntos de hoje a 1 dia
        */
         
        $timestampdepois = $timestamp + 86400;
	$data_mais_1d = date('Y-m-d', $timestampdepois);
	$SQL = "select u.nome, u.email, s.codaprovador, s.codsolicitante, date_format(a.datainicio, '%d/%m/%Y' ) as dtinicio from RHlicenca a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A' and a.datainicio<='$data_mais_1d'";
	$res = mysql_query($SQL);
	while ($linha=mysql_fetch_array($res)) {
            
            $cod=$linha['cod'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datainicio = $linha['dtinicio'];
                
		// Notifica o solicitante
                $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de licen&ccedil;a pr&ecirc;mio para inicio em $datainicio at&eacute; o momento n&atilde;o foi aprovada pelo(a) chefe. Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                // Notifica o apovador
                IdentifiqueOaprovador($codaprovador);
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .= "<br> Verificamos a exist&ecirc;ncia de uma solicita&ccedil;&atilde;o de licen&ccedil;a pr&ecirc;mio do colaborador $nomesolicitante para in&iacute;cio";
                $mensagem .= " em $datainicio at;&eacute; o momento n;&atilde;o aprovada. Pedimos-lhe a gentileza de analis&aacute;-la pois a demora pode comprometer os prazos"; 
                $mensagem .= " exigidos pelo  RH da Unicamp. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                //envia_email("[cron]Sistema RH - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
	}
        
        /*
           Notificacoes aos aprovadores - Faltas abonadas
	*/
        
        $timestampdepois = $timestamp + 86400;
	$data_mais_1d = date('Y-m-d', $timestampdepois);
	$SQL = "select u.nome, u.email, s.codaprovador, s.codsolicitante, date_format(a.datafalta, '%d/%m/%Y' ) as dtfalta,a.codigofalta  from RHartigo a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A' and a.datafalta<='$data_mais_1d'";
	$res = mysql_query($SQL);
	while ($linha=mysql_fetch_array($res)) {
            
                $cod=$linha['cod'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datafalta = $linha['dtfalta'];
                $desctipo="F1 - Falta abonada (artigo 31)";
		 if ($linha['codigofalta']=="F2") {
                    $desctipo="F2 - Falta integral abonada";
                }
                // Notifica o solicitante
                $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o $desctipo, para o dia $datafalta at&eacute; o momento n&atilde;o foi aprovada pelo(a) chefe. Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois isso pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
                
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                // Notifica o apovador
                IdentifiqueOaprovador($codaprovador);
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .= "<br> Verificamos a exist&ecirc;ncia de uma solicita&ccedil;&atilde;o $desctipo, do(a) colaborador(a) $nomesolicitante, para o dia";
                $mensagem .= " $datafalta at&eacute; o momento n;&atilde;o aprovada. Pedimos-lhe a gentileza de analis&aacute;-la pois a demora pode comprometer os prazos exigidos pelo"; 
                $mensagem .= " RH da Unicamp. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                //envia_email("[cron]Sistema RH - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
	}
        
        /*
            Notificacoes aos aprovadores - Ferias
        */
        
        $SQL = "select u.nome, u.email, s.codaprovador, s.codsolicitante, s.datacadastro, s.lotacao, date_format(a.datainicio, '%d/%m/%Y' ) as dtinicio,a.total as totaldias  from RHferias a left join RHsolicitacoes s on a.cod=s.cod left join tblusuarios u on s.codsolicitante=u.coduser where s.status='A'";
	$res = mysql_query($SQL);
        $datadehoje=date('Y-m-d');
	while ($linha=mysql_fetch_array($res)) {
            $datacadastro=$linha['datacadastro'];
            $data_mais_7d= dtSomar($datacadastro,7);
            if ($data_mais_7d <= $datadehoje) {
                $cod=$linha['cod'];
                $totaldias=$linha['totaldias'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $datainicio = $linha['dtinicio'];
                $lotacao=$linha['lotacao'];
                IdentifiqueOaprovador($codaprovador);
                // Notifica o solicitante
                $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de f&eacute;rias para o inicio em $datainicio, por $totaldias dias at&eacute; o momento n&atilde;o foi aprovada pelo(a) chefe, $nomeaprovador. <br>Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois isso pode comprometer os prazos exigidos pelo RH da Unicamp.<br><br>";
		$mensagem .= $atenciosamenteRH;
                
		envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                // Notifica o apovador
                $email_notif=$emailaprovador;
                if ($codsubstituto <> "") {
                            $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                            $email_notif=$email_notif . "," . $emailsubstituto;
                } else {
                            $mensagem = "Caro(a) $nomeaprovador,";
                }
                $mensagem .= "<br> Verificamos a exist&ecirc;ncia de uma solicita&ccedil;&atilde;o de f&eacute;rias, do(a) colaborador(a) $nomesolicitante, para inicio em";
                $mensagem .= " $datainicio, por $totaldias dias at&eacute; o momento n&atilde;o aprovada. Pedimos-lhe a gentileza de analis&aacute;-la pois a demora pode comprometer os prazos exigidos pelo"; 
                $mensagem .= " RH da Unicamp. <br>" . $atenciosamenteRH;
                envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                //envia_email("[cron]Sistema RH - F&eacute;rias - Lembrete ao aprovador [$lotacao]", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
          }
	}
        
        /*
         Notificacoes aos aprovadores - Transportes
        */
        
        $SQL = "select u.nome,u.email,t.cod,t.ano,t.coduser,t.codarea,t.codaprovador,t.codrecurso,t.veiculo,t.datasaida,t.finalidade,t.destino,t.status,";
        $SQL .= "date_format(t.datasaida, '%d/%m/%Y &agrave;s %H:%i') as dtsaida,v.descricao as modeloveiculo,v.antes as antes, v.antes_dh as antes_dh, r.descricao as nomerecurso from transpedidos t";
        $SQL .=" left join tblusuarios u on t.coduser=u.coduser left join transpveiculos v on t.veiculo=v.cod  left join transprecurso";
        $SQL .=" r on t.codrecurso=r.cod where t.status='P' or t.status='R'";
	$res = mysql_query($SQL);
        //echo "sddsds" . mysql_num_rows($res);
        //echo $SQL;
        $datadehoje=date('Y-m-d');
        //echo mysql_num_rows($res);
        $assunto = "Sistema Intranet - Transportes";
	while ($linha=mysql_fetch_array($res)) {
            $antes_horas=$linha['antes'];
            if ($linha['antes_dh'] =='d') {
                $antes_horas=$antes_horas * 24;
            }
            if ($antes_horas <=24) {
                $antes_horas = $antes_horas+24;
            }
            $data_antecipacao=strftime("%Y-%m-%d:%H:%M:%S", strtotime("+$antes_horas hours"));
            $datasaida=$linha['datasaida'];
            $status=$linha['status'];
            //echo "\n Data Saida: " . $datasaida . " Antes: $antes_horas Data Antecipacao: " . $data_antecipacao . " >" . $linha['cod'] . " " . $linha['ano'];
            //echo "\n---------------------------------------------------------------------------------";
            if ($data_antecipacao >= $datasaida || $status=='R') {
                $cod=$linha['cod'];
                $ano=$linha['ano'];
		$emailsolicitante = $linha['email'];
                $nomesolicitante=$linha['nome'];
                $codaprovador=$linha['codaprovador'];
                $modeloveiculo=$linha['modeloveiculo'];
                $nomerecurso=$linha['nomerecurso'];
                $destino=$linha['destino'];
                $finalidade=$linha['finalidade'];
                $dtsaida=$linha['dtsaida'];
               
                // Notifica o solicitante
                if ($status=='P') {
					IdentifiqueOaprovador($codaprovador);
					
                    $mensagem = "Caro(a) $nomesolicitante,<br>  Verificamos que sua solicita&ccedil;&atilde;o de ve&iacute;culo n&uacute;mero $cod / $ano com data de sa&iacute;da em $dtsaida at&eacute; o momento n&atilde;o foi analisada pelo aprovador(a), $nomeaprovador. Consulte-o(a) para saber os motivos da demora na an&aacute;lise, pois isso pode, caso seja um ve&iacute;culo externo, comprometer os prazos exigidos pela &aacute;rea de transportes da DGA.<br><br>";
                    $mensagem .= "<br> Atenciosamente, APOPER/IA.";
                    //echo "\n\n" . $mensagem . "\n" . $emailsolicitante; 
                    envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                    // Notifica o apovador
                    $email_notif=$emailaprovador;
                    if ($codsubstituto <> "") {
                                $mensagem="Caros $nomeaprovador e $nomesubstituto, ";
                                $email_notif=$email_notif . "," . $emailsubstituto;
                    } else {
                                $mensagem = "Caro(a) $nomeaprovador,";
                    }
                    $mensagem .= "<br> Verificamos a exist&ecirc;ncia de uma solicita&ccedil;&atilde;o de ve&iacute;culo n&uacute;mero $cod / $ano criada pelo(a) colaborador(a) $nomesolicitante, com data de sa&iacute;da em $dtsaida";
                    $mensagem .= " at&eacute; o momento n&atilde;o aprovada. Pedimos-lhe a gentileza de analis&aacute;-la pois a demora pode comprometer, no caso de ve&iacute;culo externo, os prazos exigidos pela &aacute;rea de transporte da DGA. <br><br>Outros dados da solicita&ccedil;&atilde;o:<br>"; 
                    $mensagem .= "Ve&iacute;culo: $modeloveiculo<br>Recurso:$nomerecurso<br>Destino:$destino<br>Finalidade:$finalidade";
                    $mensagem .= "<br>Para acessar a referida solicita&ccedil;&atilde;o, selecione o m&oacute;dulo <b>Transportes</b> da Intranet(www.iar.unicamp.br/intranet) e clique na op&ccedil;&atilde;o <b>APROVAR SOLICITA&Ccedil;&Otilde;ES</b><br><br>  Atenciosamente, APOPER/IA.";

                    envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $email_notif);
                    //envia_email("[cron]Modulo de transportes - Lembrete ao aprovador", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
                } else {
                    $mensagem =  "Caro(a) $nomesolicitante,<br>  Verificamos que a solicita&ccedil;&atilde;o de ve&iacute;culo n&uacute;mero $cod / $ano com data de sa&iacute;da em $dtsaida foi retornada a voc&ecirc; pelo Gestor do sistema de Transporte do IA, cobrando-lhe alguma a&ccedil;&atilde;o para prosseguimento."; 
                    $mensagem .= "Por favor, acesse a referida solicita&ccedil;&atilde;o, fa&ccedil;a as corre&ccedil;&otilde;es necess&aacute;rias solicitadas pelo Gestor ou cancele-a se for o caso.<br><br>";
		    $mensagem .= "<br> Atenciosamente, <br>Sistema Intranet - M&oacute;dulo de Transportes.";
                    envia_email($assunto, $eM1 . $mensagem . $NaoResponda . $eM2, $emailsolicitante);
                    //envia_email("[cron]Modulo de transportes - Notifica&ccedil;&atilde;o ao solicitante", $eM1 . $mensagem . $NaoResponda . $eM2, "intra.ia@iar.unicamp.br");
                }
          }
	}
        
        // envia email, somente de segunda-feira, para ATU com as faltas abonadas dos funcionarios da semana anterior
        if (date('N')==1) {
            $ArrStatus= array("A"=>"Aguardando Aprova&ccedil;&atilde;o","P"=>"Pendente para RH","T"=>"Aguardando Retorno","Z"=>"Aguardando Portaria","U"=>"Retornada ao Solicitante","F"=>"Finalizada","R"=>"Recusada","C"=>"Cancelada");
            $datahoje = date('Y-m-d');
            $datasemanaatras = date('Y-m-d', strtotime($datahoje. ' - 7 days'));
	    $SQL_COUNT = "Select COUNT(*) 
                    from RHartigo art 
                    left join RHsolicitacoes s on art.cod=s.cod 
                    left join tblusuarios u on s.coduser=u.coduser 
                    left join areas a on s.lotacao=a.CodArea 
                    where s.tipo='G' and art.datafalta>='$datasemanaatras' and art.datafalta<'$datahoje' and (u.tipo='FUNCIONARIO') 
                    order by art.datafalta asc,u.nome";
            $SQL = "Select u.nome, a.Descricao as departamento, u.coduser, art.datafalta, date_format(art.datafalta, '%d/%m/%Y') as dtfalta, art.justificativa,art.codigofalta, s.tipo,s.status 
                    from RHartigo art 
                    left join RHsolicitacoes s on art.cod=s.cod 
                    left join tblusuarios u on s.coduser=u.coduser 
                    left join areas a on s.lotacao=a.CodArea 
                    where s.tipo='G' and art.datafalta>='$datasemanaatras' and art.datafalta<'$datahoje' and (u.tipo='FUNCIONARIO') 
                    order by art.datafalta asc,u.nome";
            $func = mysql_query("$SQL");
            $num_result = mysql_query("$SQL_COUNT");
            if (mysql_fetch_array($num_result)==0) {
                $corpo = "<BR>N&atilde;o houveram pedidos de faltas abonadas no per&iacute;odo de ".substr($datasemanaatras,8,2)."/".substr($datasemanaatras,5,2)."/".substr($datasemanaatras,0,4)." a ".substr($datahoje,8,2)."/".substr($datahoje,5,2)."/".substr($datahoje,0,4);
            } else {
                $corpo = "<BR>Solicita&ccedil;&otilde;es de faltas abonadas no per&iacute;odo de ".substr($datasemanaatras,8,2)."/".substr($datasemanaatras,5,2)."/".substr($datasemanaatras,0,4)." a ".substr($datahoje,8,2)."/".substr($datahoje,5,2)."/".substr($datahoje,0,4)."<BR><BR>
                <table width='100%' border='1' cellpadding='0' cellspacing='0' style='border-collapse:collapse;'>
                    <tr>
        		<td width='10%' align='center'>
                            Data falta
                        </td>
                        <td width='15%' align='center'>
                            Nome
                        </td>
                        <td width='5%' align='center'>
                            Matr&iacute;cula
                        </td>
                        <td width='15%' align='center'>
                            Departamento
                        </td>
                        <td width='7%' align='center'>
                            Tipo de abonada
                        </td>
                        <td width='14%' align='center'>
                            Justificativa
                        </td>
                        <td width='10%' align='center'>
                            Status
                        </td>
                    </tr>";
                while ($linha=mysql_fetch_array($func)) {
                $corpo .= "<tr>
                        <td align='center'>".$linha['dtfalta']."</td>
                        <td align='center'>".$linha['nome']."</td>
                        <td align='center'>".$linha['coduser']."</td>
                        <td align='center'>".$linha['departamento']."</td>
                        <td align='center'>";
                        if ($linha['codigofalta']=="F1") { 
                            $corpo .= "F1 - Falta abonada - Artigo 31</td>";
                        } else { 
                            $corpo .= "F2 - Falta integral abonada</td>";
                        }
                $corpo .= "<td align='center'>".$linha['justificativa']."</td>
                        <td align='center'>".$ArrStatus[$linha['status']]."</td>
                    </tr>";
                }
            $corpo .= "</table>";
            }
        $titulo = "[Sistema RH/IA] Relatorio de faltas abonadas - periodo de ".substr($datasemanaatras,8,2)."/".substr($datasemanaatras,5,2)."/".substr($datasemanaatras,0,4)." a ".substr($datahoje,8,2)."/".substr($datahoje,5,2)."/".substr($datahoje,0,4);
        envia_email($titulo, $corpo, "atuia@iar.unicamp.br");
        }
         
?>
