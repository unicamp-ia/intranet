<?php
// Busca no organograma o chefe imediato para a area $CodArea
// Assinala codigo do usuario e nome 
// CodArea deve estar iniciada no procedimento
// Alterado por Edson Giordani em Janeiro/2020 - Array hierarquia e alteracao de aprovador

include ("../base/FuncoesOrganograma.php");
 
$hierarquia=array();
//Monta a hierarquia somente para a area acima(S)
MontaHierarquia($CodArea,'N','S','N','C');

if (count($hierarquia) ==0) {
    echo "[busca_aprovador]. Não foi possível montar a arvore hierarquica para a area $areauser.";
    die();
}

if ($hierarquia['acima']['matr']) {
        // Assinala como aprovador o chefe imediato
        $codimediato = $hierarquia['acima']['matr'];
        $emailimediato = $hierarquia['acima']['email'];
} 
