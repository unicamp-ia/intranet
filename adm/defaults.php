<?
/**
 * /cg/discente/defaults.php
 * @author Josué Cintra
 * @version 1.00 11-11-2008 00:00 Creation 
 * @version 1.01 28-06-2011 15:30 Alteração do nome da Sessão para compatibilizar com a Extranet, de forma a permitir que os usuários da CG façam seu login na Extranet. Alteração do redirecionamento para a página de login da Extranet (/extranet) e setando a Sessão do Discente a partir da sessão gerada no login datravés da Extranet.
 *
 * Define os valores iniciais da Sessão; 
 * define variáveis para checagem de privilégios de acesso;
 * monitora se o usuário está autenticado ou não;
 * inclui /cg/functionsInc.php
 * 
 */
session_name('EXTRANET');
session_start();
/*  # variáveis de autenticação --------------------------------------------------
  session_register("ses_disc_id");
  session_register("ses_disc_nome");
  session_register("ses_disc_nick");
  session_register("ses_disc_sexo");
  session_register("ses_disc_tipo_usr");
  session_register("ses_disc_area");
  session_register("ses_disc_permissoes");
*/
# seta a tabela ascii para o idioma português ----------------------------------
# para validação de expressões regulres ----------------------------------------
setlocale(LC_ALL,"pt_PT.ISO8859-1");

if( isset( $_GET['logout'] ) || isset( $_GET['sair'] ) ) { session_destroy(); $_SESSION = null; }

/**
 * Inicializa variáveis utilizadas para checar se o usuário logado pode ter acesso a essa área
 */
$area        = 'CG';
$headerTitle = 'Área Discente da CG/IA';
$usrTipo     = 'Discente';
$tipoPerm    = 'Discente';
/**
 * Compatibiliza a Sessão da Área Discente com a setada na Extranet
 */
if(!empty($_SESSION['ses_auth']) && empty($_SESSION['ses_disc_id']))
{
	if(empty($_SESSION['ses_auth']['apelido']))
	{
		$nick = explode(" ", $_SESSION['ses_auth']['nome']);
		$_SESSION['ses_auth']['apelido'] = $nick[0];
	}
	$_SESSION['ses_disc_id'] = $_SESSION['ses_auth']['usuarioID'];
	$_SESSION['ses_disc_nome'] = $_SESSION['ses_auth']['nome'];
	$_SESSION['ses_disc_nick'] = $_SESSION['ses_auth']['apelido'];
	$_SESSION['ses_disc_sexo'] = $_SESSION['ses_auth']['sexo'];
	$_SESSION['ses_disc_tipo_usr'] = $_SESSION['ses_auth']['tipo_usuario'];
	
	if(!empty($_SESSION['ses_auth']['permissoes']))
	{
		foreach($_SESSION['ses_auth']['permissoes'] AS $k=>$arrPermissoes)
		{
			if($arrPermissoes['permissao_depto']=='CG' && ($arrPermissoes['permissao']=='Discente' || $arrPermissoes['permissao']=='Participante Externo'))
			{
				$_SESSION['ses_disc_area'] = $arrPermissoes['permissao_depto'];
				$_SESSION['ses_disc_permissoes'] = $arrPermissoes['permissao'];
			}
		}
	}
}

/**
 *  Verifica se o usuário efetuou seu login
 */
$host  = $_SERVER['HTTP_HOST'];
$uri   = ($_SERVER['HTTP_HOST']=='localhost'?'/iar':'').'/extranet';
$extra = 'logar.php';
// verifica se o usuário está autenticado
if( empty($_SESSION['ses_disc_id']) && $_SERVER['PHP_SELF'] != "{$uri}/{$extra}" ) {
    header("Location: http".($_SERVER['HTTP_HOST']=='localhost'?'':'s')."://{$host}{$uri}/{$extra}");
    exit();
} elseif( $area != $_SESSION['ses_disc_area'] && $_SESSION['ses_disc_permissoes']!='Super Usuário' && $_SERVER['PHP_SELF'] != "{$uri}/{$extra}" ) {
	//echo "<h1>{$area} != {$_SESSION['ses_disc_area']}<br> && {$_SESSION['ses_disc_permissoes']} != Super Usuário<br> && {$_SERVER['PHP_SELF']} != {$uri}/{$extra}</h1>\n";
	//echo "<pre>[".$_SERVER['PHP_SELF'].": ".__LINE__."]   <b>session</b> "; print_r( $_SESSION ); echo "</pre>";
	echo "<h1>Seu tipo de usuário não tem privilégios para acessar essa área</h1>\n";
	die();
}
include_once('functionsInc.php');
?>